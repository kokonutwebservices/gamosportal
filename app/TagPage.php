<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagPage extends Model
{
    protected $guarded = [];

    public function tag () {
        return $this->belongsTo(Tag::class);
    }

    public function order () {
        return $this->max('order')+1;
    }
}
