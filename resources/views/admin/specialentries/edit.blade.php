@extends('admin.layouts.master')

@section('content')

@include('admin.layouts.errors')

{!! Form::open(['action'=>['SpecialEntriesController@update', $specialentry->id], 'method'=>'POST', 'enctype' => 'multipart/form-data']) !!}

<div class="form-row">

    <div class="form-group col-md-6">
        <label for="client_id">Πελάτης</label>
        <select class="form-control" id="client_id" name="client_id">
            @foreach ($clients as $client)
                @if($client->id == $specialentry->client_id)
                <option value="{{$client->id}}" selected>{{$client->name}}</option>
                @else
                <option value="{{$client->id}}">{{$client->name}}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-12">
        {{Form::label('name', 'Όνομα Επιχείρησης')}}
        {{Form::text('name', $specialentry->name, ['class'=>'form-control', 'placeholder' => 'Το όνομα όπως θα φαίνεται στην καταχώρηση', 'required'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('slug', 'slug')}}
        {{Form::text('slug', $specialentry->slug, ['class'=>'form-control', 'placeholder' => 'friendly url', 'required'] )}}
    </div>
    
    <div class="form-group col-md-6">
        {{Form::label('logo', 'Logo')}}
        {{Form::file('logo')}}
        <img src="/storage/logos/special/{{$specialentry->logo}}">
    </div>

    <div class="form-group col-md-12">
        {{Form::label('person', 'Υπεύθυνος')}}
        {{Form::text('person', $specialentry->person, ['class'=>'form-control', 'placeholder' => 'Όνομα Υπευθύνου'] )}}
    </div>

    <div class="form-group col-md-12">
        {{Form::label('address', 'Διεύθυνση')}}
        {{Form::text('address', $specialentry->address, ['class'=>'form-control', 'placeholder' => 'Διεύθυνση'] )}}
    </div>

    <div class="form-group col-md-4">
        {{Form::label('phone', 'Τηλέφωνο')}}
        {{Form::text('phone', $specialentry->phone, ['class'=>'form-control', 'placeholder' => 'Τηλέφωνο'] )}}
    </div>

    <div class="form-group col-md-4">
        {{Form::label('mobile', 'Κινητό')}}
        {{Form::text('mobile', $specialentry->mobile, ['class'=>'form-control', 'placeholder' => 'Κινητό'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('website', 'Website')}}
        {{Form::text('website', $specialentry->website, ['class'=>'form-control', 'placeholder' => 'Website'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('email', 'Email')}}
        {{Form::text('email', $specialentry->email, ['class'=>'form-control', 'placeholder' => 'Email'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('facebook', 'Facebook')}}
        {{Form::text('facebook', $specialentry->facebook, ['class'=>'form-control', 'placeholder' => 'Facebook'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('google', 'Google+')}}
        {{Form::text('google', $specialentry->google, ['class'=>'form-control', 'placeholder' => 'Google+'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('twitter', 'Twitter')}}
        {{Form::text('twitter', $specialentry->twitter, ['class'=>'form-control', 'placeholder' => 'twitter'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('instagram', 'Instagram')}}
        {{Form::text('instagram', $specialentry->instagram, ['class'=>'form-control', 'placeholder' => 'Instagram'] )}}
    </div> 

    <div class="form-group col-md-6">
        {{Form::label('pinterest', 'Pinterest')}}
        {{Form::text('pinterest', $specialentry->pinterest, ['class'=>'form-control', 'placeholder' => 'Pinterest'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('youtube', 'YouTube')}}
        {{Form::text('youtube', $specialentry->youtube, ['class'=>'form-control', 'placeholder' => 'YouTube'] )}}
    </div>
    
    <div class="form-group col-md-6">
        {{Form::label('vimeo', 'Vimeo')}}
        {{Form::text('vimeo', $specialentry->vimeo, ['class'=>'form-control', 'placeholder' => 'Vimeo'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('map', 'Google Maps')}}
        {{Form::text('map', $specialentry->map, ['class'=>'form-control', 'placeholder' => 'Google Maps'] )}}
    </div>
    
    <div class="col-md-12">
        <hr>
    </div>

    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group ">
        {{Form::label('image', 'Εικόνα Προφίλ')}}
        {{Form::file('image')}}
        <img src="/storage/entries/special/{{$specialentry->image}}">
    </div>

    <div class="form-group col-md-12">
        <label for="profile">Κείμενο Προφίλ</label>        
        <textarea class="form-control tinyMCE" id="profile" name="profile" required >{{$specialentry->profile}}</textarea>
    </div> 

    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-12">
        {{Form::label('metatitle', 'Τίτλος')}}
        {{Form::text('metatitle', $specialentry->metatitle, ['class'=>'form-control', 'placeholder' => 'Τίτλος', 'required'] )}}
    </div> 

    <div class="form-group col-md-12">
        {{Form::label('metadescription', 'Meta Description')}}
        {{Form::text('metadescription', $specialentry->metadescription, ['class'=>'form-control', 'placeholder' => 'Meta Description', 'required'] )}}
    </div>

    <div class="form-group col-md-12">
        {{Form::label('metakeywords', 'Meta Keywords')}}
        {{Form::text('metakeywords', $specialentry->metakeywords, ['class'=>'form-control', 'placeholder' => 'Meta Keywords', 'required'] )}}
    </div>
    
    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-12">
        {{Form::label('offer', 'Προσφορά στο Gamos Deals')}}
        {{Form::text('offer', $specialentry->offer, ['class'=>'form-control', 'placeholder' => 'url προσφοράς'] )}}
    </div>

    <div class="form-group col-md-6">
        Συμμετοχή New Life:
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="newlife" class="form-check-input" type="radio" id="inlineCheckbox1" value="1" <?php if($specialentry->newlife == 1){echo "checked";}else{echo"";}?>> Ναι
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="newlife" class="form-check-input" type="radio" id="inlineCheckbox2" value="0" <?php if($specialentry->newlife == 0){echo "checked";}else{echo"";}?>> Όχι
            </label>
        </div>
    </div>

    <div class="form-group col-md-6">
        Ενεργό:
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="active" class="form-check-input" type="radio" id="inlineCheckbox1" value="1" <?php if($specialentry->active == 1){echo "checked";}else{echo"";}?>> Ναι
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="active" class="form-check-input" type="radio" id="inlineCheckbox2" value="0" <?php if($specialentry->active == 0){echo "checked";}else{echo"";}?>> Όχι
            </label>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
    </div>
    <h4>Ημερομηνίες</h4>
    <div class="col-md-12">
    <p>Δημιουργία Καταχώρησης: {{ Carbon\Carbon::parse($specialentry->created_at)->toDateTimeString() }}</p>
    </div>
    <div id="dates">
        <div class="form-group col-md-4 text-center">
            {{Form::label('start_at', 'Έναρξη Καταχώρησης')}}
            {{Form::date('start_at', $specialentry->start_at, ['class'=>'form-control date', 'id'=>'start_at'] )}}
        </div>

        <div class="form-group col-md-4 text-center">
            {{Form::label('end_at', 'Λήξη Καταχώρησης')}}
            {{Form::date('end_at', $specialentry->end_at, ['class'=>'form-control', 'id'=>'end_at'] )}}
        </div>    

        <div class="form-group col-md-4">
            <p class="text-center"><strong>Αυτόματη Απενεργοποίηση</strong></p>
            <p class="text-center">{{Carbon\Carbon::parse($specialentry->start_at)->addYear()->addMonth()->toDateTimeString()}}</p>
        </div>

        <div class="col-md-12 text-center">
            <span class="btn btn-success" id="renew"><i class="fas fa-sync-alt" aria-hidden="true"></i> Ανανέωση</span>

        </div>

    </div>

    <div class="col-md-12">
        <hr>
    </div>

    <a href="/admin/specialentries/{{$specialentry->id}}/images" class="btn btn-default mr-md-3">Φωτογραφίες</a>

    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-12">    
        <div class="col-md-2">
            {{Form::hidden('_method', 'PUT')}}
            {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
        </div>

        <div class="col-md-2">
            {{Form::hidden('_method', 'PUT')}}
            {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
        </div>

        <div class="col-md-2">
            {{Form::hidden('_method', 'PUT')}}
            {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
        </div>

        <div class="col-md-2">  
            <a href="/admin/entries" class="btn btn-warning pull-right">Cancel</a>
        </div>

        <div class="col-md-2">
        {!! Form::close() !!}

            {!! Form::open(['action' => ['SpecialEntriesController@destroy', $specialentry->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'pull-right', 'id' =>'delete' ]) !!}

            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}

        {!! Form::close() !!}  
        </div>
    </div>
</div>
{!! Form::close() !!}

    
@endsection