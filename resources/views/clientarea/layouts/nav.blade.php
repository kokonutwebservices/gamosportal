<nav class="navbar navbar-expand-lg justify-content-between navbar-dark adminnav">
<a href="/clientarea"><img src="/storage/header/logo.png" style="width: 200px;" class="d-inline-block align-middle admin-logo"></a>
  @if(!Request::is('*/login') AND !Request::is('*/password*') )
    <a class="navbar-brand" href="/clientarea/"> Περιοχή Πελατών - Διαχείριση Καταχώρησεων</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
  
        @if (Auth::check())
        <div>
            <ul class="navbar-nav mr-auto navbar-right">
                <li class="nav-item">
                <a class="nav-link" href="/" target="_blank">Frontend <span class="sr-only"></span></a>
                </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> {{auth()->user()->name}}</a>
                <div class="dropdown-menu" aria-labelledby="dropdown05">
                    <a class="dropdown-item" href="#">Προφίλ</a>
                    <a class="dropdown-item" href="/clientarea/logout">Αποσύνδεση</a>
                </div>
                </li>
            </ul>
        </div>
        @endif
    </div>
    @endif
</nav>