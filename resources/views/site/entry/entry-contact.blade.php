<div class="d-none d-lg-block">
    <div class="align-self-center">
        <div class="logo d-none d-lg-block d-flex align-items-center thumbnail">
            <img class="m-auto d-block" src="/storage/logos/{{$category->id}}/{{$entry->logo}}" alt="Λογότυπο {{$entry->name}}" title="Λογότυπο {{$entry->name}}">
        </div>
    </div>
    
    <div class="" itemscope itemtype="http://schema.org/Person">
        <ul class="list-group">
            @if($entry->person)
            <li class="list-group-item">
                <i class="fas fa-user mr-3" aria-hidden="true"></i> <span itemprop="name">{{$entry->person}}</span>
            </li>
            @endif
            @if($entry->address)
            <li class="list-group-item">
                <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="mb-1">
                    <i class="fas fa-map-marker-alt mr-3" aria-hidden="true"></i>                        
                        @foreach ($entry->seperateAddresses($entry->address) as $address)
                            <span itemprop="streetAddress"> {{$address}} </span> {{ count($entry->seperateAddresses($entry->address)) >1 && !$loop->last ? '|' : '' }}
                        @endforeach
                </div>
            </li>
            @endif
            @if($entry->phone)
                <li class="list-group-item">
                    <i class="fas fa-phone mr-3" aria-hidden="true"></i> 
                    @foreach ($entry->seperatePhones($entry->phone) as $phone)
                        <a class="call" href="tel:+30{{$phone}}"><span itemprop="telephone">{{$phone}}</span></a> {{count($entry->seperatePhones($entry->phone))>1 ? '|' : ''}}
                    @endforeach
                </li>       
            @endif
            @if($entry->mobile)
                <li class="list-group-item">
                    <i class="fas fa-mobile-alt mr-3" aria-hidden="true"></i>
                    @foreach ($entry->seperatePhones($entry->mobile) as $mobile)
                        <a class="call" href="tel:+30{{$mobile}}"> <span itemprop="telephone">{{$mobile}}</span></a> {{count($entry->seperatePhones($entry->mobile))>1 ? '|' : ''}}                 
                    @endforeach
                </li>       
            @endif
            {{-- @if($entry->email)
                <li class="list-group-item">
                    <a class="email" href="mailto:{{$entry->email}}?subject=Είδα την καταχώρησή σας στο Gamos Portal" itemprop="email">
                        <i class="fas fa-envelope mr-3" aria-hidden="true"></i>                
                        {{$entry->email}}
                    </a>
                </li>       
            @endif --}}
            @if($entry->website)
                <li class="list-group-item">
                    <a href="http://{{$entry->website}}?utm_source=gamosportal&utm_medium=referral" target="_blank" rel="nofollow" itemprop="url">
                        <i class="fas fa-globe mr-3" aria-hidden="true"></i>
                        {{$entry->website}}
                    </a>
                </li>       
            @endif
            <li class="list-group-item">
                @if($entry->facebook)
                    <a href="http://{{$entry->facebook}}" rel="nofollow" target="_blank"><i class="fab fa-facebook fa-2x mr-3" aria-hidden="true"></i></a>    
                @endif
                @if($entry->twitter)
                    <a href="http://{{$entry->twitter}}" rel="nofollow" target="_blank"><i class="fab fa-twitter fa-2x mr-3" aria-hidden="true"></i></a>    
                @endif
                @if($entry->instagram)
                    <a href="http://{{$entry->instagram}}" rel="nofollow" target="_blank"><i class="fab fa-instagram fa-2x mr-3" aria-hidden="true"></i></a>    
                @endif
                @if($entry->pinterest)
                    <a href="http://{{$entry->pinterest}}" rel="nofollow" target="_blank"><i class="fab fa-pinterest-p fa-2x mr-3" aria-hidden="true"></i></a>    
                @endif
                @if($entry->youtube)
                    <a href="http://{{$entry->youtube}}" rel="nofollow" target="_blank"><i class="fab fa-youtube fa-2x mr-3" aria-hidden="true"></i></a>    
                @endif 
                @if($entry->vimeo)
                <a href="http://{{$entry->vimeo}}" rel="nofollow" target="_blank"><i class="fab fa-vimeo-v fa-2x mr-3" aria-hidden="true"></i></a>    
                @endif            
            </li>
        </ul>
        {{-- <a id="" class="btn btn-primary" href="#contact-form" role="button">Στείλτε Μήνυμα</a> --}}
    </div>
    {{-- <div class="">
            @if($entry->map)
            <style>
            #map {
            width: 100%;
            height: 300px;
            background-color: grey;
            }
        </style>
        <div id="map" class="d-none d-lg-block"></div>
        <script>
        function initMap() {
            var uluru = {lat: {{Maps::lat($entry->map)}}, lng: {{Maps::lon($entry->map)}}};
            var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: uluru
            });
            var marker = new google.maps.Marker({
            position: uluru,
            map: map
            });
        }
        </script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATXsZY-e5x34l-ilC2sLA5Ftzy3xZPuUc&callback=initMap">
        </script>
        @endif
    </div> --}}
  
</div>