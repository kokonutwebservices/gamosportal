@extends('site.layouts.master')

@section('metatags')
<title>Error 404</title>
<meta name="description" content="" >
<meta name="keywords" content="" >
@endsection

@section('content')
<div class="container">
    <div class="body">
    <h1>Αυτή η σελίδα δεν υπάρχει. </h1>
    <p>Φαίνεται πως βρεθήκατε εδώ κατά λάθος. Συνεχίστε την αναζήτηση στο Gamos Portal, κάνοντας πίσω ή επιλέγοντας την περιοχή ή την επαγγελματική κατηγορία που θέλετε.</p>
    <img class="d-block mx-auto my-3" src="/storage/steps.png" alt="Gamos Portal - Σε κάθε βήμα του γάμου σας" title="Gamos Portal - Σε κάθε βήμα του γάμου σας">
 

</div>
</div>
@endsection
