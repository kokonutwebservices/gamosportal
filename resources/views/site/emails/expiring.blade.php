<h1>Η καταχώρηση {{$name}} βρίσκεται στον τελευταίο μήνα</h1>

<ul>
    <li>Ημερομηνία Λήξης: {{$end_at}}</li>
    @if ($person)
        <li>Υπεύθυνος: {{$person}}</li>
    @endif    
    <li>Επαγ. Κατηγ. : {{$category}}</li>
    <li>Περιφέρεια: {{$district}}</li>
    @if ($phone)
        <li>Τηλέφωνο: {{$phone}}</li>
    @endif
    @if ($mobile)
        <li>Κινητό: {{$mobile}}</li>
    @endif
    @if ($email)
        <li>Email: {{$email}}</li>
    @endif
    @if ($representative)
        <li>Πωλητής: {{$representative}} </li>
    @endif
</ul>
