<?php

namespace App\Http\Controllers;

use App\Entry;
use Illuminate\Http\Request;
use App\Category;
use App\District;
use App\Client;
use App\Tag;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Kyslik\ColumnSortable\Sortable;
use InterImage;

class EntriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entries = Entry::sortable()->paginate(50);
        return view('admin.entries.index', compact('entries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $districts = District::all();
        $clients = Client::all();
        $tags = Tag::all();

        return view('admin.entries.create', compact('categories', 'districts', 'clients', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //Validation
        $this->validate(request(), [
            'client_id' => 'required',
            'cat_id' => 'required',
            'dist_id' => 'required',
            'name' => 'required',
            'slug' => 'required',
            'logo' => 'required',
            'image' => 'required',
            'profile' => 'required',
        ]);

        //Images check
        //logo
        $logo = $request->file('logo'); //Get the logo file
        $ext = $logo->getClientOriginalExtension(); //Get the extension
        $logoFileNameBase = $request->input('slug'); //Get the slug to use as filename base
        $logoFileName = $logoFileNameBase .'-logo.'.$ext; //create a new filename
        $cat_id = request('cat_id');
        request()->file('logo')->storeAs("logos/$cat_id", $logoFileName); //Store file
        
              
        //Entry Image
        $image = $request->file('image'); //Get the logo file
        $ext = $image->getClientOriginalExtension(); //Get the extension
        $imageFileNameBase = $request->input('slug'); //Get the slug to use as filename base
        $cat_id = $request->input('cat_id');
        $dist_id = $request->input('dist_id');
        $imageFileName = $imageFileNameBase .'-'.$cat_id.'-'.$dist_id.'.'.$ext; //create a new filename
        request()->file('image')->storeAs("entries/$cat_id", $imageFileName); //Store file

        InterImage::make(Storage::get("entries/$cat_id/$imageFileName"))
                ->resize(null, 360, function($constraint){
                    $constraint->aspectRatio();
                })
                ->save(Storage::path('entries/'.$cat_id.'/small/'.$imageFileName));
        

        //Regular expressions για να αφαιρεθεί το http:// και https:// από το url, αν υπάρχει
        $facebook = preg_replace('#^https?://#', '',$request->input('facebook'));
        $google = preg_replace('#^https?://#', '',$request->input('google'));
        $twitter = preg_replace('#^https?://#', '',$request->input('twitter'));
        $instagram = preg_replace('#^https?://#', '',$request->input('instagram'));
        $pinterest = preg_replace('#^https?://#', '',$request->input('pinterest'));
        $youtube = preg_replace('#^https?://#', '',$request->input('youtube'));
        $vimeo = preg_replace('#^https?://#', '',$request->input('vimeo'));

        //Expiration date
        $expires = $request->input('end_at');
        if ($expires == NULL) {
            $expires = Carbon::parse(request('start_at'))->addYear();
        } else {
            $expires = $request->input('end_at');
        }

        //Get last order
        $entriesNumber = Entry::where('cat_id', $request->input('cat_id'))->where('dist_id', $request->input('dist_id'))->get();
        $entriesCount = count($entriesNumber);
        if ($entriesCount > 0){
        $lastEntry = Entry::where('cat_id', $request->input('cat_id'))->where('dist_id', $request->input('dist_id'))->orderBy('order')->get()->last();
        $order = $lastEntry->order + 1;
        } else {
            $order = 1;
        }
        //Creation
        Entry::create([
            'client_id' => $request->input('client_id'),
            'cat_id' => $request->input('cat_id'),
            'dist_id' => $request->input('dist_id'),
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'logo' => $logoFileName,
            'person' => $request->input('person'),
            'address' => $request->input('address'),
            'area' => $request->input('area'),
            'phone' => $request->input('phone'),
            'mobile' => $request->input('mobile'),
            'website' => preg_replace('#^https?://#','',$request->input('website')),
            'email' => $request->input('email'),
            'facebook' => $facebook,
            'google' => $google,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'pinterest' => $pinterest,
            'youtube' => $youtube,
            'vimeo' => $vimeo,
            'chat_id' => $request->input('chat_id'),
            'map' => $request->input('map'),
            'image' => $imageFileName,
            'profile' => $request->input('profile'),
            'metatitle' => $request->input('metatitle'),
            'metadescription' => $request->input('metadescription'),
            'metakeywords' => $request->input('metakeywords'),
            'offer' => $request->input('offer'),
            'newlife' => $request->input('newlife'),
            'active' => $request->input('active'),
            'order' => $order,
            'start_at' => $request->input('start_at'),
            'end_at' => $expires,
        ]);

        $id = Entry::all()->last()->id;

        Entry::find($id)->tags()->sync($request->tags);

        //Redirect
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.entries.edit', $id); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.entries.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.entries.create');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function edit(Entry $entry)
    {
        $districts = District::all();
        $categories = Category::all();
        $clients = Client::orderBy('name')->get();
        $tags = Tag::all();
        return view('admin.entries.edit', compact('entry', 'districts', 'categories', 'clients', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entry $entry)
    {
        $districts = District::all();
        $categories = Category::all();

        //Validation
        $this->validate(request(), [
            'name' => 'required',
            'slug' => 'required',
            'profile' => 'required',
        ]);

        //Update
        //Entry Logo
        if($request->hasFile('logo')){
            if($entry->logo){
                //Αν ανεβαίνει εικόνα και υπάρχει ήδη άλλο αρχείο το όνομα αρχείου παραμένει ίδιο 
                $logoFileName = $entry->logo;             
                request()->file('logo')->storeAs("logos/$entry->cat_id", $logoFileName);
            } else {
                //Αν δεν υπάρχει ήδη αρχείο
                $logo = $request->file('logo'); //Get the logo file
                $ext = $logo->getClientOriginalExtension(); //Get the extension
                $logoFileNameBase = $request->input('slug'); //Get the slug to use as filename base
                $logoFileName = $logoFileNameBase .'-logo.'.$ext; //create a new filename
                $cat_id = request('cat_id');
                
                request()->file('logo')->storeAs("logos/$cat_id", $logoFileName); //Store file
                
            }
        } else {
            //Αν δεν ανεβαίνει άλλο αρχείο για αντικατάσταση
            $logoFileName = $entry->logo;
        }
        //Entry Image
        if($request->hasFile('image')){
            if($entry->image){
                //Αν ανεβαίνει εικόνα και υπάρχει ήδη άλλο αρχείο το όνομα αρχείου παραμένει ίδιο 
                $filename = $entry->image;
                request()->file('image')->storeAs("entries/$entry->cat_id", $filename);
                
                InterImage::make(Storage::get('entries/'.$entry->cat_id.'/'.$entry->image))
                ->resize(null, 360, function($constraint){
                    $constraint->aspectRatio();
                })->save(Storage::path('entries/'.$entry->cat_id.'/small/'.$entry->image));
                
            } else {
                //Αν δεν υπάρχει ήδη αρχείο
                //Profile Image
                $image = $request->file('image'); //Get the logo file
                $ext = $image->getClientOriginalExtension(); //Get the extension
                $imageFileNameBase = $request->input('slug'); //Get the slug to use as filename base
                $cat_id = $request->input('cat_id');
                $dist_id = $request->input('dist_id');
                $imageFileName = $imageFileNameBase .'-'.$cat_id.'-'.$dist_id.'.'.$ext; //create a new filename
                request()->file('image')->storeAs("entries/$cat_id", $imageFileName); //Store file

                $filename = $imageFileName;
               
                InterImage::make(Storage::get("entries/$cat_id/$imageFileName"))
                        ->resize(null, 360, function($constraint){
                            $constraint->aspectRatio();
                        })
                        ->save(Storage::path('entries/'.$entry->category->id.'/small/'.$imageFileName));
            }
        } else {
            //Αν δεν ανεβαίνει αρχείο
            $filename = $entry->image;
        }

        $facebook = preg_replace('#^https?://#', '',$request->input('facebook'));
        $google = preg_replace('#^https?://#', '',$request->input('google'));
        $twitter = preg_replace('#^https?://#', '',$request->input('twitter'));
        $instagram = preg_replace('#^https?://#', '',$request->input('instagram'));
        $pinterest = preg_replace('#^https?://#', '',$request->input('pinterest'));
        $youtube = preg_replace('#^https?://#', '',$request->input('youtube'));
        $vimeo = preg_replace('#^https?://#', '',$request->input('vimeo'));

        $entry->client_id = $request->input('client_id');
        $entry->cat_id = $request->input('cat_id');
        $entry->dist_id = $request->input('dist_id');
        $entry->name = $request->input('name');
        $entry->slug = $request->input('slug');
        $entry->logo = $logoFileName;
        $entry->person = $request->input('person');
        $entry->address = $request->input('address');
        $entry->area = $request->input('area');
        $entry->phone = $request->input('phone');
        $entry->mobile = $request->input('mobile');
        $entry->website = preg_replace('#^https?://#','',$request->input('website'));
        $entry->email = $request->input('email');
        $entry->facebook = $facebook;
        $entry->google = $google;
        $entry->twitter = $twitter;
        $entry->instagram = $instagram;
        $entry->pinterest = $pinterest;
        $entry->youtube = $youtube;
        $entry->vimeo = $vimeo;
        $entry->chat_id = $request->input('chat_id');
        $entry->map = $request->input('map');
        $entry->image = $filename;
        $entry->profile = $request->input('profile');
        $entry->metatitle = $request->input('metatitle');
        $entry->metadescription = $request->input('metadescription');
        $entry->metakeywords = $request->input('metakeywords');
        $entry->offer = $request->input('offer');
        $entry->newlife = $request->input('newlife');
        $entry->active = $request->input('active');
        $entry->order = $request->input('order');
        $entry->start_at = Carbon::parse($request->input('start_at'));
        $entry->end_at = Carbon::parse(request('start_at'))->addYear(); //προσθέτει 1 έτος στην ημ/νία έναρξης
        $entry->save();

        $entry->tags()->sync($request->tags);


        //Redirect
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.entries.edit', $entry);
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.entries.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.entries.create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Entry $entry)
    {
        
        $banners = $entry->banners;

        foreach ($banners as $banner) {
            $image = $banner->banner;
            Storage::delete("banners/$image");

            $banner->pages()->detach();
            $banner->infoPages()->detach();
            $banner->categories()->detach();
            $banner->districts()->detach();
            $banner->home()->detach();
            $banner->delete();
        }

        $logo = $entry->logo;
        Storage::delete("logos/$entry->cat_id/$logo");
        
        $image = $entry->image;
        Storage::delete("entries/$entry->cat_id/$image");

        foreach ($entry->albums as $album) {
            foreach ($album->images as $image) {
                $image->delete();
            }
            $album->delete();
            Storage::deleteDirectory("uploads/$album->id");
        }

        $entry->delete();

        return redirect()->route('admin.entries.index');
        
    }

    public function search(Request $request, Entry $entry){

        $keyword = request('query');
        $results = Entry::where('name', 'LIKE', '%'.$keyword.'%')->get();

        return view('admin.entries.results', compact('results'));
    }
}
