<?php

namespace App;
use App\Category;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded = ['_token'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function banners(){
        return $this->belongsToMany(Banner::class);
    }

    public function sliders(){
        return $this->belongsToMany(Slider::class);
    }

    public function validatePage($request){

        $request->validate([
            'title' => 'required|max:80',
            'slug' => 'required|max:65',
            'metatitle' => 'required|max:80',
            'metadescription' => 'required|max:300',
            'metakeywords'=> 'required',
            'body' => 'required',
            'title2' => 'required'
        ]);

    }

    public function redirectAfterSave($request) {

        if($request->submitbutton == 'savestay'){
            return redirect()->route('admin.pages.edit', $this->id);
        } elseif ($request->submitbutton == 'saveclose') {
            return redirect()->route('admin.pages.index');
        } elseif ($request->submitbutton == 'savenew'){
            return redirect()->route('admin.pages.create');
        }

    }
}
