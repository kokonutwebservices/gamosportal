@extends('site.layouts.master')

@section('content')
<div class="container">
    <div class="body">
    <h1> Σας ευχαριστούμε για την επικοινωνία! </h1>
   <p>Λάβαμε το μήνυμά σας και θα επικοινωνήσουμε μαζί σας το συντομότερο δυνατό.</p>
   <p>Μπορείτε πάντα να επικοινωνείτε μαζί μας στα παρακάτω στοιχεία:</p>
   <p><a href="tel:2102635911"><i class="fas fa-phone"></i> 2102635911</a></p>
   <p><a href="tel:6972825545"><i class="fas fa-mobile-alt"></i> 6972825545</a></p>
   <p><a href="mailto:info@gamosportal.gr"><i class="fas fa-envelope-square"></i> info@gamosportal.gr</a></p>
</div>
</div>
@endsection