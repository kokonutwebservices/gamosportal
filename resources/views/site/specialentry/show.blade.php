@extends('site.layouts.master')

@section('metatags')
<title>{{Meta::entryTitle($specialentry)}}</title>
    <meta name="description" content="{{$specialentry->metadescription}}">
    <meta name="keywords" content="{{$specialentry->metakeywords}}">
    <meta name="geo.region" content="GR">
    <meta name="geo.position" content="{{$specialentry->map}}">
    <meta name="ICBM" content="{{$specialentry->map}}">
    <meta property="og:title" content="{{$specialentry->metatitle}}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://gamosportal.gr/specialentries/{{$specialentry->slug}}">
    <meta property="og:description" content="{{$specialentry->metadescription}}">
    <meta property="og:image" content="https://gamosportal.gr/storage/entries/special/{{$specialentry->image}}">
    
    <script src='https://www.google.com/recaptcha/api.js'></script>
    {{Meta::canonical($specialentry)}}
@endsection

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="container entry">
    <div class="row">
        <ol class="breadcrumb"> 
            <li class="breadcrumb-item"><a href="/">Αρχική</a></li>
            <li class="breadcrumb-item"><a href="/specialentries">Special Entries</a></li>
            <li class="breadcrumb-item active">{{$specialentry->name}}</li>
            </ol>
    </div>

    <div class="row">
        <div class="col-md-12 text-center"><h1 id="specialentryname"> {{$specialentry->name}} </h1></div>
            <div class="col-md-3 d-block d-md-none  ">
                @include('site.specialentry.mobile-contact')
            </div>
            <div class="col-md-3 d-none d-md-block  ">
                @include('site.specialentry.left-contact')
            </div>
            <div class="col-md-6">
                <nav class="nav nav-pills d-flex flex-wrap flex-md-row justify-content-md-between flex-column entrymenu">
                    <li class="nav-item"><a href="/specialentries/{{$specialentry->slug}}" class="nav-link {{\Route::current()->getName() == 'profile' ? 'active' : '' }}">Προφίλ</a></li>
                    <li class="nav-item"><a href="/specialentries/{{$specialentry->slug}}/photos" class="nav-link {{\Route::current()->getName() == 'albums' || \Route::current()->getName() == 'photos' ? 'active' : '' }}">Φωτογραφίες</a></li>
                    {{-- @if(count($specialentry->videos) > 0)
                        <li class="nav-item"><a href="/specialentries/{{$specialentry->slug}}/video" class="nav-link {{\Route::current()->getName() == 'video' ? 'active' : '' }}">Video</a></li>
                    @endif --}}
                    @if ($specialentry->facebook)
                        <li class="nav-item"><a href="/specialentries/{{$specialentry->slug}}/facebook" class="nav-link {{\Route::current()->getName() == 'facebook' ? 'active' : '' }}">Timeline</a></li>
                    @endif
                </nav>

                @yield('specialentrycontent')
            </div>
            <div class="col-md-3">
                <div class="fb-page d-none d-md-block" 
                    data-href="{{$specialentry->facebook}}"
                    data-small-header="false" 
                    data-adapt-container-width="true" 
                    data-hide-cover="false" 
                    data-show-facepile="true">
                </div>
                <div class="col-xs-12"><hr></div>
                <div id="contact-form">
                    @include('site.specialentry.contact')
                </div> 
            </div>
    </div>

</div>    
@endsection