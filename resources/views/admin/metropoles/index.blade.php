@extends('admin.layouts.master')

@section('content')
    <h1>Μητροπόλεις</h1>

    <ul class="list-group">
        @foreach ($metropoles as $metropole)
            <li class="list-group-item">
                <a href="/admin/metropoles/{{$metropole->id}}/edit">{{$metropole->name}}</a>
            </li>            
        @endforeach
    </ul>
    

    <a href="/admin/metropoles/create" class="btn btn-primary">Προσθήκη Μητρόπολης</a>
@endsection