<ul class="nav flex-column list-group">
  <li class="nav-item"><a href="#" id="menu1"><strong> Πελατολόγιο </strong></a>
    <ul id="submenu1">
      <li class="nav-item {{Request::is('admin/categories#') ? 'active' : '' }} ">
        <a class="nav-link" href="/admin/categories"><i class="fa fa-angle-right" aria-hidden="true"></i> Κατηγορίες</a>
      </li>

      <li class="nav-item {{Request::is('admin/entries') ? 'active' : '' }}">
        <a class="nav-link" href="/admin/entries"><i class="fa fa-angle-right" aria-hidden="true"></i> Καταχωρήσεις</a>
      </li>

      <li class="nav-item {{Request::is('admin/clients') ? 'active' : '' }}">
        <a class="nav-link" href="/admin/clients"><i class="fa fa-angle-right" aria-hidden="true"></i> Πελάτες</a>
      </li>
      <li class="nav-item {{Request::is('admin/representatives') ? 'active' : '' }}">
        <a class="nav-link" href="/admin/representatives"><i class="fa fa-angle-right" aria-hidden="true"></i> Πωλητές</a>
      </li>
    </ul>
  </li>
  <li class="nav-item {{Request::is('admin/pages') ? 'active' : '' }}">
    <a class="nav-link" href="#"><strong>Σελίδες Κατηγοριών</strong></a>
    <ul>
      <li class="nav-item {{Request::is('admin/categories#') ? 'active' : '' }} ">
        <a class="nav-link" href="/admin/pages"><i class="fa fa-angle-right" aria-hidden="true"></i> Σελίδες</a>
      </li>
      <li class="nav-item {{Request::is('admin/categories#') ? 'active' : '' }} ">
        <a class="nav-link" href="/admin/sliders"><i class="fa fa-angle-right" aria-hidden="true"></i> Sliders</a>
      </li>
    </ul>
  </li>

  <li class="nav-item  {{Request::is('admin/infopages') ? 'active' : '' }}">
    <a class="nav-link" href="/admin/infopages"><strong>Σελίδες Πληροφοριών</strong></a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="#"><strong>Tag System</strong></a>
    <ul>
      <li class="nav-item  {{Request::is('admin/tags') ? 'active' : '' }}">
        <a class="nav-link" href="/admin/tags"><i class="fa fa-angle-right" aria-hidden="true"></i>Tags</a>
      </li>
      <li class="nav-item  {{Request::is('admin/tagpages') ? 'active' : '' }}">
        <a class="nav-link" href="/admin/tagpages"><i class="fa fa-angle-right" aria-hidden="true"></i>Tag Pages</a>
      </li>
    </ul>
  </li>

  <li class="nav-item  {{Request::is('admin/districts') ? 'active' : '' }}">
    <a class="nav-link" href="{{route('admin.districts.index')}}"><strong>Περιφέρειες</strong></a>
  </li>
    <li class="nav-item">
    <a class="nav-link" href="#"><strong>Facilities</strong></a>
    <ul id="submenu1">
      <li class="nav-item {{Request::is('admin/metropoles') ? 'active' : '' }} ">
        <a class="nav-link" href="/admin/metropoles"><i class="fa fa-angle-right" aria-hidden="true"></i> Μητροπόλεις</a>
      </li>

      <li class="nav-item {{Request::is('admin/churches') ? 'active' : '' }} ">
        <a class="nav-link" href="/admin/churches"><i class="fa fa-angle-right" aria-hidden="true"></i> Εκκλησίες</a>
      </li>

      <li class="nav-item {{Request::is('admin/municipalities') ? 'active' : '' }}">
        <a class="nav-link" href="/admin/municipalities"><i class="fa fa-angle-right" aria-hidden="true"></i> Δημαρχεία</a>
      </li>
    </ul>
  </li>

    </li>
    <li class="nav-item {{Request::is('admin/banners') ? 'active' : '' }}">
      <a class="nav-link" href="/admin/banners"><strong>Banners</strong></a>
    </li>

    <li class="nav-item {{Request::is('admin/news') ? 'active' : '' }}">
      <a class="nav-link" href="/admin/news"><strong>News</strong></a>
    </li>

    <li class="nav-item {{Request::is('admin/testimonials') ? 'active' : '' }}">
      <a class="nav-link" href="/admin/testimonials"><strong>Testimonials</strong></a>
    </li>

</ul>