@extends('admin.layouts.master')

@section('content')

<h1>Προσθήκη Testimonial</h1>

<form action="/admin/testimonials" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}

    <div class="row">
        <div class="form-group col-md-7">
            <div class="form-group">
                <label for="name">Όνομα</label>
                <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="">
            </div>
        </div>

        <div class="form-group col-md-5">
            <label for="image">Εικόνα</label>
            <input type="file"
            class="form-control" name="image" id="image" >
            <small id="helpId" class="form-text text-muted">Φωτογραφία ή λογότυπο πελάτη</small>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
              <label for="testimonial">Testimonial</label>
              <textarea class="form-control tinyMCE" name="testimonial" id="testimonial" rows="3"></textarea>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="company">Εταιρεία</label>
                <input type="text" class="form-control" name="company" id="company" aria-describedby="helpId" placeholder="">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="position">Θέση στην εταιρεία</label>
                <input type="text" class="form-control" name="position" id="position" aria-describedby="helpId" placeholder="">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
              <label for="link">Link</label>
              <input type="text" class="form-control" name="link" id="link" aria-describedby="helpId" placeholder="">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-6">
            Ενεργό:
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input name="published" class="form-check-input" type="radio" id="inlineCheckbox1" value="1" > Ναι
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input name="published" class="form-check-input" type="radio" id="inlineCheckbox2" value="0" checked> Όχι
                </label>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary" name="submitbutton" value="save-stay">Save & Stay</button>
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-success" name="submitbutton" value="save-close">Save & Close</button>
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-secondary" name="submitbutton" value="save-new">Save & New</button>
        </div>
        <div class="col-md-2">
            <a href="/admin/testimonials" class="btn btn-warning">Cancel</a>
        </div>

    </div>   

</form>

@include('admin.layouts.errors')
    
@endsection