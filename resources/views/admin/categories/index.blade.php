@extends('admin.layouts.master')

    @section('content')
    <div class="col-md-12">
    <ul id="sortable-cat" class="list-group">
    {{ csrf_field() }}
    
        @foreach ($categories as $category)

            @if ($category->cat_parent_id == 0)

                <li><strong><a href="/admin/categories/{{$category->id}}/edit"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> {{$category->cat_name}}</strong></a>

                    <ul id="sortable-cat">

                    <?php $subcategories = App\Category::where('cat_parent_id', $category->id)->orderBy('cat_order', 'asc')->get() ?>

                    @foreach ($subcategories as $subcategory)

                        <li id="{{$subcategory->cat_parent_id}}-{{$subcategory->id}}"><a href="/admin/categories/{{$subcategory->id}}/edit"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> {{$subcategory->cat_name}}</a></li>

                    @endforeach

                    </ul>
                
                </li>

            @endif

        @endforeach

    </ul>
    </div>
    <div>
        <a href="categories/create" class="btn btn-primary">Δημιουργία Κατηγορίας</a>
    </div>

@endsection

