<h2 class="col-md-12">ΔΕΙΤΕ ΣΤΟ GAMOS PORTAL <br> <small>Οι πιο πρόσφατες καταχωρήσεις κορυφαίων επιχειρήσεων</small></h2>
@foreach($entries as $entry)

    <div class="latest-item col-md-6 col-lg-4 pull-left">
        @if($entry->image)
            <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}"><img loading="lazy" class="thumbnail" src="storage/entries/{{$entry->cat_id}}/small/{{$entry->image}}" alt="{{$entry->name}}"></a>
        @endif                    
        <div class="card-body">
            <h4 class="card-title"><a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}">{{$entry->name}}</a></h4>
            <small>{{$entry->category->cat_name}}</small>
            <hr>
            <p class="card-text">{!! \Illuminate\Support\Str::words($entry->profile, 25) !!} <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}" class="more"><i class="fab fa-gratipay"></i></a></p>                            
        </div>
    </div>                 
@endforeach