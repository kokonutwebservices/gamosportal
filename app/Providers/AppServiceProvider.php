<?php

namespace App\Providers;

use App\Page;
use App\District;
use App\InfoPage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::share('pages', Cache::remember('pages', 3600, function(){
            return Page::with('category.entries')->orderBy('order')->get();
        }));

        View::share('infoPages', Cache::remember('infoPages', 604800, function(){
            return InfoPage::all();
        }));
        
        View::share('districts', Cache::remember('districts', 604800, function() {
            return District::withCount(['entries', 'churches', 'municipalities'])->get();
         }));
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      
    }
}
