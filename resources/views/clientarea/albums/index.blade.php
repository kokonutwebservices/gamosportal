@extends('clientarea.layouts.master')

@section('leftnav')
    @include('clientarea.layouts.leftnav')
@endsection

@section('content')
@if(Auth::user()->id == $entry->client_id)

<h2 class="mb-3">Albums Καταχώρησης</h2>
<!-- Button trigger modal -->
<button type="button" class="text-dark float-right" data-toggle="modal" data-target="#exampleModal">
    <i class="far fa-question-circle"></i>
</button>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Οδηγίες</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Μπορείτε να δημιουργήσετε όσα album επιθυμείτε. Κάθε album μπορεί να έχει μέχρι 24 φωτογραφίες. Προτείνουμε να μην έχει λιγότερες από 15.</p>
          <p>Κάθε album μπορεί να έχει ένα κείμενο περιγραφής, ώστε ο επισκέπτης να έχει περισσότερες πληροφορίες.</p>
          <p>Για να προσθέσετε ή να αφαιρέσετε φωτογραφίες, κάντε κλικ στον τίτλο του album.</p>
          <p>Μην ξεχάσετε να επιλέξετε ποια φωτογραφία θέλετε να εμφανίζεται ως εξώφυλλο.</p>
          <p>Για να αλλάξετε τη σειρά των album μπορείτε να τα "τσιμπήσετε" με το ποντίκι και να τα μετακινήσετε στη θέση που θέλετε.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <hr>
    {{ csrf_field() }}

    {{--  Πλαίσιο σφαλμάτων  --}}
    @include('admin.layouts.errors') 
    @if (session('message'))
        <div class="alert alert-success" id="successMessage">
            {{ session('message') }}
        </div>
    @endif

    <ul id="clientsAlbumSortable" class="list-group">
    
        @foreach($albums as $album)
            <?php $images = App\Image::where('album_id', $album->id)->get(); ?>
            <li id="{{$album->entry_id}}-{{$album->id}}" class="list-group-item">
            <div class="media">
                @if($album->cover)
                    <a href="albums/{{$album->id}}/edit"><img src="/storage/uploads/{{$album->id}}/{{$album->cover}}" class="d-flex mr-3" style="width: 50px;"></a>
                @else
                    <a href="albums/{{$album->id}}/edit"><img src="/storage/default.png" class="d-flex mr-3" style="width: 50px;"></a>
                @endif            
                <div class="media-body">
                <a href="albums/{{$album->id}}/edit">{{$album->name}} - {{\Carbon\Carbon::parse($album->created_at)->diffForHumans()}}</a>
                <div> (Περιεχ. {{count($images)}} Εικόνες)</div>
                @if($album->description)
                <div>Κείμενο Περιγραφής: <i class="fa fa-check text-success" aria-hidden="true"></i></div>
                @endif
                <div><a class="btn btn-danger pull-right" href="albums/{{$album->id}}/delete"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                </div>
            </div>
            </li>            

        @endforeach
    </ul>

    <a href="albums/create" class="btn btn-primary">Δημιουργία album</a>
@else
<span class="text text-danger">Δεν μπορείτε να δείτε αυτό το περιεχόμενο</span>
@endif
@endsection