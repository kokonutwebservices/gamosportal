@extends('admin.layouts.master')

@section('content')

    <div class="col-md-6">

        <h1>Διαχειριστές</h1>

       <ul>
        @foreach ($admins as $admin)
            <li>
                <a href="{{ route('admin.edit', $admin) }}">{{ $admin->name }}</a>
            </li>
        @endforeach
       </ul>

    </div>
    @include('admin.layouts.errors')

@endsection