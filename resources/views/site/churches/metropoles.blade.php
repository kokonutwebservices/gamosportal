@extends('site.layouts.master')

@section('metatags')
    <title>{{$district->name}} - Εκκλησίες για Θρησκευτικό Γάμο</title> 
    <meta name="description" content="Βρείτε εκκλησίες ανά Μητρόπολη στην περιφέρεια {{ $district->name }}">
@endsection

@section('content')

<div class="container">
    <h1 class="text-center">{{$district->name}} : Εκκλησίες για γάμο</h1>
    <div class="row d-flex justify-content-center mt-5">
        <div class="col-md-9 mb-5">
            Για την καλύτερη αναζήτηση εκκλησιών για το γάμο σας, επιλέξτε τη Μητρόπολη που σας ενδιαφέρει, ώστε να δείτε τις περιοχές που καλύπτει, καθώς και τις εκκλησίες.
        </div>
        <div class="clearfix"></div>
        <div class="col-md-9">
            @include('site.churches.selectmetropole')
        </div>
    </div>
</div>
@endsection