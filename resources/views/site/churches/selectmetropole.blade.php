<form action="/{{$district->slug}}/ekklisies" method="POST">
    @csrf
    
    <div class="form-group">
        <select class="form-control" name="metropole" id="metropole" onchange='this.form.submit()'>
          <option {{!$lastSegment ? 'selected' : ''}} disabled>Επιλέξτε Μητρόπολη</option>
          @foreach ($metropoles as $metropole)
              <option {{$metropole->id == $lastSegment ? 'selected' : ''}} value="{{$metropole->id}}">{{$metropole->name}}</option>        
          @endforeach
        </select>
    </div>        
</form>