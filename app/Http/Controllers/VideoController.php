<?php

namespace App\Http\Controllers;

use App\Video;
use App\Entry;
use Illuminate\Http\Request;

class VideoController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $entry = Entry::find($id);
        $videos = Video::where('entry_id', $id)->orderBy('order')->get();
        return view('admin/entries/videoindex', compact('entry', 'videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $entry = Entry::find($id);

        $video = new Video;

        $service = request('vservice');
        $url = request('video');

        if($service == 'YouTube'){
            if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
              } else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
              } else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
              } else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
              }
              else if (preg_match('/youtube\.com\/verify_age\?next_url=\/watch%3Fv%3D([^\&\?\/]+)/', $url, $id)) {
                  $values = $id[1];
              } else {
                  return redirect()->back()->with('errorMessage', 'Δεν επιλέξατε Video από το YouTube');
              }
              $video->YouTubeId = $id[1];                         
        } elseif($service == 'Vimeo'){
            if(preg_match_all('#(http://vimeo.com)/([0-9]+)#i',$url,$id) OR preg_match_all('#(https://vimeo.com)/([0-9]+)#i',$url,$id)){
                $video->VimeoId = $id[2][0];
            } else {
                return redirect()->back()->with('errorMessage', 'Δεν επιλέξατε Video από το Vimeo');
            }
            // $video->VimeoId = substr(parse_url($url, PHP_URL_PATH), 1);
            
        }elseif($service == 'Facebook'){
            if(preg_match('~/videos/(?:t\.\d+/)?(\d+)~i',$url,$id)){
                $video->FacebookId = $id[1];
                
            } else {
                return redirect()->back()->with('errorMessage', 'Δεν επιλέξατε Video από το Facebook');
            }            
        } else {
            return redirect()->back()->with('errorMessage', 'Επιλέξτε αν το video είναι YouTube, Vimeo ή Facebook');
        }
        
        $video->entry_id = $entry->id;
        $video->save();

        return redirect()->back()->with('message', 'Το video αποθηκεύτηκε με επιτυχία');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entry $id, Video $videoid)
    {
        $video = $videoid;

        $video->delete();

        return redirect()->back()->with('message', 'Το video διαγράφηκε με επιτυχία');
    }

    //Function που παίρνει την νέα σειρά από το Ajax call του sortable, αναλύει το id κάθε item, και γράφει στην db το νέο order 
    public function order(){
        $orders = explode(',',$_POST['order']);
            foreach ($orders as $neworder=>$value){
                $parts = explode('-',$value);
                $entryid = $parts[0];
                $id = $parts[1];			
                $order = $neworder+1;
                $video = Video::find($id);
                $video->order = $order;
                $video->save();
            }
    }
}
