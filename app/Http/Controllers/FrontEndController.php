<?php

namespace App\Http\Controllers;

use Mail;
use Session;
use App\Home;
use App\Link;
use App\News;
use App\Page;
use App\Album;
use App\Entry;
use App\Image;
use App\Video;
use App\Church;
use App\Slider;
use App\Category;
use App\District;
use App\InfoPage;
use App\Metropole;
use App\Municipality;
use App\SpecialEntry;
use App\SpecialImage;
use Illuminate\Http\Request;
use App\Mail\EntryContactForm;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;
use App\Mail\EntryContactRepNotification;
use App\Mail\EntryContactAdminNotification;
use Carbon\Carbon;

class FrontEndController extends Controller
{
    public function home(){
        $districts = District::withCount([
            'churches',
            'municipalities',
            'entries' => function($query){
            $query->where('active', 1);
        }])->get();
        $entries = Entry::with(['category', 'district'])->where('active', 1)->orderBy('start_at', 'DESC')->take(12)->get();
        $showcase_a = Home::where('position', 'Showcase A')->first();
        $banner = $showcase_a->banners()->first();

        $news = News::orderBy('order')->get();

        return view('site.index', compact('districts', 'news', 'entries'));
    }

    public function feelingLucky()
    {
        $entries = Entry::where('active', '1')->get()->toArray();
        $id = array_rand($entries, 1);
        $entry = Entry::find($id);
        return view('site.treats.lucky', compact('entry'));
    }

    public function pages ($slug){

        $page = Page::with([
            'banners', 
            'category', 
            'category.entries' => function($query) {
                $query->with('tags','district')->where('active', 1);
            },
            'sliders'
        ])->where('slug', $slug)->first();
        $infoPage = InfoPage::where('slug', $slug)->first();
        $district = District::where('slug', $slug)->first();
        $categories = Category::all();    

        

        //Έλεγχος αν το slug ανήκει σε Page ή σε InfoPage και κατεύθυνση σε ανάλογο view
        if ($page){
            // $banners = $page->banners()->get();
            $entries = $page->category->entries->shuffle();
        
            $sliders = $page->sliders->where('expires', '>', \Carbon\Carbon::now()->subDays(15))->shuffle();
            
            $tags = $entries->pluck('tags')->flatten()->unique('name')->sortBy('name');


            try {
                // Get relative blog items, based on tag with same name as the page category
                $blogItems = DB::connection('mysql_blog')->table('abz76_k2_tags')
                ->where('abz76_k2_tags.name', str_replace('-','', $page->category->cat_name))
                ->join('abz76_k2_tags_xref', 'abz76_k2_tags.id', '=', 'abz76_k2_tags_xref.tagID')
                ->join('abz76_k2_items', 'abz76_k2_items.id', '=', 'abz76_k2_tags_xref.itemID')
                ->join('abz76_k2_categories', 'abz76_k2_categories.id', '=', 'abz76_k2_items.catid') // Joining with the categories table
                ->where(function ($query) {
                    $query->where('abz76_k2_items.catid', 1)
                          ->orWhere('abz76_k2_items.catid', 2)
                          ->orWhere('abz76_k2_items.catid', 6);
                })
                ->orderBy('abz76_k2_items.publish_up', 'desc')
                ->take(6)
                ->select([
                    'abz76_k2_items.*', 
                    'abz76_k2_categories.name as category_name', 
                    'abz76_k2_categories.alias as category_alias' // Select category alias
                ])
                ->get();        
                
            } catch (\Throwable $th) {
                $blogItems = NULL;
            }
                        
            return view('site.pages.show', compact('page', 'entries', 'sliders', 'tags', 'blogItems'));

        } elseif($infoPage){
            return view('site.infopages.show', compact('infoPage'));
        } elseif ($district){
            // dd($district->banners);
            $banners = [];
            // $banners = [];
            return view('site.districts.show', compact('district', 'categories', 'banners'));
        } else {
            return abort(404);
        }

    }

    public function bridalexpo(){
        $entries = Entry::where('newlife', '1')->inRandomOrder()->get();
        $pages = Page::with('category.entries')->orderBy('order')->get();

        return view ('site.pages.bridalexpo', compact('entries', 'pages'));
    }

    public function categories($distSlug, $catSlug){
        
        $district = District::where('slug', $distSlug)->firstOrFail();
        
        $category = Category::where('cat_slug', $catSlug)->firstOrFail();
        
        $banners = $category->banners()->get();
        $entries = Entry::where('end_at', '>', Carbon::today())->where('dist_id', $district->id)->where('cat_id', $category->id)->orderBy('order')->get();

        
        return view('site.categories.show', compact('entries', 'district', 'category', 'banners'));
    }

    public function municipalities($districtSlug){

        $district = District::where('slug', $districtSlug)->first();
       
        $municipalities = Municipality::where('district_id', $district->id)->get();

        return view('site.categories.municipalities', compact('district', 'municipalities'));
        
    }

    public function searchMunicipality(Request $request, $districtSlug) {
        
        $district = District::where('slug', $districtSlug)->first();

        $keyword = request('query');
        
        $results = Municipality::where('name', 'LIKE', '%'.$keyword.'%')->orWhere('address', 'LIKE', '%'.$keyword.'%')->get()->where('district_id', $district->id);       

        return view('site.categories.municipalitiesresults', compact('district', 'results', 'keyword'));

    }

    public function municipalityEntry($districtSlug, $municipalitySlug){
        $district = District::where('slug', $districtSlug)->first();
        $municipality = Municipality::where('slug', $municipalitySlug)->first();

        return view('site.entry.municipality', compact('district', 'municipality'));
    }


    public function churchEntry($districtSlug, $churchSlug){
        
        $district = District::where('slug', $districtSlug)->first();
        $church = Church::where('slug', $churchSlug)->first();
        
        
        return view('site.entry.church', compact('church', 'district'));
    }

    public function churchesPage(){

        $churches = Church::all()->random(42);

        return view('site.pages.churches', compact('churches'));

    }

    public function searchChurchesPage(Request $request) {

        $keyword = request('query');
        
        $results = Church::where('name', 'LIKE', '%'.$keyword.'%')->orWhere('address', 'LIKE', '%'.$keyword.'%')->get();

        return view('site.pages.churchesresults', compact('results', 'keyword'));

    }

    public function municipalitiesPage(){

        $municipalities = Municipality::all()->random(42);

        return view('site.pages.municipalities', compact('municipalities'));

    }

    public function searchMunicipalitiesPage(Request $request) {

        $keyword = request('query');
        
        $results = Municipality::where('name', 'LIKE', '%'.$keyword.'%')->orWhere('address', 'LIKE', '%'.$keyword.'%')->get();

        return view('site.pages.municipalitiesresults', compact('results', 'keyword'));

    }

    public function entry ($districtSlug, $categorySlug, $entrySlug)
    {
        $district = District::where('slug', $districtSlug)->first();
        $category = Category::where('cat_slug', $categorySlug)->first();
        $entry = Entry::where('slug', $entrySlug)->firstOrFail();
        $firstAlbum = Album::where('entry_id', $entry->id)->orderBy('order', 'asc')->first();
        $firstImages = Image::where('album_id', $firstAlbum->id)->orderBy('order', 'asc')->take(9)->get();

        if(!$entry->active) {

           return view('site.entry.expired', compact('district', 'category','entry'));
        }

        return view('site.entry.profile', compact('district', 'category', 'entry', 'firstAlbum', 'firstImages'));
    }

    public function albums($districtSlug, $categorySlug, $entrySlug){
        $district = District::where('slug', $districtSlug)->first();
        $category = Category::where('cat_slug', $categorySlug)->first();
        $entry = Entry::where('slug', $entrySlug)->firstOrFail();
        $albums = Album::where('entry_id', $entry->id)->orderBy('order', 'asc')->orderBy('id')->get();
        return view('site.entry.albums', compact('district', 'category', 'entry', 'albums'));
    }

    public function album($districtSlug, $categorySlug, $entrySlug, $albumid){

        $district = District::where('slug', $districtSlug)->first();
        $category = Category::where('cat_slug', $categorySlug)->first();
        $entry = Entry::where('slug', $entrySlug)->firstOrFail();
        $albums = Album::where('entry_id', $entry->id)->where('id', '<>', $albumid)->orderBy('order')->get();
        $album = Album::findOrFail($albumid);
        $images = Image::where('album_id', $album->id)->orderBy('order', 'asc')->get();
        return view('site.entry.photos', compact('district', 'category', 'entry', 'albums', 'album', 'images'));
    }

    public function video($districtSlug, $categorySlug, $entrySlug){
        $district = District::where('slug', $districtSlug)->first();
        $category = Category::where('cat_slug', $categorySlug)->first();
        $entry = Entry::where('slug', $entrySlug)->firstOrFail();
        $videos = Video::where('entry_id', $entry->id)->orderBy('order', 'asc')->get();

        return view('site.entry.video', compact('district', 'category', 'entry', 'videos'));
    }

    public function facebook ($districtSlug, $categorySlug, $entrySlug){
        $district = District::where('slug', $districtSlug)->first();
        $category = Category::where('cat_slug', $categorySlug)->first();
        $entry = Entry::where('slug', $entrySlug)->firstOrFail();

        return view('site.entry.facebook', compact('district', 'category', 'entry'));
    }

    public function contact($districtSlug, $categorySlug, $entrySlug){
        $district = District::where('slug', $districtSlug)->first();
        $category = Category::where('cat_slug', $categorySlug)->first();
        $entry = Entry::where('slug', $entrySlug)->firstOrFail();

        return view('site.entry.contact', compact('district', 'category', 'entry'));
    }

    public function sendMail(Request $request, District $district, Category $category, Entry $entry){

        $this->validate($request, [
            'name' => 'required|min:3',
            'phone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
            'email' => 'required|email',
            'date' => 'min:3',
            'message' => 'min:20',
            'agree' => 'required',
            'g-recaptcha-response' => 'required'
        ],
        [
            'agree.required' => 'Είναι απαραίτητο να συμφωνήσετε με τους όρους',
            'message.min' => 'Το μήνυμα θα πρέπει να έχει μήκος τουλάχιστο 20 χαρακτήρες',
            'phone.regex' => 'Το τηλέφωνο έχει κάποιο λάθος',
            'name.min' => 'Το όνομα είναι πολύ μικρό'

        ]
    );

        $data = array(
            'name' => $request->name,
            'category' => $entry->category->cat_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'date' => $request->date,
            'bodyMessage' => $request->message,
            'entryemail' => $request->entryemail,
            'entryName' => $entry->name,
            'repEmail' => $entry->client->representative->email
        );
        
        Mail::send(new EntryContactForm($request, $category, $entry));
        Mail::send(new EntryContactAdminNotification($request, $category, $entry));
        

        if($entry->client->representative->email !== 'zervas@gamosportal.gr'){

            Mail::send(new EntryContactRepNotification($category, $entry));

        };

        return redirect()->back()->with('message', 'Η αποστολή του μηνύματός σας ολοκληρώθηκε');
    }

    public function infoPageContact (Request $request){

        $this->validate($request, [
            'agree' => 'required',
            'name' => 'required',
            'person' => 'required',
            'phone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
            'email' => 'required|email',
            'message' => 'required|min:20'
        ],
        [
            'agree.required' => 'Είναι απαραίτητο να συμφωνήσετε με τους όρους'
        ]
    );

        $data = array(
            'name' => $request->name,
            'person' => $request->person,
            'email' => $request->email,
            'website' => $request->website,
            'phone' => $request->phone,
            'phone2' => $request->phone2,
            'bodyMessage' => $request->message,
        );

        Mail::send('site.emails.request', $data, function($message) use ($data) {
            $message->to('zervas@gamosportal.gr');
            $message->subject('Request Προβολής στο Gamos Portal');
        });

        return redirect('thanks');
    }

    public function thanks(){
        return view('site.infopages.thanks');
    }

    public function phonecall(){

        $entry = $_POST['entry'];
        
        $data = array(
            'entry' => $entry
        );

        Mail::send('site.emails.phonenotification', $data, function($message) use ($data) {
            $message->to('zervas@gamosportal.gr');
            $message->subject('Τηλεφωνική κλήση');
        });
    }
    public function emailsent(){

        $entry = $_POST['entry'];
        
        $data = array(
            'entry' => $entry
        );

        Mail::send('site.emails.emailnotification', $data, function($message) use ($data) {
            $message->to('zervas@gamosportal.gr');
            $message->subject('Email');
        });
    }

    //Special Entries

    public function specialentry ($specialentrySlug){
        
        $specialentry = SpecialEntry::where('slug', $specialentrySlug)->first();

        return view('site.specialentry.profile', compact('specialentry'));

    }

    public function specialentryphotos($specialentrySlug){

        $specialentry = SpecialEntry::where('slug', $specialentrySlug)->first();

        $specialimages = SpecialImage::where('specialentry_id', $specialentry->id)->orderBy('order')->get();

        return view('site.specialentry.photos', compact('specialentry', 'specialimages'));
        

    }

    public function specialentryfacebook ($specialentrySlug) {
        $specialentry = SpecialEntry::where('slug', $specialentrySlug)->first();

        return view ('site.specialentry.facebook', compact('specialentry'));
    }

    public function sendMailSpecial(Request $request, SpecialEntry $specialentry){

        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
            'email' => 'required|email',
            'message' => 'min:20',
            'agree' => 'required'
        ],
        [
            'agree.required' => 'Είναι απαραίτητο να συμφωνήσετε με τους όρους'
        ]
    );

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'bodyMessage' => $request->message,
            'entryemail' => $request->entryemail,
            'entryName' => $specialentry->name
        );

        
        Mail::send('site.emails.entrycontact', $data, function($message) use ($data) {
            $message->to($data['entryemail']);
            $message->replyTo($data['email']);
            $message->subject('Μήνυμα από την καταχώρησή σας '.$data['entryName'].' στο Gamos Portal');
        });

        Mail::send('site.emails.gpentrycontact', $data, function($message) use ($data) {
            $message->to('zervas@gamosportal.gr');
            $message->subject('Μήνυμα για την καταχώρηση '. $data['entryName']);
        });

        return redirect()->back()->with('message', 'Η αποστολή του μηνύματός σας ολοκληρώθηκε');
    }

    
}

