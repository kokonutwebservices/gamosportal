@foreach($districts as $district)

    @if ($district->entries_count + $district->churches_count + $district->municipalities_count)
        <a class="dropdown-item" href="/{{$district->slug}}">{{$district->name}}</a>
    @endif
    
    
@endforeach
