<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metropole extends Model
{

    public function churches() {
        return $this->hasMany(Church::class);
    }

    public function district () {
        return $this->belongsTo(District::class);
    }

    public static function validateMetropole($request) {
        $request->validate([
            'name' => 'required|min:5',
            'district_id' => 'required'
        ]);
    }

    public function redirectAfterSave($request) {

        if($request->submitbutton === "saveclose") {
            return redirect ()->route('admin.metropoles.index');
        } elseif ($request->submitbutton === "savestay") {
            return redirect ()->route('admin.metropoles.edit', $this->id);
        } elseif ($request->submitbutton === "savenew") {
            return redirect ()->route('admin.metropoles.create');
        };
    }

}
