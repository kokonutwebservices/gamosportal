
<div class="container">
    <div class="row">
    <div class="col-md-9">
        <ul class="list-unstyled row">
            @foreach($pages as $page)
                <li class="col-md-6"><a href="/{{$page->slug}}">{{$page->title}}</a></li>
            @endforeach
                <li class="col-md-6"><a href="/bridal-expo.php">Bridal Expo</a></li>
                <li class="col-md-6"><a href="/ekklisies.php">Εκκλησίες</a></li>
                <li class="col-md-6"><a href="/dimarxeia.php">Δημαρχεία</a></li>
        </ul>
    </div>
    <div class="col-md-3">
        <ul class="list-unstyled">
            @foreach($infoPages as $infoPage)
                <li><a href="/{{$infoPage->slug}}">{{$infoPage->title}}</a></li>
            @endforeach
                <li><a href="/testimonials.php">Είπαν για εμάς</a></li>
        </ul>
    </div>

    <div class="d-md-none footer-social">
        <ul class="list-inline">
            <li class="list-inline-item"><a class="" href="https://www.facebook.com/gamos.portal" target="_blank" rel="nofollow"><i class="fab fa-facebook-f"></i></a></li>
            <li class="list-inline-item"><a class="" href="https://www.instagram.com/gamosportal" target="_blank" rel="nofollow"><i class="fab fa-instagram"></i></a></li>
            <li class="list-inline-item"><a class="" href="https://www.pinterest.com/gamosportal" target="_blank" rel="nofollow"><i class="fab fa-pinterest-p"></i></a></li>
            <li class="list-inline-item"><a class="" href="https://www.youtube.com/channel/UCVCkcaCepRwhu-8BWn7ggoQ" target="_blank" rel="nofollow"><i class="fab fa-youtube"></i></a></li>
            {{-- <li class="list-inline-item"><a class="" href="https://www.twitter.com/gamosportal" target="_blank" rel="nofollow"><i class="fab fa-twitter"></i></a></li> --}}
            
        </ul>
    </div>
        
    </div>
    <div class="d-flex flex-row flex-wrap justify-content-between mt-5">
        <div class="d-none d-md-block mt-5"><a href="https://www.kokonut.gr" target="_blank" rel="nofollow" title="Κατασκευή Ιστοσελίδων - Web Development & Marketing">created by Kokonut Web Services</a></div>
        {{-- <div class="">
            <div class="col-lg-4 my-3">
                <a href="https://gamosportal.gr"><img loading="lazy" class="rounded mx-lg-3 mx-md-2" src="/storage/footer/logo-portal.jpg" alt="Gamos Portal - Ηλεκτρονικος οδηγός για το γάμο" title="Gamos Portal - Ηλεκτρονικος οδηγός για το γάμο"></a>
            </div>
            <div class="col-lg-4 my-3">
                    <a href="https://gamos-portal.gr" target="_blank" rel="nofollow"><img loading="lazy" class="rounded mx-lg-3 mx-md-2" src="/storage/footer/logo-blog.jpg" alt="Άρθρα, συμβουλές, ιδέες" title="Άρθρα, συμβουλές, ιδέες"></a>
            </div>
            <div class="col-lg-4 my-3">
                    <a href="https://gamos-deals.gr" target="_blank" rel="nofollow"><img loading="lazy" class="rounded mx-lg-3 mx-md-2" src="/storage/footer/logo-deals.jpg" alt="Προσφορές Γάμου" title="Προσφορές Γάμου"></a>
            </div>            
        </div> --}}
        <div class="d-md-none">created by <a href="https://www.kokonut.gr" target="_blank" title="Κατασκευή Ιστοσελίδων - Web Development & Marketing" rel="nofollow">Kokonut Web Services</a></div>
    </div>
</div>

