<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EntryContactAdminNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $entry;
    public $category;
    public $request;
     
    public function __construct($request, $category, $entry)
    {
        $this->request = $request;
        $this->category = $category;
        $this->entry = $entry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('site.emails.entryadminnotification')
            ->to('zervas@gamosportal.gr')
            ->subject("Ο πελάτης ". $this->entry->name ." είχε μήνυμα");
    }
}
