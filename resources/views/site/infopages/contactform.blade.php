<div class="infoContact">
    <h2 class="mb-5">Στείλτε μας μήνυμα</h2>

    <form action="/add-your-business.php" method="POST" class="form-horizontal">
        {{ csrf_field() }}

        <div class="form-group">
        {{-- <label for="name"><i class="fas fa-briefcase"></i></label> --}}
        <input type="text" class="form-control" name="name" id="name" placeholder="Επωνυμία Επιχείρησης" value="{{old('name')}}">
        </div>
        

        <div class="form-group">
        {{-- <label for="person">Όνομα Υπευθύνου</label> --}}
        <input type="text" name="person" id="person" class="form-control" placeholder="Όνομα Υπευθύνου" value="{{old('person')}}">
        </div>

        <div class="form-group">
        {{-- <label for="phone">Τηλέφωνο Επικοινωνίας</label> --}}
        <input type="text" name="phone" id="phone" class="form-control" placeholder="Τηλέφωνο Επικοινωνίας" value="{{old('phone')}}">
        </div>

        <div class="form-group">
        {{-- <label for="email">Email</label> --}}
        <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="{{old('email')}}">
        </div>

        <div class="form-group">
        {{-- <label for="website">Website</label> --}}
        <input type="text" name="website" id="website" class="form-control" placeholder="Website" value="{{old('website')}}">
        </div>

        <div class="form-group">
        {{-- <label for="message">Το μήνυμά σας</label> --}}
        <textarea class="form-control" placeholder="Το μήνυμά σας" name="message" id="message" rows="3">{{old('message')}}</textarea>
        </div>

        <div class="form-check">
            <input type="checkbox" class="form-check-input mr-5" name="agree" id="terms" value="agree">
            <label class="form-check-label ml-3 mb-3">      
                <a href="/terms.php" target="_blank">Συμφωνώ με τους όρους χρήσης και την πολιτική απορρήτου</a>
            </label>      
        </div>

        <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>

        <button type="submit" class="btn btn-primary">Αποστολή</button>

    </form>

    {{--  Πλαίσιο σφαλμάτων  --}}
    @include('admin.layouts.errors') 
    @if (session('message'))
        <div class="alert alert-success" id="successMessage">
            {{ session('message') }}
        </div>
    @endif
</div>