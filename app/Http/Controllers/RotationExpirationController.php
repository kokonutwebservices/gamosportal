<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use App\District;
use App\Category;
use App\Entry;
use App\Slider;
use App\Banner;
use Mail;

class RotationExpirationController extends Controller
{
    public function entriesRotation(){
        $districts = District::all();
        foreach ($districts as $district){
            $categories = Category::all();
            foreach ($categories as $category){
                $entries = Entry::where('dist_id', $district->id)->where('cat_id', $category->id)->orderby('order')->get();
                $count = count($entries);
                foreach ($entries as $order=>$entry){
                    if($order+1 != $count)
                    {
                        $newOrder = $entry->order +1; 
                        echo "$entry->name is not last. Order is $entry->order. It will become $newOrder <br>";

                        $entry->order = $newOrder;
                        $entry->save();
                    } else {
                        $newOrder = 1;
                        echo "$entry->name is last. Order is $entry->order. It will become $newOrder <br>";
                        $entry->order = $newOrder;
                        $entry->save();
                    }  
                    // $entry->order = ($order+1);
                    // $entry->save();
                }
            }
        }
    }

    public function entriesExpiration(){

        $entriesToExpire = Entry::where('end_at', '>', \Carbon\Carbon::now())->where('end_at', '<', \Carbon\Carbon::now()->addMonth())->orderBy('end_at', 'asc')->get();

        foreach ($entriesToExpire as $entry){
            $data = array(
                'name' => $entry->name,
                'category' => $entry->category->cat_name,
                'district' => $entry->district->name,
                'person' => $entry->person,                    
                'phone' => $entry->phone,
                'mobile' => $entry->mobile,
                'email' => $entry->email,
                'end_at' => $entry->end_at,
                'representative' => $entry->client->representative->name,
                'emails' => [
                    'info@gamosportal.gr'
                ]
            );

            if ($entry->client->representative->email != "zervas@gamosportal.gr") {
                $data['emails'] = Arr::prepend($data['emails'], $entry->client->representative->email);
            }
                                        
            Mail::send('site.emails.expiring', $data, function($message) use ($data) {
                $message->to($data['emails']);
                $message->subject("Βρίσκεται στον τελευταίο μήνα: $data[name]");
            });                        
        }

        $entriesExpired = Entry::where('end_at', '<', \Carbon\Carbon::now())->orderBy('end_at', 'asc')->get();

        foreach ($entriesExpired as $entry){
            if(\Carbon\Carbon::parse($entry->end_at)->addMonth() < \Carbon\Carbon::now()){
                
                $entry->active = 0;
                $entry->save();

                $data = array(
                    'name' => $entry->name,
                    'category' => $entry->category->cat_name,
                    'district' => $entry->district->name,
                    'person' => $entry->person,                    
                    'phone' => $entry->phone,
                    'mobile' => $entry->mobile,
                    'email' => $entry->email,
                    'end_at' => $entry->end_at,
                    'representative' => $entry->client->representative->name,
                    'emails' => [
                        'info@gamosportal.gr'
                    ]
                );

                if ($entry->client->representative->email != "zervas@gamosportal.gr") {
                    $data['emails'] = Arr::prepend($data['emails'], $entry->client->representative->email);
                }
                
                if(Carbon::parse($entry->end_at)->addDays(45) > Carbon::now() ) {
                    Mail::send('site.emails.dropped', $data, function($message) use ($data) {
                        $message->to($data['emails']);
                        $message->subject("Έληξε και απενεργοποιήθηκε: $data[name]");
                    }); 
                }               


            } else {
                $data = array(
                    'name' => $entry->name,
                    'category' => $entry->category->cat_name,
                    'district' => $entry->district->name,
                    'person' => $entry->person,                    
                    'phone' => $entry->phone,
                    'mobile' => $entry->mobile,
                    'email' => $entry->email,
                    'end_at' => $entry->end_at,
                    'representative' => $entry->client->representative->name,
                    'emails' => [
                        'info@gamosportal.gr'
                    ]
                );

                if ($entry->client->representative->email != "zervas@gamosportal.gr") {
                    $data['emails'] = Arr::prepend($data['emails'], $entry->client->representative->email);
                }
                                                    
                Mail::send('site.emails.expired', $data, function($message) use ($data) {
                    $message->to($data['emails']);
                    $message->subject("Έχει λήξει: $data[name]");
                }); 
            }
        }
    }

    public function slidersExpiration (Slider $sliders) {

        $slidersToExpire = Slider::where('expires', '>', \Carbon\Carbon::now())->where('expires', '<', \Carbon\Carbon::now()->addMonth())->orderBy('expires', 'asc')->get();

        foreach ($slidersToExpire as $slider) {

            $data = array(
                'name' => $slider->entry->name,                
                'expires' => $slider->expires
            );

            // dd($data);
                                        
            Mail::send('site.emails.sliderexpiring', $data, function($message) use ($data) {
                $message->to('info@gamosportal.gr');
                $message->subject("Slider banner στον τελευταίο μήνα: $data[name]");
            }); 

        }

        $slidersExpired = Slider::where('expires', '<', \Carbon\Carbon::now())->orderBy('expires', 'asc')->get();

        foreach ($slidersExpired as $slider) {

            $data = array(
                'name' => $slider->entry->name,                
                'expires' => $slider->expires
            );

            if (\Carbon\Carbon::parse($slider->expires)->addDays(15) < \Carbon\Carbon::now()) {
                Mail::send('site.emails.sliderdroped', $data, function($message) use ($data) {
                    $message->to('info@gamosportal.gr');
                    $message->subject("Slider banner έληξε και απενεργοποιήθηκε: $data[name]");
                }); 
            } else {
                Mail::send('site.emails.sliderexpired', $data, function($message) use ($data) {
                    $message->to('info@gamosportal.gr');
                    $message->subject("Slider banner που έληξε: $data[name]");
                }); 
            }



            // dd($data);
                                        
            
        }

    }

    // Χρησιμοποιήθηκε για να προστεθούν 3 μήνες σε όλες τις λήξεις, λόγω COVID-19 Lockdown
    // public function changeDates (){

    //     $entries = Entry::all();
        
    //     echo "<h1>Entries</h1>";

    //     foreach ($entries as $entry) {
    //         echo "$entry->name ends at $entry->end_at and will now end at ". Carbon::parse($entry->end_at)->addMonths(3) ."<br>";
    //         $entry->end_at = Carbon::parse($entry->end_at)->addMonths(3);
    //         $entry->save();
            
    //     }

    //     $banners = Banner::all();

    //     echo "<h1>Banners</h1>";

    //     foreach ($banners as $banner) {
    //         if ($banner->entry) {
    //             echo "Το banner της καταχώρησης:". $banner->entry->name ."λήγει στις ". $banner->expires. " και θα λήγει στις ". Carbon::parse($banner->expires)->addMonths(3) ."<br>";
    //             $banner->expires = Carbon::parse($banner->expires)->addMonths(3);
    //             $banner->save();
    //         }
    //     }


    //     $sliders = Slider::all();

    //     echo "<h1>Slider Banners</h1>";

    //     foreach ($sliders as $slider) {
    //         if ($slider->entry) {
    //             echo "Το sldider banner της καταχώρησης:". $slider->entry->name ." λήγει στις ". $slider->expires . " και θα λήγει στις ". Carbon::parse($slider->expires)->addMonths(3) ."<br>";
    //             $slider->expires = Carbon::parse($slider->expires)->addMonths(3);
    //             $slider->save();
    //         }
    //     }

    // }
}
