@extends('admin.layouts.master')

@section('content')

    <div class="col-md-6">

        <h1>Login</h1>

        {!! Form::open(['action'=> 'SessionsController@store', 'method' => 'POST']) !!}

        <div class="form-group">
            {{Form::label('email', 'Email')}}
            {{Form::email('email', '', ['class'=>'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::label('password', 'Password')}}
            {{Form::password('password', ['class'=>'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::submit('Login',  ['class'=>'btn btn-primary'])}}
        </div>


        {!! Form::close() !!}
        
        @include('admin.layouts.errors')

    </div>
    
    

@endsection