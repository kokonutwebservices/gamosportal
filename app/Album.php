<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $guarded = ['_token'];
    
    public function entry(){
        return $this->belongsTo(Entry::class);
    }

    public function images(){
        return $this->hasMany(Image::class);
    }
}
