<?php

namespace App\Http\Controllers;

use App\Album;
use App\Entry;
use App\Client;
use App\Image;
use Storage;
use Illuminate\Http\Request;

class AlbumsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $entry = Entry::find($id);
        $albums = Album::where('entry_id', $id)->orderBy('order', 'asc')->get();
        
        return view('admin.entries.albumsIndex', compact('entry', 'albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $entry = Entry::find($id);
        return view('admin.entries.albumsCreate', compact('entry'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required'
        ]);

        $album = new Album;
        $album->name = $request->input('name');
        $album->description = $request->input('description');
        $album->entry_id = $request->input('entry_id');
        $album->save();

        $newAlbumId = Album::all()->last()->id;
        
        return redirect("/admin/entries/albums/$album->entry_id/$newAlbumId/edit");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit($entryid, Album $album, $albumid)
    {
        $entry = Entry::find($entryid);
        $album = Album::find($albumid);
        $images = Image::where('album_id', $albumid)->orderBy('order')->get();

        return view('admin.entries.albumsEdit', compact('entry', 'album', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entry $entryid, Album $albumid)
    {
        $entry =$entryid;
        $album = $albumid;

        $album->description = request('description');
        $album->save();

        return redirect()->back()->with('message', 'Η περιγραφή του album ενημερώθηκε');
    }


    public function updateCover(Request $request, Entry $entryid, Album $albumid){

        $album = $albumid;

        $album->cover = request('cover');
        $album->save();

        return redirect()->back()->with('message', 'Το εξώφυλλο αποθηκεύτηκε με επιτυχία');

    }

    public function updateTitle(Request $request, $entryid, $albumid) {
        $album = Album::find($albumid);
        $album->name = request('name');

        $album->save();

        return redirect()->back()->with('message', 'Το όνομα ενημερώθηκε με επιτυχία');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy(Album $albumid)
    {
        $album = $albumid;

        $images = Image::where('album_id', $album->id)->get();

        foreach ($images as $image){
            $image->delete();
        }

        Storage::deleteDirectory("uploads/$album->id");

        $album->delete();

        return redirect()->back()->with('message', 'Το album διαγράφηκε με επιτυχία');
    }

    //Function που παίρνει την νέα σειρά από το Ajax call του sortable, αναλύει το id κάθε item, και γράφει στην db το νέο order 
    public function order(){
        
        $orders = explode(',',$_POST['order']);
        
        foreach ($orders as $neworder=>$value){
            $parts = explode('-',$value);
            $entryid = $parts[0];
			$id = $parts[1];			
            $order = $neworder+1;
            $album = Album::find($id);
            $album->order = $order;
            $album->save();
        }
    }
}
