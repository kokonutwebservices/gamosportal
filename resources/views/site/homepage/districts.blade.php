<h1 class="text-center py-2 my-0">Ο ΓΑΜΟΣ ΘΑ ΓΙΝΕΙ ΕΔΩ:</h1>
<span class="px-1 text-center d-block mx-auto mt-2 mb-2">Επιλέξτε περιοχή</span>
<ul class="list-unstyled text-center px-2 py-2">
@php
    $counter = 0;
    foreach($districts as $district){
        if ($district->entries_count + $district->churches_count + $district->municipalities_count  > 0) {
            $counter++;
        }
    }
    if ($counter < 9) {
        $districtClass = "few";
    } else {
         $districtClass = "many";
    }
@endphp

    @foreach ($districts as $district)

        @if(count($district->entries) > 0)
            
            <li class="{{$districtClass}}"><a href="{{route('district', [$district->slug])}}"> {{$district->name}} </a></li>
        
        @elseif ($district->churches_count  OR $district->municipalities_count)
 
        <li class="{{$districtClass}}"><a href="{{route('district', [$district->slug])}}"> {{$district->name}}</a></li>

        @endif

    @endforeach
</ul>