@extends ('site.layouts.master')

@section('metatags')
    <title>Δημαρχεία | Πολιτικός Γάμος, Λίστα Δημαρχείων.</title>
    <meta name="description" content="Βρείτε όλα τα δημαρχεία για πολιτικό γάμο, με τα στοιχεία επικοινωνίας τους" >
    <meta name="keywords" content="δημαρχεία, πολιτικός γάμος, politikos gamos, dimarxeia" >
@endsection

@section ('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section id="slider">
    <div class="container">
        <div class="row">
            <div class="showcase col-md-12">   
                <h1>Δημαρχεία για πολιτικό γάμο</h1>     
                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="2000">
                    <div class="carousel-inner">
                        {{--  <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>  --}}
                         <div class="carousel-item active" data-slide="1">                            
                            <img class="d-block w-100" src="storage/gpnl.png" alt="New Life - Η μεγαλύτερη Έκθεση Γάμου" title="New Life - Η μεγαλύτερη  Έκθεση Γάμου" target="_blank">                                                    
                        </div> 
                        {{--  @if($page->image2)                        
                            <div class="carousel-item " data-slide="2">
                                <a href="{{$page->link2}}">
                                    <img class="d-block w-100" src="storage/pages/{{$page->image2}}"alt="{{$page->title}} " title="{{$page->title}} - {{$page->caption_title_1}}">
                                    <div class="carousel-caption d-none d-md-block">
                                            <h3>{{$page->caption_title_2}}</h3>
                                            <p>{{$page->caption_description_2}}</p>
                                    </div>
                                </a>
                            </div>                        
                        @endif
                        @if($page->image3)
                            <div class="carousel-item" data-slide="3">
                                <a href="{{$page->link2}}">
                                    <img class="d-block w-100" src="images/pages/{{$page->image3}}" alt="{{$page->title}} " title="{{$page->title}} - {{$page->caption_title_1}}">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h3>{{$page->caption_title_3}}</h3>
                                            <p>{{$page->caption_description_3}}</p>
                                    </div>
                                 </a>
                            </div>
                        @endif  --}}
                    </div>
                </div>
            </div>
        </div>    
    </div>
</section>
<section id="main">
<div class="container">
    <div class="row">
        <div class="page-description">
            <p> Σκέφτεστε να κάνετε πολιτικό γάμο; Βρείτε τώρα το δημαρχείο της περιοχής σας στο Gamos Portal με τα στοιχεία επικοινωνίας που θα χρειαστείτε.</p>
        </div>
        <div class="col-md-9 latest">
            <h2>Δείτε μερικά από τα δημαρχεία που μπορείτε να βρείτε στο Gamos Portal</h2>
            <div>
                <form action="/dimarxeia.php" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group d-flex">
                    <input type="text"
                        class="form-control" name="query" id="" aria-describedby="helpId" value="{{$keyword}}">
                        <button type="submit" class="float-right"><i class="fas fa-search"></i></button>
                        <a class="btn btn-default" href="/dimarxeia.php"><i class="fas fa-undo-alt"></i></a>
                    </div>
                </form>
            </div>
            <div class="d-flex flex-wrap">
                @foreach($results as $municipality)                
                    <div class="latest-item col-md-4 pull-left">
                        <div class="card-body">
                            <h4 class="card-title"><a href="/{{$municipality->district->slug}}/dimarxeia/{{$municipality->slug}}">{{$municipality->name}}</a></h4>
                            {{-- <p><a href="tel:{{$municipality->phone}}">{{$municipality->phone}}</a></p> --}}
                            <p><a class="text-secondary" href="https://maps.google.com/?ll={{$municipality->map}}" target="_blank">{{$municipality->address}}</a></p>
                            <small><a href="/{{$municipality->district->slug}}/dimarxeia"> {{$municipality->district->name}} </a></small>                       
                        </div>
                    </div>
                @endforeach            
            </div>
        </div>
        <div class="col-md-3">
            <div id="categories" class="sidebar-element">
                <h3>Βρείτε στο Gamos Portal: </h3>
                <nav>
                @foreach (Pages::pagesList() as $page)
                    <a href="{{$page->slug}}" class="nav-link">{{$page->title}}</a>
                @endforeach
                </nav>
            </div>
            @if (count($results) > 8)
            <div class="fb-page" 
            data-href="https://www.facebook.com/gamos.portal"
            data-tabs="timeline"
            data-height="820" 
            data-small-header="false" 
            data-adapt-container-width="true" 
            data-hide-cover="false" 
            data-show-facepile="true">
            <blockquote cite="https://www.facebook.com/gamos.portal" 
            class="fb-xfbml-parse-ignore">
                <a href="https://www.facebook.com/gamos.portal">Gamos Portal</a>
            </blockquote>
        </div>
            @endif
        </div>
    </div>
</div>
</section>
</div>    
@endsection