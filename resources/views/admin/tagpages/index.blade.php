@extends('admin.layouts.master')

@section('content')
    <h1>Tag Pages</h1>
    <a href="/admin/tagpages/create" class="btn btn-primary">Δημιουργία</a>

    <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Τίτλος</th>
            <th scope="col">Slug</th>
            <th scope="col">Tag</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody id="sortableTag">
            @foreach ($tagpages as $page)
                <tr id="{{$page->id}}">
                    <td><a href="/admin/tagpages/{{$page->id}}/edit">{{$page->title}}</td>
                    <td>{{$page->slug}}</td>
                    <td>{{$page->tag->name}}</td>
                    <td>
                        <form action="/admin/tagpages/{{$page->id}}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                        </form>
                    </td>
              </tr>
            @endforeach
          
          
        </tbody>
      </table>
      
      
@endsection