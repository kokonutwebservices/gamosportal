<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoPage extends Model
{
    protected $guarded = ['_token'];

    public function banners(){
        return $this->belongsToMany(Banner::class);
    }
}
