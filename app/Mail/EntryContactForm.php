<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EntryContactForm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $entry;
    public $category;
    public $request;
     
    public function __construct($request, $category, $entry)
    {
        $this->request = $request;
        $this->category = $category;
        $this->entry = $entry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('site.emails.entrycontactform')
            ->to($this->entry->email)
            ->subject('Έχετε μήνυμα από το Gamos Portal')
            ->from('info@gamosportal.gr')
            ->replyTo($this->request->email);
    }
}
