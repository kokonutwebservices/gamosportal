<?php

namespace App\Http\Controllers;

use App\Slider;
use App\Entry;
use App\Page;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;
use Storage;

class SlidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        return view('admin.sliders.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $entries = Entry::all();
        $pages = Page::all();
        return view('admin.sliders.create', compact('entries', 'pages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'entry_id' => 'required',
            'image' => 'required|image|mimes:jpeg,jpg,png,gif|max:1000',
            'url' => 'required',
            'expires' => 'required'
        ]);

        //Slider Image
        $sliderImage = $request->file('image');//Get the slider file
        $ext = $sliderImage->getClientOriginalExtension(); //Get the extension
        $entry = Entry::where('id', request('entry_id'))->first();
        if(!$entry) {
            $sliderFileNameBase = $sliderImage->getClientOriginalName();
            $imageFileName = $sliderFileNameBase;
        } else {
            $sliderFileNameBase = $entry->slug; //Get the slug to use as filename base
            $imageFileName = $sliderFileNameBase . "-slider.".$ext;
        }
        
        request()->file('image')->storeAs('sliders', $imageFileName); //Store file

        $slider = new Slider;
        $slider->entry_id = request('entry_id');
        $slider->image = $imageFileName;
        $slider->description = request('description');
        $slider->url = request('url');
        $slider->expires = request('expires');
        $slider->save();
        $slider->pages()->sync($request->page);

        $id = Slider::latest()->first()->id;

        //Redirect
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.sliders.edit', $id)->with('message', 'Το slide αποθηκεύτηκε'); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.sliders.index')->with('message', 'Το slide αποθηκεύτηκε');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.sliders.create')->with('message', 'Το slide αποθηκεύτηκε');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider, Page $pages, Entry $entry)
    {

        // $entry = $slider->entry;
        $pages = $pages->all();
        $entries = Entry::all();
        return view('admin.sliders.edit', compact('slider', 'pages', 'entries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $this->validate(request(),[
            'entry_id' => 'required',
            'image' => 'image|mimes:jpeg,jpg,png,gif|max:1000',
            'url' => 'required',
            'expires' => 'required'
        ]);


        //Update Slider Image
        if ($request->hasFile('image')) {
            //Αν υπάρχει εικόνα...
            Storage::delete("sliders/$slider->image");
            $sliderImage = $request->file('image');//Get the slider file
            $ext = $sliderImage->getClientOriginalExtension(); //Get the extension
            $entry = Entry::where('id', request('entry_id'))->first();
            $sliderFileNameBase = $entry->slug; //Get the slug to use as filename base
            $imageFileName = $sliderFileNameBase . "-slider.".$ext;
            request()->file('image')->storeAs('sliders', $imageFileName); 
            
        } else {
            $imageFileName = $slider->image;
        }


        $slider->entry_id = request('entry_id');
        $slider->image = $imageFileName;
        $slider->description = request('description');
        $slider->url = request('url');
        $slider->expires = request('expires');
        $slider->save();
        $slider->pages()->sync($request->page);


        //Redirect
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.sliders.edit', $slider)->with('message', 'Το banner αποθηκεύτηκε'); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.sliders.index')->with('message', 'Το banner αποθηκεύτηκε');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.sliders.create')->with('message', 'Το banner αποθηκεύτηκε');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $image = $slider->image;
        Storage::delete("sliders/$image");

        $slider->delete();

        return redirect()->route('admin.sliders.index')->with("message", "Το slide διαγράφηκε");
    }

    public function sliderentry(){
        //function που δέχεται το ajax call για τις καταχωρήσεις που αντιστοιχούν σε έναν πελάτη

        if($_POST['entry'] != 0){
            $id = $_POST['entry'];
            $entry = Entry::find($id);
            $district = $entry->district->slug;
            $category = $entry->category->cat_slug;
            echo "/$district/$category/$entry->slug";
        }    
    }
}
