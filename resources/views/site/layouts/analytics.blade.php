<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-24586968-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-24586968-1');
    gtag('config', 'AW-957693429');
</script>

<script>
    // Define dataLayer and the gtag function.

    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }


    // IMPORTANT - DO NOT COPY/PASTE WITHOUT MODIFYING REGION LIST

    // Set default consent for specific regions according to your requirements

    gtag('consent', 'default', {

        'ad_storage': 'denied',

        'ad_user_data': 'denied',

        'ad_personalization': 'denied',

        'analytics_storage': 'denied',

        'regions': [
            'US', 'CA', 'GB', 'FR', 'DE', 'IT', 'ES', 'NL', 'SE', 'NO', 'DK', 'FI', 'BE', 'AU', 'NZ', 'JP',
            'KR', 'SG', 'HK', 'TW',
            'GR', 'PT', 'IE', 'AT', 'PL', 'CZ', 'HU', 'RO', 'BG', 'HR', 'SI', 'SK', 'EE', 'LV', 'LT', 'CY',
            'MT', 'LU'
        ]

    });

    // Set default consent for all other regions according to your requirements

    gtag('consent', 'default', {

        'ad_storage': 'denied',

        'ad_user_data': 'denied',

        'ad_personalization': 'denied',

        'analytics_storage': 'denied'

    });
</script>
