@extends('site.entry.show')

@section('entrycontent')
    <div id="videos">
        @foreach($videos as $video)

        @if($video->YouTubeId)
            <div class="video-item" id="{{$entry->id}}-{{$video->id}}">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{$video->YouTubeId}}" allowfullscreen></iframe>
                </div>
            </div>
        @elseif($video->VimeoId)
            <div class="video-item" id="{{$entry->id}}-{{$video->id}}">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe src="https://player.vimeo.com/video/{{$video->VimeoId}}?byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            </div>
        @elseif($video->FacebookId)
            <div class="video-item">
                 <!-- Load Facebook SDK for JavaScript -->
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>

                <!-- Your embedded video player code -->
                <div class="fb-video" data-href="https://www.facebook.com/facebook/videos/{{$video->FacebookId}}/" data-width="auto" data-show-text="false">
                    <div class="fb-xfbml-parse-ignore">                
                        <a href="https://www.facebook.com/facebook/videos/10153231379946729/"></a>
                    </blockquote>
                    </div>
                </div>
            </div>
        @endif
        
    @endforeach
    </div>
   
    <div class="col-lg-12 clearfix">
        <div class="float-left">
            <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}/video#videos" class="btn btn-default float-right my-3" ><i class="fas fa-arrow-alt-circle-left"></i> Φωτογραφίες</a>
        </div>
        <div class="float-right">
            @if ($entry->facebook)
            <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}/facebook#facebook" class="btn btn-default float-right my-3" >Timeline <i class="fas fa-arrow-alt-circle-right"></i></a>
            @endif
        </div>
    </div>
@endsection
