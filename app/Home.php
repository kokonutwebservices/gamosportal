<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    protected $table = 'home';

    public function banners(){
        return $this->belongsToMany(Banner::class);
    }
}
