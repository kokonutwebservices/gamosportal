<div class="js-cookie-consent cookie-consent">

    <span class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
    </span>

    <div class="clearfix"></div>

    <button class="js-cookie-consent-agree cookie-consent__agree">
        {{ trans('cookieConsent::texts.agree') }}
    </button>

    <button class="cookie-consent__link" onclick="window.location.href='/privacy.php'">
        {!! trans('cookieConsent::texts.link') !!}
    </button>

</div>
