<div class="col more">
    <h3>Δείτε ακόμη στο Gamos Portal</h3>
    <div class="d-flex flex-wrap">
        @foreach(More::moreEntries($entry) as $item)
            <div class="col-md-4 col-lg-2 my-3">
                <a href="/{{$item->district->slug}}/{{$item->category->cat_slug}}/{{$item->slug}}">
                    <img loading="lazy" class="thumbnail" src="/storage/entries/{{$item->cat_id}}/{{$item->image}}" alt="{{$item->category->cat_name}} - {{$item->district->name}} " title="{{$item->category->cat_name}} - {{$item->district->name}} ">
                    <div class="text-center">
                        {{$item->name}}
                    </div>
                </a>
            </div>
        @endforeach
    </div>
</div>