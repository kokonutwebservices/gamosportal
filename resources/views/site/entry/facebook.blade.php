@extends('site.entry.show')

@section('entrycontent')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="#facebook" class="d-flex justify-content-center">
    <div class="fb-page d-block d-lg-none" data-href="{{$entry->facebook}}" data-tabs="timeline" data-width="300" data-height="800" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
    <div class="fb-page d-none d-lg-block" data-href="{{$entry->facebook}}" data-tabs="timeline" data-width="500" data-height="800" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
</div>

<div class="col-lg-12 clearfix">
  <div class="float-left">
      @if (count($entry->videos) > 0)
        <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}/video#videos" class="btn btn-default float-right my-3" ><i class="fas fa-arrow-alt-circle-left"></i> Video</a>
      @else
        <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}/photos#albums" class="btn btn-default float-right my-3" ><i class="fas fa-arrow-alt-circle-left"></i> Φωτογραφίες </a>
      @endif
  </div>
  <div class="float-right">
    <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}#profile" class="btn btn-default float-right my-3" >Προφίλ <i class="fas fa-arrow-alt-circle-right"></i></a>
  </div>
</div>
@endsection