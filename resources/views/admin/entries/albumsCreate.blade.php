@extends('admin.layouts.master')

@section('content')

<h2>Προσθήκη Album στην καταχώρηση {{$entry->name}}</h2>

<div class="col-md-6">
{!!Form::open(['method' => 'POST', 'action' => 'AlbumsController@store']) !!}

<div class="form-group">
{{Form::label('name', 'Ονομασία Album')}}
{{Form::text('name', '', ['class' => 'form-control'])}}
</div>

<div class="form-group">
{{Form::label('description', 'Περιγραφή Album')}}
<textarea name="description" class="form-control tinyMCE"></textarea>
</div>

<div class="form-group">
{{Form::hidden('entry_id', $entry->id)}}
{{Form::button('Δημιουργία', ['type' => 'submit', 'class' => 'btn btn-primary'])}}
</div>

{!!Form::close()!!}
</div>

@endsection