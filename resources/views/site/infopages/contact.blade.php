<div class="col-md-6">
@include('site.infopages.contactform')
</div>
<div class="col-md-6">
    <h2 class="text-right mb-5">Προβάλλετε την επιχείρησή σας αποτελεσματικά</h2>
    <div class="info mx-auto d-flex justify-content-center align-items-center"><i class="fas fa-chevron-right"></i><div class="d-inline-flex counter" data-count="240000">0</div> <div class="d-inline-flex text-center"> επισκέψεις / έτος </div></div>
    <div class="info mx-auto d-flex justify-content-center align-items-center"><i class="fas fa-chevron-right"></i><div class="d-inline-flex counter" data-count="250">0</div> <div class="d-inline-flex text-center"> keywords στην 1η σελίδα</div></div>
    <div class="info mx-auto d-flex justify-content-center align-items-center"><i class="fas fa-chevron-right"></i><div class="d-inline-flex counter" data-count="{{More::entryCount()}}">0</div> <div class="d-inline-flex text-center"> επαγγελματίες </div></div>
    <div class="info mx-auto d-flex justify-content-center align-items-center">Χορηγός Επικοινωνίας έκθεσης New Life</div>
</div>

<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            facebook: "154394237983754", // Facebook page ID
            call_to_action: "Message us", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->
<script>
document.getElementsByTagName("textarea").value = "Fifth Avenue, New York City";
</script>