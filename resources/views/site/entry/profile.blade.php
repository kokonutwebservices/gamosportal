@extends('site.entry.show')

@section('entrycontent')
    <div id="profile" class="profile">
        <img loading="eager" class="thumbnail profile-pic d-none d-lg-block mx-auto"
            src="/storage/entries/{{ $category->id }}/{{ $entry->image }}"
            alt="{{ $entry->category->cat_name }} {{ $entry->name }}"
            title="{{ $entry->category->cat_name }} {{ $entry->name }}">

        <div class="mt-3">
            {!! $entry->profile !!}

            <div class="mt-3 mb-3">
                @if ($entry->newlife)
                    <a href="/bridal-expo.php"><img src="/storage/bridal_expo.jpg" alt="Συμμετέχουμε στην έκθεση New Life"
                            title="Συμμετέχουμε στην έκθεση Bridal Expo"></a>
                @endif
            </div>


            <div class="my-gallery card-columns">
                @foreach ($firstImages as $image)
                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="card">
                        <a href="/storage/uploads/{{ $image->album->id }}/{{ $image->image }}" itemprop="contentUrl"
                            data-size="{{ $image->imageSize() }}">
                            <img loading="lazy" src="/storage/uploads/{{ $image->album->id }}/{{ $image->image }}"
                                itemprop="thumbnail" alt="{{ $entry->name }} - {{ $category->cat_name }}" />
                        </a>
                        <figcaption class="d-none" itemprop="caption description">{{ $image->description }} </figcaption>
                    </figure>
                @endforeach
            </div>
            <a href="/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}/{{ $entry->slug }}/photos#albums"
                class="btn btn-default float-right my-3">Περισσότερες Φωτογραφίες <i
                    class="fas fa-arrow-alt-circle-right"></i></a>


        </div>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        {{-- <div class="addthis_inline_share_toolbox_vgy1"></div> --}}
    </div>


    {{-- PHOTOSWIPE --}}
    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
             It's a separate element, as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
            <!-- don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader--active when preloader is running -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>
    </div>
@endsection
