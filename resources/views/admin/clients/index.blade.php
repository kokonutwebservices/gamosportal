@extends('admin.layouts.master')

@section('content')
<h1>Πελάτες Gamos Portal</h1>

<a href="clients/create" class="btn btn-primary pull-left">Προσθήκη Πελάτη</a>
<div class="col-md-12">
    <div class="form-group col-md-4 pull-right">
    {!!Form::open(['method' => 'POST', 'action' => 'ClientsController@search'])!!}
        <div class="input-group">
        <input type="text" class="form-control" name="query" placeholder="Αναζήτηση" aria-label="Αναζήτηση">
        <span class="input-group-btn">
            <button class="btn btn-secondary" type="submit">Go!</button>
        </span>
        </div>
    {!!Form::close()!!}
    </div>
</div>

<table class="table">
    <thead class="thead-inverse">
        <tr>
            <th>@sortablelink('id')</th>
            <th>@sortablelink('name', 'Όνομα Πελάτη')</th>
            <th>@sortablelink('email', 'Email')</th>
            <th>@sortablelink('representative', 'Πωλητής')</th>
            <th>Καταχωρήσεις</th>
        </tr>
    </thead>
    @foreach($clients as $client)
    <tr>
        <td>{{$client->id}}</td>
        <td><a href="/admin/clients/{{$client->id}}/edit"> {{$client->name}} </a></td>        
        <td>{{$client->email}}</td>
        <td><a href="/admin/representatives/{{$client->representative['id']}}/edit">{{$client->representative['name']}}</a></td>
        <td>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-info d-block m-auto" data-toggle="modal" data-target="#client-{{$client->id}}">
                <i class="fas fa-th-list"></i>
            </button>
            
            <!-- Modal -->
            <div class="modal fade bd-example-modal-lg" id="client-{{$client->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Καταχωρήσεις Πελάτη: {{$client->name}} </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <table>
                            <tr>
                                <th>Καταχώρηση</th>
                                <th>Κατηγορία</th>
                                <th>Λήξη</th>
                            </tr>
                            @foreach ($client->entries as $entry)
                            <tr>
                                <td><a href="/admin/entries/{{$entry->id}}/edit">{{$entry->name}}</a></td>
                                <td>{{$entry->category->cat_name}}</td>
                                <td>{{$entry->end_at}}</td>
                            </tr>
                            @endforeach
                        </table>            
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
                </div>
            </div>

        </td>
    </tr>
@endforeach

</table>




<a href="clients/create" class="btn btn-primary">Προσθήκη Πελάτη</a>
@endsection