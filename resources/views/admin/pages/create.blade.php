@extends('admin.layouts.master')

@section('content')

<h1>Δημιουργία Σελίδας</h1>

<form action="/admin/pages" method="POST">
@csrf

<div class="form-group">
    <label for="title">Τίτλος Σελίδας</label>
    <input type="text" class="form-control" name="title" id="title" placeholder="">
    @error('title')
      <small class="form-text text-danger">{{$message}}</small>          
    @enderror
  </div>

  <div class="form-group">
    <label for="slug">Slug (friendly url)</label>
    <input type="text" class="form-control" name="slug" id="slug" placeholder="">
    @error('slug')
      <small class="form-text text-danger">{{$message}}</small>          
    @enderror
  </div>

  <div class="form-group">
    <label for="metatitle">Meta Title</label>
    <input type="text" class="form-control" name="metatitle" id="metatitle" placeholder="" >
    @error('metatitle')
      <small class="form-text text-danger">{{$message}}</small>          
    @enderror
  </div>

  <div class="form-group">
    <label for="metadescription">Meta Description</label>
    <input type="text" class="form-control" name="metadescription" id="metadescription" placeholder="" >
    @error('metadescription')
      <small class="form-text text-danger">{{$message}}</small>          
    @enderror
  </div>
  
  <div class="form-group">
    <label for="metakeywords">Meta Keywords</label>
    <input type="text" class="form-control" name="metakeywords" id="metakeywords" placeholder="" >
    @error('metakeywords')
      <small class="form-text text-danger">{{$message}}</small>          
    @enderror
  </div>

  <div class="form-group">
    <label for="body">Κείμενο</label>
    <textarea class="form-control tinyMCE" name="body" id="body" rows="3" ></textarea>
    @error('body')
      <small class="form-text text-danger">{{$message}}</small>          
    @enderror
  </div>

  <div class="form-group">
    <label for="title2">Τίτλος 2 (πάνω από τις καταχωρήσεις)</label>
    <input type="text" class="form-control" name="title2" id="title2" placeholder="" >
    @error('title2')
      <small class="form-text text-danger">{{$message}}</small>          
    @enderror
  </div>

  <div class="form-group">
    <label for="category_id">Σχετική Κατηγορία</label>
    <select class="form-control" name="category_id" id="category_id">
      <option disabled selected>Επιλέξτε σχετική κατηγορία</option>
      @foreach ($categories as $category)

          <option value="{{$category->id}}">{{$category->cat_name}}</option>
          
      @endforeach
    </select>
  </div>

  <div class="d-flex justify-content-around">
      @include('admin.layouts.savebuttons')
      <a href="/admin/pages" class="btn btn-warning">Cancel</a>
  </div>

</form>

@endsection


{{-- <div class="col-md-12">
    <h1>Δημιουργία νέας σελίδας</h1>
</div>
<div class="col-md-12">
@include('admin.layouts.errors')

{!! Form::open(['action'=>'PagesController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data' ]) !!}
    <div class="form-group">
        {{Form::label('title', 'Τίτλος Σελίδας')}}
        {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Τίτλος', 'required'])}}
    </div>
    <div class="form-group">
        {{Form::label('slug', 'Slug (friendly url)')}}
        {{Form::text('slug', '', ['class' => 'form-control', 'placeholder' => 'url', 'required' ])}}
    </div>
    <div class="form-group">
        {{Form::label('metatitle', 'Meta Title')}}
        {{Form::text('metatitle', '', ['class' => 'form-control', 'placeholder' => 'Τίτλος browser μέχρι 60 χαρακτήρες', 'required' ])}}
    </div>
    <div class="form-group">
        {{Form::label('metadescription', 'Meta Description')}}
        {{Form::text('metadescription', '', ['class'=>'form-control', 'placeholder' => 'Περιγραφή σελίδας (μέχρι 150 χαρακτήρες)', 'required'])}}
    </div>
    <div class="form-group">
        {{Form::label('metakeywords', 'Meta Keywords')}}
        {{Form::text('metakeywords', '', ['class'=>'form-control', 'placeholder' => 'Keyowords σελίδας, χωρισμένα με ,', 'required'])}}
    </div>
    <div class="form-group">
        {{Form::label('body', 'Κείμενο')}}
        {{Form::textarea('body', '', ['class'=>'form-control tinyMCE', 'required'])}}
    </div>
    <div class="form-group">
        {{Form::label('title2', 'Τίτλος 2 (πάνω από τις καταχωρήσεις)')}}
        {{Form::text('title2', '', ['class'=>'form-control', 'placeholder' => 'Τίτλος 2', 'required'])}}
    </div>
    <hr>
    <div class="form-group">
        <label for="category_id">Σχετική Κατηγορία</label>
        <select class="form-control" id="category_id" name="category_id">
            @foreach ($categories as $category)
                @if($category->cat_parent_id >0)
                    <option value="{{$category->id}}">{{$category->cat_name}}</option>
                @endif
            @endforeach
        </select>
    </div>
    
    <div class="form-group">
            <div class="col-md-2">                
                {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
            </div>  

            <div class="col-md-2">
                {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
            </div>

            <div class="col-md-2">
                {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
            </div>

            <div class="col-md-2">  
                <a href="/admin/pages" class="btn btn-warning pull-right">Cancel</a>
            </div>    
    </div>

{!! Form::close() !!}



</div> --}}