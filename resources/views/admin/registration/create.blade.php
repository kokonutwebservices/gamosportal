@extends('admin.layouts.master')

@section('content')

    <div class="col-md-6">

        <h1>Δημιουργία λογαριασμού χρήστη</h1>

        {!! Form::open(['action'=>'RegistrationController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data' ]) !!}

        <div class="form-group">
            {{Form::label('name', 'Όνομα')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Όνομα', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('email', 'Email')}}
            {{Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Email', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('password', 'Password')}}
            {{Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('password_confirmation', 'Επιβεβαίωση Password')}}
            {{Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Password', 'required'])}}
        </div>

        {{Form::submit('Register', ['class' => 'btn btn-primary'])}}

        {!! Form::close() !!}

    </div>
    @include('admin.layouts.errors')

@endsection