<?php

namespace App\Http\Controllers;

use App\Entry;
use App\TagPage;
use App\Tag;
use Illuminate\Http\Request;

class TagPagesController extends Controller
{
    public function show($slug) {

        $tagPages = TagPage::all()->sortBy('order');

        $tagPage = TagPage::where('slug', $slug)->firstOrFail();

        $tag = $tagPage->tag;
        
        $entries = Entry::whereHas('tags', function($query) use ($tag){
            return $query->where('tag_id', $tag->id);
        })->get();
        
        
        return view('site.tagpages.show', compact('tagPages','tagPage', 'entries'));

    }
}
