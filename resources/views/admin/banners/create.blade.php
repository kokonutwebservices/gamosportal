@extends('admin.layouts.master')

@section('content')
<h1>Δημιουργία Banner</h1>

@include('admin.layouts.errors')
<hr>

{!!Form::open(['method'=>'POST', 'action'=>'BannersController@store', 'enctype'=>'multipart/form-data'])!!}
<div class="form-group">
  <label for="entry">Καταχώρηση: </label>
  <select class="form-control" name="entry_id" id="entry" required>
    <option selected>Επιλέξτε Καταχώρηση</option>
        @foreach($entries as $entry)
            <option value="{{$entry->id}}">{{$entry->name}}</option>        
        @endforeach
    <option value="0">Καμία</option>
  </select>
</div>


<div class="form-group">
  <label for="type"></label>
  <select class="form-control" name="type" id="type" required>
    <option>Τύπος banner</option>
    <option value="square">Τετράγωνο</option>
    <option value="vertical">Κάθετο</option>
    <option value="horizontal">Οριζόντιο</option>
  </select>
</div>

<div class="form-group">
  <label for="banner">Επιλέξτε αρχείο banner</label>
  <input type="file" class="form-control-file" name="banner" id="banner" placeholder="" aria-describedby="fileHelpId" required>
  <small id="fileHelpId" class="form-text text-muted">Το αρχείο θα πρέπει να είναι .jpg, .png ή .gif</small>
</div>

{{--  <div class="form-group">
  <label for="caption"></label>
  <input type="text" class="form-control" name="caption" id="caption" aria-describedby="helpId" placeholder="">
  <small id="helpId" class="form-text text-muted">Λεζάντα εικόνας</small>
</div>  --}}

<div class="form-group">
  <label for="url"></label>
  <input type="text" class="form-control" name="url" id="url" aria-describedby="helpId" placeholder="" required>
  <small id="helpId" class="form-text text-muted">Προσθέστε το link</small>
</div>

<div class="form-group">
  <label for="expires">Ημερομηνία Λήξης</label>
  <input type="date" class="form-control date" name="expires" id="expires" aria-describedby="helpId" placeholder="" required>
  <small id="helpId" class="form-text text-muted">Ημερομηνία της μορφής dd-mm-yyy</small>
</div>

<hr>

<h2>Τοποθέτηση</h2>
<hr>
<div class="card">
    <div class="card-body">
        <h4>Αρχική Σελίδα</h4>
        <div class="form-check">

        @php
            $showcase_a = App\Home::where('position', 'Showcase A')->first();
            $banner1 = $showcase_a->banners()->first();
            $showcase_b = App\Home::where('position', 'Showcase B')->first();
            $banner2 = $showcase_b->banners()->first();
        @endphp

        @if($banner1)
            <label class="form-check-label text-danger" for="">                
                Showcase-1
            </label>
        @else
            <label class="form-check-label" for="showcase-1">
                <input type="checkbox" class="form-check-input" name="homepage" id="showcase-1" value="1">
                Showcase-1                  
            </label>            
        @endif

        @if($banner2)
            <label class="form-check-label text-danger" for="">                
                Showcase-2                  
            </label>            
        @else
            <label class="form-check-label" for="showcase-2">
                <input type="checkbox" class="form-check-input" name="homepage" id="showcase-2" value="2">
                Showcase-2                  
            </label>            
        @endif
        
        <label class="form-check-label" for="sidebar">
            <input type="checkbox" class="form-check-input" name="homepage" id="sidebar" value="3">
            Sidebar              
        </label>  
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <h4>Περιφέρειες</h4>
        <div class="form-check">
            @foreach($districts as $district)
                <label class="form-check-label" for="{{$district->id}}">
                    <input type="checkbox" class="form-check-input" name="district[]" id="{{$district->id}}" value="{{$district->id}}">
                    {{$district->name}}                     
                </label>   
            @endforeach
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <h4>Κατηγορίες <small class="text-danger">(πρέπει να επιλεγεί και περιφέρεια)</small></h4>
        <div class="form-check">
            @foreach($categories as $category)
                <label class="form-check-label" for="{{$category->id}}">            
                    <input type="checkbox" class="form-check-input" name="category[]" id="{{$category->id}}" value="{{$category->id}}">
                    {{$category->cat_name}}                    
                </label>
            @endforeach
        </div>
    </div>
<div>
<div class="card">
    <div class="card-body">
        <h4>Σελίδες Κατηγοριών </h4>
        <div class="form-check">
            @foreach($pages as $page)
                <label class="form-check-label" for="{{$page->id}}">
                    <input type="checkbox" class="form-check-input" name="page[]" id="{{$page->id}}" value="{{$page->id}}">
                    {{$page->title}}                     
                </label>   
            @endforeach
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <h4>Πληροφοριακές Σελίδες</h4>
        <div class="form-check">
            @foreach($infoPages as $infoPage)
                <label class="form-check-label" for="{{$page->id}}">           
                    <input type="checkbox" class="form-check-input" name="infoPage[]" id="{{$infoPage->id}}" value="{{$infoPage->id}}">
                    {{$infoPage->title}}                    
                </label>   
            @endforeach
        </div>
    </div>
</div>
<hr>
<div class="form-group">    
    <div class="col-md-2">
        {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
    </div>

    <div class="col-md-2">
        {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
    </div>

    <div class="col-md-2">
        {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
    </div>

    <div class="col-md-2">  
        <a href="/admin/banners" class="btn btn-warning pull-right">Cancel</a>
    </div>

</div>

{!!Form::close()!!}

<div class="col-md-12">
    <div id="uploading" style="display:none;" class="alert alert-info">Παρακαλούμε περιμένετε. Γίνεται βελτιστοποίηση της εικόνας.</div>
</div>

    
@endsection