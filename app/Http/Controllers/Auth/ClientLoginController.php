<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ClientLoginController extends Controller
{
    public function __construct(){
        $this->middleware('guest:client', ['except' => 'destroy']);
    }


    public function showLoginForm(){
        return view('clientarea.auth.login');
    }

    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        //Απόπειρα σύνδεσης
        if (Auth::guard('client')->attempt(request(['email', 'password']))) {
            //Αν γίνει σύνδεση....
            $clientid = Auth::guard('client')->id();
           
            return redirect("/clientarea");
        }
        //Αν δεν γίνει...
        return redirect()->back()->withInput($request->only('email'))->withErrors('Τα στοιχεία εισόδου δεν είναι σωστά. Προσπαθήστε ξανά.');

    }

    public function destroy(){
        
        Auth::guard('client')->logout();
        return redirect('/clientarea');
    }
}
