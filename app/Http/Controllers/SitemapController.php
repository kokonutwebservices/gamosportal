<?php

namespace App\Http\Controllers;

use App\Page;
use App\Entry;
use App\Church;
use App\TagPage;
use App\Category;
use App\District;
use App\InfoPage;
use App\Metropole;
use App\Municipality;

class SitemapController extends Controller
{
    public function index()
    {
        $districts = District::whereHas('entries')->orWhereHas('churches')->orWhereHas('municipalities')->get();
        // $categories = Category::all();
        $pages = Page::with('category')->get();
        $infopages = InfoPage::all(['slug', 'updated_at']);
        $entries = Entry::all();
        $metropoles = Metropole::with('churches')->get();
        $tagpages = TagPage::all();

        return response()->view('site.sitemaps.index', compact('districts', 'pages', 'infopages', 'entries', 'metropoles', 'tagpages'))->header('Content-Type', 'text/xml');;
    }
}
