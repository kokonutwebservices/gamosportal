@extends('site.layouts.master')
@section('metatags')
<title>{{$church->metatitle ? $church->metatitle : $church->name. '-'. $church->metropole->name }}</title>
    <meta name="description" content="{{$church->metadescription ? $church->metadescription : $church->name.' '. $church->metropole->name }}">
    <meta name="keywords" content="{{$church->metakeywords}}">
    <meta name="geo.region" content="GR">
    <meta name="geo.position" content="{{$church->map}}">
    <meta name="ICBM" content="{{$church->map}}">
    <meta property="og:image" content="">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="canonical" href="https://gamosportal.gr/{{$church->district->slug}}/ekklisies/{{$church->metropole->id}}/{{$church->slug}}">
@endsection

@section('content') 
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
{{--  Breadcrubs  --}}
<div class="container entry">
    <div class="row">
        <ol class="breadcrumb"> 
        <li class="breadcrumb-item"><a href="/">Αρχική</a></li>
        <li class="breadcrumb-item"><a href="/{{$district->slug}}">{{$district->name}}</a></li>
        <li class="breadcrumb-item"><a href="/{{$district->slug}}/ekklisies">Εκκλησίες</a></li>
        <li class="breadcrumb-item active">{{$church->name}}</li>
        </ol>
    </div>

    <div class="row">
        <div class="col-md-12 text-center"><h1 id="churchname"> {{$church->name}} </h1></div>
        <div class="col-md-3">
            <h2>Επικοινωνία</h2>
            <ul class="list-group">
                <li class="list-group-item">
                    <i class="fas fa-user mr-3" aria-hidden="true"></i> <span itemprop="name">{{$church->person}}</span>
                </li>
                <li class="list-group-item">
                    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <i class="fas fa-map-marker-alt mr-3" aria-hidden="true"></i> <span itemprop="streetAddress"> {{$church->address}} </span>
                    </div>
                </li>
                @if($church->phone)
                    <li class="list-group-item">
                        <a class="" href="tel:+30{{$church->phone}}"><i class="fas fa-phone mr-3" aria-hidden="true"></i> <span itemprop="telephone">{{$church->phone}}</span></a>
                    </li>       
                @endif
                @if($church->mobile)
                    <li class="list-group-item">
                        <a class="" href="tel:+30{{$church->mobile}}"><i class="fas fa-mobile-alt mr-3" aria-hidden="true"></i> <span itemprop="telephone">{{$church->mobile}}</span></a>
                    </li>       
                @endif
                @if($church->email)
                    <li class="list-group-item">
                        <a class="" href="mailto:{{$church->email}}" itemprop="email">
                            <i class="fas fa-envelope mr-3" aria-hidden="true"></i>                
                            {{$church->email}}
                        </a>
                    </li>       
                @endif
                @if($church->website)
                    <li class="list-group-item">
                        <a href="https://{{$church->website}}" target="_blank" rel="nofollow" itemprop="url">
                            <i class="fas fa-globe mr-3" aria-hidden="true"></i>
                            {{$church->website}}
                        </a>
                    </li>       
                @endif
                
            </ul>
        </div>
        <div class="col-md-6">
            @if($church->map)
        <style>
        #map {
        width: 100%;
        height: 400px;
        background-color: grey;
        }
    </style>
    <div id="map"></div>
    <script>
        function initMap() {
            var uluru = {lat: {{Maps::lat($church->map)}}, lng: {{Maps::lon($church->map)}}};
            var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: uluru
            });
            var marker = new google.maps.Marker({
            position: uluru,
            map: map
            });
        }
        </script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATXsZY-e5x34l-ilC2sLA5Ftzy3xZPuUc&callback=initMap">
        </script>
    @endif
        </div>
        
    </div>
    <div class="row">
    </div>
</div>
    
@endsection