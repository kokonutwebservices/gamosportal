<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Church extends Model
{
    use Sortable;
    protected $guarded = ['_token'];

    public $sortableChurch = ['id', 'name', 'district_id', 'metropole_id'];

    public function district(){
        return $this->belongsTo(District::class);
    }

    public function metropole() {
        return $this->belongsTo(Metropole::class);
    }

    public function banners(){
        return $this->belongsToMany(Banner::class);
    }

    public static function validateChurch ($request) {

        return $request->validate([
            'district_id' => 'required',
            'metropole_id' => 'required',
            'name' => 'required|min:5',
            'slug' => 'required',
        ]);
    }

    public function redirectAfterSave($request) {

        if($request->submitbutton === "saveclose") {
            return redirect ()->route('admin.churches.index');
        } elseif ($request->submitbutton === "savestay") {
            return redirect ()->route('admin.churches.edit', $this->id);
        } elseif ($request->submitbutton === "savenew") {
            return redirect ()->route('admin.churches.create');
        };
    }
}
