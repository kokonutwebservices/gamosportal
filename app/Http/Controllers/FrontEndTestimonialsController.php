<?php

namespace App\Http\Controllers;

use App\Testimonial;
use Illuminate\Http\Request;

class FrontEndTestimonialsController extends Controller
{
    public function show() {
        $testimonials = Testimonial::all()->sortBy('order');

        return view('site.testimonials.show', compact('testimonials'));
    }
}
