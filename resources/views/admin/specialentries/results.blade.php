@extends('admin.layouts.master')

@section('content')

    <h1>Special Entries</h1>
    {{--  Πλαίσιο σφαλμάτων  --}}

    <div class="col-md-12">
        <div class="form-group col-md-4 pull-right">
        {!!Form::open(['method' => 'POST', 'action' => 'SpecialEntriesController@search'])!!}
            <div class="input-group">
            <input type="text" class="form-control" name="query" placeholder="Αναζήτηση" aria-label="Αναζήτηση">
            <span class="input-group-btn">
                <button class="btn btn-secondary" type="submit">Go!</button>
            </span>
            </div>
        {!!Form::close()!!}
        </div>
    </div>
    <table class="table">
    <thead class="thead-inverse">
        <tr>
        <th>@sortablelink('id')</th>
        <th>@sortablelink('name', 'Όνομα Καταχώρησης')</th>        
        <th>@sortablelink('client_id', 'Πελάτης') </th>
        <th>@sortablelink('end_at', 'Ημ. Λήξης')</th>
        </tr>
    </thead>
        @foreach($results as $specialentry)
            <tr>
                <td>{{$specialentry->id}}</td>
                <td><a href="/admin/specialentries/{{$specialentry->id}}/edit">{{$specialentry->name}}</a></td>
                <td>{{$specialentry->client->name}}</td>
                <td>{{$specialentry->end_at}}</td>
            </tr>
        @endforeach
    </table>

    {{--  {{ $results->links() }}  --}}

{{--  <div class="col-md-12">
    <a href="entries/create" class="btn btn-primary">Δημιουργία Καταχώρησης</a>
</div>  --}}


@endsection