<?php

namespace App\Http\Controllers;

use App\Tag;
use App\TagPage;
use Illuminate\Http\Request;

class AdminTagPagesController extends Controller
{
       
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tagpages = TagPage::all()->sortBy('order');
        return view('admin.tagpages.index', compact('tagpages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();

        return view('admin.tagpages.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
       $this->validate($request, [
            'tag_id' => 'required',
            'title' => 'required',
            'slug' => 'required',
            'metatitle' => 'required',
            'metadescription' => 'required'
        ]);

        $page = new TagPage;
        $page->tag_id = request('tag_id');
        $page->title = request('title');
        $page->slug = request('slug');
        $page->metatitle = request('metatitle');
        $page->metadescription = request('metadescription');
        $page->metakeywords = request('metakeywords');
        $page->body = request('body');
        $page->title2 = request('title2');
        $page->order = $page->order();
        $page->save();
        
        $id = TagPage::all()->last()->id;

        if ($request->submitbutton == 'save-close') {

            return redirect()->route('admin.tagpages.index');

        } elseif ($request->submitbutton == 'save-stay') {

            return redirect()->route('admin.tagpages.edit', $id);

        } elseif ($request->submitbutton == 'save-new') {

            return redirect()->route('admin.tagpages.create');

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TagPage  $tagPage
     * @return \Illuminate\Http\Response
     */
    public function edit(TagPage $tagpage)
    {
        $tags = Tag::all();
        return view('admin.tagpages.edit', compact('tagpage', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TagPage  $tagPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TagPage $tagpage)
    {
        $this->validate($request, [
            'tag_id' => 'required',
            'title' => 'required',
            'slug' => 'required',
            'metatitle' => 'required',
            'metadescription' => 'required'
        ]);

        $tagpage->tag_id = request('tag_id');
        $tagpage->title = request('title');
        $tagpage->slug = request('slug');
        $tagpage->metatitle = request('metatitle');
        $tagpage->metadescription = request('metadescription');
        $tagpage->metakeywords = request('metakeywords');
        $tagpage->body = request('body');
        $tagpage->title2 = request('title2');
        $tagpage->save();

        if ($request->submitbutton == 'save-close') {

            return redirect()->route('admin.tagpages.index');

        } elseif ($request->submitbutton == 'save-stay') {

            return redirect()->route('admin.tagpages.edit', $tagpage);

        } elseif ($request->submitbutton == 'save-new') {

            return redirect()->route('admin.tagpages.create');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TagPage  $tagPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(TagPage $tagpage)
    {
        $tagpage->delete();
        
        return redirect()->route('admin.tagpages.index');
    }

    public function order(){

        $orders = explode(',', $_POST['order']);

        foreach ($orders as $neworder=>$value) {
            
            $order = $neworder+1;
            $page = TagPage::find($value);
            $page->order = $order;
            $page->save();
        }

    }
}
