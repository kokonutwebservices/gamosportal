<div class="contact">
    <div id="message-title" class="d-block">
    <h2 class="text-center">Στείλτε μας μήνυμα</h2>
    <p class="text-center">για περισσότερες πληροφορίες ή προσφορά</p>
    <i class="fas fa-chevron-down fa-2x d-block mx-auto" aria-hidden="true"></i>
    </div>
    {{--  Πλαίσιο σφαλμάτων  --}}
    @include('admin.layouts.errors') 
    @if (session('message'))
        <div class="alert alert-success" id="">
            {{ session('message') }}
        </div>
    @endif
    <div id="message-body">
        {!!Form::open(['action' => ['FrontEndController@sendMailSpecial', $specialentry], 'method'=>'POST'])!!}
    
        <div class="form-group">
            {{Form::label('name', 'Όνομα')}}
            {{Form::text('name', '', ['class'=>'form-control', 'placeholder' => 'Το όνομά σας', 'required'])}}      
        </div>
        <div class="form-group">
            {{Form::label('phone', 'Τηλέφωνο')}}
            {{Form::text('phone', '', ['class'=>'form-control', 'placeholder' => 'Το τηλέφωνό σας', 'required'])}}      
        </div>
        <div class="form-group">
            {{Form::label('email', 'Email')}}
            {{Form::text('email', '', ['class'=>'form-control', 'placeholder' => 'Το email σας', 'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('message', 'Μήνυμα')}}
            {{Form::textarea('message', '', ['class'=>'form-control', 'placeholder' => 'Το μήνυμά σας', 'required'] )}}  
        </div>
        <div class="form-group">
            <a href="/terms.php" target="_blank">Διάβασα και αποδέχομαι τους όρους</a>
            {{ Form::checkbox('agree', 1, null, ['class' => 'ml-5']) }}  
        </div>
        {{Form::hidden('entryemail', $specialentry->email)}}
        <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;">
        </div>
        <div class="form-group col-xs-12">
            {{Form::submit('Αποστολή', ['class'=>'btn btn-primary pull-left', 'id' => 'send'])}}
        </div>
        {!!Form::close()!!}
    </div>
    </div>
    {{--  Πλαίσιο σφαλμάτων  --}}
    @include('admin.layouts.errors') 
    @if (session('message'))
        <div class="alert alert-success" id="">
            {{ session('message') }}
        </div>
    @endif