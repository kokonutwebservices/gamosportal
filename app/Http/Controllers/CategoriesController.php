<?php

namespace App\Http\Controllers;

use App\Category;
use App\District;
use Illuminate\Http\Request;
use Storage;

class CategoriesController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $categories)
    {   
        
        $categories = $categories->all();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parents = Category::where('cat_parent_id', '0')->get();
        $districts = District::all();

        return view('admin.categories.create', compact('parents', 'districts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $districts = District::all();

        $this->validate(request(), [
            'cat_name' => 'required',
            'cat_slug' => 'required',
            'cat_img' => 'required|image:jpg,png|max:500'
        ]);

        $category = new Category;
        
        $category->cat_name = $request->input('cat_name');
        $category->cat_slug = $request->input('cat_slug');
        $category->cat_description = $request->input('cat_description');
        $category->cat_parent_id = $request->input('cat_parent_id');

        //Category Order
        if($request->input('cat_order')){
            $newOrder = $request->input('cat_order');
            $parentId = $category->cat_parent_id;
            $orders = Category::where('cat_order', '>=', $newOrder)->where('cat_parent_id', $parentId)->get();
            foreach ($orders as $order){
                $order->cat_order = $order->cat_order + 1 ;
                $order->save();
            }
            $category->cat_order = $request->input('cat_order');
        } else {
            $request->cat_order = 100;
        }

        //Category Image        
        $file = $request->file('cat_img'); //Get the file
        $ext = $file->getClientOriginalExtension(); //Get the extension
        $fileNameBase = $request->input('cat_slug');  //Get the slug to use as filename base        
        $filename = $fileNameBase.'.'.$ext; //create a new filename
        request()->file('cat_img')->storeAs('categories', $filename); //Store file
        
        $category->cat_img = $filename;
        
        foreach ($districts as $district){
            $i = $district->id;
            $title = "meta_title_$i";            
            $description = "meta_description_$i";
            $keywords = "meta_keywords_$i";

            $category->$title = $request->input($title);
            $category->$description = $request->input($description);
            $category->$keywords = $request->input($keywords);
        }
        
        $category->save();

        $id = Category::all()->last()->id;        
                
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.categories.edit', $id);
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.categories.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.categories.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $parents = Category::where('cat_parent_id', '0')->get();
        $districts = District::all();

        return view('admin.categories.edit', compact('category', 'parents', 'districts')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $districts = District::all();

        //Validation
        $this->validate(request(),[
            'cat_name' => 'required',
            'cat_slug' => 'required'
        ]);

        //Category Image
        if($request->hasFile('cat_img')){
            //Αν υπάρχει εικόνα, το όνομα αρχείου παραμένει ίδιο
            $filename = $category->cat_img;
            request()->file('cat_img')->storeAs('categories', $filename); 
        } else {
            $filename = $category->cat_img;
        }
        
        $category->cat_name = $request->input('cat_name');
        $category->cat_slug = $request->input('cat_slug');
        $category->cat_description = $request->input('cat_description');
        
        
        //Υπολογισμός νέας σειράς cat_order
        //Αν το parent_id μείνει ίδιο
        if($request->input('cat_parent_id') == $category->cat_parent_id){
            $category->cat_parent_id = $request->input('cat_parent_id');
            
            //Περ. 1 => Το Order μένει ίδιο
            if ($request->input('cat_order') == $category->cat_order){
                $category->cat_order = $request->input('cat_order');

            //Περ.2 => Το νέο order είναι μεγαλύτερο από το υπάρχον
            } elseif ($request->input('cat_order') > $category->cat_order){
                $newOrder = $request->input('cat_order');
                $orders = Category::where('cat_order', '>', $category->cat_order)->where('cat_order', '<=', $newOrder)->get();
                foreach ($orders as $order){
                    $order->cat_order = $order->cat_order-1;
                    $order->save();
                }
                $category->cat_order = $newOrder;
            //Περ.3 => Το νέο order είναι μικρότερο από το υπάρχον    
            } elseif($request->input('cat_order') < $category->cat_order){
                $newOrder = $request->input('cat_order');
                $orders = Category::where('cat_order', '<', $category->cat_order)->where('cat_order', '>=', $newOrder)->get();
                foreach ($orders as $order){
                    $order->cat_order = $order->cat_order+1;
                    $order->save();
                }
                $category->cat_order = $newOrder;
            }
        //Αν το parent_id αλλάξει, η νέα σειρά είναι τελευταία
        } else {
            $orders = Category::where('cat_order', '>', $category->cat_order)->get();
            foreach ($orders as $order){
                $order->cat_order = $order->cat_order-1;
                $order->save();
            }
            $category->cat_parent_id = $request->input('cat_parent_id');
            $category->cat_order = 100;
        }
        
        foreach ($districts as $district){
            $i = $district->id;
            $title = "meta_title_$i";            
            $description = "meta_description_$i";
            $keywords = "meta_keywords_$i";

            $category->$title = $request->input($title);
            $category->$description = $request->input($description);
            $category->$keywords = $request->input($keywords);
        }

        $category->save();

        if($request->submitbutton == 'save'){
            return redirect()->route('admin.categories.edit', $category->id);
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.categories.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.categories.create');
        }
               
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        

        $filename = $category->cat_img;
       
        $file = "categories/$filename";
        Storage::delete($file);
        $catOrder = $category->cat_order;
        $parentId = $category->cat_parent_id;
        $orders = Category::where('cat_order', '>', $catOrder)->where('cat_parent_id', '=', $parentId)->get();
        foreach ($orders as $order){
            $order->cat_order = $order->cat_order - 1 ;
            $order->save();
        }

        $category->delete();

        return redirect('admin/categories');
    }

    //Function που παίρνει την νέα σειρά από το Ajax call του sortable, αναλύει το id κάθε item, και γράφει στην db το νέο cat_order 
    public function order(){
        
        $orders = explode(',',$_POST['order']);
        
        foreach ($orders as $neworder=>$value){
            $parts = explode('-',$value);
            $parentid = $parts[0];
			$catid = $parts[1];			
            $order = $neworder+1;
            $category = Category::find($catid);
            $category->cat_order = $order;
            $category->save();
        }
    }
}
