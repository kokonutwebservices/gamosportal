/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./jquery-ui');
// require('./magnific-popup');
require('./popper');
require('./fontawesome-all.min');
require('./offcanvas');
require('./slick.min');


// window.Vue = require('vue');
// Vue.config.productionTip = false;


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     // el: '#app'
// });

/******Admin scripts ******/

//Create & Edit Pages reset fields
$('.reset').click(function() {
    $(this).closest('.slide').find("#image, #caption_title, #caption_description, #link").val("");
});


/*** Password Generator ***/
// Generate a password string
function randString(id) {
    var dataSet = $(id).attr('data-character-set').split(',');
    var possible = '';
    if ($.inArray('a-z', dataSet) >= 0) {
        possible += 'abcdefghijklmnopqrstuvwxyz';
    }
    if ($.inArray('A-Z', dataSet) >= 0) {
        possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    if ($.inArray('0-9', dataSet) >= 0) {
        possible += '0123456789';
    }
    if ($.inArray('#', dataSet) >= 0) {
        possible += '![]{}()%&*$#^<>~@|';
    }
    var text = '';
    for (var i = 0; i < $(id).attr('data-size'); i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

// Create a new password
$(".getNewPass").click(function() {
    var field = $(this).closest('div').find('input[rel="gp"]');
    field.val(randString(field));
});

// Auto Select Pass On Focus
$('input[rel="gp"]').on("click", function() {
    $(this).select();
});

/***Renew button ***/
if ($('input[id=start_at]').length) {
    var start = $('input[id=start_at]').val();

    var year = start.slice(0, 4);

    var new_year = +year + 1;

    var last_part = start.slice(4);

    var new_date = new_year + last_part;

    var new_end = new_year + 1;

    var new_expire = new_end + last_part;

    $('#renew').on('click', function() {
        $(this).closest('#dates').find('#start_at').val(new_date);
        $(this).closest('#dates').find('#end_at').val(new_expire);
    });
}

$(document).ready(function() {
    var _token = $('input[name="_token"]').val();

    /*Σειρά Κατηγοριών*/
    $('ul #sortable-cat').sortable({
        axis: 'y',
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('categoryorder', { order: newOrder, _token: _token });
        }
    });

    /*Σειρά Σελίδων*/
    $('#pageSortable').sortable({
        axis: 'y',
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('pageorder', { order: newOrder, _token: _token });
        }
    });

    /*Σειρά Info Pages*/
    $('#infoPageSortable').sortable({
        axis: 'y',
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('infopagesorder', { order: newOrder, _token: _token });
        }
    });

    // Σειρά Tag Pages
    $('#sortableTag').sortable({
        axis: 'y',
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('tagpagesorder', { order: newOrder, _token: _token });
        }
    })

    /*Σειρά Album*/
    $('#albumSortable').sortable({
        axis: 'y',
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('albumorder', { order: newOrder, _token: _token });
        }
    });
    /*Σειρα Album ClientArea*/
    $('#clientsAlbumSortable').sortable({
        axis: 'y',
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('albums/albumorder', { order: newOrder, _token: _token });
        }
    });

    // Σειρά Εικόνων
    $('#imagesSortable').sortable({

        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('imageorder', { order: newOrder, _token: _token });
        }
    });

    // Σειρά Εικόνων Special entries
    $('#specialImagesSortable').sortable({

        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('imageorder', { order: newOrder, _token: _token });
        }
    });

    // Σειρά Εικόνων -> Clientarea
    $('#clientsImagesSortable').sortable({

        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('imageorder', { order: newOrder, _token: _token });
        }
    });

    // Image Description - Clientarea
    $('.desc-button').on('click', function() {
        $(this).siblings('.image-description').slideToggle();
    })

    //Image Change Album - Clientarea
    $('.change-album').on('click', function() {
        $(this).siblings('.albumslist').slideToggle();
    })

    // Σειρά Video
    $('#videolist').sortable({
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('videoorder', { order: newOrder, _token: _token });
        }
    });

    // Σειρά News
    $('#newsSortable').sortable({
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('news/newsorder', { order: newOrder, _token: _token });
        }
    });

    // Σειρά Testimonials
    $('#testimonials').sortable({
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toLocaleString();
            $.post('testimonials/testimonialsorder', { order: newOrder, _token: _token });
        }
    });



    // Σειρά Video ->clientArea
    $('#clientsVideoList').sortable({
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.post('video/videoorder', { order: newOrder, _token: _token });
        }
    });

    //Μετά την επιλογή καταχώρησης, στα banner, βάζει το url στο σχετικό πεδίο
    $('#entry').change(function() {
        var entry = $(this).val();

        $.ajax({
            type: "POST",
            url: "/admin/banners/entry",
            data: { entry: entry, _token: _token },
            success: function(result) {
                $('#url').val(result);
            }
        });
    });


    //Μετά την επιλογή καταχώρησης, στα slider, βάζει το url στο σχετικό πεδίο
    $('#sliderentry').change(function() {
        var sliderentry = $(this).val();

        $.ajax({
            type: "POST",
            url: "/admin/sliders/sliderentry",
            data: { entry: sliderentry, _token: _token },
            success: function(result) {
                $('#sliderurl').val(result);
            }
        });
    });
});


$("button[type=submit]").on('click', function() {
    $("#uploading").slideDown();
});

// Διαγραφή success message μετά από 3sec
setTimeout(function() {
    $('#successMessage').fadeOut('fast');
}, 6000); // <-- time in milliseconds

$('h5').on('click', function() {
    $(this).next('.album-description').slideToggle();
});

//Script διαγραφής για φόρμα με id=delete
$("#delete").on("submit", function() {
    return confirm("Θέλετε σίγουρα να προχωρήσετε σε διαγραφή;");
});
$("#deleteentry").on("submit", function() {
    return confirm("Θέλετε σίγουρα να προχωρήσετε σε διαγραφή; Αν υπάρχουν banner θα διαγραφούν και αυτά.");
});

$('.date').datepicker({

    dateFormat: 'dd-mm-yy'

});


/******FrontEnd scripts ******/

// Mobile menu
$('#districts-menu').on('click', function() {

    $('#districts-submenu').slideToggle('slow');

})

$('#prof-menu').on('click', function() {

    $('#prof-submenu').slideToggle('slow');

})


// Script που αντικαθιστά την εικόνα αφού φορτώσει η σελίδα


$('ul.nav li.dropdown').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});

// PHOTOSWIPE GALLERY
require('./photoswipeGallery');

// END OF PHOTOSWIPE GALLERY


// //Feeling lucky script
// $('.amaksi .lucky-img').on('click', function(){
//   $.ajax({
//     type: "GET",
//     url: "/lucky",
//     data: {},
//     success: function(result){
//       $('.amaksi').html(result);
//     }
//   });
// });

// $('.amaksi .btn-lucky').on('click', function(){
//   $.ajax({
//     type: "GET",
//     url: "/lucky",
//     data: {},
//     success: function(result){
//       $('.amaksi').html(result);
//     }
//   });
// });


$('#message-title').on('click', function() {
    $('#message-body').slideToggle();
});

// $('.parent-container').magnificPopup({
//   delegate: 'a', // child items selector, by clicking on it popup will open
//   type: 'image',
//   gallery:{enabled:true},
//   titleSrc: function(){
//     return data('title');
//   },
//   // other options
//   image: {
//     verticalFit: true,
//     titleSrc: function(item) {

//       var caption = item.el.attr('title');

//       var pinItURL = "https://pinterest.com/pin/create/button/";

//       // Refer to https://developers.pinterest.com/pin_it/
//       // Το data-entrylink στέλνει το pin στην καταχώρηση
//       pinItURL += '?url=' + 'https://www.gamosportal.gr' + item.el.attr('data-entrylink');
//       pinItURL += '&media='+ 'https://www.gamosportal.gr' + item.el.attr('href');
//       pinItURL += '&description=' + caption;


//       return caption + '<a data-pin-do="buttonPin" data-pin-round="true" class="pin-it pull-right mr-5" href="'+pinItURL+'" target="_blank"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_round_red_16.png" /></a>';
//     }
//   }
// });



//Mobile entry contact
$('#address').on('click', function() {
    $('#showaddress').slideToggle();
})
$('#phone').on('click', function() {
    $('#showphone').slideToggle();
})
$('#mobile').on('click', function() {
    $('#showmobile').slideToggle();
})
$('#website').on('click', function() {
    $('#showwebsite').slideToggle();
})
$('#email').on('click', function() {
    $('#showemail').slideToggle();
})
$('#social').on('click', function() {
    $('#socialmedia').slideToggle();
})

//Σελίδα Add Your Business - count effect
$('.counter').each(function() {
    var $this = $(this),
        countTo = $this.attr('data-count');

    $({ countNum: $this.text() }).animate({
        countNum: countTo
    }, {
        duration: 4000,
        easing: 'linear',
        step: function() {
            $this.text(Math.floor(this.countNum));
        },
        complete: function() {
            $this.text(this.countNum);
            //alert('finished');
        }
    });
});

$('.call').on('click', function() {
    var entry = $('h1').first().text();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/phonecall",
        type: "POST",
        data: { entry },

    });
});

$('.email').on('click', function() {
    var entry = $('h1').first().text();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/emailsent",
        type: "POST",
        data: { entry },

    });
});


// Pages Slider
//Initialize your slider in your script file
$("#carousel-slider").slick({
    arrows: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    mobileFirst: true
});