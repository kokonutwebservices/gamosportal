<div class="logo d-flex align-items-center thumbnail">
    <img class="m-auto d-block" src="/storage/logos/special/{{$specialentry->logo}}" alt="Λογότυπο {{$specialentry->name}}" title="Λογότυπο {{$specialentry->name}}">
</div>
<h2 class="d-none d-md-block">Επικοινωνία</h2>
<div itemscope itemtype="http://schema.org/Person">
    <ul class="list-group">
        @if($specialentry->person)
        <li class="list-group-item">
            <i class="fas fa-user mr-3" aria-hidden="true"></i> <span itemprop="name">{{$specialentry->person}}</span>
        </li>
        @endif
        <li class="list-group-item">
            <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                {{-- <a href="https://www.google.com/maps/@?api=1&map_action=map&center={{$specialentry->map}}&zoom=17" target="_blank" rel="nofollow"><i class="fas fa-map-marker-alt mr-3" aria-hidden="true"></i> <span itemprop="streetAddress"> {{$specialentry->address}} </span></a> --}}
                <a href="https://www.google.com/maps/search/?api=1&query={{$specialentry->map}}" target="_blank" rel="nofollow"><i class="fas fa-map-marker-alt mr-3" aria-hidden="true"></i> <span itemprop="streetAddress"> {{$specialentry->address}} </span></a>
            </div>
        </li>
        @if($specialentry->phone)
            <li class="list-group-item">
                <a class="call" href="tel:+30{{$specialentry->phone}}"><i class="fas fa-phone mr-3" aria-hidden="true"></i> <span itemprop="telephone">{{$specialentry->phone}}</span></a>
            </li>       
        @endif
        @if($specialentry->mobile)
            <li class="list-group-item">
                <a class="call" href="tel:+30{{$specialentry->mobile}}"><i class="fas fa-mobile-alt mr-3" aria-hidden="true"></i> <span itemprop="telephone">{{$specialentry->mobile}}</span></a>
            </li>       
        @endif
        @if($specialentry->email)
            <li class="list-group-item">
                <a class="email" href="mailto:{{$specialentry->email}}?subject=Είδα την καταχώρησή σας στο Gamos Portal" itemprop="email">
                    <i class="fas fa-envelope mr-3" aria-hidden="true"></i>                
                    {{$specialentry->email}}
                </a>
            </li>       
        @endif
        @if($specialentry->website)
            <li class="list-group-item">
                <a href="http://{{$specialentry->website}}?utm_source=gamosportal&utm_medium=referral" target="_blank" rel="nofollow" itemprop="url">
                    <i class="fas fa-globe mr-3" aria-hidden="true"></i>
                    {{$specialentry->website}}
                </a>
            </li>       
        @endif
        <li class="list-group-item">
            @if($specialentry->facebook)
                <a href="http://{{$specialentry->facebook}}" rel="nofollow" target="_blank"><i class="fab fa-facebook fa-2x mr-3" aria-hidden="true"></i></a>    
            @endif
            @if($specialentry->twitter)
                <a href="http://{{$specialentry->twitter}}" rel="nofollow" target="_blank"><i class="fab fa-twitter fa-2x mr-3" aria-hidden="true"></i></a>    
            @endif
            @if($specialentry->google)
                <a href="http://{{$specialentry->google}}" rel="nofollow" target="_blank"><i class="fab fa-google-plus fa-2x mr-3" aria-hidden="true"></i></a>    
            @endif
            @if($specialentry->instagram)
                <a href="http://{{$specialentry->instagram}}" rel="nofollow" target="_blank"><i class="fab fa-instagram fa-2x mr-3" aria-hidden="true"></i></a>    
            @endif
            @if($specialentry->pinterest)
                <a href="http://{{$specialentry->pinterest}}" rel="nofollow" target="_blank"><i class="fab fa-pinterest-p fa-2x mr-3" aria-hidden="true"></i></a>    
            @endif
            @if($specialentry->youtube)
                <a href="http://{{$specialentry->youtube}}" rel="nofollow" target="_blank"><i class="fab fa-youtube fa-2x mr-3" aria-hidden="true"></i></a>    
            @endif 
            @if($specialentry->vimeo)
            <a href="http://{{$specialentry->vimeo}}" rel="nofollow" target="_blank"><i class="fab fa-vimeo-v fa-2x mr-3" aria-hidden="true"></i></a>    
        @endif            
        </li>
    </ul>
    @if($specialentry->map)
        <style>
        #map {
        width: 100%;
        height: 400px;
        background-color: grey;
        }
    </style>
    <div id="map"></div>
    <script>
      function initMap() {
        var uluru = {lat: {{Maps::lat($specialentry->map)}}, lng: {{Maps::lon($specialentry->map)}}};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3g_-cFADG2BIavJk_6hfhBRZM3YymKV0&callback=initMap">
    </script>
    @endif
</div>