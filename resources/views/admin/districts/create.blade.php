@extends('admin.layouts.master')

@section('content')

<h1>Δημιουργία Περιφέρειας</h1>

<div class="">

<form method="post" action="{{route('districts.store')}}">
    @csrf

    <div class="form-group">
        <label for="name">Όνομα Περιφέρειας</label>
        <input id="name" class="form-control" type="text" name="name" value="{{old('name')}}">
        @error('name')
            <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="slug">slug</label>
        <input id="slug" class="form-control" type="text" name="slug" value="{{old('slug')}}">
        @error('slug')
            <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="metatitle">Meta Title Περιφέρειας</label>
        <input id="metatitle" class="form-control" type="text" name="metatitle" value="{{old('metatitle')}}">
        @error('metatitle')
            <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="metadescription">Meta Description Περιφέρειας</label>
        <input id="metadescription" class="form-control" type="text" name="metadescription" value="{{old('metadescription')}}">
        @error('metadescription')
            <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="metakeywords">Meta Keywords Περιφέρειας</label>
        <input id="metakeywords" class="form-control" type="text" name="metakeywords" value="{{old('metakeywords')}}">
        @error('metakeywords')
            <span class="text-danger">{{$message}}</span>
        @enderror
    </div>

    <div class="d-flex justify-content-around">
        <button class="btn btn-primary" type="submit" name="submitbutton" value="save-close">Save & Close</button>
        <button class="btn btn-success" type="submit" name="submitbutton" value="save">Save</button>
        <button class="btn btn-btn-secondary" type="submit" name="submitbutton" value="save-new">Save & New</button>
        <a class="btn btn-warning" href="{{route('districts.index')}}">Cancel</a>
    </div>
    
</form>

</div>


@endsection