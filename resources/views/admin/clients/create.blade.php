@extends('admin.layouts.master')

@section('content')

<h1>Δημιουργία Νέου Πελάτη</h1>
<div class="col-md-9">
    {{Form::open(['action' => 'ClientsController@store', 'method' => 'POST'])}}

    <div class="form-group">
        {{Form::label('name', 'Εμπορική Επωνυμία')}}
        {{Form::text('name', '', ['class'=>'form-control', 'placeholder' => 'Το όνομα του πελάτη, π.χ. Ζέρβας Χρυσοβαλάντης ή Ζέρβας Α.Ε.'])}}
    </div>

    <div class="form-group">
        {{Form::label('representative', 'Πωλητής')}}
        <select name="representative" id="" class="form-control">
            <option selected>Επιλέξτε πωλητή</option>
            @foreach ($representatives as $representative)
                <option value="{{$representative->id}}">{{$representative->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        {{Form::label('email', 'Email')}}
        {{Form::text('email', '', ['class'=>'form-control', 'placeholder' => 'email'])}}
    </div>

    <div class="form-group">
        {{Form::label('password', 'Password')}}
        {{Form::text('password', '', ['class'=>'form-control', 'placeholder' => 'password', 'rel' => 'gp', 'data-size' => '8', 'data-character-set' => 'a-z,0-9'])}} <button type="button" class="btn btn-default btn-lg getNewPass"><span class="fas fa-sync-alt"></span></button>
    </div>

    <div class="form-group col-md-12">    
        <div class="col-md-2">
            {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
        </div>

        <div class="col-md-2">
            {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
        </div>

        <div class="col-md-2">
            {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
        </div>

        <div class="col-md-2">  
            <a href="/admin/clients" class="btn btn-warning pull-right">Cancel</a>
        </div>

    </div>

{{Form::close()}}
@endsection