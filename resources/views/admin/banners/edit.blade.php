@extends('admin.layouts.master')

@section('content')

<h1>Επεξεργασία banner</h1>
{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif
<hr>

{!!Form::open(['method'=>'POST', 'action'=>['BannersController@update', $banner->id], 'enctype'=>'multipart/form-data'])!!}
<div class="form-group">
  <label for="entry">Καταχώρηση: </label>
  <select class="form-control" name="entry_id" id="entry">
    <option selected>Επιλέξτε Καταχώρηση</option>
    @foreach($entries as $entry)
        @if($banner->entry_id == $entry->id)
            <option value="{{$entry->id}}" selected>{{$entry->name}}</option>  
        @else
            <option value="{{$entry->id}}">{{$entry->name}}</option>  
        @endif              
    @endforeach
    <option value="0">Καμία</option>
  </select>
</div>

<div class="form-group">
  <label for="type"></label>
  <select class="form-control" name="type" id="type">
    <option>Τύπος banner</option>
    @if($banner->type == 'square')
        <option value="square" selected>Τετράγωνο</option>
        <option value="vertical">Κάθετο</option>
        <option value="horizontal">Οριζόντιο</option>
    @elseif($banner->type == 'vertical')
        <option value="square">Τετράγωνο</option>
        <option value="vertical" selected>Κάθετο</option>
        <option value="horizontal">Οριζόντιο</option>
    @elseif($banner->type == 'horizontal')
        <option value="square">Τετράγωνο</option>
        <option value="vertical">Κάθετο</option>
        <option value="horizontal" selected>Οριζόντιο</option>        
    @endif
  </select>
</div>

<div class="form-group">
  <label for="banner">Επιλέξτε αρχείο banner</label>
  <input type="file" class="form-control-file" name="banner" id="banner" placeholder="" aria-describedby="fileHelpId">
  <small id="fileHelpId" class="form-text text-muted">Το αρχείο θα πρέπει να είναι .jpg, .png ή .gif</small>
  <img src="/storage/banners/{{$banner->banner}}" style="width:200px;">
</div>

<div class="form-group">
  <label for="url"></label>
  <input type="text" class="form-control" name="url" id="url" aria-describedby="helpId" placeholder="" value="{{$banner->url}}">
  <small id="helpId" class="form-text text-muted">Προσθέστε το link</small>
</div>

<div class="form-group">
  <label for="expires">Ημερομηνία Λήξης</label>
  <input type="date" class="form-control date" name="expires" id="expires" aria-describedby="helpId" placeholder="" value="{{$banner->expires}}">
  <small id="helpId" class="form-text text-muted">Ημερομηνία της μορφής dd-mm-yyy</small>
</div>

<hr>

<h2>Τοποθέτηση</h2>
<hr>
<div class="card">
    <div class="card-body">
        <h4>Αρχική Σελίδα</h4>
        <div class="form-check">
            @foreach($homebanners as $homebanner)
                <label class="form-check-label" for="{{$homebanner->id}}">
                    @if($homebanner->banners()->where('banner_id', $banner->id)->count() > 0)
                        <input type="checkbox" class="form-check-input" name="homepage" id="{{$homebanner->id}}" value="{{$homebanner->id}}" checked>
                    {{$homebanner->position}}
                    @else
                    <input type="checkbox" class="form-check-input" name="homepage" id="{{$homebanner->id}}" value="{{$homebanner->id}}">
                    {{$homebanner->position}}
                    @endif            
                </label>   
            @endforeach
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-check">
            @foreach($districts as $district)
                <label class="form-check-label" for="{{$district->id}}">
                    @if($district->banners()->where('banner_id', $banner->id)->count() > 0)
                        <input type="checkbox" class="form-check-input" name="district[]" id="{{$district->id}}" value="{{$district->id}}" checked>
                    {{$district->name}}
                    @else
                    <input type="checkbox" class="form-check-input" name="district[]" id="{{$district->id}}" value="{{$district->id}}">
                    {{$district->name}}
                    @endif            
                </label>   
            @endforeach
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-check">
            @foreach($categories as $category)
                <label class="form-check-label" for="{{$category->id}}">
                    @if($category->banners()->where('banner_id', $banner->id)->count() > 0)
                        <input type="checkbox" class="form-check-input" name="category[]" id="{{$category->id}}" value="{{$category->id}}" checked>
                    {{$category->cat_name}}
                    @else
                    <input type="checkbox" class="form-check-input" name="category[]" id="{{$category->id}}" value="{{$category->id}}">
                    {{$category->cat_name}}
                    @endif            
                </label>
            @endforeach
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-check">
            @foreach($pages as $page)
                <label class="form-check-label" for="{{$page->id}}">
                    @if($page->banners()->where('banner_id', $banner->id)->count() > 0)
                        <input type="checkbox" class="form-check-input" name="page[]" id="{{$page->id}}" value="{{$page->id}}" checked>
                    {{$page->title}}
                    @else
                    <input type="checkbox" class="form-check-input" name="page[]" id="{{$page->id}}" value="{{$page->id}}">
                    {{$page->title}}
                    @endif            
                </label>   
            @endforeach
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-check">
            @foreach($infoPages as $infoPage)
                <label class="form-check-label" for="{{$infoPage->id}}">
                    @if($infoPage->banners()->where('banner_id', $banner->id)->count() > 0)
                        <input type="checkbox" class="form-check-input" name="infoPage[]" id="{{$infoPage->id}}" value="{{$infoPage->id}}" checked>
                    {{$infoPage->title}}
                    @else
                    <input type="checkbox" class="form-check-input" name="infoPage[]" id="{{$infoPage->id}}" value="{{$infoPage->id}}">
                    {{$infoPage->title}}
                    @endif            
                </label>   
            @endforeach
        </div>
    </div>
</div>

<hr>
<div class="form-group">    
    <div class="col-md-2">
        {{Form::hidden('_method', 'PUT')}}
        {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
    </div>

    <div class="col-md-2">
        {{Form::hidden('_method', 'PUT')}}
        {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
    </div>

    <div class="col-md-2">
        {{Form::hidden('_method', 'PUT')}}
        {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
    </div>

    <div class="col-md-2">  
        <a href="/admin/banners" class="btn btn-warning pull-right">Cancel</a>
    </div>

    <div class="col-md-2">
    {!! Form::close() !!}

        {!! Form::open(['action' => ['BannersController@destroy', $banner->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'pull-right', 'id' =>'delete' ]) !!}

        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}

    {!! Form::close() !!}  
    </div>
</div>

{!!Form::close()!!}
</div>
<div id="uploading" style="display:none;" class="alert alert-info">Παρακαλούμε περιμένετε. Γίνεται βελτιστοποίηση της εικόνας.</div>
@endsection