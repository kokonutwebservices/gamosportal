@component('mail::message')
# Μήνυμα για πελάτη

Το παρακάτω μήνυμα στάλθηκε στην καταχώρηση **{{$entry->name}}**, στην κατηγορία **{{$category->cat_name}}**.

## Στοιχεία μηνύματος

* Όνομα: {{$request->name}}
* Τηλέφωνο: {{$request->phone}}
* email: {{$request->email}}
* Πιθανή ημερομηνία: {{$request->date}}
* Μήνυμα: {{$request->message}}



---
###### Το παραπάνω είναι για ενημέρωση. Δεν απαιτείται κάποια ενέργεια.


@endcomponent
