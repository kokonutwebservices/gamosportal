@extends('clientarea.layouts.master')

@section('leftnav')
    @include('clientarea.layouts.leftnav')
@endsection

@section('content')

<h2>Προσθήκη Album στην καταχώρηση {{$entry->name}}</h2>

<div class="col-md-6">
{!!Form::open(['method' => 'POST', 'action' => ['ClientAreaController@albumsStore', $entry->id]]) !!}

<div class="form-group">
{{Form::label('name', 'Ονομασία Album')}}
{{Form::text('name', '', ['class' => 'form-control'])}}
</div>

<div class="form-group">
{{Form::label('description', 'Περιγραφή Album')}} <span>(προαιρετικό)</span>
<textarea name="description" class="form-control tinyMCE"></textarea>
</div>

<div class="form-group">
{{Form::hidden('entry_id', $entry->id)}}
{{Form::button('Δημιουργία', ['type' => 'submit', 'class' => 'btn btn-primary float-left'])}}
</div>
<a class="btn btn-warning float-right" style="color: #fff" href="/clientarea/{{$entry->id}}/albums">Ακύρωση</a>
{!!Form::close()!!}
</div>
    
@endsection