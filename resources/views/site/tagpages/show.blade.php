@extends ('site.layouts.master')

@section('metatags')
    <title>{{$tagPage->metatitle}}</title>
    <meta name="description" content="{{$tagPage->metadescription}}" >
    <meta name="keywords" content="{{$tagPage->metakeywords}}" >
    <meta property='og:title' content='{{$tagPage->title}}'>
    <meta property='og:description' content='{{$tagPage->description}}'>
    <meta property='og:locale' content='el_GR'>
    @if (count($entries))
        <meta property='og:image' content="https://gamosportal.gr/storage/entries/{{$entries->first()->cat_id}}/{{$entries->first()->image}}">
    @else 
    <meta property='og:image' content="https://gamosportal.gr/storage/step.png">
    @endif    
@endsection

@section ('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<section id="main">
<div class="container">
    <h1 class="text-center">{{$tagPage->title}}</h1>

    <div class="row">
        <div class="tagPage-description">
            <p> {!!$tagPage->body!!} </p>
        </div>
        <div class="col-md-9 mb-5 latest">
            <h2>{{$tagPage->title2}}</h2>
            <div class="d-none d-md-flex flex-wrap justify-content-end mb-3 mr-5"><a class="tag" href="{{url()->previous()}}">Επιστροφή</a></div>
            <div class="d-flex flex-wrap">
                @foreach($entries as $entry)
                @if (Carbon\Carbon::parse($entry->end_at)->addMonth() > Carbon\Carbon::now())
                    <div class="latest-item col-md-6 col-lg-4 pull-left">
                        @if($entry->image)
                        <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}"><img class="thumbnail" src="/storage/entries/{{$entry->cat_id}}/{{$entry->image}}" alt="{{$entry->category->cat_name}} {{$entry->name}}" title="{{$entry->category->cat_name}} {{$entry->name}}"></a>
                        @endif
                        <div class="card-body">
                            <h4 class="card-title"><a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}">{{$entry->name}}</a></h4>
                            <small> {{$entry->district->name}} </small>
                            <div>
                            {!! \Illuminate\Support\Str::words(strip_tags($entry->profile), 25) !!} <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}" class=""><i class="fas fa-external-link-alt" aria-hidden="true"></i></a>                           
                            </div>
                            <div>
                                @if (count($entry->tags))
                                    <span><i class="fas fa-tags mt-3"></i></span>                                
                                @endif
                                @foreach ($entry->tags as $tag)
                                    <a href="/tags/{{$tag->page->slug}}" class="tag">{{$tag->name}}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>                    
                @endif
                @endforeach            
            </div>
            <div class="d-flex d-md-none justify-content-end mt-5 mb-3 mr-3"><a class="tag" href="{{url()->previous()}}">Επιστροφή</a></div>
        </div>
        <div class="col-md-3">
            <h2 class="mt-0">Δείτε ακόμη</h2>
            <ul class="list-group">

                @foreach ($tagPages as $page)
                    @if ($tagPage->title != $page->title)
                    <li class="list-group-item"><a href="/tags/{{$page->slug}}">{{$page->title}}</a></li>
                        
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</div>
</section>
</div>    
@endsection