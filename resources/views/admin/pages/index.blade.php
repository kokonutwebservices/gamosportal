@extends ('admin.layouts.master')

@section ('content')
    
    <h1>Σελίδες Κατηγοριών</h1>
    {{ csrf_field() }} 
    @if(count($pages) > 0)
    <ul id="pageSortable" class="list-group">
        
        @foreach ($pages as $page)
            <li id="{{$page->id}}" class="list-item"><a href="pages/{{$page->id}}/edit">{{$page->title}}</a></li>
        @endforeach
    </ul>
    @else
    <div class="alert alert-danger">Δεν υπάρχουν σελίδες</div>
    @endif
        
    <hr>
    <a class="btn btn-info" href="pages/create">Δημιουργία νέας σελίδας</a>
@endsection