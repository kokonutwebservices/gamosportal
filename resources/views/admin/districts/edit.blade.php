@extends('admin.layouts.master')

@section('content')
<h1>Επεξεργασία Περιφέρειας: {{$district->name}} </h1>

<form method="post" action="{{route('admin.districts.update', $district)}}">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="name">Όνομα Περιφέρειας</label>
        <input id="name" class="form-control" type="text" name="name" value="{{$district->name}}">
        @error('name')
            <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="slug">slug</label>
        <input id="slug" class="form-control" type="text" name="slug" value="{{$district->slug}}">
        @error('slug')
            <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="metatitle">Meta Title Περιφέρειας</label>
        <input id="metatitle" class="form-control" type="text" name="metatitle" value="{{$district->metatitle}}">
        @error('metatitle')
            <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="metadescription">Meta Description Περιφέρειας</label>
        <input id="metadescription" class="form-control" type="text" name="metadescription" value="{{$district->metadescription}}">
        @error('metadescription')
            <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="metakeywords">Meta Keywords Περιφέρειας</label>
        <input id="metakeywords" class="form-control" type="text" name="metakeywords" value="{{$district->metakeywords}}">
        @error('metakeywords')
            <span class="text-danger">{{$message}}</span>
        @enderror
    </div>

    <div class="d-flex justify-content-around">
        <button class="btn btn-primary" type="submit" name="submitbutton" value="save-close">Save & Close</button>
        <button class="btn btn-success" type="submit" name="submitbutton" value="save">Save</button>
        <button class="btn btn-btn-secondary" type="submit" name="submitbutton" value="save-new">Save & New</button>
        <a class="btn btn-warning" href="{{route('admin.districts.index')}}">Cancel</a>
    
    
</form>
        <form action="{{route('admin.districts.destroy', $district)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger" type="submit">DELETE</button>
        </form>
    </div>

</div>


@endsection