<div class="pr-5">
    <h2>Καλωσήλθατε!</h2>
    <p class="text-left">Για να διαχειριστείτε οποιαδήποτε καταχώρηση, κάντε κλικ στον τίτλο ή τη φωτογραφία</p>
    <p class="text-left">Αν χρειάζεστε οποιαδήποτε βοήθεια, μη διαστάσετε να επικοινωνήσετε μαζί μας. </p>
    <p><i class="fas fa-phone"></i> 21026359188</p>
    <p><a href="mailto:info@gamosportal.gr"><i class="fas fa-envelope"></i> info@gamosportal.gr</a></p>
</div>