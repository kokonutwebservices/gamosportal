@extends('site.layouts.master')
@section('metatags')
    <title>{{ $district->name }} - Δημαρχεία - {{ $municipality->metatitle ? $municipality->metatitle : $municipality->name }}</title>
    <meta name="description"
        content="{{ $municipality->metadescription ? $municipality->metadescription : $municipality->name }}">
    <meta name="keywords" content="{{ $municipality->metakeywords }}">
    <meta name="geo.region" content="GR">
    <meta name="geo.position" content="{{ $municipality->map }}">
    <meta name="ICBM" content="{{ $municipality->map }}">
    <meta property="og:image" content="">
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    {{-- Breadcrubs --}}
    <div class="container entry">
        <div class="row">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Αρχική</a></li>
                <li class="breadcrumb-item"><a href="/{{ $district->slug }}">{{ $district->name }}</a></li>
                <li class="breadcrumb-item"><a href="/{{ $district->slug }}/dimarxeia">Δημαρχεία</a></li>
                <li class="breadcrumb-item active">{{ $municipality->name }}</li>
            </ol>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <h1 id="churchname"> {{ $municipality->name }} </h1>
            </div>
            <div class="col-md-3">
                <h2>Επικοινωνία</h2>
                <ul class="list-group">
                    <li class="list-group-item">
                        <i class="fas fa-user mr-3" aria-hidden="true"></i> <span
                            itemprop="name">{{ $municipality->person }}</span>
                    </li>
                    <li class="list-group-item">
                        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                            <i class="fas fa-map-marker-alt mr-3" aria-hidden="true"></i> <span itemprop="streetAddress">
                                {{ $municipality->address }} </span>
                        </div>
                    </li>
                    @if ($municipality->phone)
                        <li class="list-group-item">
                            <a class="" href="tel:+30{{ $municipality->phone }}"><i class="fas fa-phone mr-3"
                                    aria-hidden="true"></i> <span
                                    itemprop="telephone">{{ $municipality->phone }}</span></a>
                        </li>
                    @endif
                    @if ($municipality->mobile)
                        <li class="list-group-item">
                            <a class="" href="tel:+30{{ $municipality->mobile }}"><i
                                    class="fas fa-mobile-alt mr-3" aria-hidden="true"></i> <span
                                    itemprop="telephone">{{ $municipality->mobile }}</span></a>
                        </li>
                    @endif
                    @if ($municipality->email)
                        <li class="list-group-item">
                            <a class="" href="mailto:{{ $municipality->email }}" itemprop="email">
                                <i class="fas fa-envelope mr-3" aria-hidden="true"></i>
                                {{ $municipality->email }}
                            </a>
                        </li>
                    @endif
                    @if ($municipality->website)
                        <li class="list-group-item">
                            <a href="https://{{ $municipality->website }}" target="_blank" rel="nofollow" itemprop="url">
                                <i class="fas fa-globe mr-3" aria-hidden="true"></i>
                                {{ $municipality->website }}
                            </a>
                        </li>
                    @endif

                </ul>
            </div>
            <div class="col-md-6">
                @if ($municipality->map)
                    <style>
                        #map {
                            width: 100%;
                            height: 400px;
                            background-color: grey;
                        }
                    </style>
                    <div id="map"></div>
                    <script>
                        function initMap() {
                            var uluru = {
                                lat: {{ Maps::lat($municipality->map) }},
                                lng: {{ Maps::lon($municipality->map) }}
                            };
                            var map = new google.maps.Map(document.getElementById('map'), {
                                zoom: 15,
                                center: uluru
                            });
                            var marker = new google.maps.Marker({
                                position: uluru,
                                map: map
                            });
                        }
                    </script>
                    <script async defer
                                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATXsZY-e5x34l-ilC2sLA5Ftzy3xZPuUc&callback=initMap">
                    </script>
                @endif
                <h3>Τι χρειάζεται να γνωρίζετε:</h3>
                <p>Σύμφωνα με το Νόμο 4483/2017:</p>
                <p>Σε κάθε Δήμο, ο Δήμαρχος ορίζει με πάγια πράξη του, περισσότερες τακτές ημέρες της εβδομάδας, <strong>μη
                        αποκλειομένου του Σαββάτου και της Κυριακής</strong>, και ορισμένη ώρα ενάρξεως, για την τέλεση των γάμων για
                        τους οποίους υποβλήθηκαν σχετικές αιτήσεις <strong>μέχρι και την προηγούμενη ημέρα</strong>. Με την ίδια πράξη του, ο
                        Δήμαρχος μπορεί να ορίζει την τέλεση των πολιτικών γάμων σε περισσότερα του ενός δημοτικά κτίρια ή
                        <strong>και σε εξωτερικούς κοινόχρηστους δημοτικούς χώρους</strong>. Ο Δήμαρχος μεριμνά για την κατάλληλη διαρρύθμιση
                        και διακόσμηση του χώρου, ώστε να δίνεται στην τελετή του γάμου η επισημότητα που της ταιριάζει.».
                </p>
                <p>Δείτε αναλυτικά τα δικαιολογητικά για το γάμο <a href="https://www.gamos-portal.gr/idees-symvoules/dikaiologitika-gia-gamo" target="_blank">εδώ</a></p>
            </div>

            <div class="col-md-3">
                <h2>Ετοιμάζετε πολιτικό γάμο;</h2>
                <p>Βρείτε στο GamosPortal.gr:</p>
                <ul class="list-group">
                    <li class="list-group-item"><a href="/nifika.php">Νυφικά φορέματα</a></li>
                    <li class="list-group-item"><a href="/vradina-endymata">Βραδυνά φορέματα</a></li>
                    <li class="list-group-item"><a href="/kostoumi-gamprou.php">Κοστούμι γαμπρού</a></li>
                    <li class="list-group-item"><a href="/anthostolismos-gamou.php">Στολισμός Δημαρχείου</a></li>
                    <li class="list-group-item"><a href="/prosklitiria-mpomponieres-eidi-gamou.php">Προσκλητήρια -</a>
                        Μπομπονιέρες</li>
                    <li class="list-group-item"><a href="/fotografoi-gamou.php">Φωτογράφοι γάμου</a></li>
                    <li class="list-group-item"><a href="/nyfiko-aytokinito">Νυφικό Αυτοκίνητο</a></li>
                    <li class="list-group-item"><a href="/ktimata-gamou.php">Κτήματα γάμου</a></li>
                    <li class="list-group-item"><a href="/aithouses-deksioseon.php">Αίθουσες Δεξιώσεων</a></li>
                    <li class="list-group-item"><a href="/ksenodoxeia-gamou.php">Ξενοδοχεία γάμου</a></li>
                    <li class="list-group-item"><a href="/catering-gamou.php">Catering γάμου</a></li>
                    <li class="list-group-item"><a href="/eksoplismos-deksioseon.php">Εξοπλισμός δεξιώσεων</a></li>
                </ul>
            </div>

        </div>
        <div class="row">
        </div>
    </div>
@endsection
