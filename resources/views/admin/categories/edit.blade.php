@extends('admin.layouts.master')

@section('content')

    <h1>Επεξεργασία Κατηγορίας: {{$category->cat_name}} </h1>

    @include('admin.layouts.errors')
    
    <div class="col-md-12">

    {!! Form::open(['action' => ['CategoriesController@update', $category->id], 'method' => 'POST', 'enctype' => 'multipart/form-data' ]) !!}
    
    <div class="form-group">
        {{Form::label('cat_name', 'Όνομα Κατηγορίας')}}
        {{Form::text('cat_name', $category->cat_name, ['class' => 'form-control', 'required'])}}
    </div>

    <div class="form-group">
        {{Form::label('cat_slug', 'slug')}}
        {{Form::text('cat_slug', $category->cat_slug, ['class' => 'form-control', 'required'])}}
    </div>

    <div class="form-group">
        {{Form::label('cat_description', 'Κείμενο περιγραφής κατηγορίας')}}
        {{Form::textarea('cat_description', $category->cat_description, ['class' => 'form-control tinyMCE'])}}
    </div>

    <div class="form-group">
        {{Form::label('cat_parent_id', 'Μητρική Κατηγορία')}}
        <select class="form-control" name="cat_parent_id">
            <option value="0">Καμία - Αυτή είναι μητρική</option>
            @foreach ($parents as $parent)
                @if($parent->id == $category->cat_parent_id)
                    <option value="{{$parent->id}}" selected>{{$parent->cat_name}}</option>
                @else
                    <option value="{{$parent->id}}">{{$parent->cat_name}}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div class="form-group">
        {{Form::label('cat_order', 'Σειρά')}}
        {{Form::number('cat_order', $category->cat_order, ['class' => 'form-control'])}}
    </div>

    <div class="form-group">
        {{Form::label('cat_img', 'Εικόνα Κατηγορίας')}}
        {{Form::file('cat_img', ['id'=>'image'])}}
        @if($category->cat_img)
            <img class="thumbnail" src="/storage/categories/{{$category->cat_img}}">
        @endif
    </div>

    @foreach ($districts as $district)
        <?php 
            $i = $district->id;
            $title = 'meta_title_'.$i;
            $metatitle = $category->$title;
            $description = 'meta_description_'.$i;
            $metadescription = $category->$description;
            $keywords = 'meta_keywords_'.$i;
            $metakeywords = $category->$keywords;
            
        ?>
        <ul class="list-group">
           <li class="list-group-item">
                <div class="form-group">
                    <h2>Meta Tags για {{$district->name}} </h2>
                    {{Form::label('meta_title_'.$i, "Meta Title")}}
                    {{Form::text('meta_title_'.$i, $metatitle, ['class' => 'form-control'])}}
                    {{Form::label('meta_description_'.$i, "Meta Description")}}
                    {{Form::text('meta_description_'.$i, $metadescription, ['class' => 'form-control'])}}
                    {{Form::label('meta_keywords_'.$i, "Meta Keywords")}}
                    {{Form::text('meta_keywords_'.$i, $metakeywords, ['class' => 'form-control'])}}
                </div>
           </li>
        </ul>
    @endforeach

        <div class="form-group">
            <div class="col-md-2">  
                {{Form::hidden('_method', 'PUT')}}  
                {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
            </div>  

            <div class="col-md-2">
                {{Form::hidden('_method', 'PUT')}}  
                {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
            </div>

            <div class="col-md-2">
                {{Form::hidden('_method', 'PUT')}}  
                {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
            </div>

            <div class="col-md-2">  
                <a href="/admin/categories" class="btn btn-warning pull-right">Cancel</a>
            </div>

            <div class="col-md-2">
            {!! Form::close() !!}

                {!! Form::open(['action' => ['CategoriesController@destroy', $category->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'pull-right' ]) !!}

                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}

            {!! Form::close() !!}  
            </div>
        </div>

    </div>



@endsection