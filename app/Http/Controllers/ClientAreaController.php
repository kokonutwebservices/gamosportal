<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Entry;
use App\Album;
use App\Image;
use App\Video;
use Illuminate\Support\Facades\Storage;
use InterImage;

class ClientAreaController extends Controller
{
    public function __construct(){
        $this->middleware('auth:client');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clientarea.show');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $client = auth()->user();
        $entries = $client->entries;
        return view('clientarea.show', compact('client', 'entries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateInfo(Request $request, $entryid)
    {
        $this->validate($request, [
            'profile' => 'required'
        ]);
        
        $entry = Entry::where('id', $entryid)->first();
        $client = $entry->client; 
        //Update
        //Entry Logo
        if($request->hasFile('logo')){
            //Αν υπάρχει εικόνα, το όνομα αρχείου παραμένει ίδιο
            $logo = $entry->logo;
            request()->file('logo')->storeAs("logos/$entry->cat_id", $logo);

        } else {
            $logo = $entry->logo;
        }

        //Entry Image
        if($request->hasFile('image')){
            //Αν υπάρχει εικόνα, το όνομα αρχείου παραμένει ίδιο
            $filename = $entry->image;
            request()->file('image')->storeAs("/entries/$entry->cat_id", $filename); 
            
            InterImage::make(Storage::get('entries/'.$entry->cat_id.'/'.$entry->image))
                ->resize(null, 360, function($constraint){
                    $constraint->aspectRatio();
                })->save(Storage::path('entries/'.$entry->cat_id.'/small/'.$entry->image));

        } else {
            $filename = $entry->image;
        }

        $facebook = preg_replace('#^https?://#', '',$request->input('facebook'));
        $google = preg_replace('#^https?://#', '',$request->input('google'));
        $twitter = preg_replace('#^https?://#', '',$request->input('twitter'));
        $instagram = preg_replace('#^https?://#', '',$request->input('instagram'));
        $pinterest = preg_replace('#^https?://#', '',$request->input('pinterest'));
        $youtube = preg_replace('#^https?://#', '',$request->input('youtube'));
     
       
        $entry->logo = $logo;
        $entry->person = $request->input('person');
        $entry->address = $request->input('address');
        $entry->area = $request->input('area');
        $entry->phone = $request->input('phone');
        $entry->mobile = $request->input('mobile');
        $entry->website = $request->input('website');
        $entry->email = $request->input('email');
        $entry->facebook = $facebook;
        $entry->google = $google;
        $entry->twitter = $twitter;
        $entry->instagram = $instagram;
        $entry->pinterest = $pinterest;
        $entry->youtube = $youtube;
        $entry->chat_id = $request->input('chat_id');
        $entry->map = $request->input('map');
        $entry->image = $filename;
        $entry->profile = $request->input('profile');
        $entry->save();


        //Redirect
        if($request->submitbutton == 'save'){
            return redirect("/clientarea/$entry->id/info");
        } elseif ($request->submitbutton == 'save-close') {
            return redirect("/clientarea");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function info($entryid){
        
        $client = auth()->user();
        $entry = Entry::where('id', $entryid)->first();

        return view('clientarea.entry.info', compact('client', 'entry'));
    }

    public function albums($entryid){
        $client = auth()->user();
        $entry = Entry::where('id', $entryid)->first();
        $albums = Album::where('entry_id', $entryid)->orderBy('order', 'asc')->get();
        
        return view('clientarea.albums.index', compact('client', 'entry', 'albums'));
    }

    public function albumsCreate($entryid){

        $client = auth()->user();
        $entry = Entry::where('id', $entryid)->first();

        return view('clientarea.albums.create', compact('client', 'entry'));
        
    }

    public function albumsStore(Request $request, $entryid){

        $client = auth()->user();
        $entry = Entry::where('id', $entryid)->first();

        $this->validate(request(),[
            'name' => 'required'
        ]);

        $album = new Album;
        $album->name = $request->input('name');
        $album->description = $request->input('description');
        $album->entry_id = $request->input('entry_id');
        $album->save();
        
        return redirect("/clientarea/$entry->id/albums");
    }

    public function albumsEdit($entryid, Album $albumid){

        $album = $albumid;
        $entry = $album->entry;
        $client = auth()->user();
        $images = Image::where('album_id', $album->id)->orderBy('order')->get();

        return view('clientarea.albums.edit', compact('album', 'entry', 'client', 'images'));
    }

    public function albumsUpdateTitle(Request $request, Entry $entryid, Album $albumid){
        
        $album = $albumid;
        
        $album->name = request('name');

        $album->save();

        return redirect()->back()->with('message', 'Το όνομα ενημερώθηκε με επιτυχία');
    }

    public function albumsUpdateDescription(Request $request, Entry $entryid, Album $albumid){
       
        $album = $albumid;
        
        $album->description = request('description');
        $album->save();

        return redirect()->back()->with('message', 'Η περιγραφή του album ενημερώθηκε');
    }

    public function albumsUpdateCover(Request $request, Entry $entryid, Album $albumid){
        $album = $albumid;
        
        $album->cover = request('cover');
        $album->save();

        return redirect()->back()->with('message', 'Το εξώφυλλο αποθηκεύτηκε με επιτυχία');
    }


    public function albumsDestroy(Entry $entryid, Album $albumid){
        $client = auth()->user();
        $entry = $entryid;

        $album = $albumid;
        
        $images = Storage::deleteDirectory("uploads/$album->id");

        $album->delete();

        return redirect()->back()->with('message', 'Το album διαγράφηκε με επιτυχία');
    }

    public function albumsOrder(){

        $orders = explode(',', $_POST['order']);
        
        foreach ($orders as $neworder=>$value){
            $parts = explode('-',$value);
            // $entryid = $parts[0];
			$id = $parts[1];			
            $order = $neworder+1;
            $album = Album::find($id);
            $album->order = $order;
            $album->save();
        }
    }

    public function imagesStore(Request $request, Entry $entryid, Album $albumid){
        $album = $albumid;
        $entry = $album->entry;
        
        if (count(request('image')) > request('available')){
            return redirect()->back()->withErrors(['Δεν μπορείτε να προσθέσετε περισσότερες από '.request('available').' φωτογραφίες']);
        } else {
        
        $this->validate(request(),[
            'image.*' => 'image|max:1000|mimes:jpeg',
            
        ]);
        
        //Για το μαζικό upload το πρόβλημα με το filename είναι ότι με το timestamp τα ονόματα συνέπιπταν και δεν αποθηκεύονταν όλα τα αρχεία.
        //Παρατήρησα ότι στο tmp το αρχείο έπαιρνε ένα μοναδικό όνομα, το οποίο απομονώνω και το προσθέτω στο slug της καταχώρησης.
        
        foreach($request->image as $file){
        
            $file_path = $file->getPathName();
            $file_sub = substr($file_path, -9, -4);

            $ext = $file->getClientOriginalExtension(); //Get the extension
        
            $filename = $entry->slug. "-" . $file_sub.".".$ext;

            $upload = $file->storeAs('uploads/'.$album->id, $filename); //Store file

            // $filepath = public_path('storage/uploads/'.$album->id.'/'.$filename);

            // try {
            //     \Tinify\setKey("xVBaCSpeVqNE0VFe5_KNBpXweIB-YwZL");
            //     $source = \Tinify\fromFile($filepath);
            //     $source->toFile($filepath);
            // } catch(\Tinify\AccountException $e) {
            //     // Verify your API key and account limit.
            //     return redirect()->back()->with('error', $e->getMessage());
            // } catch(\Tinify\ClientException $e) {
            //     // Check your source image and request options.
            //     return redirect()->back()->with('error', $e->getMessage());
            // } catch(\Tinify\ServerException $e) {
            //     // Temporary issue with the Tinify API.
            //     return redirect()->back()->with('error', $e->getMessage());
            // } catch(\Tinify\ConnectionException $e) {
            //     // A network connection error occurred.
            //     return redirect()->back()->with('error', $e->getMessage());
            // } catch(Exception $e) {
            //     // Something else went wrong, unrelated to the Tinify API.
            //     return redirect()->back()->with('error', $e->getMessage());
            // }


            $image = new Image;
            $image->image = $filename;
            $image->album_id = $album->id;
            $image->save();
        }
        return redirect()->back()->with('message', 'Οι εικόνες ανέβηκαν με επιτυχία.') ;}
    }

    public function imagesOrder(){
        $orders = explode(',',$_POST['order']);
        
        foreach ($orders as $neworder=>$value){
            $parts = explode('-',$value);
            $albumid = $parts[0];
            $id = $parts[1];			
            $order = $neworder+1;
            $image = Image::find($id);
            $image->order = $order;
            $image->save();
        }
    }

    public function imageStoreDescription(Request $request, Entry $entryid, Album $albumid, Image $imageid){
        $entry = $entryid;
        $album = $albumid;
        $image = $imageid;

        $image->description = request('description');
        $image->save();

        return redirect()->back();
    }

    public function changeAlbum(Request $request, Entry $entryid, Album $albumid, Image $imageid){

        $newAlbumId = request('newAlbum');
        $oldAlbum = $albumid;
        $image = $imageid;

        $newAlbum = Album::find($newAlbumId);

        if(count($newAlbum->images) >= 24){
            return redirect()->back()->with('message', 'Αυτό το album έχει ήδη 24 εικόνες');
        } else {
            $filename = $image->image;
            
            $oldFilePath = "uploads/$oldAlbum->id/$filename";

            $newFilePath = "uploads/$newAlbumId/$filename";

            Storage::move($oldFilePath, $newFilePath);          

            $image->album_id = $newAlbumId;
            $image->save();

            return redirect()->back()->with('message', 'Η εικόνα μεταφέρθηκε με επιτυχία');
        }
    }

    public function imageDestroy(Album $albumid, Image $imageid){
    
        $album = $albumid;
        $image = $imageid;
        
        Storage::delete("uploads/$album->id/$image->image");

        $image->delete();

        return redirect()->back()->with('message', 'Η εικόνα διαγράφηκε με επιτυχία');
    }

    public function videoIndex($entryid){

        
        $entry = Entry::where('id', $entryid)->first();
        $client = auth()->user();
        $videos = $entry->videos()->orderBy('order')->get();

        return view('clientarea.video.index', compact('entry', 'videos', 'client'));

    }

    public function videoStore(Request $request, $entryid){

        $entry = Entry::where('id', $entryid)->first();
        $client = auth()->user();
        
        $video = new Video;

        $service = request('vservice');
        $url = request('video');

        if($service == 'YouTube'){
            if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
                } else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
                } else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
                } else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
                }
                else if (preg_match('/youtube\.com\/verify_age\?next_url=\/watch%3Fv%3D([^\&\?\/]+)/', $url, $id)) {
                    $values = $id[1];
                } else {
                    return redirect()->back()->with('errorMessage', 'Δεν επιλέξατε Video από το YouTube');
                }
                $video->YouTubeId = $id[1];                         
        } elseif($service == 'Vimeo'){
            if(preg_match_all('#(http://vimeo.com)/([0-9]+)#i',$url,$id) OR preg_match_all('#(https://vimeo.com)/([0-9]+)#i',$url,$id)){
                $video->VimeoId = $id[2][0];
            } else {
                return redirect()->back()->with('errorMessage', 'Δεν επιλέξατε Video από το Vimeo');
            }      
        } elseif($service == 'Facebook'){
            if(preg_match('~/videos/(?:t\.\d+/)?(\d+)~i',$url,$id)){
                $video->FacebookId = $id[1];  
            } else {
                return redirect()->back()->with('errorMessage', 'Δεν επιλέξατε Video από το Facebook');
            }
        } 
        else {
            return redirect()->back()->with('errorMessage', 'Επιλέξτε αν το video είναι YouTube, Vimeo ή Facebook');
        }

        $video->entry_id = $entry->id;
        $video->save();

        return redirect()->back()->with('message', 'Το video αποθηκεύτηκε με επιτυχία');
    }

    public function videoDestroy(Client $clientid, Entry $entryid, Video $videoid){

        $video = $videoid;
        
        $video->delete();

        return redirect()->back()->with('message', 'Το video διαγράφηκε με επιτυχία');
    }

    //Function που παίρνει την νέα σειρά από το Ajax call του sortable, αναλύει το id κάθε item, και γράφει στην db το νέο order 
    public function videoOrder(){
        $orders = explode(',',$_POST['order']);
            foreach ($orders as $neworder=>$value){
                $parts = explode('-',$value);
                $entryid = $parts[0];
                $id = $parts[1];			
                $order = $neworder+1;
                $video = Video::find($id);
                $video->order = $order;
                $video->save();
            }
    }

    
}