@extends('admin.layouts.master')

@section('content')

<h2>Επεξεργασία Album στην καταχώρηση {{$entry->name}}</h2>
{{ csrf_field() }} 
<h5 id="albumdesc">Τίτλος Album  <i class="far fa-edit" aria-hidden="true"></i></h5>

<div class="col-md-12 album-description">
    {!!Form::open(['action' => ['AlbumsController@updateTitle', $entry->id, $album->id], 'method'=> 'POST'  ])!!}
       {{Form::text('name', $album->name, ['class'=>'form-control'])}}
        {{Form::hidden('_method', 'PUT')}}
        {{Form::button('Αποθήκευση', ['type' => 'submit', 'class' => 'btn btn-primary'])}}
    {!!Form::close()!!}
</div>

<h5 id="albumdesc">Κείμενο Περιγραφής  <i class="far fa-edit" aria-hidden="true"></i></h5>

<div class="col-md-12 album-description">
    {!!Form::open(['action' => ['AlbumsController@update', $entry->id, $album->id], 'method'=> 'POST'  ])!!}
        <textarea class="form-group tinyMCE" name="description"> {{$album->description}} </textarea>
        {{Form::hidden('_method', 'PUT')}}
        {{Form::button('Αποθήκευση', ['type' => 'submit', 'class' => 'btn btn-primary'])}}
    {!!Form::close()!!}
</div>

@if (Storage::exists('uploads/'.$album->id) && count(Storage::files('uploads/'.$album->id)) > 0)
<h5 id="albumdesc">Εξώφυλλο Album  <i class="far fa-edit" aria-hidden="true"></i></h5>
<div class="col-md-12 album-description">
<div class="media">
    @if($album->cover)
        <img class="d-flex mr-3" src="/storage/uploads/{{$album->id}}/{{$album->cover}}" style="width: 100px">
    @endif
    
</div>

{!!Form::open(['method' => 'POST', 'action' => ['AlbumsController@updateCover', $entry->id, $album->id] ])!!}
    @foreach($images as $key=>$image)
        {{Form::label('cover', $key+1)}}
        {{Form::radio('cover', $image->image)}}        
    @endforeach
    {{Form::button('Επιλογή', ['class' => 'btn btn-primary', 'type' => 'submit'])}}
{!!Form::close()!!}
</div>
@endif
<div class="col-md-12">
    <hr>
</div>
<?php
    //Έλεγχος ώστε ο αριθμός των αρχείων να μην ξεπερνάει τα 24
    $imageNo = count(Storage::files('uploads/'.$album->id));
    $avail = 24 - $imageNo;
?>
@if($imageNo >= 24 OR $avail <= 0)
    <p>Το album έχει 24 φωτογραφίες. Δεν μπορείτε να προσθέσετε περισσότερες</p>
@else    
    <p>Το album "{{$album->name}}" έχει {{$imageNo}} φωτογραφίες. Μπορείτε να προσθέσετε ακόμη {{$avail}} </p>

<div class="col-md-12">
    <hr>
</div>
    
{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif
@endif

{{--  Λίστα εικόνων  --}}
<div class="col-md-12 d-flex flex-wrap" id="imagesSortable">

@foreach($images as $key=>$image)

<div class="img-thumbnail col-md-3 adminAlbumImage" id="{{$album->id}}-{{$image->id}}">
<?php $number = $key+1; ?>
{{$number}}
    <img src="/storage/uploads/{{$album->id}}/{{$image->image}}" style="width:200px;" class="mx-auto mb-3 d-block">
    
    {{--  Κουμπί περιγραφής  --}}
    <button class="btn btn-info desc-button"><i class="far fa-comment"></i></i></button>
    
    {{--  Κουμπί μετακίνησης σε άλλο άλμπουμ  --}}
    <button class="btn btn-info change-album" data-toggle="modal" data-target="#AlbumChange" id="{{$image->id}}"><i class="fa fa-suitcase" aria-hidden="true"></i></button>

    {{--  Κουμπί Διαγραφής  --}}
    <a class="btn btn-info" href="/admin/entries/albums/{{$entry->id}}/{{$album->id}}/{{$image->id}}/delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
       
    {{--  Λίστα albums  --}}
    <div class="albumslist">        
        <div class="">
            <h5 class="modal-title">Μετακίνηση σε άλλο album</h5>
        </div>
        <div class="">
            {!!Form::open(['action' => ['ImagesController@changeAlbum', $entry->id, $album->id, $image->id], 'method' => 'POST'])!!}
            
            <select class="custom-select mb-3" name="newAlbum">
                <?php $albums = App\Album::where('entry_id', $entry->id)->get(); ?>

                @foreach($albums as $albumItem)
                    @if($albumItem->id == $album->id)
                        <option value="{{$albumItem->id}}" selected> {{$albumItem->name}} </option>
                    @else
                        <option value="{{$albumItem->id}}"> {{$albumItem->name}} </option>
                    @endif
                @endforeach
            </select>        
            </div>
            <div class="">
                <button type="submit" class="btn btn-primary">Αποθήκευση</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Κλείσιμο</button>
            </div>
            {!!Form::close()!!}        
        </div>

        <div class="image-description">            
            <div class="">
                <h5 class="modal-title">Περιγραφή εικόνας</h5>
            </div>
                <div class="">
                    {!!Form::open(['action' => ['ImagesController@storeDescription', $entry->id, $album->id, $image->id], 'method' => 'POST'])!!}
                    <textarea name="description" class="form-control mb-3"> {!!$image->description!!} </textarea>
                    
                
                <button type="submit" class="btn btn-primary">Αποθήκευση</button>
                {!!Form::close()!!}
                </div>
        </div>
</div>
    
@endforeach
</div>

<div class="col-md-12">
    <hr>
</div>

{{--  Φόρμα ανεβάσματος εικόνων  --}}
{!!Form::open(['method'=>'POST', 'action'=>['ImagesController@store', $entry->id, $album->id], 'class'=>'form', 'enctype'=>'multipart/form-data'])!!}
@if($avail > 0)
    <div class="form-controller">
        {{Form::file('image[]', ['multiple', 'class' => 'form-control-file'])}}
    </div>
    <div class="col-md-12">
    <hr>
    </div>
@endif
    <div class="form-controller">
        {{Form::hidden('available', $avail)}}
        {{Form::button('Προσθήκη', ['type'=>'submit', 'class' => 'btn btn-primary'])}}
        <a href="/admin/entries/albums/{{$entry->id}}" class="btn btn-secondary">Επιστροφή στη λίστα των Album</a>
    </div>
{!!Form::close()!!}

<div id="uploading" style="display:none;" class="alert alert-info">Παρακαλούμε περιμένετε. Γίνεται βελτιστοποίηση των εικόνων.</div>

@endsection
