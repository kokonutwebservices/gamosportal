@if (Request::is('clientarea/*'))
<h2>{{$entry->name}}</h2>
<ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link" href="/clientarea">Καταχωρήσεις</a>
  </li>

  <li class="nav-item {{Request::is('*/info') ? 'active' : '' }}">
    <a class="nav-link" href="/clientarea/{{$entry->id}}/info">Στοιχεία επικοινωνίας</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="/clientarea/{{$entry->id}}/albums">Διαχείριση Album</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="/clientarea/{{$entry->id}}/video">Διαχείριση Video</a>
  </li>
</ul>
@else
  @include('clientarea.welcome')
@endif

