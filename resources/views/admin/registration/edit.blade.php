@extends('admin.layouts.master')

@section('content')

    <div class="col-md-6">

        <h1>Επεξεργασία λογαριασμού χρήστη {{ $user->name }}</h1>

        <form action="{{ route('admin.update', $user) }}" method="post">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="name">Όνομα</label>
                <input class="form-control" type="text" id="name" name="name" value="{{ $user->name }}">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control" type="text" id="email" name="email" value="{{ $user->email }}">
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" type="password" id="password" name="password" value="">
            </div>


            <button type="submit" class="btn btn-primary" >Update</button>

        </form>

    </div>
    @include('admin.layouts.errors')

@endsection