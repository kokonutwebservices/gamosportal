@extends('admin.layouts.master')

@section('content')

<h1>Δημιουργία Νέας Καταχώρησης</h1>

{!! Form::open(['action'=>'EntriesController@store', 'method'=>'POST', 'enctype' => 'multipart/form-data']) !!}

<div class="form-row">

    <div class="form-group col-md-6">
        <label for="client_id">Πελάτης</label>
        <select class="form-control" id="client_id" name="client_id">
            @foreach ($clients as $client)
                
                <option value="{{$client->id}}">{{$client->name}}</option>
                
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-12">
        {{Form::label('name', 'Όνομα Επιχείρησης')}}
        {{Form::text('name', '', ['class'=>'form-control', 'placeholder' => 'Το όνομα όπως θα φαίνεται στην καταχώρηση', 'required'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('slug', 'slug')}}
        {{Form::text('slug', '', ['class'=>'form-control', 'placeholder' => 'friendly url', 'required'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('logo', 'Logo')}}
        {{Form::file('logo', ['required'] )}}
    </div>

    <div class="form-group col-md-12">
        {{Form::label('person', 'Υπεύθυνος')}}
        {{Form::text('person', '', ['class'=>'form-control', 'placeholder' => 'Όνομα Υπευθύνου'] )}}
    </div>

    <div class="form-group col-md-12">
        {{Form::label('address', 'Διεύθυνση')}}
        {{Form::text('address', '', ['class'=>'form-control', 'placeholder' => 'Διεύθυνση'] )}}
    </div>

    <div class="form-group col-md-4">
        <label for="tags">Tags</label>
        <select multiple class="form-control" name="tags[]" id="tags">
          
          @foreach ($tags as $tag)
  
              <option value="{{$tag->id}}">{{$tag->name}}</option>
              
          @endforeach
        </select>
      </div>

    <div class="form-group col-md-4">
        {{Form::label('phone', 'Τηλέφωνο')}}
        {{Form::text('phone', '', ['class'=>'form-control', 'placeholder' => 'Τηλέφωνο'] )}}
    </div>

    <div class="form-group col-md-4">
        {{Form::label('mobile', 'Κινητό')}}
        {{Form::text('mobile', '', ['class'=>'form-control', 'placeholder' => 'Κινητό'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('website', 'Website')}}
        {{Form::text('website', '', ['class'=>'form-control', 'placeholder' => 'Website'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('email', 'Email')}}
        {{Form::text('email', '', ['class'=>'form-control', 'placeholder' => 'Email'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('facebook', 'Facebook')}}
        {{Form::text('facebook', '', ['class'=>'form-control', 'placeholder' => 'Facebook'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('google', 'Google+')}}
        {{Form::text('google', '', ['class'=>'form-control', 'placeholder' => 'Google+'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('twitter', 'Twitter')}}
        {{Form::text('twitter', '', ['class'=>'form-control', 'placeholder' => 'twitter'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('instagram', 'Instagram')}}
        {{Form::text('instagram', '', ['class'=>'form-control', 'placeholder' => 'Instagram'] )}}
    </div> 

    <div class="form-group col-md-6">
        {{Form::label('pinterest', 'Pinterest')}}
        {{Form::text('pinterest', '', ['class'=>'form-control', 'placeholder' => 'Pinterest'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('youtube', 'YouTube')}}
        {{Form::text('youtube', '', ['class'=>'form-control', 'placeholder' => 'YouTube'] )}}
    </div> 

    <div class="form-group col-md-6">
        {{Form::label('vimeo', 'Vimeo')}}
        {{Form::text('vimeo', '', ['class'=>'form-control', 'placeholder' => 'Vimeo'] )}}
    </div> 

    <div class="form-group col-md-6">
        {{Form::label('chat_id', 'Chat ID')}}
        {{Form::text('chat_id', '', ['class'=>'form-control', 'placeholder' => 'Chat ID'] )}}
    </div> 

    <div class="form-group col-md-6">
        {{Form::label('map', 'Google Maps')}}
        {{Form::text('map', '', ['class'=>'form-control', 'placeholder' => 'Google Maps'] )}}
    </div>
    
    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-6">
        <label for="category_id">Κατηγορία</label>
        <select class="form-control" id="cat_id" name="cat_id">
            @foreach ($categories as $category)
                @if($category->cat_parent_id >0)
                    <option value="{{$category->id}}">{{$category->cat_name}}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-6">
        <label for="district_id">Περιφέρεια</label>
        <select class="form-control" id="dist_id" name="dist_id">
            @foreach ($districts as $district)
                <option value="{{$district->id}}">{{$district->name}}</option>                
            @endforeach
        </select>
    </div>

    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group ">
        {{Form::label('image', 'Εικόνα Προφίλ')}}
        {{Form::file('image', ['required'] )}}
    </div>

    <div class="form-group col-md-12">
        {{Form::label('profile', 'Κείμενο Προφίλ')}}
        {{Form::textarea('profile', '', ['class'=>'form-control tinyMCE', 'placeholder' => 'Κείμενο Προφίλ'] )}}
    </div> 

    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-12">
        {{Form::label('metatitle', 'Τίτλος')}}
        {{Form::text('metatitle', '', ['class'=>'form-control', 'placeholder' => 'Τίτλος', 'required'] )}}
    </div> 

    <div class="form-group col-md-12">
        {{Form::label('metadescription', 'Meta Description')}}
        {{Form::text('metadescription', '', ['class'=>'form-control', 'placeholder' => 'Meta Description', 'required'] )}}
    </div>

    <div class="form-group col-md-12">
        {{Form::label('metakeywords', 'Meta Keywords')}}
        {{Form::text('metakeywords', '', ['class'=>'form-control', 'placeholder' => 'Meta Keywords', 'required'] )}}
    </div>
    
    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-12">
        {{Form::label('offer', 'Προσφορά στο Gamos Deals')}}
        {{Form::text('offer', '', ['class'=>'form-control', 'placeholder' => 'url προσφοράς'] )}}
    </div>

    <div class="form-group col-md-6">
        Συμμετοχή Bridal Expo:
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="newlife" class="form-check-input" type="radio" id="inlineCheckbox1" value="1"> Ναι
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="newlife" class="form-check-input" type="radio" id="inlineCheckbox2" value="0" checked> Όχι
            </label>
        </div>
    </div>

    <div class="form-group col-md-6">
        Ενεργό:
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="active" class="form-check-input" type="radio" id="inlineCheckbox1" value="1" > Ναι
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="active" class="form-check-input" type="radio" id="inlineCheckbox2" value="0" checked> Όχι
            </label>
        </div>
    </div>

    <div class="col-md-12">
        <hr>
    </div>
    <h4>Ημερομηνίες</h4>
    <div class="col-md-12">
    <p>Δημιουργία Καταχώρησης: {{ Carbon\Carbon::now()->toDateTimeString() }}</p>
    </div>
    
    <div class="form-group col-md-4 text-center">
        {{Form::label('start_at', 'Έναρξη Καταχώρησης')}}
        {{Form::date('start_at', Carbon\Carbon::now()->toDateTimeString(), ['class'=>'form-control', 'id'=>'datepicker'] )}}
    </div>
    <div class="form-group col-md-4 text-center">
        {{Form::label('end_at', 'Λήξη Καταχώρησης')}}
        {{Form::date('end_at', Carbon\Carbon::now()->addYear()->toDateTimeString(), ['class'=>'form-control', 'id'=>'datepicker'] )}}
    </div>
    <div class="form-group col-md-4">
        <p class="text-center"><strong>Απενεργοποίηση Καταχώρησης</strong></p>
        <p class="text-center">{{Carbon\Carbon::now()->addYear()->addMonth()->toDateTimeString()}}</p>
    </div>

    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-12">    
        <div class="col-md-2">  
        {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
        </div>

        <div class="col-md-2">
            {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
        </div>

        <div class="col-md-2">
            {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
        </div>

        <div class="col-md-2">  
            <a href="/admin/entries" class="btn btn-warning pull-right">Cancel</a>
        </div>  
    </div>
</div>
{!! Form::close() !!}


@endsection