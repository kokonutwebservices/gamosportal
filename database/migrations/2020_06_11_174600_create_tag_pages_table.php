<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tag_id');
            $table->text('title');
            $table->text('slug');
            $table->text('metatitle');
            $table->text('metadescription');
            $table->text('metakeywords')->nullable();
            $table->text('body')->nullable();
            $table->text('title2')->nullable();
            $table->integer('order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_pages');
    }
}
