@extends('admin.layouts.master')

@section('content')

    <h1>Special Entries</h1>

    <div class="col-md-12">
        <a href="specialentries/create" class="btn btn-primary pull-left">Δημιουργία Ειδ. Καταχώρησης</a>
        <div class="form-group col-md-4 pull-right">
            {!!Form::open(['method' => 'POST', 'action' => 'SpecialEntriesController@search'])!!}
                <div class="input-group">
                <input type="text" class="form-control" name="query" placeholder="Αναζήτηση" aria-label="Αναζήτηση">
                <span class="input-group-btn">
                    <button class="btn btn-secondary" type="submit">Go!</button>
                </span>
                </div>
            {!!Form::close()!!}
            </div>
    </div>

    <table class="table">
        <thead class="thead-inverse">
            <tr>
            <th>@sortablelink('id')</th>
            <th>@sortablelink('name', 'Όνομα Καταχώρησης')</th>
            <th>@sortablelink('client_id', 'Πελάτης') </th>
            <th>@sortablelink('end_at', 'Ημ. Λήξης')</th>
            </tr>
        </thead>
            @foreach($specialentries as $specialentry)
                <tr>
                    <td>{{$specialentry->id}}</td>
                    <td><a href="specialentries/{{$specialentry->id}}/edit">{{$specialentry->name}}</a></td>
                    <td>{{$specialentry->client->name}}</td>
                    <td>{{$specialentry->end_at}}</td>
                </tr>
            @endforeach
        </table>

    <div class="col-md-12">
        <a href="specialentries/create" class="btn btn-primary">Δημιουργία Ειδ. Καταχώρησης</a>
    </div>

@endsection