<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\FrontEndChurchController;
use App\Http\Controllers\FrontEndController;
use App\Http\Controllers\RepresentativesController;


Route::get('/', 'FrontEndController@home')->name('home');

Route::get('/admin', 'SessionsController@index')->name('admin')->middleware('auth');

//Δημιουργία/Είσοδος Διαχειριστών
Route::get('admin/admins', 'RegistrationController@index');
Route::get('admin/register', 'RegistrationController@create')->middleware('auth');
Route::post('admin/register', 'RegistrationController@store');
Route::get('admin/{user}/edit', 'RegistrationController@edit')->name('admin.edit');
Route::put('admin/{user}', 'RegistrationController@update')->name('admin.update');
Route::get('/admin/login', 'SessionsController@create')->name('admin.login')->middleware('guest');
Route::post('/admin/login', 'SessionsController@store');
Route::get('/admin/logout', 'SessionsController@destroy');

//Admin Panel
Route::group(['middleware' => 'auth', 'prefix' => 'admin', 'as' => 'admin.'], function() {

    //Κατηγορίες
    Route::post('/categoryorder', 'CategoriesController@order');
    Route::resource('/categories', 'CategoriesController')->name('*','categories');

    // //Σελίδες SEO
    Route::post('pageorder', 'AdminPagesController@order');
    Route::resource('pages', 'AdminPagesController')->name('*', 'pages');

    //Πληροφοριακές Σελίδες 
    Route::post('/infopagesorder', 'InfoPagesController@order');
    Route::resource('/infopages', 'InfoPagesController')->name('*', 'infopages');

    //Περιφέρειες
    Route::resource('/districts', 'DistrictsController')->name('*', 'districts');

    //Πελάτες
    Route::post('/clients/search', 'ClientsController@search');
    Route::resource('/clients', 'ClientsController')->name('*', 'clients');

    //Πωλητές
    Route::resource('/representatives', 'RepresentativesController')->name('*', 'representatives');

    //Καταχωρήσεις
    Route::post('/entries/search', 'EntriesController@search');
    Route::resource('/entries', 'EntriesController')->name('*', 'entries');

    //Διαχείριση Φωτογραφιών
    Route::get('/entries/albums/{id}', 'AlbumsController@index');
    Route::get('/entries/albums/{id}/create', 'AlbumsController@create');
    Route::post('/entries/albums', 'AlbumsController@store');
    Route::put('/entries/albums/{entryid}/{albumid}/updatedescription', 'AlbumsController@update');
    Route::post('/entries/albums/{entryid}/{albumid}/updatecover', 'AlbumsController@updateCover');
    Route::put('/entries/albums/{entryid}/{albumid}/updatetitle', 'AlbumsController@updateTitle');
    Route::get('/entries/albums/{entryid}/{albumid}/edit', 'AlbumsController@edit');
    Route::post('/entries/albums/albumorder', 'AlbumsController@order');
    Route::get('/entries/albums/{albumid}/delete', 'AlbumsController@destroy');
    Route::post('/entries/albums/{entryid}/{albumid}/', 'ImagesController@store');
    Route::post('/entries/albums/{entryid}/{albumid}/imageorder', 'ImagesController@order');
    Route::get('/entries/albums/{entryid}/{albumid}/{imageid}/delete', 'ImagesController@destroy');
    Route::post('/entries/albums/{entryid}/{albumid}/{imageid}', 'ImagesController@storeDescription');
    Route::post('/entries/albums/{entryid}/{albumid}/{imageid}/edit', 'ImagesController@changeAlbum');

    //Διαχείριση video
    Route::get('/entries/video/{id}', 'VideoController@index');
    Route::post('/entries/video/videoorder', 'VideoController@order');
    Route::post('/entries/video/{id}', 'VideoController@store');
    Route::get('/entries/video/{id}/{videoid}/delete', 'VideoController@destroy');

    //Facilities: Εκκλησίες - Δημαρχεία
    Route::post('/churches/search', 'AdminChurchController@search');
    Route::resource('/churches', 'AdminChurchController')->name('*', 'churches');

    // Μητροπόλεις
    Route::resource('/metropoles', 'AdminMetropolesController')->name('*', 'metropoles');

    // Δημαρχεία
    Route::post('/municipalities/search', 'MunicipalityController@search');
    Route::resource('/municipalities', 'MunicipalityController')->name('*', 'municipalities');


    // Tags
    Route::resource('/tags', 'AdminTagsController')->name('*', 'tags');

    // Tag Pages
    Route::post('/tagpagesorder', 'AdminTagPagesController@order');
    Route::resource('/tagpages', 'AdminTagPagesController')->name('*', 'tagpages');
    

    //Banners
    Route::post('/banners/entry', 'BannersController@cliententry');
    Route::resource('/banners', 'BannersController')->name('*','banners');

    //Sliders
    Route::post('/sliders/sliderentry', 'SlidersController@sliderentry');
    Route::resource('/sliders', 'SlidersController')->name('*', 'sliders');

    //News
    Route::post('/news/newsorder', 'NewsController@order');
    Route::resource('/news', 'NewsController')->name('*', 'news');

    // Testimonials
    Route::post('/testimonials/testimonialsorder', 'TestimonialsController@order');
    Route::resource('/testimonials', 'TestimonialsController')->name('*', 'testimonials');

});

//Clientarea
Route::prefix('clientarea')->group(function(){
    Route::get('/login', 'Auth\ClientLoginController@showLoginForm')->name('clientarea.login');
    Route::post('/login', 'Auth\ClientLoginController@login');
    Route::get('/logout', 'Auth\ClientLoginController@destroy');
    Route::get('/', 'ClientAreaController@index');
    Route::get('/', 'ClientAreaController@show');
    Route::get('/{entryid}/info', 'ClientAreaController@info');
    Route::put('/{entryid}/info', 'ClientAreaController@updateInfo');
    //Password reset
    Route::post('/password/email', 'Auth\ClientForgotPasswordController@sendResetLinkEmail')->name('client.password.email');
    Route::get('/password/reset', 'Auth\ClientForgotPasswordController@showLinkRequestForm')->name('client.password.request');
    Route::post('/password/reset', 'Auth\ClientResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\ClientResetPasswordController@showResetForm')->name('client.password.reset');
    //Albums
    Route::get('/{entryid}/albums', 'ClientAreaController@albums');
    Route::get('/{entryid}/albums/create', 'ClientAreaController@albumsCreate');
    Route::post('/{entryid}/albums', 'ClientAreaController@albumsStore');
    Route::get('/{entryid}/albums/{albumid}/edit', 'ClientAreaController@albumsEdit');
    // Route::put('/{clientid}/{entryid}/albums/{albumid}', 'ClientAreaController@albumsUpdate');
    Route::put('/{entryid}/albums/{albumid}/updatetitle', 'ClientAreaController@albumsUpdateTitle');
    Route::put('/{entryid}/albums/{albumid}/updatedescription', 'ClientAreaController@albumsUpdateDescription');
    Route::post('/{entryid}/albums/{albumid}/updatecover', 'ClientAreaController@albumsUpdateCover');
    Route::post('/{entryid}/albums/albumorder', 'ClientAreaController@albumsOrder');
    Route::get('/{entryid}/albums/{albumid}/delete', 'ClientAreaController@albumsDestroy');
    //Images
    Route::post('/{entryid}/{albumid}', 'ClientAreaController@imagesStore');
    Route::post('/{entryid}/albums/{albumid}/imageorder', 'ClientAreaController@imagesOrder');
    Route::get('/{albumid}/{imageid}/delete', 'ClientAreaController@imageDestroy');
    Route::post('{entryid}/albums/{albumid}/{imageid}', 'ClientAreaController@imageStoreDescription');
    Route::post('/{entryid}/albums/{albumid}/{imageid}/edit', 'ClientAreaController@changeAlbum');
    //Διαχείριση video
    Route::get('/{entryid}/video', 'ClientAreaController@videoIndex');
    Route::post('/{entryid}/video/videoorder', 'ClientAreaController@videoOrder');
    Route::post('/{entryid}/video/addvideo', 'ClientAreaController@videoStore');
    Route::get('/{entryid}/video/{videoid}/delete', 'ClientAreaController@videoDestroy');
});


//FrontEnd
// Route::get('/lucky', 'FrontEndController@feelingLucky');
Route::get('/sitemap.xml', 'SitemapController@index');

Route::get('/ekklisies.php', 'FrontEndChurchController@churchesPage');
Route::post('/ekklisies.php', 'FrontEndController@searchChurchesPage');
Route::get('/dimarxeia.php', 'FrontEndController@municipalitiesPage');
Route::post('/dimarxeia.php', 'FrontEndController@searchMunicipalitiesPage');
Route::get('/bridal-expo.php', 'FrontEndController@bridalexpo');
Route::get('testimonials.php', 'FrontEndTestimonialsController@show');
Route::post('/emailsent', 'FrontEndController@emailsent');
Route::post('/phonecall', 'FrontEndController@phonecall');
Route::get('/rotation', 'RotationExpirationController@entriesRotation');
Route::get('/expirations', 'RotationExpirationController@entriesExpiration');
// Route::get('/changedates', 'RotationExpirationController@changeDates');
Route::get('/slidersexpirations', 'RotationExpirationController@slidersExpiration');
Route::get('/tags/{slug}', 'TagPagesController@show');
Route::get('/thanks', 'FrontEndController@thanks');
Route::get('/{slug}', 'FrontEndController@pages')->name('page');
Route::post('/{slug}', 'FrontEndController@infoPageContact');
Route::get('/{district}', 'FrontEndController@district')->name('district');
Route::get('/{district}/dimarxeia', 'FrontEndController@municipalities');
Route::post('/{district}/dimarxeia', 'FrontEndController@searchMunicipality');
Route::get('/{district}/dimarxeia/{municipality}', 'FrontEndController@municipalityEntry');

Route::get('/{district}/ekklisies', 'FrontEndMetropoleController@index');
Route::post('/{district}/ekklisies', 'FrontEndMetropoleController@select');
Route::get('/{district}/ekklisies/{metropole}', 'FrontEndMetropoleController@show');
Route::post('/{district}/ekklisies/{metropole}', 'FrontEndChurchController@search');
Route::get('/{district}/ekklisies/{metropole}/{church}', 'FrontEndChurchController@show');

Route::get('/{district}/{category}', 'FrontEndController@categories');
Route::get('/{district}/{category}/{entry}', 'FrontEndController@entry')->name('profile');
Route::get('/{district}/{category}/{entry}/photos', 'FrontEndController@albums')->name('albums');
Route::get('/{district}/{category}/{entry}/photos/{albumid}', 'FrontEndController@album')->name('photos');
Route::get('/{district}/{category}/{entry}/video', 'FrontEndController@video')->name('video');
Route::get('/{district}/{category}/{entry}/facebook', 'FrontEndController@facebook')->name('facebook');
Route::get('/{district}/{category}/{entry}/contact', 'FrontEndController@contact')->name('contact');
Route::post('/{district}/{category}/{entry}/contact', 'FrontEndController@sendMail');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
