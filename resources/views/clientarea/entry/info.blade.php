@extends('clientarea.layouts.master')

@section('leftnav')
    @include('clientarea.layouts.leftnav')
@endsection

@section('content')
@if(Auth::user()->id == $entry->client_id)

<div class="col-md-12">
    <h1>Στοιχεία Καταχώρησης  </h1>
    <hr>

    @include('admin.layouts.errors')
    {!! Form::open(['action'=>['ClientAreaController@updateInfo', $entry->id], 'method'=>'POST', 'enctype' => 'multipart/form-data']) !!}

    <div class="form-row">

        <div class="col-md-12" role="">
        <h2> {{$entry->name}} </h2>
            Κατηγορία: {{$entry->category->cat_name}}<br> 
            Περιφέρεια: {{$entry->district->name}}
        </div>

        <div class="col-md-12">
            <hr>
        </div>

        <div class="form-group col-md-6">
            {{Form::label('logo', 'Logo')}}
            {{Form::file('logo')}}
            <img src="/storage/logos/{{$entry->cat_id}}/{{$entry->logo}}">
        </div>

        <div class="form-group col-md-12">
            {{Form::label('person', 'Υπεύθυνος')}}
            {{Form::text('person', $entry->person, ['class'=>'form-control', 'placeholder' => 'Όνομα Υπευθύνου'] )}}
        </div>

        <div class="form-group col-md-12">
            {{Form::label('address', 'Διεύθυνση')}}
            {{Form::text('address', $entry->address, ['class'=>'form-control', 'placeholder' => 'Διεύθυνση'] )}}
        </div>

        <div class="form-group col-md-4">
            {{Form::label('area', 'Περιοχή')}}
            {{Form::text('area', $entry->area, ['class'=>'form-control', 'placeholder' => 'Περιοχή - για περισσότερες, χωρίστε με κόμμα'] )}}
        </div>

        <div class="form-group col-md-4">
            {{Form::label('phone', 'Τηλέφωνο')}}
            {{Form::text('phone', $entry->phone, ['class'=>'form-control', 'placeholder' => 'Τηλέφωνο'] )}}
        </div>

        <div class="form-group col-md-4">
            {{Form::label('mobile', 'Κινητό')}}
            {{Form::text('mobile', $entry->mobile, ['class'=>'form-control', 'placeholder' => 'Κινητό'] )}}
        </div>

        <div class="form-group col-md-6">
            {{Form::label('website', 'Website')}}
            {{Form::text('website', $entry->website, ['class'=>'form-control', 'placeholder' => 'Website'] )}}
        </div>

        <div class="form-group col-md-6">
            {{Form::label('email', 'Email')}}
            {{Form::text('email', $entry->email, ['class'=>'form-control', 'placeholder' => 'Email'] )}}
        </div>

        <div class="form-group col-md-6">
            {{Form::label('facebook', 'Facebook')}}
            {{Form::text('facebook', $entry->facebook, ['class'=>'form-control', 'placeholder' => 'Facebook'] )}}
        </div>

        <div class="form-group col-md-6">
            {{Form::label('google', 'Google+')}}
            {{Form::text('google', $entry->google, ['class'=>'form-control', 'placeholder' => 'Google+'] )}}
        </div>

        <div class="form-group col-md-6">
            {{Form::label('twitter', 'Twitter')}}
            {{Form::text('twitter', $entry->twitter, ['class'=>'form-control', 'placeholder' => 'twitter'] )}}
        </div>

        <div class="form-group col-md-6">
            {{Form::label('instagram', 'Instagram')}}
            {{Form::text('instagram', $entry->instagram, ['class'=>'form-control', 'placeholder' => 'Instagram'] )}}
        </div> 

        <div class="form-group col-md-6">
            {{Form::label('pinterest', 'Pinterest')}}
            {{Form::text('pinterest', $entry->pinterest, ['class'=>'form-control', 'placeholder' => 'Pinterest'] )}}
        </div>

        <div class="form-group col-md-6">
            {{Form::label('youtube', 'YouTube')}}
            {{Form::text('youtube', $entry->youtube, ['class'=>'form-control', 'placeholder' => 'YouTube'] )}}
        </div>

        <div class="form-group col-md-6">
            {{Form::label('map', 'Google Maps')}}
            {{Form::text('map', $entry->map, ['class'=>'form-control', 'placeholder' => 'Google Maps'] )}}
        </div>
        
        <div class="col-md-12">
            <hr>
        </div>

        <div class="col-md-12">
            <hr>
        </div>

        <div class="form-group ">
            {{Form::label('image', 'Εικόνα Προφίλ')}}: <span>Μέγιστο πλάτος: 850px - Μόνο αρχεία .jpg</span>
            {{Form::file('image')}}
            <img src="/storage/entries/{{$entry->cat_id}}/{{$entry->image}}">
        </div>

        <div class="form-group col-md-12">
            <label for="profile">Κείμενο Προφίλ</label>        
            <textarea class="form-control tinyMCE" id="profile" name="profile" >{{$entry->profile}}</textarea>
        </div> 


        <div class="col-md-12">
            <hr>
        </div>
        

        <a href="albums" class="btn btn-default mr-md-3">Φωτογραφίες</a>
        <a href="/admin/entries/video/{{$entry->id}}" class="btn btn-default mr-md-3">Video</a>

        <div class="col-md-12">
            <hr>
        </div>

        <div class="form-group col-md-12">    
            <div class="col-md-2">
                {{Form::hidden('_method', 'PUT')}}
                {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
            </div>

            <div class="col-md-2">
                {{Form::hidden('_method', 'PUT')}}
                {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
            </div>

            <div class="col-md-2">  
                <a href="/admin/entries" class="btn btn-warning pull-right">Cancel</a>
            </div>

        </div>
    </div>
    {!! Form::close() !!}
</div>
@else
<span class="text text-danger">Δεν μπορείτε να δείτε αυτό το περιεχόμενο</span>
@endif
@endsection