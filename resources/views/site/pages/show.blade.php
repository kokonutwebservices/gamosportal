@extends ('site.layouts.master')

@section('metatags')
    <title>{{ $page->metatitle }}</title>
    <meta name="description" content="{{ $page->metadescription }}">
    <meta name="keywords" content="{{ $page->metakeywords }}">
    <meta property="og:type" content="website" />
    <meta property='og:url' content='https://gamosportal.gr/{{ $page->slug }}'>
    <meta property='og:title' content='{{ $page->title }}'>
    <meta property='og:description' content='{{ $page->description }}'>
    <meta property='og:locale' content='el_GR'>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@gamosportal" />
    <meta name="twitter:creator" content="@gamosportal" />
    <link rel="canonical" href="https://gamosportal.gr/{{ $page->slug }}">
    @if (count($page->category->entries))
        <meta property='og:image'
            content="https://gamosportal.gr/storage/entries/{{ $page->category->entries->first()->cat_id }}/{{ $page->category->entries->first()->image }}">
    @else
        <meta property='og:image' content="https://gamosportal.gr/storage/step.png">
    @endif
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"
        integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"
        integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('head-scripts')
    <!-- Event snippet for Nyfika Page View conversion page -->
    <script>
        gtag('event', 'conversion', {
            'send_to': 'AW-957693429/hwv7CPK74pMCEPX71MgD'
        });
    </script>
@endsection

@section('content')

    <section id="slider">
        <div class="container">
            <div class="row mt-4">
                <ol class="breadcrumb my-0" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="/">
                            <span itemprop="name">Αρχική</span>
                        </a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope
                        itemtype="https://schema.org/ListItem">
                        <span itemprop="name">{{ $page->title }}</span>
                        <meta itemprop="position" content="2" />
                    </li>
                </ol>

            </div>
            <div class="row">
                <div class="showcase col-md-12 px-0">
                    <h1>{{ $page->title }} </h1>

                    @if ($sliders->count())
                        <div id="carousel-slider">
                            @foreach ($sliders as $slider)
                                {{-- Carousel by http://kenwheeler.github.io/slick/ --}}
                                <div class="carousel slide">
                                    <a href="{{ $slider->url }}" target="{{ !$slider->entry ? '_blank' : '' }}">
                                        <img src="/storage/sliders/{{ $slider->image }}"
                                            alt="{{ $slider->entry ? $slider->entry->name : $page->title }}"
                                            title="{{ $slider->entry ? $slider->entry->name : $page->title }}"
                                            class="d-block w-100" style="width:1200px;">
                                        @if ($slider->entry || $slider->description)
                                            <div class="caption d-md-block">
                                                @if ($slider->entry)
                                                    <span class="slider-title">{{ $slider->entry->name }}</span>
                                                @endif
                                                <div class="d-none d-md-block">{!! $slider->description !!}</div>
                                            </div>
                                        @endif
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </section>
    <section id="main">
        <div class="container">
            <div class="row">

                <div class="page-description">
                    <p> {!! $page->body !!} </p>
                </div>
                <div class="col-md-9">
                    <div class="latest">
                        <h2 class="pt-4">{{ $page->title2 }}</h2>
                        <div class="d-flex flex-wrap justify-content-end mb-3 mr-3">
                            @foreach (Pages::districtsList() as $district)
                                @if ($district->entries->where('cat_id', $page->category->id)->count())
                                    <a href="{{ $district->slug }}/{{ $page->category->cat_slug }}"
                                        class="btn btn-dark mx-1 mb-2">{{ $district->name }}</a>
                                @endif
                            @endforeach
                        </div>
                        <div class="d-flex flex-wrap">
                            @foreach ($entries as $entry)
                                @if (Carbon\Carbon::parse($entry->end_at)->addMonth() > Carbon\Carbon::now())
                                    <div class="latest-item col-md-6 col-lg-4 pull-left">
                                        @if ($entry->image)
                                            <a
                                                href="/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}/{{ $entry->slug }}">
                                                <img class="thumbnail"
                                                    src="storage/entries/{{ $entry->cat_id }}/small/{{ $entry->image }}"
                                                    alt="{{ $entry->category->cat_name }} {{ $entry->name }}"
                                                    title="{{ $entry->category->cat_name }} {{ $entry->name }}"
                                                    loading="lazy">
                                            </a>
                                        @endif
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a
                                                    href="/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}/{{ $entry->slug }}">{{ $entry->name }}</a>
                                            </h4>
                                            <span class="district my-3">
                                                <i class="far fa-map"></i>
                                                <a class="text-dark"
                                                    href="/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}">
                                                    {{ $entry->district->name }}
                                                </a>
                                            </span>
                                            <div class="clearfix"></div>
                                            <div class="mt-3" itemprop="description">
                                                {!! \Illuminate\Support\Str::words(strip_tags($entry->profile), 25) !!} <a
                                                    href="/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}/{{ $entry->slug }}"
                                                    class=""><i class="fas fa-external-link-alt"
                                                        aria-hidden="true"></i></a>
                                            </div>
                                            @if (count($entry->tags))
                                                <span><i class="fas fa-tags mt-3"></i></span>
                                            @endif
                                            @foreach ($entry->tags as $tag)
                                                @if ($tag && $tag->page)
                                                    <a href="/tags/{{ $tag->page->slug }}"
                                                        class="tag">{{ $tag->name }}</a>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>

                    @if ($page->extratext)
                        <div class="faq">
                            <h2>Συχνές ερωτήσεις</h2>
                            {!! $page->extratext !!}
                        </div>
                    @endif

                </div>

                <div class="col-md-3">
                    @if (count($tags))
                        <div id="tagpages-list" class="sidebar-element">
                            <h3 class="mt-0 mb-4">Δημοφιλείς Αναζητήσεις</h3>
                            <nav>
                                @foreach ($tags as $tag)
                                    @if ($tag && $tag->page)
                                        <a href="tags/{{ $tag->page->slug }}"
                                            class="nav-link list-group-item">{{ $tag->page->title }}</a>
                                    @endif
                                @endforeach
                            </nav>
                        </div>
                    @endif
                    @if (count($page->banners))
                        <div id="banners" class="sidebar-element">
                            @foreach ($page->banners as $banner)
                                <div>
                                    @if ($banner->expires > Carbon\Carbon::now())
                                        @if ($banner->entry_id)
                                            <a href="{{ $banner->url }}">
                                                <img src="/storage/banners/{{ $banner->banner }}" class="thumbnail"
                                                    alt="Διαφημιστικό Banner {{ $banner->url }}">
                                            </a>
                                        @else
                                            <a href="{{ $banner->url }}?utm_source=gamosportal&utm_medium=referral"
                                                target="_blank" rel="nofollow">
                                                <img src="/storage/banners/{{ $banner->banner }}" class="thumbnail"
                                                    alt="Διαφημιστικό Banner {{ $banner->url }}">
                                            </a>
                                        @endif
                                    @endif

                                </div>
                            @endforeach
                        </div>
                    @endif

                    <div id="categories" class="sidebar-element">
                        <h3 class="mt-0">Βρείτε στο Gamos Portal: </h3>
                        <nav>
                            @foreach ($pages as $page)
                                <a href="{{ $page->slug }}" class="nav-link list-group-item">{{ $page->title }}</a>
                            @endforeach
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if ($blogItems && $blogItems->count())
        <section id="blog" class="">
            <div class="container">
                <div class="row">
                    <div class="news">
                        <div class="border-b-2 border-b-black w-1/2">
                            <h2 class="uppercase md:px-12">Σχετικά Άρθρα από το Gamos Portal Blog</h2>
                        </div>

                        @foreach ($blogItems as $item)
                        <div class="news-item" itemscope itemtype="https://schema.org/Article">
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="/blog/{{ $item->category_alias }}/{{ $item->alias }}" target="_blank" rel="nofollow" itemprop="mainEntityOfPage">
                                        <img loading="lazy" class="mr-3 px-0"
                                             src="https://gamosportal.gr/blog/media/k2/items/cache/{{ md5('Image' . $item->id) }}_L.jpg"
                                             alt="{{ $item->title }}" itemprop="image">
                                    </a>
                                </div>
                                <div class="col-md-9">
                                    <h3 class="mt-md-0" itemprop="headline">{{ $item->title }}</h3>
                                    <div class="d-none d-md-block" itemprop="articleBody">{!! $item->introtext !!}</div>
                                    
                                    <!-- Additional meta tags for structured data -->
                                    <meta itemprop="datePublished" content="{{ \Carbon\Carbon::parse($item->publish_up)->toIso8601String() }}">
                                    <meta itemprop="dateModified" content="{{ \Carbon\Carbon::parse($item->modified)->toIso8601String() }}">
                                    <meta itemprop="author" content="GamosPortal.gr"> <!-- Replace 'Author Name' accordingly -->
                                    <meta itemprop="publisher" content="GamosPortal.gr"> <!-- Replace with actual publisher -->
                
                                </div>
                            </div>
                        </div>
                    @endforeach
                    
                    </div>
                </div>
            </div>
        </section>
    @endif

    </div>
@endsection
