@extends('admin.layouts.master')

@section('content')

<h1>Επεξεργασία slide {{$slider->entry ? $slider->entry->name : ''}}</h1>
{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif
<hr>

{!! Form::open(['method'=>'POST', 'action'=>['SlidersController@update', $slider->id], 'enctype'=>'multipart/form-data']) !!}
<div class="form-group">
    <label for="entry">Καταχώρηση: </label>
    <select class="form-control" name="entry_id" id="sliderentry" required>
      <option selected>Επιλέξτε Καταχώρηση</option>
      @foreach($entries as $entry)
          @if($slider->entry_id == $entry->id)
              <option value="{{$entry->id}}" selected>{{$entry->name}}</option>  
          @else
              <option value="{{$entry->id}}">{{$entry->name}}</option>  
          @endif              
      @endforeach
      <option value="0">Καμία</option>
    </select>
</div>

<div class="form-group">
    <label for="image">Επιλέξτε αρχείο slide</label>
    <input type="file" class="form-control-file" name="image" id="slider" placeholder="" aria-describedby="fileHelpId">
    <small id="fileHelpId" class="form-text text-muted">Το αρχείο θα πρέπει να είναι .jpg, .png ή .gif</small>
    <img src="/storage/sliders/{{$slider->image}}" style="width:200px;">
</div>

<div class="form-group">
    <label for="description">Περιγραφή</label>
<textarea class="form-control tinyMCE" name="description" id="description" rows="3">{{$slider->description}}</textarea>
</div>

<div class="form-group">
    <label for="url"></label>
    <input type="text" class="form-control" name="url" id="sliderurl" aria-describedby="helpId" placeholder="" value="{{$slider->url}}">
    <small id="helpId" class="form-text text-muted">Προσθέστε το link</small>
</div>

<div class="form-group">
    <label for="expires">Ημερομηνία Λήξης</label>
    <input type="date" class="form-control date" name="expires" id="expires" aria-describedby="helpId" placeholder="" value="{{$slider->expires}}">
    <small id="helpId" class="form-text text-muted">Ημερομηνία της μορφής dd-mm-yyy</small>
</div>

<hr>

<div class="card">
    <div class="card-body">
        <div class="form-check">
            @foreach($pages as $page)
                <label class="form-check-label" for="{{$page->id}}">
                    @if($page->sliders()->where('slider_id', $slider->id)->count() > 0)
                        <input type="checkbox" class="form-check-input" name="page[]" id="{{$page->id}}" value="{{$page->id}}" checked>
                    {{$page->title}}
                    @else
                    <input type="checkbox" class="form-check-input" name="page[]" id="{{$page->id}}" value="{{$page->id}}">
                    {{$page->title}}
                    @endif            
                </label>   
            @endforeach
        </div>
    </div>
</div>

<hr>
<div class="form-group">    
    <div class="col-md-2">
        {{Form::hidden('_method', 'PUT')}}
        {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
    </div>

    <div class="col-md-2">
        {{Form::hidden('_method', 'PUT')}}
        {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
    </div>

    <div class="col-md-2">
        {{Form::hidden('_method', 'PUT')}}
        {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
    </div>

    <div class="col-md-2">  
        <a href="/admin/sliders" class="btn btn-warning pull-right">Cancel</a>
    </div>

    <div class="col-md-2">
        {!! Form::close() !!}
    
            {!! Form::open(['action' => ['SlidersController@destroy', $slider->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'pull-right', 'id' =>'delete' ]) !!}
    
            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
    
        {!! Form::close() !!}  
    </div>
</div>

{!! Form::close() !!}
    
@endsection