<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsFromPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn('image1');
            $table->dropColumn('caption_title_1');
            $table->dropColumn('caption_description_1');
            $table->dropColumn('link1');
            $table->dropColumn('image2');
            $table->dropColumn('caption_title_2');
            $table->dropColumn('caption_description_2');
            $table->dropColumn('link2');
            $table->dropColumn('image3');
            $table->dropColumn('caption_title_3');
            $table->dropColumn('caption_description_3');
            $table->dropColumn('link3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->string('image1');
            $table->string('caption_title_1');
            $table->text('caption_description_1');
            $table->string('link1');
            $table->string('image2');
            $table->string('caption_title_2');
            $table->text('caption_description_2');
            $table->string('link2');
            $table->string('image3');
            $table->string('caption_title_3');
            $table->text('caption_description_3');
            $table->string('link3');
        });
    }
}
