@extends('admin.layouts.master')

@section('content')

    <h1>Δημιουργία Κατηγορίας</h1>

    <div class="col-md-12">
    @include('admin.layouts.errors')

    {!! Form::open(['action' => ['CategoriesController@store'], 'method' => 'POST', 'enctype' => 'multipart/form-data' ]) !!}
    
    <div class="form-group">
        {{Form::label('cat_name', 'Όνομα Κατηγορίας')}}
        {{Form::text('cat_name', '', ['class' => 'form-control', 'required'])}}
    </div>

    <div class="form-group">
        {{Form::label('cat_slug', 'slug')}}
        {{Form::text('cat_slug', '', ['class' => 'form-control', 'required'])}}
    </div>

    <div class="form-group">
        {{Form::label('cat_description', 'Κείμενο περιγραφής κατηγορίας')}}
        {{Form::textarea('cat_description', '', ['class' => 'form-control tinyMCE'])}}
    </div>

    <div class="form-group">
        {{Form::label('cat_parent_id', 'Μητρική Κατηγορία')}}
        <select class="form-control" name="cat_parent_id">
            <option value="0">Καμία - Αυτή είναι μητρική</option>
            @foreach ($parents as $parent)                
                <option value="{{$parent->id}}">{{$parent->cat_name}}</option>               
            @endforeach
        </select>
    </div>

    <div class="form-group">
        {{Form::label('cat_order', 'Σειρά')}}
        {{Form::number('cat_order', '', ['class' => 'form-control', 'min' => '1'])}}
    </div>

    <div class="form-group">
        {{Form::label('cat_img', 'Εικόνα Κατηγορίας')}}
        {{Form::file('cat_img', ['id'=>'image'])}}
    </div>

    @foreach ($districts as $district)
        <?php 
            $i = $district->id;
            $title = 'meta_title_'.$i;
            $description = 'meta_description_'.$i;
            $keywords = 'meta_keywords_'.$i;
            
        ?>
        <ul class="list-group">
           <li class="list-group-item">
                <div class="form-group">
                    <h2>Meta Tags για {{$district->name}} </h2>
                    {{Form::label('meta_title_'.$i, "Meta Title")}}
                    {{Form::text('meta_title_'.$i, '', ['class' => 'form-control'])}}
                    {{Form::label('meta_description_'.$i, "Meta Description")}}
                    {{Form::text('meta_description_'.$i, '', ['class' => 'form-control'])}}
                    {{Form::label('meta_keywords_'.$i, "Meta Keywords")}}
                    {{Form::text('meta_keywords_'.$i, '', ['class' => 'form-control'])}}
                </div>
           </li>
        </ul>
    @endforeach

    <div class="form-group">    
            <div class="col-md-2">  
            {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
        </div>  

    <div class="col-md-2">
            {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
        </div>

        <div class="col-md-2">
            {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
        </div>

        <div class="col-md-2">  
            <a href="/admin/categories" class="btn btn-warning pull-right">Cancel</a>
        </div>  
    </div>


    {!! Form::close() !!}   

    </div>



@endsection