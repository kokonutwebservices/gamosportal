@extends('admin.layouts.master')

@section('content')
    <h1>Καταχωρήσεις Gamos Portal</h1>
    
    <div class="col-md-12">
    <a href="entries/create" class="btn btn-primary pull-left">Δημιουργία Καταχώρησης</a>
        <div class="form-group col-md-4 pull-right">
        {!!Form::open(['method' => 'POST', 'action' => 'EntriesController@search'])!!}
            <div class="input-group">
            <input type="text" class="form-control" name="query" placeholder="Αναζήτηση" aria-label="Αναζήτηση">
            <span class="input-group-btn">
                <button class="btn btn-secondary" type="submit">Go!</button>
            </span>
            </div>
        {!!Form::close()!!}
        </div>
    </div>
    <table class="table">
    <thead class="thead-inverse">
        <tr>
        <th>@sortablelink('id')</th>
        <th>@sortablelink('name', 'Όνομα Καταχώρησης')</th>
        <th>@sortablelink('cat_id', 'Κατηγορία')</th>
        <th>@sortablelink('dist_id', 'Περιοχή') </th>
        <th>@sortablelink('client_id', 'Πελάτης') </th>
        <th>@sortablelink('end_at', 'Ημ. Λήξης')</th>
        <th>Προβολή Καταχώρησης</th>
        </tr>
    </thead>
        @foreach($entries as $entry)
            <tr class="{{$entry->end_at < \Carbon\Carbon::now() ? "bg-danger text-light" : "" }}">
                <td>{{$entry->id}}</td>
                <td><a class="{{$entry->end_at < \Carbon\Carbon::now() ? "text-light" : ""}}" href="entries/{{$entry->id}}/edit">{{$entry->name}}</a></td>
                <td>{{$entry->category->cat_name}}</td>
                <td>{{$entry->district->name}}</td>
                <td>{{$entry->client->name}}</td>
                <td >{{$entry->end_at}}</td>
                <td class="text-center"><a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}" target="_blank"><i class="fas fa-link"></i></td>
            </tr>
        @endforeach
    </table>

    {{ $entries->links() }}

<div class="col-md-12">
    <a href="entries/create" class="btn btn-primary">Δημιουργία Καταχώρησης</a>
</div>


@endsection