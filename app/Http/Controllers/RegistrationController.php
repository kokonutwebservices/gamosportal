<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class RegistrationController extends Controller
{

    public function index()
    {
        $admins = User::all();

        return view('admin.registration.index', compact('admins'));
    }

    public function create()
    {

        return view('admin.registration.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]);

        $user = User::create(request(['name', 'email', 'password']));

        auth()->login($user);

        return redirect()->route('admin');
    }

    public function edit(User $user)
    {
        return view('admin.registration.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $attributes = $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'sometimes|'
        ]);
        
        
        if ($request->password) {
            $user->update($attributes);
        } else {
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
            ]);
        }

        return redirect()->route('admin.edit', $user);

   

    }
}
