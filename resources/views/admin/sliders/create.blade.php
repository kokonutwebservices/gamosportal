@extends('admin.layouts.master')

@section('content')

<h1>Δημιουργία Slider</h1>
@include('admin.layouts.errors')
<hr>

{!! Form::open(['method'=>'POST', 'action'=>'SlidersController@store', 'enctype'=>'multipart/form-data']) !!}

<div class="form-group">
    <label for="entry">Καταχώρηση: </label>
    <select class="form-control" name="entry_id" id="sliderentry" required>
        <option selected>Επιλέξτε Καταχώρηση</option>
            @foreach($entries as $entry)
                <option value="{{$entry->id}}">{{$entry->name}}</option>        
            @endforeach
        <option value="0">Καμία</option>
    </select>
</div>

<div class="form-group">
    <label for="image">Επιλέξτε εικόνα για το Slider</label>
    <input type="file" class="form-control-file" name="image" id="slider" placeholder="" aria-describedby="fileHelpId" required>
    <small id="fileHelpId" class="form-text text-muted">Το αρχείο θα πρέπει να είναι .jpg, .png ή .gif</small>
</div>

<div class="form-group">
  <label for="description">Περιγραφή</label>
  <textarea class="form-control tinyMCE" name="description" id="description" rows="3"></textarea>
</div>

<div class="form-group">
    <label for="url"></label>
    <input type="text" class="form-control" name="url" id="sliderurl" aria-describedby="helpId" placeholder="" required>
    <small id="helpId" class="form-text text-muted">Προσθέστε το link</small>
</div>

<div class="form-group">
    <label for="expires">Ημερομηνία Λήξης</label>
    <input type="date" class="form-control date" name="expires" id="expires" aria-describedby="helpId" placeholder="" required>
    <small id="helpId" class="form-text text-muted">Ημερομηνία της μορφής dd-mm-yyy</small>
</div>

<hr>

<h2>Τοποθέτηση</h2>
<hr>
<div class="card">
    <div class="card-body">
        <h4>Σελίδες Κατηγοριών </h4>
        <div class="form-check">
            @foreach($pages as $page)
                <label class="form-check-label" for="{{$page->id}}">
                    <input type="checkbox" class="form-check-input" name="page[]" id="{{$page->id}}" value="{{$page->id}}">
                    {{$page->title}}                     
                </label>   
            @endforeach
        </div>
    </div>
</div>

<hr>

<div class="form-group">    
    <div class="col-md-2">
        {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
    </div>

    <div class="col-md-2">
        {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
    </div>

    <div class="col-md-2">
        {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
    </div>

    <div class="col-md-2">  
        <a href="/admin/sliders" class="btn btn-warning pull-right">Cancel</a>
    </div>

</div>

{!! Form::close()!!}
    
@endsection