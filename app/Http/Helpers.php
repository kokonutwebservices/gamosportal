<?php

use App\Page;
use App\InfoPage;
use App\Album;
use App\Entry;
use App\District;
use App\Municipality;
use App\Church;
use App\Image;

class Pages {
    public static function pagesList()
    {
        $pages = Page::with('category.entries')->orderBy('order')->get();
        
        return $pages;
    }

    public static function infoPagesList()
    {
        $infoPages = InfoPage::all();
        return $infoPages;
    }

    public static function districtsList(){
        $districts = District::with('entries')->get();
        return $districts;
    }
}

//Παίρνει τις συντεταγμένες της κάθε καταχώρησης και τις χωρίζει σε μήκος και πλάτος
class Maps{
    public static function lat($map){
        $coord = explode(",", $map);
        $lat = $coord['0'];
        return $lat;
    }

    public static function lon($map){
        $coord = explode(",", $map);
        $lon = $coord['1'];
        return $lon;
    }
}

//Για το Έχετε επισκεφθεί
class Visited{
    public static function alsoVisited(){
        $alsovisited = array_slice(array_unique(Session::get('visited')), -6);
        return $alsovisited;
    }
}

class More{
    public static function moreEntries($entry){
        $more = Entry::where('cat_id', '<>', $entry->cat_id)->where('dist_id', $entry->dist_id)->inRandomOrder()->take(6)->get();
        return $more;
    }

    public static function entryCount(){
        $entries = Entry::all();
        return count($entries);
    }
}


class Meta{
    public static function title($district, $category){
        switch ($district->id){
            case 1: $metatitle = $category->meta_title_10;
            break;
            case 2: $metatitle = $category->meta_title_2;
            break;
            case 3: $metatitle = $category->meta_title_1;
            break;
            case 4: $metatitle = $category->meta_title_3;
            break;
            case 5: $metatitle = $category->meta_title_4;
            break;
            case 6: $metatitle = $category->meta_title_5;
            break;
            case 7: $metatitle = $category->meta_title_6;
            break;
            case 8: $metatitle = $category->meta_title_7;
            break;
            case 9: $metatitle = $category->meta_title_8;
            break;
            case 10: $metatitle = $category->meta_title_9;
            break;
            case 11: $metatitle = $category->meta_title_11;
            break;
            case 12: $metatitle = $category->meta_title_12;
            break;
            case 13: $metatitle = $category->meta_title_13;
            break;
        }
        return $metatitle;
    }

    public static function description($district, $category){
        switch ($district->id){
            case 1: $description = $category->meta_description_10;
            break;
            case 2: $description = $category->meta_description_2;
            break;
            case 3: $description = $category->meta_description_1;
            break;
            case 4: $description = $category->meta_description_3;
            break;
            case 5: $description = $category->meta_description_4;
            break;
            case 6: $description = $category->meta_description_5;
            break;
            case 7: $description = $category->meta_description_6;
            break;
            case 8: $description = $category->meta_description_7;
            break;
            case 9: $description = $category->meta_descriptione_8;
            break;
            case 10: $description = $category->meta_description_9;
            break;
            case 11: $description = $category->meta_description_11;
            break;
            case 12: $description = $category->meta_description_12;
            break;
            case 13: $description = $category->meta_description_13;
            break;
        }
        return $description;
    }

    public static function keywords($district, $category){
        switch ($district->id){
            case 1: $keywords = $category->meta_keywords_10;
            break;
            case 2: $keywords = $category->meta_keywords_2;
            break;
            case 3: $keywords = $category->meta_keywords_1;
            break;
            case 4: $keywords = $category->meta_keywords_3;
            break;
            case 5: $keywords = $category->meta_keywordse_4;
            break;
            case 6: $keywords = $category->meta_keywords_5;
            break;
            case 7: $keywords = $category->meta_keywords_6;
            break;
            case 8: $keywords = $category->meta_keywords_7;
            break;
            case 9: $keywords = $category->meta_keywordse_8;
            break;
            case 10: $keywords = $category->meta_keywords_9;
            break;
            case 11: $keywords = $category->meta_keywords_11;
            break;
            case 12: $keywords = $category->meta_keywords_12;
            break;
            case 13: $keywords = $category->meta_keywords_13;
            break;
        }
        return $keywords;
    }

    public static function entryTitle($entry){
       
        if (Request::segment(4) == 'photos'){
            if (Request::segment(5)){
                $album = App\Album::where('id', Request::segment(5))->first();
                return $entry->name." - Φωτογραφίες -".$album->name;
            } else 
            return $entry->name." - Φωτογραφίες";
        } elseif (Request::segment(4) == 'facebook'){
            return $entry->name." - Facebook Timeline";
        } elseif (Request::segment(4) == 'video'){
            return $entry->name." - Video";
        }
         else {
            return $entry->metatitle;
        }
    }

    public static function canonical($entry){
        if (Request::segment(4) == 'photos' OR Request::segment(4) == 'facebook' OR Request::segment(4) == 'video'){
            echo '<link rel="canonical" href="https://www.gamosportal.gr/'.$entry->district->slug.'/'. $entry->category->cat_slug .'/'.$entry->slug.'">';
        }
    }
}

class Facilities{
    public static function municipalities($district){
        $municipalities = Municipality::where('district_id', $district->id)->get();
        return $municipalities;
    }

    public static function churches($district){
        $churches = Church::where('district_id', $district->id)->get();
        return $churches;
    }
}

class Random{
    public static function randomImage(){
        $entry = Entry::where('active', '1')->get()->random();
        $album = Album::where('entry_id', $entry->id)->get()->random();
        $image = Image::where('album_id', $album->id)->get()->random();
        
        return $image;
    }
}
?>