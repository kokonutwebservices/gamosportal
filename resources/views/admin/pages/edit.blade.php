@extends('admin.layouts.master')

@section('content')

<h1>Επεξεργασία Σελίδας: {{$page->title}} </h1>

<form action="/admin/pages/{{$page->id}}" method="POST">
    @method('PUT')
    @csrf

    <div class="form-group">
      <label for="title">Τίτλος Σελίδας</label>
      <input type="text" class="form-control" name="title" id="title" placeholder="" value="{{$page->title}}">
      @error('title')
        <small class="form-text text-danger">{{$message}}</small>          
      @enderror
    </div>

    <div class="form-group">
      <label for="slug">Slug (friendly url)</label>
      <input type="text" class="form-control" name="slug" id="slug" placeholder="" value="{{$page->slug}}">
      @error('slug')
        <small class="form-text text-danger">{{$message}}</small>          
      @enderror
    </div>

    <div class="form-group">
      <label for="metatitle">Meta Title</label>
      <input type="text" class="form-control" name="metatitle" id="metatitle" placeholder="" value="{{$page->metatitle}}">
      @error('metatitle')
        <small class="form-text text-danger">{{$message}}</small>          
      @enderror
    </div>

    <div class="form-group">
      <label for="metadescription">Meta Description</label>
      <input type="text" class="form-control" name="metadescription" id="metadescription" placeholder="" value="{{$page->metadescription}}">
      @error('metadescription')
        <small class="form-text text-danger">{{$message}}</small>          
      @enderror
    </div>
    
    <div class="form-group">
      <label for="metakeywords">Meta Keywords</label>
      <input type="text" class="form-control" name="metakeywords" id="metakeywords" placeholder="" value="{{$page->metakeywords}}">
      @error('metakeywords')
        <small class="form-text text-danger">{{$message}}</small>          
      @enderror
    </div>

    <div class="form-group">
      <label for="body">Κείμενο</label>
      <textarea class="form-control tinyMCE" name="body" id="body" rows="3" >{{$page->body}}</textarea>
      @error('body')
        <small class="form-text text-danger">{{$message}}</small>          
      @enderror
    </div>

        <div class="form-group">
      <label for="extratext">Extra Κείμενο</label>
      <textarea class="form-control " name="extratext" id="extratext" rows="3" >{{$page->extratext}}</textarea>
      @error('extratext')
        <small class="form-text text-danger">{{$message}}</small>          
      @enderror
    </div>

    <div class="form-group">
      <label for="title2">Τίτλος 2 (πάνω από τις καταχωρήσεις)</label>
      <input type="text" class="form-control" name="title2" id="title2" placeholder="" value="{{$page->title2}}">
      @error('title2')
        <small class="form-text text-danger">{{$message}}</small>          
      @enderror
    </div>

    <div class="form-group">
      <label for="category_id">Σχετική Κατηγορία</label>
      <select class="form-control" name="category_id" id="category_id">
        <option disabled>Επιλέξτε σχετική κατηγορία</option>
        @foreach ($categories as $category)

            <option {{$category->id == $page->category_id ? 'selected' : ''}} value="{{$category->id}}">{{$category->cat_name}}</option>
            
        @endforeach
      </select>
    </div>

    <div class="d-flex justify-content-around">
        @include('admin.layouts.savebuttons')
        <a href="/admin/pages" class="btn btn-warning">Cancel</a>
   


</form>

    <form action="/admin/pages/{{$page->id}}" method="POST" id="delete">
    @method('DELETE')
    @csrf

    <button type="submit" class="btn btn-danger">Delete</button>

    </form>
    </div>

@endsection
