<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChurchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('churches', function (Blueprint $table) {
            $table->increments('id');
            $table->text('district_id');
            $table->text('name');
            $table->text('slug');
            $table->text('person')->nullable();
            $table->text('address')->nullable();
            $table->text('area')->nullable();
            $table->text('phone')->nullable();
            $table->text('mobile')->nullable();
            $table->text('website')->nullable();
            $table->text('email')->nullable();
            $table->text('map')->nullable();            
            $table->text('image')->nullable();
            $table->text('profile')->nullable();
            $table->text('metatitle')->nullable();
            $table->text('metadescription')->nullable();
            $table->text('metakeywords')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('churches');
    }
}
