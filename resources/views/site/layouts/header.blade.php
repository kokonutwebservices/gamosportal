<!-- Logo και Top Banners-->
<div class="col-md-4 logo">
	<a href="/" ><img loading="eager" class="d-block mx-auto" src="/storage/header/logo.png" alt="Gamos Portal Logo" title="Gamos Portal - Ο Γάμος σας στα δάχτυλά σας"></a>
</div>
<div class="col-md-8 d-none d-md-block header-right">
	<div class="pull-right title">
		<img loading="eager" class="moto" src="/storage/moto.png" alt="Σε κάθε βήμα του γάμου, Gamos Portal.">		
	</div>
	<ul class="nav headersocial">
		<li><a class="px-2" href="https://www.facebook.com/gamos.portal" target="_blank" rel="nofollow"><i class="fab fa-facebook fa-lg"></i></a></li>
      <li><a class="px-2" href="https://www.instagram.com/gamosportal" target="_blank" rel="nofollow"><i class="fab fa-instagram fa-lg"></i></a></li>
      <li><a class="px-2" href="https://www.pinterest.com/gamosportal" target="_blank" rel="nofollow"><i class="fab fa-pinterest fa-lg"></i></a></li>
      <li><a class="px-2" href="https://www.youtube.com/channel/UCVCkcaCepRwhu-8BWn7ggoQ" target="_blank" rel="nofollow"><i class="fab fa-youtube fa-lg"></i></a></li>      
      {{-- <li><a class="px-2" href="https://www.twitter.com/gamosportal" target="_blank" rel="nofollow"><i class="fab fa-twitter fa-lg"></i></a></li> --}}
	</ul>
</div>