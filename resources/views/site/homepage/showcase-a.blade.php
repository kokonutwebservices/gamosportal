@php
    $showcase_a = App\Home::where('position', 'Showcase A')->first();
    $banner = $showcase_a->banners()->first();                
@endphp
@if($banner AND $banner->expires > Carbon\Carbon::now())
    <a href="{{$banner->url}}">
        <img class="mx-auto p-1" src="storage/banners/{{$banner->banner}}" alt="Κάντε κλικ για να δείτε την εταιρεία {{isset($banner->entry) ? $banner->entry->name : " "}}" title="Κάντε κλικ για να δείτε την εταιρεία {{isset($banner->entry) ? $banner->entry->name : " "}}"></a>    
    {{-- <div class="text-right pr-2 banner-desc"><i class="fab fa-gratipay"></i> {{{$banner->entry->name}}} - {{$banner->entry->category->cat_name}}</div></a> --}}
@endif