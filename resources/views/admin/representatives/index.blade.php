@extends('admin.layouts.master')

@section('content')

<h1>Πωλητές Gamos Portal</h1>

<ul class="list-group">
    @foreach ($representatives as $representative)
        <li class="list-group-item"><a href="/admin/representatives/{{$representative->id}}/edit">{{$representative->name}}</a></li>    
    @endforeach
</ul>


<a href="/admin/representatives/create" class="btn btn-primary mt-5">Προσθήκη Πωλητή</a>
    
@endsection