@extends('admin.layouts.master')

@section('content')

<h1>Προσθήκη Μητρόπολης</h1>

<form action="/admin/metropoles" method="POST">

@csrf

<div class="form-group">
  <label for="district_id">Περιφέρεια</label>
  <select class="form-control" name="district_id" id="district_id" value="{{old('district_id')}}">
    <option disabled selected>Επιλέξτε Περιφέρεια</option>
    @foreach ($districts as $district)
        <option {{old('district_id') == $district->id ? 'selected' : ''}} value="{{$district->id}}">{{$district->name}}</option>
    @endforeach
  </select>
  @error('district_id')
    <small id="helpId" class="form-text text-danger">{{$message}}</small>      
  @enderror
</div>

<div class="form-group">
  <label for="name">Ονομασία Μητρόπολης</label>
  <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="π.χ. Μητρόπολη Ν. Ιωνίας" value="{{old('name')}}">
  @error('name')
    <small id="helpId" class="form-text text-danger">{{$message}}</small>      
  @enderror
</div>

<div class="form-group">
  <label for="areas">Περιοχές που καλύπτει</label>
  <input type="text" class="form-control" name="areas" id="areas" aria-describedby="helpId" placeholder="π..χ Νέα Ιωνία, Ηράκλειο">
</div>

  <div class="d-flex justify-content-around">
    <button type="submit" class="btn btn-primary" name="submitbutton" value="saveclose">Save & Close</button>
    <button type="submit" class="btn btn-success" name="submitbutton" value="savestay">Save & Stay</button>
    <button type="submit" class="btn btn-secondary" name="submitbutton" value="savenew">Save & New</button>
    <a href="/admin/metropoles" class="btn btn-warning">Cancel</a>

  </div>

</form>
    
@endsection