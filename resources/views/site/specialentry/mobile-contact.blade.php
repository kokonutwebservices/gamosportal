<div class="logo d-flex align-items-center thumbnail">
    <img class="m-auto d-block" src="/storage/logos/special/{{$specialentry->logo}}" alt="Λογότυπο {{$specialentry->name}}" title="Λογότυπο {{$specialentry->name}}">
</div>
<div class="row my-2">
    @if ($specialentry->person)
    <div class="col-xs-12 mb-2">
            <i class="fas fa-user mr-3"></i> {{$specialentry->person}}
    </div>        
    @endif
    @if ($specialentry->address)
    <div id="address" class="col-xs-2 my-2">
            <i class="fas fa-map-marker-alt"></i>
    </div>   
    @endif
    @if ($specialentry->phone)
    <div id="phone" class="col-xs-2 my-2">
        <i class="fas fa-phone"></i>
    </div>
    @endif
    @if ($specialentry->mobile)
    <div id="mobile" class="col-xs-2 my-2">
        <i class="fas fa-mobile-alt"></i>
    </div>       
    @endif
    @if ($specialentry->website)
    <div id="website" class="col-xs-2 my-2">
        <i class="fas fa-globe"></i>
    </div>       
    @endif
    @if($specialentry->email)
    <div id="email" class="col-xs-2 my-2">
        <i class="fas fa-envelope"></i>
    </div>
    @endif
    <div class="col-xs-2 my-2">
        <a href="#contact-form">
            <i class="fas fa-envelope-open"></i>
        </a>        
    </div>
</div>
<div>
    <div id="showaddress" class="my-2" style="display:none;">
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <a href="https://www.google.com/maps/search/?api=1&query={{$specialentry->map}}" target="_blank" rel="nofollow"><i class="far fa-map"></i> <span itemprop="streetAddress"> {{$specialentry->address}} </span></a>
        </div>
    </div>
    <div id="showphone" class="my-2" style="display:none;">
        <a class="call" href="tel:+30{{$specialentry->phone}}"> Πατήστε για κλήση: {{$specialentry->phone}}</a>        
    </div>
    <div id="showmobile" class="my-2" style="display:none;">
        <a class="call" href="tel:+30{{$specialentry->mobile}}">Πατήστε για κλήση: {{$specialentry->mobile}}</a>        
    </div>
    <div id="showwebsite" class="my-2" style="display:none;">
        Website: <a href="http://{{$specialentry->website}}" target="_blank" rel="nofollow" itemprop="url">{{$specialentry->website}}</a>        
    </div>
    <div id="showemail" class="my-2" style="display:none;">
        <a class="email" href="mailto:{{$specialentry->email}}?subject=Είδα την καταχώρησή σας στο Gamos Portal">email: {{$specialentry->email}}</a>        
    </div>
</div>


