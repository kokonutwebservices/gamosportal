@extends('admin.layouts.master')

@section('content')

<h1>Επεξεργασία Δημοσίευσης</h1>
@include('admin.layouts.errors')
<hr>
{!!Form::open(['method' => 'post', 'action' => ['NewsController@update', $new->id], 'enctype' => 'multipart/form-data'])!!}

<div class="form-group">
{{Form::label('title', 'Τίτλος')}}
{{Form::text('title', $new->title, ['class' => 'form-control', 'required'])}}
</div>

<div class="form-group">
{{Form::label('image', 'Εικόνα')}}
{{Form::file('image', ['id'=>'image'])}}
<img src="/storage/news/{{$new->image}}" class="thumbnail" style="width:200px">
</div>

<div class="form-group">
{{Form::label('description', 'Κείμενο')}}
<textarea class="tinyMCE" name="description">{{$new->description}}</textarea>
</div>

<div class="form-group">
{{Form::label('url', 'Link')}}
{{Form::text('url', $new->url, ['class' => 'form-control', 'required'])}}
</div>

<div class="form-group">
            <div class="col-md-2">  
                {{Form::hidden('_method', 'PUT')}}  
                {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
            </div>  

            <div class="col-md-2">
                {{Form::hidden('_method', 'PUT')}}  
                {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
            </div>

            <div class="col-md-2">
                {{Form::hidden('_method', 'PUT')}}  
                {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
            </div>

            <div class="col-md-2">  
                <a href="/admin/news" class="btn btn-warning pull-right">Cancel</a>
            </div>

            <div class="col-md-2">
            {!! Form::close() !!}

                {!! Form::open(['action' => ['NewsController@destroy', $new->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'pull-right' ]) !!}

                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}

            {!! Form::close() !!}  
            </div>
        </div>


{!!Form::close()!!}
    
@endsection