@extends('admin.layouts.master')

@section('content')

<h1>Εκκλησίες</h1>

{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif

<div class="col-md-12">
    <div class="form-group col-md-4 pull-right">
    {!!Form::open(['method' => 'POST', 'action' => 'AdminChurchController@search'])!!}
        <div class="input-group">
        <input type="text" class="form-control" name="query" placeholder="Αναζήτηση" aria-label="Αναζήτηση">
        <span class="input-group-btn">
            <button class="btn btn-secondary" type="submit">Go!</button>
        </span>
        </div>
    {!!Form::close()!!}
    </div>
</div>

<table class="table">
<thead class="thead-inverse">
    <tr>
    <th>@sortablelink('id')</th>
        <th>@sortablelink('name', 'Όνομα Εκκλησίας')</th>
        <th>@sortablelink('metropole_id', 'Μητρόπολη')</th>
        <th>@sortablelink('district_id', 'Περιφέρεια') </th>
    </tr>
</thead>

    @foreach($churches as $church)
        <tr>
            <td>{{$church->id}}</td>
            <td><a href="/admin/churches/{{$church->id}}/edit">{{$church->name}}</a></td>
            <td>
                @if ($church->metropole)
                    {{$church->metropole->name}}</td>
                @endif 
            <td>{{$church->district->name}}</td>
        </tr>
    @endforeach

</table>

{{ $churches->links() }}

<div>
    <a href="churches/create" class="btn btn-primary">Προσθήκη Νέας Εκκλησίας</a>
</div>
    
@endsection