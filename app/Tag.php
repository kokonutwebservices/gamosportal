<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = []; 
    protected $with = ['tagpage'];

    public function page () {
        return $this->hasOne(TagPage::class);
    }

    public function entries() {
        return $this->belongsToMany(Entry::class);
    }

    public function tagpage()
    {
        return $this->belongsTo(TagPage::class);
    }
}
