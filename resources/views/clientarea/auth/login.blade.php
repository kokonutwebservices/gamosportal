@extends('clientarea.layouts.master')

@section('content')

<div class="col-md-6">

        <h1>Clients Login</h1>
        <p>Καλωσήλθατε στην Περιοχή Διαχείρισης του Gamos Portal.</p>
        <p>Παρακαλούμε εισάγετε τα στοιχεία εισόδου.</p>

        {!! Form::open(['action'=> 'Auth\ClientLoginController@login', 'method' => 'POST']) !!}

        <div class="form-group">
            {{Form::label('email', 'Email')}}
            {{Form::email('email', '', ['class'=>'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::label('password', 'Password')}}
            {{Form::password('password', ['class'=>'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::submit('Είσοδος', ['class'=>'btn btn-primary'])}}
            <a href="{{ route ('client.password.request') }}" class="pull-right">Ξέχασα τον κωδικό μου</a>
        </div>      

        {!! Form::close() !!}
        
        @include('admin.layouts.errors')

    </div>
    
@endsection