@extends ('site.layouts.master')

@section('metatags')
    <title>Οι επαγγελματίες της Bridal Expo που προβάλλονται στο Gamos Portal </title>
    <meta name="description" content="Δείτε ποιοι επαγγελματίες που βρίσκετε στο Gamos Portal συμμετέχουν στην Bridal Expo" >
    <meta name="keywords" content="" >
@endsection

@section ('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section id="slider">
    <div class="container">
        <div class="row">
            <div class="showcase col-md-12">   
                <h1>Gamos Portal - Bridal Expo: δείτε από κοντά κορυφαίους επαγγελματίες</h1>     
                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="2000">
                    <div class="carousel-inner">
                        {{--  <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>  --}}
                        {{-- <div class="carousel-item active" data-slide="1">                            
                            <img class="d-block w-100" src="{{ asset('storage/sliders/bridal-expo-slider.jpg') }}" alt="Bridal Expo - Bridal Fashion Week" title="Bridal Expo - Bridal Fashion Week">
                                                    
                        </div> --}}
                        {{--  @if($page->image2)                        
                            <div class="carousel-item " data-slide="2">
                                <a href="{{$page->link2}}">
                                    <img class="d-block w-100" src="storage/pages/{{$page->image2}}"alt="{{$page->title}} " title="{{$page->title}} - {{$page->caption_title_1}}">
                                    <div class="carousel-caption d-none d-md-block">
                                            <h3>{{$page->caption_title_2}}</h3>
                                            <p>{{$page->caption_description_2}}</p>
                                    </div>
                                </a>
                            </div>                        
                        @endif
                        @if($page->image3)
                            <div class="carousel-item" data-slide="3">
                                <a href="{{$page->link2}}">
                                    <img class="d-block w-100" src="images/pages/{{$page->image3}}" alt="{{$page->title}} " title="{{$page->title}} - {{$page->caption_title_1}}">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h3>{{$page->caption_title_3}}</h3>
                                            <p>{{$page->caption_description_3}}</p>
                                    </div>
                                 </a>
                            </div>
                        @endif  --}}
                    </div>
                </div>
            </div>
        </div>    
    </div>
</section>
<section id="main">
<div class="container">
    <div class="row">
        <div class="page-description">
            <p> Η Bridal Expo είναι η πιο λαμπερή έκθεση για μελλόνυμφους, η οποία πραγματοποιείται κάθε χρόνο στο Ζάππειο, με την συμμετοχή των κορυφαίων επιχειρήσεων στον χώρο του γάμου

                    Το Gamos Portal είναι χορηγός επικοινωνίας της έκθεσης και εκθέτης. Στο stand μας μπορείτε να μας συναντήσετε και να κερδίσετε πολλές μικρές και μεγάλες εκπλήξεις.
                    
                    Σε αυτή την σελίδα μπορείτε να δείτε τους επαγγελματίες που βρίσκετε στο Gamos Portal και θα συναντήσετε και στην Bridal Expo, έτσι ώστε να γνωρίζετε από πριν την δουλειά και τις προτάσεις τους, αλλά και να μπορείτε να ανατρέξετε ξανά μετά την έκθεση. </p>
        </div>
        <div class="col-md-9 latest">
            <h2>Δείτε επιχειρήσεις που προβάλλονται στο Gamos Portal και συμμετέχουν στη Bridal Expo</h2>
            <div class="d-flex flex-wrap">
                @foreach($entries as $entry)
                @if (Carbon\Carbon::parse($entry->end_at)->addMonth() > Carbon\Carbon::now())
                    <div class="latest-item col-md-4 pull-left">
                        @if($entry->image)
                            <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}"><img class="thumbnail" src="storage/entries/{{$entry->cat_id}}/{{$entry->image}}" alt="Card image cap"></a>
                        @endif
                        <div class="card-body">
                            <h4 class="card-title"><a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}">{{$entry->name}}</a></h4>
                            <small> {{$entry->district->name}} </small>
                            {!! \Illuminate\Support\Str::words($entry->profile, 25) !!} <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}" class=""><i class="fas fa-external-link-alt" aria-hidden="true"></i></a>                           
                        </div>
                    </div>                    
                @endif
                @endforeach            
            </div>
        </div>
        <div class="col-md-3">
            <div id="categories" class="sidebar-element">
                <h3>Βρείτε στο Gamos Portal: </h3>
                <nav>
                @foreach ($pages as $page)
                    <a href="{{$page->slug}}" class="nav-link">{{$page->title}}</a>
                @endforeach
                </nav>
            </div>
            @if (count($entries) > 8)
            <div class="fb-page" 
            data-href="https://www.facebook.com/gamos.portal"
            data-tabs="timeline"
            data-height="820" 
            data-small-header="false" 
            data-adapt-container-width="true" 
            data-hide-cover="false" 
            data-show-facepile="true">
            <blockquote cite="https://www.facebook.com/gamos.portal" 
            class="fb-xfbml-parse-ignore">
                <a href="https://www.facebook.com/gamos.portal">Gamos Portal</a>
            </blockquote>
        </div>
            @endif
        </div>
    </div>
</div>
</section>
</div>    
@endsection