<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialImage extends Model
{
    
    public function specialentry(){

        return $this->belongsTo(SpecialEntry::class);

    }
}
