<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Municipality extends Model
{
    use Sortable;
    protected $guarded = ['_token'];

    public $sortable = ['id', 'name', 'district_id'];

    public function district(){
        return $this->belongsTo(District::class);
    }

    public function banners(){
        return $this->belongsToMany(Banner::class);
    }
}
