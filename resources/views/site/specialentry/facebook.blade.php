@extends('site.specialentry.show')

@section('specialentrycontent')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="d-flex justify-content-center">
    <div class="fb-page" data-href="{{$specialentry->facebook}}" data-tabs="timeline" data-width="500" data-height="800" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
</div>
<div class="col-md-12">
  <div class="float-left">
      <a href="/specialentris/{{$specialentry->slug}}/photos" class="btn btn-default float-right my-3" ><i class="fas fa-arrow-alt-circle-left"></i> Φωτογραφίες </a>
  </div>
  <div class="float-right">
        <a href="/specialentries/{{$specialentry->slug}}" class="btn btn-default float-right my-3" ><i class="fas fa-arrow-alt-circle-right"></i> Προφίλ </a>
    </div>
</div>
@endsection