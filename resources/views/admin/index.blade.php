@extends('admin.layouts.master')

@section('content')

<div class="row mt-5">
    <div class="col-md-4 mb-5">
        <div class="card-deck">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Πελάτες</h4>
                    <div>
                        <a href="/admin/clients">Λίστα Πελατών</a>
                    </div>
                    <div>
                        <a href="/admin/clients/create">Προσθήκη Πελάτη</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-5">
        <div class="card-deck">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Καταχωρήσεις</h4>
                    <div>
                        <a href="/admin/entries">Λίστα Καταχωρήσεων</a>
                    </div>
                    <div>
                        <a href="/admin/entries/create">Προσθήκη Καταχώρησης</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-5">
        <div class="card-deck">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">News</h4>
                    <div>
                        <a href="/admin/news">Λίστα Νέων</a>
                    </div>
                    <div>
                        <a href="/admin/news/create">Προσθήκη Νέων</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-5">
        <div class="card-deck">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Sliders</h4>
                    <div>
                        <a href="/admin/sliders">Λίστα Sliders</a>
                    </div>
                    <div>
                        <a href="/admin/sliders/create">Προσθήκη Slider</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-5">
        <div class="card-deck">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Σελίδες Κατηγοριών</h4>
                    <div>
                        <a href="/admin/pages">Λίστα Σελίδων</a>
                    </div>
                    <div>
                        <a href="/admin/pages/create">Προσθήκη Σελιδας</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-deck">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Κατηγορίες</h4>
                    <div>
                        <a href="/admin/categories">Λίστα Κατηγοριών</a>
                    </div>
                    <div>
                        <a href="/admin/categories/create">Προσθήκη Κατηγορίας</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
