@extends ('site.layouts.master')

@section('metatags')
    <title>Εκκλησίες | Θρησκευτικός Γάμος, Βάπτιση.</title>
    <meta name="description" content="Βρείτε όλες τις εκκλησίες για θρησκευτικό γάμο σε όλη την Ελλάδα, με τα στοιχεία τους" >
    <meta name="keywords" content="εκκλησίες, θρησκευτικός γάμος, ekklisies gia gamo, λίστα εκκλησιών" >
@endsection

@section ('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section id="slider">
    <div class="container">
        <div class="row">
            <div class="showcase col-md-12">   
                <h1>Εκκλησίες για γάμο</h1>     
                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="2000">
                    <div class="carousel-inner">
                        {{--  <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>  --}}
                        {{-- <div class="carousel-item active" data-slide="1">                            
                            <img class="d-block w-100" src="storage/sliders/gpnl.png" alt="Εκκλησίες για θρησκευτικό γάμο" title="Εκκλησίες για θρησκευτικό γάμο" target="_blank">  
                        </div>  --}}
                        {{--  @if($page->image2)                        
                            <div class="carousel-item " data-slide="2">
                                <a href="{{$page->link2}}">
                                    <img class="d-block w-100" src="storage/pages/{{$page->image2}}"alt="{{$page->title}} " title="{{$page->title}} - {{$page->caption_title_1}}">
                                    <div class="carousel-caption d-none d-md-block">
                                            <h3>{{$page->caption_title_2}}</h3>
                                            <p>{{$page->caption_description_2}}</p>
                                    </div>
                                </a>
                            </div>                        
                        @endif
                        @if($page->image3)
                            <div class="carousel-item" data-slide="3">
                                <a href="{{$page->link2}}">
                                    <img class="d-block w-100" src="images/pages/{{$page->image3}}" alt="{{$page->title}} " title="{{$page->title}} - {{$page->caption_title_1}}">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h3>{{$page->caption_title_3}}</h3>
                                            <p>{{$page->caption_description_3}}</p>
                                    </div>
                                 </a>
                            </div>
                        @endif  --}}
                    </div>
                </div>
            </div>
        </div>    
    </div>
</section>
<section id="main">
<div class="container churches">
    <div class="row">
        <div class="page-description">
            <p> Ετοιμάζεστε για θρησκευτικό γάμο; Βρείτε τώρα τις εκκλησίες της περιοχής σας στο Gamos Portal με τα στοιχεία επικοινωνίας που θα χρειαστείτε.</p>
            <p>Κάντε κλικ σε όποια εκκλησία θέλετε να δείτε ή επιλέξτε την περιοχή που σας ενδιαφέρει από τη σχετική λίστα.</p>
        </div>
        <div class="col-md-9 latest">
            <h2>Δείτε μερικές από τις εκκλησίες που μπορείτε να βρείτε στο Gamos Portal</h2>
            <div>
                <form action="/ekklisies.php" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group d-flex">
                    <input type="text"
                        class="form-control" name="query" id="" aria-describedby="helpId" placeholder="Αναζήτηση στις Εκκλησίες">
                        <button type="submit" class="float-right"><i class="fas fa-search"></i></button>
                    </div>
                </form>
            </div>
            <div class="d-flex flex-wrap">
                @foreach($churches as $church)                
                    <div class="latest-item col-md-4 pull-left">
                        <div class="card-body">
                            <a href="/{{$church->district->slug}}/ekklisies/{{$church->slug}}"><h4 class="card-title">{{$church->name}}</h4>
                            {{-- <p><a href="tel:{{$church->phone}}">{{$church->phone}}</a></p> --}}
                            <p>{{$church->address}}</p> 
                            <small>{{$church->district->name}}</small>
                            </a>                     
                        </div>
                    </div>
                @endforeach            
            </div>
        </div>
        <div class="col-md-3">
            <div class="sidebar-element">
                <h3>Βρείτε Εκκλησίες</h3>               
                <nav>
                    @foreach (Pages::districtsList() as $district)
                        @if (count($district->churches) > 0)
                            <a href="/{{$district->slug}}/ekklisies" class="nav-link">{{$district->name}}</a>
                        @endif                        
                    @endforeach
                </nav>
            </div>
            <div id="categories" class="sidebar-element">
                <h3>Βρείτε στο Gamos Portal: </h3>
                <nav>
                @foreach (Pages::pagesList() as $page)
                    <a href="{{$page->slug}}" class="nav-link">{{$page->title}}</a>
                @endforeach
                </nav>
            </div>
            @if (count($churches) > 8)
            <div class="fb-page" 
            data-href="https://www.facebook.com/gamos.portal"
            data-tabs="timeline"
            data-height="820" 
            data-small-header="false" 
            data-adapt-container-width="true" 
            data-hide-cover="false" 
            data-show-facepile="true">
            <blockquote cite="https://www.facebook.com/gamos.portal" 
            class="fb-xfbml-parse-ignore">
                <a href="https://www.facebook.com/gamos.portal">Gamos Portal</a>
            </blockquote>
        </div>
            @endif
        </div>
    </div>
</div>
</section>
</div>    
@endsection