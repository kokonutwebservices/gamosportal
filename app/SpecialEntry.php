<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialEntry extends Model
{
    protected $guarded = ['_token'];

    public $sortable = ['id', 'name', 'client_id', 'end_at'];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function specialimages(){
        return $this->hasMany(SpecialImage::class);
    }
}
