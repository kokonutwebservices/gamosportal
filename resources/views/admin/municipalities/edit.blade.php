@extends('admin.layouts.master')

@section('content')

<h1>Επεξεργασία Δημαρχείου: {{$municipality->name}}</h1>

@include('admin.layouts.errors')

{!! Form::open(['action'=>['MunicipalityController@update', $municipality->id], 'method'=>'POST', 'enctype' => 'multipart/form-data']) !!}

<div class="form-row">

    <div class="form-group col-md-12">
        {{Form::label('name', 'Όνομα Δημαρχείου')}}
        {{Form::text('name', $municipality->name, ['class'=>'form-control', 'placeholder' => 'Το όνομα όπως θα φαίνεται στην καταχώρηση', 'required'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('slug', 'slug')}}
        {{Form::text('slug', $municipality->slug, ['class'=>'form-control', 'placeholder' => 'friendly url', 'required'] )}}
    </div>

    <div class="form-group col-md-12">
        {{Form::label('person', 'Υπεύθυνος')}}
        {{Form::text('person', $municipality->person, ['class'=>'form-control', 'placeholder' => 'Όνομα Υπευθύνου'] )}}
    </div>

    <div class="form-group col-md-12">
        {{Form::label('address', 'Διεύθυνση')}}
        {{Form::text('address', $municipality->address, ['class'=>'form-control', 'placeholder' => 'Διεύθυνση'] )}}
    </div>

    <div class="form-group col-md-4">
        {{Form::label('area', 'Περιοχή')}}
        {{Form::text('area', $municipality->area, ['class'=>'form-control', 'placeholder' => 'Περιοχή'] )}}
    </div>

    <div class="form-group col-md-4">
        {{Form::label('phone', 'Τηλέφωνο')}}
        {{Form::text('phone', $municipality->phone, ['class'=>'form-control', 'placeholder' => 'Τηλέφωνο'] )}}
    </div>

    <div class="form-group col-md-4">
        {{Form::label('mobile', 'Κινητό')}}
        {{Form::text('mobile', $municipality->mobile, ['class'=>'form-control', 'placeholder' => 'Κινητό'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('website', 'Website')}}
        {{Form::text('website', $municipality->website, ['class'=>'form-control', 'placeholder' => 'Website'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('email', 'Email')}}
        {{Form::text('email', $municipality->email, ['class'=>'form-control', 'placeholder' => 'Email'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('map', 'Google Maps')}}
        {{Form::text('map', $municipality->map, ['class'=>'form-control', 'placeholder' => 'Google Maps'] )}}
    </div>
    
    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-6">
        <label for="district_id">Περιφέρεια</label>
        <select class="form-control" id="district_id" name="district_id">
            @foreach ($districts as $district)
                @if($district->id == $municipality->district_id)
                <option value="{{$district->id}}" selected>{{$district->name}}</option>
                @else
                <option value="{{$district->id}}">{{$district->name}}</option>
                @endif                
            @endforeach
        </select>
    </div>

    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group ">
        {{Form::label('image', 'Εικόνα')}}
        {{Form::file('image' )}}
        @if($municipality->image)
            <img src="/storage/municipalities/{{$municipality->image}}" style="width: 250px;" title="{{$municipality->image}}">
        @endif
    </div>

    <div class="form-group col-md-12">
        {{Form::label('profile', 'Κείμενο Προφίλ')}}
        {{Form::textarea('profile', $municipality->profile, ['class'=>'form-control tinyMCE', 'placeholder' => 'Κείμενο Προφίλ'] )}}
    </div> 

    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-12">
        {{Form::label('metatitle', 'Τίτλος')}}
        {{Form::text('metatitle', $municipality->metatitle, ['class'=>'form-control', 'placeholder' => 'Τίτλος'] )}}
    </div> 

    <div class="form-group col-md-12">
        {{Form::label('metadescription', 'Meta Description')}}
        {{Form::text('metadescription', $municipality->metadescription, ['class'=>'form-control', 'placeholder' => 'Meta Description'] )}}
    </div>

    <div class="form-group col-md-12">
        {{Form::label('metakeywords', 'Meta Keywords')}}
        {{Form::text('metakeywords', $municipality->metakeywords, ['class'=>'form-control', 'placeholder' => 'Meta Keywords'] )}}
    </div>
    
    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-12">    
        {{Form::hidden('_method', 'PUT')}}
        <div class="col-md-2">  
        {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
        </div>

        <div class="col-md-2">
            {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
        </div>

        <div class="col-md-2">
            {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
        </div>

        <div class="col-md-2">  
            <a href="/admin/municipalities" class="btn btn-warning pull-right">Cancel</a>
        </div>

        <div class="col-md-2">
        {!! Form::close() !!}

            {!! Form::open(['action' => ['MunicipalityController@destroy', $municipality->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'pull-right', 'id' =>'delete' ]) !!}

            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}

        {!! Form::close() !!}  
        </div>



    </div>
</div>
{!! Form::close() !!}
    
@endsection