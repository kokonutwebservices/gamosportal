<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('metatitle');
            $table->string('metadescription');
            $table->string('metakeywords');
            $table->text('body');
            $table->string('title2');
            $table->string('image1');
            $table->string('caption_title_1');
            $table->text('caption_description_1');
            $table->string('link1');
            $table->string('image2');
            $table->string('caption_title_2');
            $table->text('caption_description_2');
            $table->string('link2');
            $table->string('image3');
            $table->string('caption_title_3');
            $table->text('caption_description_3');
            $table->string('link3');
            $table->string('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
