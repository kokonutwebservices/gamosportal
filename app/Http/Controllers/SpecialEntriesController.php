<?php

namespace App\Http\Controllers;

use App\SpecialEntry;
use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Kyslik\ColumnSortable\Sortable;
use App\SpecialImage;

class SpecialEntriesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SpecialEntry $specialentries)
    {
        $specialentries = $specialentries->all();
        return view('admin.specialentries.index', compact('specialentries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $clients)
    {
     
        $clients = $clients->all();
        return view('admin.specialentries.create', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SpecialEntry $specialentry)
    {
        //Validation
        $this->validate(request(), [
            'client_id' => 'required',
            'name' => 'required',
            'slug' => 'required',
            'logo' => 'required',
            'image' => 'required',
            'profile' => 'required',
        ]);

        
        //Images check
        //logo
        $logo = $request->file('logo'); //Get the logo file
        $ext = $logo->getClientOriginalExtension(); //Get the extension
        $logoFileNameBase = $request->input('slug'); //Get the slug to use as filename base
        $logoFileName = $logoFileNameBase .'-logo.'.$ext; //create a new filename
        $cat_id = request('cat_id');
        request()->file('logo')->storeAs("logos/special", $logoFileName); //Store file
        $filepathLogo = public_path("storage/logos/special/$logoFileName");
        try {
            \Tinify\setKey("xVBaCSpeVqNE0VFe5_KNBpXweIB-YwZL");
            $source = \Tinify\fromFile($filepathLogo);
            $source->toFile($filepathLogo);
        } catch(\Tinify\AccountException $e) {
            // Verify your API key and account limit.
            return redirect()->back()->with('error', $e->getMessage());
        } catch(\Tinify\ClientException $e) {
            // Check your source image and request options.
            return redirect()->back()->with('error', $e->getMessage());
        } catch(\Tinify\ServerException $e) {
            // Temporary issue with the Tinify API.
            return redirect()->back()->with('error', $e->getMessage());
        } catch(\Tinify\ConnectionException $e) {
            // A network connection error occurred.
            return redirect()->back()->with('error', $e->getMessage());
        } catch(Exception $e) {
            // Something else went wrong, unrelated to the Tinify API.
            return redirect()->back()->with('error', $e->getMessage());
        }

        //Profile Image
        $image = $request->file('image'); //Get the logo file
        $ext = $image->getClientOriginalExtension(); //Get the extension
        $imageFileNameBase = $request->input('slug'); //Get the slug to use as filename base
        $imageFileName = $imageFileNameBase .'.'.$ext; //create a new filename
        request()->file('image')->storeAs("entries/special", $imageFileName); //Store file
        $filepath = public_path("storage/entries/special/$imageFileName");
        try {
            \Tinify\setKey("xVBaCSpeVqNE0VFe5_KNBpXweIB-YwZL");
            $source = \Tinify\fromFile($filepath);
            $source->toFile($filepath);
        } catch(\Tinify\AccountException $e) {
            // Verify your API key and account limit.
            return redirect()->back()->with('error', $e->getMessage());
        } catch(\Tinify\ClientException $e) {
            // Check your source image and request options.
            return redirect()->back()->with('error', $e->getMessage());
        } catch(\Tinify\ServerException $e) {
            // Temporary issue with the Tinify API.
            return redirect()->back()->with('error', $e->getMessage());
        } catch(\Tinify\ConnectionException $e) {
            // A network connection error occurred.
            return redirect()->back()->with('error', $e->getMessage());
        } catch(Exception $e) {
            // Something else went wrong, unrelated to the Tinify API.
            return redirect()->back()->with('error', $e->getMessage());
        }

        //Regular expressions για να αφαιρεθεί το http:// και https:// από το url, αν υπάρχει
        $facebook = preg_replace('#^https?://#', '',$request->input('facebook'));
        $google = preg_replace('#^https?://#', '',$request->input('google'));
        $twitter = preg_replace('#^https?://#', '',$request->input('twitter'));
        $instagram = preg_replace('#^https?://#', '',$request->input('instagram'));
        $pinterest = preg_replace('#^https?://#', '',$request->input('pinterest'));
        $youtube = preg_replace('#^https?://#', '',$request->input('youtube'));
        $vimeo = preg_replace('#^https?://#', '',$request->input('vimeo'));

        //Expiration date
        $expires = $request->input('end_at');
        if ($expires == NULL) {
            $expires = Carbon::parse(request('start_at'))->addYear();
        } else {
            $expires = $request->input('end_at');
        }

        //Get last order
        $entriesNumber = SpecialEntry::all();
        $entriesCount = count($entriesNumber);
        if ($entriesCount > 0){
        $lastEntry = SpecialEntry::last();
        $order = $lastEntry->order + 1;
        } else {
            $order = 1;
        }

         //Creation
         SpecialEntry::create([
            'client_id' => $request->input('client_id'),
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'logo' => $logoFileName,
            'person' => $request->input('person'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
            'mobile' => $request->input('mobile'),
            'website' => preg_replace('#^https?://#','',$request->input('website')),
            'email' => $request->input('email'),
            'facebook' => $facebook,
            'google' => $google,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'pinterest' => $pinterest,
            'youtube' => $youtube,
            'vimeo' => $vimeo,
            'map' => $request->input('map'),
            'image' => $imageFileName,
            'profile' => $request->input('profile'),
            'metatitle' => $request->input('metatitle'),
            'metadescription' => $request->input('metadescription'),
            'metakeywords' => $request->input('metakeywords'),
            'offer' => $request->input('offer'),
            'newlife' => $request->input('newlife'),
            'active' => $request->input('active'),
            'order' => $order,
            'start_at' => $request->input('start_at'),
            'end_at' => $expires,
        ]);

        $id = SpecialEntry::all()->last()->id;

        //Redirect
        if($request->submitbutton == 'save'){
            return redirect("/admin/specialentries/$id/edit"); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect("/admin/specialentries");
        } elseif ($request->submitbutton == 'save-new'){
            return redirect("/admin/entries/create");
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpecialEntry  $specialEntry
     * @return \Illuminate\Http\Response
     */
    public function show(SpecialEntry $specialEntry)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpecialEntry  $specialEntry
     * @return \Illuminate\Http\Response
     */
    public function edit(SpecialEntry $specialentry)
    {
        $clients = Client::all();

        return view('admin.specialentries.edit', compact('specialentry', 'clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpecialEntry  $specialEntry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpecialEntry $specialentry)
    {
        
       //Validation
       $this->validate(request(), [
            'name' => 'required',
            'slug' => 'required',
            'profile' => 'required',
        ]);

        //Update
        //Entry Logo
        if($request->hasFile('logo')){
            if($specialentry->logo){
                //Αν ανεβαίνει εικόνα και υπάρχει ήδη άλλο αρχείο το όνομα αρχείου παραμένει ίδιο 
                $logoFileName = $specialentry->logo;             
                request()->file('logo')->storeAs("logos/special", $logoFileName);
                $filepathLogo = public_path("storage/logos/special/$entry->logo");
                try {
                    \Tinify\setKey("xVBaCSpeVqNE0VFe5_KNBpXweIB-YwZL");
                    $source = \Tinify\fromFile($filepathLogo);
                    $source->toFile($filepathLogo);
                } catch(\Tinify\AccountException $e) {
                    // Verify your API key and account limit.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ClientException $e) {
                    // Check your source image and request options.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ServerException $e) {
                    // Temporary issue with the Tinify API.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ConnectionException $e) {
                    // A network connection error occurred.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(Exception $e) {
                    // Something else went wrong, unrelated to the Tinify API.
                    return redirect()->back()->with('error', $e->getMessage());
                }
            } else {
                //Αν δεν υπάρχει ήδη αρχείο
                $logo = $request->file('logo'); //Get the logo file
                $ext = $logo->getClientOriginalExtension(); //Get the extension
                $logoFileNameBase = $request->input('slug'); //Get the slug to use as filename base
                $logoFileName = $logoFileNameBase .'-logo.'.$ext; //create a new filename
                
                request()->file('logo')->storeAs("logos/special", $logoFileName); //Store file
                
                $filepathLogo = public_path("storage/logos/special/$logoFileName");
                try {
                    \Tinify\setKey("xVBaCSpeVqNE0VFe5_KNBpXweIB-YwZL");
                    $source = \Tinify\fromFile($filepathLogo);
                    $source->toFile($filepathLogo);
                } catch(\Tinify\AccountException $e) {
                    // Verify your API key and account limit.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ClientException $e) {
                    // Check your source image and request options.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ServerException $e) {
                    // Temporary issue with the Tinify API.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ConnectionException $e) {
                    // A network connection error occurred.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(Exception $e) {
                    // Something else went wrong, unrelated to the Tinify API.
                    return redirect()->back()->with('error', $e->getMessage());
                }
            }
        } else {
            //Αν δεν ανεβαίνει άλλο αρχείο για αντικατάσταση
            $logoFileName = $specialentry->logo;
        }

         //Entry Image
         if($request->hasFile('image')){
            if($specialentry->image){
                //Αν ανεβαίνει εικόνα και υπάρχει ήδη άλλο αρχείο το όνομα αρχείου παραμένει ίδιο 
                $filename = $specialentry->image;
                request()->file('image')->storeAs("entries/special", $filename);
                $filepath = public_path("storage/entries/special/$entry->image");
                try {
                    \Tinify\setKey("xVBaCSpeVqNE0VFe5_KNBpXweIB-YwZL");
                    $source = \Tinify\fromFile($filepath);
                    $source->toFile($filepath);
                } catch(\Tinify\AccountException $e) {
                    // Verify your API key and account limit.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ClientException $e) {
                    // Check your source image and request options.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ServerException $e) {
                    // Temporary issue with the Tinify API.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ConnectionException $e) {
                    // A network connection error occurred.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(Exception $e) {
                    // Something else went wrong, unrelated to the Tinify API.
                    return redirect()->back()->with('error', $e->getMessage());
                }
            } else {
                //Αν δεν υπάρχει ήδη αρχείο
                //Profile Image
                $image = $request->file('image'); //Get the logo file
                $ext = $image->getClientOriginalExtension(); //Get the extension
                $imageFileNameBase = $request->input('slug'); //Get the slug to use as filename base
                $imageFileName = $imageFileNameBase .'-.'.$ext; //create a new filename
                request()->file('image')->storeAs("entries/special", $imageFileName); //Store file
                $filepath = public_path("storage/entries/special/$imageFileName");
                try {
                    \Tinify\setKey("xVBaCSpeVqNE0VFe5_KNBpXweIB-YwZL");
                    $source = \Tinify\fromFile($filepath);
                    $source->toFile($filepath);
                } catch(\Tinify\AccountException $e) {
                    // Verify your API key and account limit.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ClientException $e) {
                    // Check your source image and request options.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ServerException $e) {
                    // Temporary issue with the Tinify API.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(\Tinify\ConnectionException $e) {
                    // A network connection error occurred.
                    return redirect()->back()->with('error', $e->getMessage());
                } catch(Exception $e) {
                    // Something else went wrong, unrelated to the Tinify API.
                    return redirect()->back()->with('error', $e->getMessage());
                }
                $filename = $imageFileName;
            }
        } else {
            //Αν δεν ανεβαίνει αρχείο
            $filename = $specialentry->image;
        }

        $facebook = preg_replace('#^https?://#', '',$request->input('facebook'));
        $google = preg_replace('#^https?://#', '',$request->input('google'));
        $twitter = preg_replace('#^https?://#', '',$request->input('twitter'));
        $instagram = preg_replace('#^https?://#', '',$request->input('instagram'));
        $pinterest = preg_replace('#^https?://#', '',$request->input('pinterest'));
        $youtube = preg_replace('#^https?://#', '',$request->input('youtube'));
        $vimeo = preg_replace('#^https?://#', '',$request->input('vimeo'));

        $specialentry->client_id = $request->input('client_id');
        $specialentry->name = $request->input('name');
        $specialentry->slug = $request->input('slug');
        $specialentry->logo = $logoFileName;
        $specialentry->person = $request->input('person');
        $specialentry->address = $request->input('address');
        $specialentry->phone = $request->input('phone');
        $specialentry->mobile = $request->input('mobile');
        $specialentry->website = preg_replace('#^https?://#','',$request->input('website'));
        $specialentry->email = $request->input('email');
        $specialentry->facebook = $facebook;
        $specialentry->google = $google;
        $specialentry->twitter = $twitter;
        $specialentry->instagram = $instagram;
        $specialentry->pinterest = $pinterest;
        $specialentry->youtube = $youtube;
        $specialentry->vimeo = $vimeo;
        $specialentry->map = $request->input('map');
        $specialentry->image = $filename;
        $specialentry->profile = $request->input('profile');
        $specialentry->metatitle = $request->input('metatitle');
        $specialentry->metadescription = $request->input('metadescription');
        $specialentry->metakeywords = $request->input('metakeywords');
        $specialentry->offer = $request->input('offer');
        $specialentry->newlife = $request->input('newlife');
        $specialentry->active = $request->input('active');
        $specialentry->order = $request->input('order');
        $specialentry->start_at = Carbon::parse($request->input('start_at'));
        $specialentry->end_at = Carbon::parse(request('start_at'))->addYear(); //προσθέτει 1 έτος στην ημ/νία έναρξης
        $specialentry->save();

        //Redirect
        if($request->submitbutton == 'save'){
            return back();
        } elseif ($request->submitbutton == 'save-close') {
            return redirect("/admin/specialentries");
        } elseif ($request->submitbutton == 'save-new'){
            return redirect("/admin/specialentries/create");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpecialEntry  $specialEntry
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpecialEntry $specialentry)
    {
        $specialentry->delete();

        return redirect("admin/specialentries");
    }

    public function search(Request $request, SpecialEntry $specialentry){

        $keyword = request('query');
        $results = SpecialEntry::where('name', 'LIKE', '%'.$keyword.'%')->get();

        return view('admin.specialentries.results', compact('results'));
    }

    public function images(SpecialEntry $specialentry){

        $specialImages = SpecialImage::where('specialentry_id', $specialentry->id)->orderBy('order')->get();

        return view ('admin.specialentries.images', compact('specialentry', 'specialImages'));

    }
}
