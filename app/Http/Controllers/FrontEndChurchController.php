<?php

namespace App\Http\Controllers;

use App\Church;
use App\District;
use App\Metropole;
use Illuminate\Http\Request;

class FrontEndChurchController extends Controller
{
    public function show ($districtSlug, Metropole $metropole, $churchSlug) {

        $district = District::where('slug', $districtSlug)->first();

        $church = Church::where('slug', $churchSlug)->firstOrFail();

        return view ('site.churches.church', compact('district', 'metropole', 'church'));

    }

    public function search(Request $request, $districtSlug){

                
        $district = District::where('slug', $districtSlug)->first();

        $metropole = Metropole::where('id', $request->metropole)->first();

        $metropoles = Metropole::where('district_id', $district->id)->get();

        // Το τελευταίο κομμάτι του url, για να βγει η επιλεγμένη Μητρόπολη selected
        $lastSegment = request()->segments(3);
        
        $keyword = request('query');
        
        $results = Church::where('name', 'LIKE', '%'.$keyword.'%')->orWhere('address', 'LIKE', '%'.$keyword.'%')->orWhere('area', 'LIKE', '%'.$keyword.'%')->get()->where('district_id', $district->id)->where('metropole_id', $metropole->id);

        return view('site.churches.churchesresults', compact('district', 'results', 'keyword', 'metropole', 'metropoles', 'lastSegment'));

    }

    public function churchesPage() {

        $metropoles = Metropole::all();
        $districts = District::all();

        return view('site.pages.churches', compact('metropoles', 'districts'));

    }


}
