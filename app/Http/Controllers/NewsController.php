<?php

namespace App\Http\Controllers;

use App\News;
use Storage;
use Illuminate\Http\Request;

class NewsController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('order')->get();

        return view('admin.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg|max:500',
            'description' => 'required',
            'url' => 'required|url'
        ]);
        
        $file = $request->file('image'); //Get the file
        $ext = $file->getClientOriginalExtension(); //Get the extension     
        $filename = time().'.'.$ext; //create a new filename
        request()->file('image')->storeAs('news', $filename); //Store file 

        $filepath = public_path('storage/news/'.$filename);        
        
        try {
            \Tinify\setKey("xVBaCSpeVqNE0VFe5_KNBpXweIB-YwZL");
            $source = \Tinify\fromFile($filepath);
            $source->toFile($filepath);
            } catch(\Tinify\AccountException $e) {
                // Verify your API key and account limit.
                return redirect()->back()->with('error', $e->getMessage());
            } catch(\Tinify\ClientException $e) {
                // Check your source image and request options.
                return redirect()->back()->with('error', $e->getMessage());
            } catch(\Tinify\ServerException $e) {
                // Temporary issue with the Tinify API.
                return redirect()->back()->with('error', $e->getMessage());
            } catch(\Tinify\ConnectionException $e) {
                // A network connection error occurred.
                return redirect()->back()->with('error', $e->getMessage());
            } catch(Exception $e) {
                // Something else went wrong, unrelated to the Tinify API.
                return redirect()->back()->with('error', $e->getMessage());
        }

        $new = new News;
        $new->title = request('title');
        $new->image = $filename;
        $new->description = request('description');
        $new->url = request('url');
        $new->save();

        $id = News::all()->last()->id;

        //Redirect
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.news.edit', $id); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.news.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.news.create');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $new = $news;

        return view('admin.news.edit', compact('new'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'url' => 'required|url'
        ]);

        $new = $news;

        if($request->hasFile('image')){
            $filename = $new->image;
            request()->file('image')->storeAs('news', $filename); //Store file 

        
        } else {
            $filename = $new->image;
        }

        $new->title = request('title');
        $new->image = $filename;
        $new->description = request('description');
        $new->url = request('url');
        $new->save();

        //Redirect
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.news.edit', $new); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.news.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.news.create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
    
        Storage::delete("news/$news->image");

        $news->delete();

        return redirect()->route('admin.news.index')->with('message', 'Η δημοσίευση διαγράφηκε');
    }

        //Function που παίρνει την νέα σειρά από το Ajax call του sortable, αναλύει το id κάθε item, και γράφει στην db το νέο order 
        public function order(){
            
        $orders = explode(',',$_POST['order']);
            foreach ($orders as $neworder=>$value){                
                $id = $value;			
                $order = $neworder+1;
                $new = News::find($id);
                $new->order = $order;
                $new->save();
            }
        }
}
