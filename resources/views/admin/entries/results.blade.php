@extends('admin.layouts.master')

@section('content')

    <h1>Καταχωρήσεις Gamos Portal</h1>
    {{--  Πλαίσιο σφαλμάτων  --}}

    <div class="col-md-12">
        <div class="form-group col-md-4 pull-right">
        {!!Form::open(['method' => 'POST', 'action' => 'EntriesController@search'])!!}
            <div class="input-group">
            <input type="text" class="form-control" name="query" placeholder="Αναζήτηση" aria-label="Αναζήτηση">
            <span class="input-group-btn">
                <button class="btn btn-secondary" type="submit">Go!</button>
            </span>
            </div>
        {!!Form::close()!!}
        </div>
    </div>
    <table class="table">
    <thead class="thead-inverse">
        <tr>
        <th>@sortablelink('id')</th>
        <th>@sortablelink('name', 'Όνομα Καταχώρησης')</th>
        <th>@sortablelink('cat_id', 'Κατηγορία')</th>
        <th>@sortablelink('dist_id', 'Περιοχή') </th>
        <th>@sortablelink('end_at', 'Ημ. Λήξης')</th>
        </tr>
    </thead>
        @foreach($results as $entry)
            <tr>
                <td>{{$entry->id}}</td>
                <td><a href="/admin/entries/{{$entry->id}}/edit">{{$entry->name}}</a></td>
                <td>{{$entry->category->cat_name}}</td>
                <td>{{$entry->district->name}}</td>
                <td>{{$entry->end_at}}</td>
            </tr>
        @endforeach
    </table>

    {{--  {{ $results->links() }}  --}}

{{--  <div class="col-md-12">
    <a href="entries/create" class="btn btn-primary">Δημιουργία Καταχώρησης</a>
</div>  --}}


@endsection