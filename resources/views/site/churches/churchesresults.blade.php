@extends('site.layouts.master')

@section('metatags')

    <title>Εκκλησίες για Γάμο - {{$metropole->name}} </title>
    
    
@endsection

@section('content')

<div class="container">

    <div class="pull-right">
        @include('site.churches.selectmetropole')
    </div>

    <div class="clearfix"></div>

    <div>
        <h1> {{$metropole->name}} </h1>
        <span class=""> {{$metropole->areas}} </span>
    </div>

    <div class="pull-right">
        
        <form action="/{{$district->slug}}/ekklisies/{{$metropole->id}}" method="POST">
            @csrf

            <div class="d-flex">                
              <input type="text" class="form-control" name="query" placeholder="Αναζήτηση" required>
              <div class="form-group">
                  <input type="hidden" class="form-control" name="metropole" id="metropole" value="{{$metropole->id}}">
              </div>
              <button type="submit" class="btn btn-secondary"><i class="fas fa-search"></i></button>
              <a class="btn btn-default" href="/{{$district->slug}}/ekklisies/{{$metropole->id}}"><i class="fas fa-undo-alt"></i></a>
            </div>

        </form>

    </div>

    <div class="clearfix"></div>

    <div class="mt-3">
        <table class="table  table-responsive">
            <thead class="thead-inverse">
                <tr>
                    <th>Όνομα Εκκλησίας</th>
                    <th>Διεύθυνση</th>
                    <th>Περιοχή</th>
                    <th>Τηλέφωνο</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($results as $church)
                        <tr>
                            <td scope="row"><a class="" href="/{{$district->slug}}/ekklisies/{{$metropole->id}}/{{$church->slug}}">{{$church->name}}</a></td>
                            <td>{{$church->address}}</td>
                            <td>{{$church->area}}</td>
                            <td><a class="text-light" href="tel:+30{{$church->phone}}">{{$church->phone}}</a></td>
                        </tr>
                    @endforeach                    
                </tbody>
        </table>
    </div>
</div>

@endsection