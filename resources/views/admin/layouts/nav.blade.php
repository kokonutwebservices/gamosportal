<div class="page-header">
<img src="/storage/header/logo.png" style="width: 200px;" class="d-inline-block align-middle admin-logo">
</div>


<nav class="navbar navbar-expand-lg navbar-dark adminnav">
    <a class="navbar-brand" href="/admin"> Administrators Panel</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    @if(!Request::is('*/login'))
    <div class="collapse navbar-collapse" id="navbarsExample05">
    <ul class="navbar-nav mr-auto navbar-left">        
        {{-- <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
        </li> --}}
    </ul>
    @if (Auth::check())
        <ul class="navbar-nav mr-auto navbar-right">
            <li class="nav-item">
            <a class="nav-link" href="/" target="_blank">Frontend <span class="sr-only"></span></a>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> {{auth()->user()->name}}</a>
            <div class="dropdown-menu" aria-labelledby="dropdown05">
                <a class="dropdown-item" href="#">Προφίλ</a>
                <a class="dropdown-item" href="/admin/register">Δημιουργία νέου λογαριασμού</a>
                <a class="dropdown-item" href="/admin/logout">Αποσύνδεση</a>
            </div>
            </li>
        </ul>
    @endif
    </div>
    @endif
</nav>