<div class="row">
    <div class="w-100">
        Gamos Portal Admin System <br>
        Created by <a href="https://www.kokonut.gr" target="_blank" rel="nofollow">Kokonut Web Services</a><br>
        <i class="fa fa-copyright" aria-hidden="true"></i> 2011 - 
        @php
            echo date("Y");
        @endphp
    </div>
</div>