<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $guarded = ['_token'];

    public function entry(){
        return $this->belongsTo(Entry::class);
    }
}
