@extends('site.layouts.master')

@section('metatags')
    <title>{{ $district->metatitle }}</title>
    <meta name="description" content="{{ $district->metadescription }}">
    <meta name="keywords" content="{{ $district->metakeywords }}">
@endsection

@section('content')
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12 d-flex flex-row row-wrap justify-content-around">
                @foreach ($banners as $banner)
                    @if (
                        $banner->expires > Carbon\Carbon::now() and
                            $banner->entry->district->id == $district->id and
                            !$banner->categories()->exists())
                        <a href="{{ $banner->url }}"><img src="/storage/banners/{{ $banner->banner }}"></a>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="row mt-4">
            <ol class="breadcrumb my-0" itemscope itemtype="https://schema.org/BreadcrumbList">
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="/">
                        <span itemprop="name">Αρχική</span>
                    </a>
                    <meta itemprop="position" content="1" />
                </li>
                <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{ $district->name }}</span>
                    <meta itemprop="position" content="2" />
                </li>
            </ol>
        </div>
        

        <div class="row mt-2">
            <h1 class="col-md-12" style="text-align: center; margin: 0 0 1rem;">Περιφέρεια: {{ $district->name }}</h1>
        </div>

        <div class="row parents">
            @foreach ($categories as $category)
                @if ($category->cat_parent_id == 0)
                    <div class="d-flex flex-row flex-wrap categories" id="{{ $category->id }}">
                        <h2 class="col-md-12 px-0">{{ $category->cat_name }}</h2>
                        <?php $subcategories = App\Category::where('cat_parent_id', $category->id)
                            ->orderBy('cat_order', 'asc')
                            ->get(); ?>
                        @foreach ($subcategories as $subcategory)
                            @if (count($subcategory->entries->where('dist_id', $district->id)->where('active', 1)) > 0)
                                <div class="col-md-3 category-item">
                                    <a href="{{ $district->slug }}/{{ $subcategory->cat_slug }}"><img class="thumbnail"
                                            src="storage/categories/{{ $subcategory->cat_img }}"
                                            alt="{{ $subcategory->cat_name }}" title="{{ $subcategory->cat_name }}"></a>
                                    <h5><a href="{{ $district->slug }}/{{ $subcategory->cat_slug }}">{{ $subcategory->cat_name }}
                                        </a></h5>
                                </div>
                            @endif
                        @endforeach
                        </ul>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="row">
            <div class="d-flex flex-row flex-wrap categories">
                <h2 class="col-md-12 px-0"> ΕΚΚΛΗΣΙΕΣ - ΔΗΜΑΡΧΕΙΑ</h2>
                <div class="col-md-3 category-item">
                    @if (count(Facilities::churches($district)) > 0)
                        <a href="/{{ $district->slug }}/ekklisies"><img class="thumbnail" src="/storage/ekklisia.jpg"
                                alt="Εκκλησίες - {{ $district->name }}" title="Δημαρχεία - {{ $district->name }}"></a>
                        <h5><a href="/{{ $district->slug }}/dimarxeia">Εκκλησίες
                                ({{ count(Facilities::churches($district)) }})</a></h5>
                    @endif
                </div>
                <div class="col-md-3 category-item">
                    @if (count(Facilities::municipalities($district)) > 0)
                        <a href="/{{ $district->slug }}/dimarxeia"><img class="thumbnail" src="/storage/dimarxeio.jpg"
                                alt="Δημαρχεία - {{ $district->name }}" title="Δημαρχεία - {{ $district->name }}"></a>
                        <h5><a href="/{{ $district->slug }}/dimarxeia">Δημαρχεία
                                ({{ count(Facilities::municipalities($district)) }})</a></h5>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
