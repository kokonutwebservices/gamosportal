<div class="col-md-12">
    <h4 class="text-center" id="entryname"><a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}">{{$entry->name}}</a></h4>
    {{--  <small>({{$entry->category->cat_name}} - {{$entry->district->name}})</small>  --}}
    <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}"><img class="lucky-img" src="storage/entries/{{$entry->cat_id}}/{{$entry->image}}" title=""></a>
    <div class="d-flex flex-row justify-content-around">
        <div class=""><a class="btn btn-lucky lucky-img"><i class="fas fa-sync-alt"></i></a></div>
        @if ($entry->phone)
            @php
                $call = $entry->phone;
            @endphp
        @else 
            @php
                $call = $entry->mobile;
            @endphp
        @endif
        <div class=""><a class="btn btn-lucky call" href="tel:{{$call}}" data-toggle="tooltip" data-placement="bottom" title="Καλέστε τώρα!"><i class="fas fa-phone mr-3" aria-hidden="true"></i></a> </div>
        <div class=""><a class="btn btn-lucky email" href="mailto:{{$entry->email}}" data-toggle="tooltip" data-placement="bottom" title="Στείλτε email τώρα!"><i class="fas fa-envelope mr-3" aria-hidden="true"></i></a> </div>
        <div class=""><a class="btn btn-lucky" href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}" data-toggle="tooltip" data-placement="bottom" title="Δείτε περισσότερα"><i class="fas fa-link"></i></a> </div>
    </div>
    <script src="{{asset('/js/app.js')}}" ></script>
</div>