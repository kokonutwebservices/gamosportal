@extends('admin.layouts.master')

@section('content')
<h1>Ενημέρωση Σελίδας Πληροφοριών</h1>
@include('admin.layouts.errors')
{!! Form::open(['action'=>['InfoPagesController@update', $infopage->id], 'method'=>'POST', 'enctype' => 'multipart/form-data']) !!}

<div class="form-group">
    {{Form::label('title', 'Τίτλος')}}
    {{Form::text('title', $infopage->title, ['class'=>'form-control', 'placeholder' => 'Τίτλος', 'required'] )}}
</div>

<div class="form-group">
    {{Form::label('slug', 'slug')}}
    {{Form::text('slug', $infopage->slug, ['class'=>'form-control', 'placeholder' => 'url', 'required' ] )}}
</div>

<div class="form-group">
    {{Form::label('metatitle', 'MetaTitle')}}
    {{Form::text('metatitle', $infopage->metatitle, ['class'=>'form-control', 'placeholder' => 'Τίτλος browser μέχρι 60 χαρακτήρες', 'required' ] )}}
</div>

<div class="form-group">
    {{Form::label('metadescription', 'MetaDescription')}}
    {{Form::text('metadescription', $infopage->metadescription, ['class'=>'form-control', 'placeholder' => 'Περιγραφή σελίδας (μέχρι 150 χαρακτήρες)', 'required'] )}}
</div>

<div class="form-group">
    {{Form::label('metakeywords', 'MetaKeywords')}}
    {{Form::text('metakeywords', $infopage->metakeywords, ['class'=>'form-control', 'placeholder' => 'Keyowords σελίδας, χωρισμένα με ,', 'required'] )}}
</div>

<div class="form-group">
    {{Form::label('body', 'Κείμενο')}}
    <textarea class="form-control tinyMCE" value="{{$infopage->body}}" name="body" required>{{$infopage->body}}</textarea>
</div>

<div class="form-group">
    {{Form::label('image', 'Εικόνα')}}
    {{Form::file('image', ['id' => 'image', 'value' => 'image'] )}}
    {{Form::checkbox('check')}} <span>Επιλέξτε για διαγραφή</span>
    @if($infopage->image)
        <img src="/storage/infopages/{{$infopage->image}}">
    @endif
</div>

<div class="form-group">
    {{Form::label('image_alt', 'Alt Tag | Image Title')}}
    {{Form::text('image_alt', $infopage->image_alt, ['class'=>'form-control'] )}}
</div>

<div class="form-group">

    <div class="col-md-2">  
        {{Form::hidden('_method', 'PUT')}}  
        {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
    </div>  

    <div class="col-md-2">
        {{Form::hidden('_method', 'PUT')}}  
        {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
    </div>

    <div class="col-md-2">
        {{Form::hidden('_method', 'PUT')}}  
        {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
    </div>

    <div class="col-md-2">  
        <a href="/admin/infopages" class="btn btn-warning pull-right">Cancel</a>
    </div>

    <div class="col-md-2">
    {!! Form::close() !!}

        {!! Form::open(['action' => ['InfoPagesController@destroy', $infopage->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'pull-right' ]) !!}

        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}

    {!! Form::close() !!}  
    </div>
</div>
@endsection