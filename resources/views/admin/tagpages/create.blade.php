@extends('admin.layouts.master')

@section('content')

<h1>Δημιουργία Tag Page</h1>

<form action="/admin/tagpages" method="POST">
    {{ csrf_field() }}
    

    <div class="form-group">
        <div class="form-group">
          <label for="tag_id">Tag</label>
          <select class="form-control" name="tag_id" id="tag_id">
            
            <option disabled selected>Επιλέξτε Tag</option>

            @foreach ($tags as $tag)

                <option value="{{$tag->id}}">{{$tag->name}}</option>
                
            @endforeach

          </select>
        </div>
    </div>

    <div class="form-group">
        <label for="title">Τίτλος</label>
        <input type="text" class="form-control" name="title" id="title" aria-describedby="helpId" placeholder="">
    </div>

    <div class="form-group">
        <label for="slug">Slug</label>
        <input type="text" class="form-control" name="slug" id="slug" aria-describedby="helpId" placeholder="">
    </div>
    
    <div class="form-group">
        <label for="metatitle">Meta Title</label>
        <input type="text" class="form-control" name="metatitle" id="metatitle" aria-describedby="helpId" placeholder="">
    </div>
    <div class="form-group">
        <label for="metadescription">Meta Description</label>
        <input type="text" class="form-control" name="metadescription" id="metadescription" aria-describedby="helpId" placeholder="">
    </div>

    <div class="form-group">
        <label for="metakeywords">Meta Keywords</label>
        <input type="text" class="form-control" name="metakeywords" id="metakeywords" aria-describedby="helpId" placeholder="">
    </div>

    <div class="form-group">
        <label for="body">Κείμενο</label>
        <textarea class="tinyMCE" name="body" id="" cols="30" rows="10"></textarea>
    </div>

    <div class="form-group">
      <label for="title2">Τίτλος 2</label>
      <input type="text"
        class="form-control" name="title2" id="title2" aria-describedby="helpId" placeholder="">
    </div>

    <div class="row justify-content-between">
        <button type="submit" class="btn btn-primary" name="submitbutton" value="save-close">Save & Close</button>
        <button type="submit" class="btn btn-success" name="submitbutton" value="save-stay">Save & Stay</button>
        <button type="submit" class="btn btn-secondary" name="submitbutton" value="save-new">Save & New</button>
        <a href="/admin/tagpages" class="btn btn-warning">Cancel</a>
    </div>
</form>

<div class="mt-3">
    @include('admin.layouts.errors')
</div>
@endsection