@extends('admin.layouts.master')

@section('content')

{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif

<h1>Links καταχώρησης: {{$entry->name}}</h1>
<span class="text-danger">Προσοχή!</span> Μία καταχώρηση μπορεί να έχει links είτε σε άλλες κατηγορίες στην ίδια περιφέρεια, είτε σε άλλες περιφέρειες στην ίδια κατηγορία.
<hr>

{!!Form::open(['method' => 'POST', 'action' => ['LinksController@update', $entry->id]])!!}
<h2>Επιλέξτε κατηγορίες</h2> 
<span>Θα προστεθούν link(s) σε αυτές τις κατηγορίες, <strong>μόνο</strong> στην περιφέρεια: <strong>{{$entry->district->name}} </strong></span>
<div class="card">
    <div class="card-body">
        <div class="form-check">
            @foreach($categories as $category)
                <label class="form-check-label" for="{{$category->id}}">
                @if($category->links()->count() > 0)
                    <input type="checkbox" class="form-check-input" name="category[]" id="{{$category->id}}" value="{{$category->id}}" checked>
                    {{$category->cat_name}}  
                @else
                    <input type="checkbox" class="form-check-input" name="category[]" id="{{$category->id}}" value="{{$category->id}}">
                    {{$category->cat_name}}  
                @endif         
                </label>   
            @endforeach
        </div>
    </div>
</div>
<hr>
<h2>Επιλέξτε Περιφέρειες</h2>
<span>Θα προστεθούν link(s) σε αυτές τις περιφέρειες <strong>μόνο</strong> στην κατηγορία: <strong> {{$entry->category->cat_name}} </strong></span>
<div class="card">
    <div class="card-body">
        <div class="form-check">
            @foreach($districts as $district)
                <label class="form-check-label" for="{{$district->id}}">
                @if($district->links()->count() > 0)
                    <input type="checkbox" class="form-check-input" name="district[]" id="{{$district->id}}" value="{{$district->id}}" checked>
                    {{$district->name}}
                @else
                    <input type="checkbox" class="form-check-input" name="district[]" id="{{$district->id}}" value="{{$district->id}}">
                    {{$district->name}}
                @endif                  
                                 
                </label>   
            @endforeach
        </div>
    </div>
</div>
<hr>
<div class="form-group">    
    <div class="col-md-2">
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Save', ['class'=>'btn btn-primary pull-left'] )}}
    </div>

    <div class="col-md-2">  
        <a href="/admin/entries/{{$entry->id}}/edit" class="btn btn-warning pull-right">Επιστροφή</a>
    </div>
</div>
{!!Form::close()!!}

@endsection