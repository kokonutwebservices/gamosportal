<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Page;


class Category extends Model
{

    public function page(){
        return $this->hasOne(Page::class);
    }

    public function banners(){
        return $this->belongsToMany(Banner::class);
    }

    public function entries(){
        return $this->hasMany(Entry::class, 'cat_id');
    }


    public static function get_category_tree($parent_id){
                
        $categories = Category::where('cat_parent_id', $parent_id)->orderBy('cat_order', 'asc')->get();
       
    }

    public function links(){
        return $this->belongsToMany(Link::class);
    }


}
