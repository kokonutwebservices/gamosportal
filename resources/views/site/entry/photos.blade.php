@extends('site.entry.show')


@section('entrycontent')
    <div id="album" class="album-description mb-5">
        <h2>{{$album->name}}</h2>
        <p>{!!$album->description!!}</p>
        <div class="d-flex flex-wrap my-gallery">
            @foreach($images as $image)

                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-md-4 mb-3">
                    <a href="/storage/uploads/{{$album->id}}/{{$image->image}}" itemprop="contentUrl" data-size="{{$image->imageSize()}}">
                        <img src="/storage/uploads/{{$album->id}}/{{$image->image}}" itemprop="thumbnail" alt="{{$image->description ? $image->description : $album->name . ' - Φωτογραφία '. $loop->iteration }}" />
                    </a>
                    <figcaption class="d-none" itemprop="caption description">{{$image->description}} </figcaption>                                                
                </figure> 
            @endforeach
        </div>
    </div>
    <div class="col-lg-12 px-0 mt-4">
        @if (count($albums) > 0)
        <h4>Περισσότερα album:</h4>
        <div class="d-flex flex-row flex-wrap">
            @foreach ($albums as $album)
                <div class="col-xs-6 col-md-4 px-1">
                    <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}/photos/{{$album->id}}">
                        @if ($album->cover)
                        <img class="thumbnail mb-1" src="/storage/uploads/{{$album->id}}/{{$album->cover}}">                
                        @else
                        <img class="thumbnail mb-1" src="/storage/amaksi.jpg">
                        @endif
                        <p class="text-left">{{$album->name}}</p>
                    </a>
                </div>          
            @endforeach
        </div>
        @endif        
    </div>
    <div class="col-lg-12 clearfix">
        <div class="float-left">
            <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}" class="btn btn-default float-right my-3" ><i class="fas fa-arrow-alt-circle-left"></i> Προφίλ </a>
        </div>
        <div class="float-right">
            @if (count($entry->videos) > 0)
                <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}/video#videos" class="btn btn-default float-right my-3" >Video <i class="fas fa-arrow-alt-circle-right"></i></a>
            @elseif($entry->facebook)
                <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}/facebook#facebook" class="btn btn-default float-right my-3" >Timeline <i class="fas fa-arrow-alt-circle-right"></i></a>
            @endif
        </div>
    </div>


{{-- PHOTOSWIPE --}}
<!-- Root element of PhotoSwipe. Must have class pswp. -->
  <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
  
    <!-- Background of PhotoSwipe. 
         It's a separate element, as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">
          
        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

          </div>

      </div>
</div>
@endsection

