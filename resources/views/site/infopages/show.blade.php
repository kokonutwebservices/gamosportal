@extends('site.layouts.master')

@section('metatags')
    <title>{{ $infoPage->metatitle }}</title>
    <meta name="description" content="{{ $infoPage->metadescription }}">
    <meta name="keywords" content="{{ $infoPage->metakeywords }}">
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
    <div class="container">
        @if ($infoPage->image)
            <img class="w-100" src="/storage/infopages/{{ $infoPage->image }}" alt="{{ $infoPage->image_alt }}">
        @endif

        <div class="body">
            {{-- <h1> {!!$infoPage->title!!} </h1> --}}
            {!! $infoPage->body !!}
            @if (Request::is('add-your-business.php'))
                @include('site.infopages.contact')
            @endif
        </div>
    </div>
@endsection
