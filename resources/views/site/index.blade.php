@extends('site.layouts.master')

@section('metatags')
    <title>Γάμος - Gamos Portal, τα πάντα για το γαμο</title>
    <meta name="description" content="Gamos Portal. ΓΑΜΟΣ ίσως η σπουδαιότερη μέρα στη ζωή σας. Το Gamos Portal είναι ένας πλήρης,  εύκολος στην χρήση οδηγός, για να οργανώσετε έναν ονειρεμένο γάμο.">
    <meta name="googlebot" content="NOODP">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="twitter:site" content="@https://twitter.com/gamosportal">
    <meta name="twitter:creator" content="@https://twitter.com/gamosportal">
    <meta name="twitter:description" content="Gamos Portal. ΓΑΜΟΣ ίσως η σπουδαιότερη μέρα στη ζωή σας. Το Gamos Portal είναι ένας πλήρης οδηγός που θα κάνει τον ΓΑΜΟ σας, αξέχαστο">
    <meta property='og:title' content='Gamos Portal - ΓΑΜΟΣ  βρείτε τα πάντα για την σημαντικότερη μέρα της ζωής σας'>
    <meta property='og:description' content='Gamos Portal. ΓΑΜΟΣ ίσως η σπουδαιότερη μέρα στη ζωή σας. Το Gamos Portal είναι ένας πλήρης οδηγός που θα κάνει τον ΓΑΜΟ σας, αξέχαστο'>
    <meta property='og:locale' content='el_GR'>
    <meta property='og:image' content="https://gamosportal.gr/storage/couple.jpg">
@endsection

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-lg-9 pr-md-2 pl-lg-0 px-0">
            
                <div class="featured row">
                    <div class="col-lg-6 px-0 showcase-a">
                        @include('site.homepage.showcase-a')
                    </div>
                    <div class="col-lg-6 perifereies">
                        @include('site.homepage.districts')
                    </div>
                </div>
           
            <hr>
            <div class="row">
                    @include('site.homepage.profile')
            </div>
            <hr>
            <div class="row">
                <div class="latest mb-3 mb-md-0 d-flex flex-wrap">
                    @include('site.homepage.latest')
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-3 pr-md-0 px-0 pl-md-3 sidebar" id="sidebar">
            {{-- <div class="sidebar-banner mb-4">
                <a href="/nifika.php">
                    <img src="/storage/banners/nyfika.jpg" alt="Δείτε Νυφικά Φορέματα στο Gamos Portal" title="Δείτε Νυφικά Φορέματα στο Gamos Portal">
                    <div class="cta">
                        <h3>Νυφικά 2022</h3>
                        <div class="clearfix"></div>
                        <button class="text-center">Discover More</button>
                    </div>                    
                </a>
            </div> --}}

            
{{-- 
            <div class="sidebar-banner mb-4">
                <a href="/anthostolismos-gamou.php">
                    <img src="/storage/banners/stolismos.jpg" alt="Δείτε Νυφικά Φορέματα στο Gamos Portal" title="Δείτε Νυφικά Φορέματα στο Gamos Portal">
                    <div class="cta">
                        <h3>Στολισμός Γάμου</h3>
                        <div class="clearfix"></div>
                        <button class="text-center">Discover More</button>
                    </div>                    
                </a>
            </div> --}}

            @include('site.homepage.sidebar-banners')

            <div class="sidebar-banner mb-4">
                <a href="https://www.vaptisiportal.gr" target="_blank">
                    <img loading="lazy" src="/storage/banners/vaptisiportal.jpg" alt="Και η βάπτιση γίνεται παιχνίδι" title="Και η βάπτιση γίνεται παιχνίδι">
                                        
                </a>
            </div>
            <div class="sidebar-banner mb-4">
                <a href="/ekklisies.php">
                    <img loading="lazy" src="/storage/ekklisia.jpg" alt="Θρησκευτικός Γάμος - Βρείτε Εκκλησίες" title="Θρησκευτικός Γάμος - Βρείτε Εκκλησίες" class="pt-1">
                    <div class="cta">
                        <h3 class="mt-0">Θρησκευτικός Γάμος</h3>
                        <div class="clearfix"></div>
                        <button class="text-center">Βρείτε Εκκλησίες</button>
                    </div>
                </a>
            </div>
            <div class="sidebar-banner mb-4">
                <a href="/dimarxeia.php">
                    <img loading="lazy" src="/storage/dimarxeio.jpg" alt="Πολιτικός Γάμος - Βρείτε Δημαρχεία" title="Πολιτικός Γάμος - Βρείτε Δημαρχεία">
                    <div class="cta">
                        <h3>Πολιτικός Γάμος</h3>
                        <div class="clearfix"></div>
                        <button class="text-center">Βρείτε Δημαρχεία</button>
                    </div>
                </a>
            </div>
            {{-- <div class="sidebar-banner">
                @include('site.homepage.lucky')
            </div> --}}
            {{-- @include('site.homepage.facebook-page') --}}
        </div>
    </div>
    <hr>
    <div class="row">
        @include('site.homepage.news')
    </div>
</div>


@endsection