<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EntryContactRepNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $category;
    public $entry;


    public function __construct($category, $entry)
    {
        $this->category = $category;
        $this->entry = $entry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('site.emails.entrycontactrepnorification')
            ->to($this->entry->client->representative->email)
            ->subject('Ο πελάτης σας '. $this->entry->name .' είχε μήνυμα' );
    }
}
