@extends('site.specialentry.show')

@section('specialentrycontent')
    <ul class="list-inline parent-container gallery">
        @foreach($specialimages as $image)
            <li class="col-md-3 thumbnail">
            <a href="/storage/uploads/special/{{$specialentry->id}}/{{$image->image}}" title="{{$image->description}}" data-entrylink="/special/{{$specialentry->slug}}">
                    <img class="" src="/storage/uploads/special/{{$specialentry->id}}/{{$image->image}}" title="click για μεγέθυνση">
                </a>
            </li>
        @endforeach
    </ul>
    
@endsection