<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Entries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function ($table) {
            $table->increments('id');
            $table->integer('cat_id');
            $table->text('dist_id');
            $table->text('name');
            $table->text('slug');
            $table->text('logo');
            $table->text('person')->nullable();
            $table->text('address')->nullable();
            $table->text('area')->nullable();
            $table->text('phone')->nullable();
            $table->text('mobile')->nullable();
            $table->text('website')->nullable();
            $table->text('email')->nullable();
            $table->text('facebook')->nullable();
            $table->text('google')->nullable();
            $table->text('twitter')->nullable();
            $table->text('instagram')->nullable();
            $table->text('pinterest')->nullable();
            $table->text('youtube')->nullable();
            $table->text('chat_id')->nullable();
            $table->text('map')->nullable();            
            $table->text('image');
            $table->text('profile');
            $table->text('metatitle')->nullable();
            $table->text('metadescription')->nullable();
            $table->text('metakeywords')->nullable();
            $table->text('offer')->nullable();
            $table->text('newlife')->nullable();
            $table->text('client_id');
            $table->boolean('active');
            $table->integer('order');
            $table->date('start_at');
            $table->date('end_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
