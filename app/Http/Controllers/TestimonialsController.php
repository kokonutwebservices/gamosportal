<?php

namespace App\Http\Controllers;

use App\Client;
use App\Testimonial;
use Storage;
use Illuminate\Http\Request;

class TestimonialsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::all();

        return view('admin.testimonials.index', compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $clients = Client::all();

        return view('admin.testimonials.create', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|min:3',
            'testimonial' => 'required|min:20',
            'image' => 'image|mimes:jpeg|max:500'
        ]);
  

        $file = $request->file('image');
        $ext = $file->getClientOriginalExtension();
        $filename = 'testimonial-'.time().'.'.$ext;
        request()->file('image')->storeAs('testimonials', $filename);


        $testimonial = new Testimonial;
        $testimonial->name = request('name');
        $testimonial->testimonial = request('testimonial');
        $testimonial->image = $filename;
        $testimonial->company = request('company');
        $testimonial->position = request('position');
        $testimonial->link = request('link');
        $testimonial->order = $testimonial->newOrder();
        $testimonial->published = request('published');

        $testimonial->save();

        $id = Testimonial::all()->last()->id;

        if($request->submitbutton == 'save-stay'){
            return redirect()->route('admin.testimonials.edit', $id); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.testimonials.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.testimonials.create');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonials  $testimonials
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('admin.testimonials.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonials  $testimonials
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial $testimonial)
    {
        
        $this->validate($request, [
            'name' => 'required|min:3',
            'testimonial' => 'required|min:20',
            'image' => 'image|mimes:jpeg|max:500'
        ]);

        $testimonial = Testimonial::find($testimonial->id);
        

        if($request->hasFile('image')) {
            $filename = $testimonial->image;
            request()->file('image')->storeAs('testimonials', $filename);
        } else {
            $filename = $testimonial->image;
        }

        
        $testimonial->name = request('name');
        $testimonial->testimonial = request('testimonial');
        $testimonial->image = $filename;
        $testimonial->company = request('company');
        $testimonial->position = request('position');
        $testimonial->link = request('link');
        $testimonial->published = request('published');

        $testimonial->save();

        if($request->submitbutton == 'save-stay'){
            return redirect()->route('admin.testimonials.edit', $testimonial); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.testimonials.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.testimonials.create');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonials  $testimonials
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {

        Storage::delete("testimonials/$testimonial->image");

        $testimonial->delete();

        return redirect()->route('admin.testimonials.index');
    }

    public function order() {
        $orders = explode(',',$_POST['order']);

        foreach ($orders as $neworder=>$value){                
            $id = $value;			
            $order = $neworder+1;
            $testimonial = Testimonial::find($id);
            $testimonial->order = $order;
            $testimonial->save();
        }
    }
}
