@extends ('admin.layouts.master')

@section('content')

<h1>Περιφέρειες</h1>

<ul class="list-group">
    @foreach ($districts as $district)

        <li class="list-group-item"><a href="districts/{{$district->id}}/edit"><span> {{$district->id}} </span> - {{$district->name}}</a></li>
        
    @endforeach
</ul>
<div class="col-md-12">
    <a href="districts/create" class="btn btn-primary">Προσθήκη Περιφέρειας</a>
</div>


@endsection