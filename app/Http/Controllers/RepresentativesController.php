<?php

namespace App\Http\Controllers;

use App\Representative;
use App\Client;
use App\Exports\ClientsExport;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class RepresentativesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $representatives = Representative::all();
        return view ('admin.representatives.index', compact('representatives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.representatives.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'name' => 'required|min:3',
            'mobile' => 'required',
            'email' => ['required', Rule::unique('representatives')]
        ]);

        Representative::create($attributes);

        return redirect('/admin/representatives');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Representative  $representative
     * @return \Illuminate\Http\Response
     */
    public function edit(Representative $representative)
    {
        return view('admin.representatives.edit', compact('representative'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Representative  $representative
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Representative $representative)
    {
        
        $attributes = request()->validate([
            'mobile' => 'required',
            'email' => 'required'
        ]);

        $representative->email = $attributes['email'];
        $representative->mobile = $attributes['mobile'];
        $representative->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Representative  $representative
     * @return \Illuminate\Http\Response
     */
    public function destroy(Representative $representative)
    {
        //
    }
}
