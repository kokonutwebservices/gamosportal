@extends('admin.layouts.master')

@section('content')

<h1> Φωτογραφίες {{$specialentry->name}} </h1>

@php
    //Έλεγχος ώστε ο αριθμός των αρχείων να μην ξεπερνάει τα 24
    $imageNo = count(Storage::files('uploads/special/'.$specialentry->id));
    $avail = 24 - $imageNo;
@endphp

@if ($imageNo <=24 AND $avail != 0)
    <p>Μπορείτε να προσθέσετε ακόμη {{$avail}} φωτογραφίες</p>
@elseif ($avail == 0)
<p>Δεν μπορείτε να προσθεσετε περισσότερες φωτογραφίες. Το άλμπουμ έχει {{$imageNo}}</p>
@endif

{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif

{{--  Λίστα εικόνων  --}}
<div class="col-md-12 d-flex flex-wrap" id="specialImagesSortable">

@foreach($specialImages as $key=>$image)
@php
    $number = $key+1;
@endphp
<div class="img-thumbnail col-md-3 adminAlbumImage" id="{{$image->id}}">
    {{$number}}
    <img src="/storage/uploads/special/{{$specialentry->id}}/{{$image->image}}" style="width:200px;" class="mx-auto mb-3 d-block">

    {{--  Κουμπί περιγραφής  --}}
    <button class="btn btn-info desc-button"><i class="far fa-comment"></i></i></button>

    {{--  Κουμπί Διαγραφής  --}}
    <a class="btn btn-info" href="/admin/specialentries/{{$specialentry->id}}/images/{{$image->id}}/delete"><i class="fa fa-trash" aria-hidden="true"></i></a>

    <div class="image-description">            
        <div class="">
            <h5 class="modal-title">Περιγραφή εικόνας</h5>
        </div>
            <div class="">
                {!!Form::open(['action' => ['SpecialImagesController@storeDescription', $specialentry->id, $image->id], 'method' => 'POST'])!!}
                <textarea name="description" class="form-control mb-3"> {!!$image->description!!} </textarea>
                
            
            <button type="submit" class="btn btn-primary">Αποθήκευση</button>
            {!!Form::close()!!}
            </div>
    </div>
       

</div>
@endforeach

</div>

{{--  Φόρμα ανεβάσματος εικόνων  --}}
{!!Form::open(['method'=>'POST', 'action'=>['SpecialImagesController@store', $specialentry->id], 'class'=>'form', 'enctype'=>'multipart/form-data'])!!}
@if($avail > 0)
    <div class="form-controller">
        {{Form::file('image[]', ['multiple', 'class' => 'form-control-file'])}}
    </div>
    <div class="col-md-12">
    <hr>
    </div>
@endif
    <div class="form-controller">
        {{Form::hidden('available', $avail)}}
        {{Form::button('Προσθήκη', ['type'=>'submit', 'class' => 'btn btn-primary'])}}
        <a href="/admin/specialentries/{{$specialentry->id}}/edit" class="btn btn-secondary">Επιστροφή στην καταχώρηση</a>
    </div>
{!!Form::close()!!}

<div id="uploading" style="display:none;" class="alert alert-info">Παρακαλούμε περιμένετε. Γίνεται βελτιστοποίηση των εικόνων.</div>
    
@endsection