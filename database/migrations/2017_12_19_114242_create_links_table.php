<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entry_id');
            $table->timestamps();
        });

        Schema::create('link_category', function (Blueprint $table) {
            $table->integer('link_id');
            $table->integer('category_id');
            $table->primary(['link_id', 'category_id']);
        });

        Schema::create('link_district', function (Blueprint $table) {
            $table->integer('link_id');
            $table->integer('district_id');
            $table->primary(['link_id', 'district_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
        Schema::dropIfExists('link_category');
        Schema::dropIfExists('link_district');
    }
}
