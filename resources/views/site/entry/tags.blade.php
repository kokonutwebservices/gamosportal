@if (count($entry->tags))
    <span><i class="fas fa-tags mr-3"></i></span>                                
@endif
@foreach ($entry->tags as $tag)
    @if ($tag && $tag->page)
    <a href="/tags/{{$tag->page->slug}}" class="tag" >{{$tag->name}}</a>
    @endif                                
@endforeach