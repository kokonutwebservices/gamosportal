@php
    $sidebar = App\Home::where('position', 'Sidebar')->first();
    $banners = $sidebar->banners()->get()->shuffle();            
@endphp
@foreach($banners as $banner)
    @if ($banner->expires > Carbon\Carbon::now())
        <div class="sidebar-banner">        
                @if ($banner->entry_id)
                    <a href="{{$banner->url}}">
                        <img loading="lazy" src="storage/banners/{{$banner->banner}}" alt="Κάντε κλικ στο banner" title="Κάντε κλικ στο banner">
                        <small class="pull-right">{{{$banner->entry->name}}}</small>
                    </a>
                @else
                <a href="{{$banner->url}}?utm_source=gamosportal&utm_medium=referral" target="_blank" rel="nofollow">
                    <img loading="lazy" src="storage/banners/{{$banner->banner}}" alt="Κάντε κλικ στο banner" title="Κάντε κλικ στο banner">        
                </a>
                @endif         
        </div>   
    @endif                                    
@endforeach