<?php

namespace App\Http\Controllers;

use App\Client;
use App\Representative;
use Illuminate\Http\Request;
use Hash;

class ClientsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();

        return view('admin.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $representatives = Representative::all();
        return view('admin.clients.create', compact('representatives'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $client = new Client;
        $client->name = request('name');
        $client->representative_id = request('representative');
        $client->email = request('email');
        $client->password = Hash::make(request('password'));
        $client->save();

        $id = Client::all()->last()->id;

        //Redirect
        if($request->submitbutton == 'save'){
            return redirect("/admin/clients/$id/edit");
        } elseif ($request->submitbutton == 'save-close') {
            return redirect("/admin/clients");
        } elseif ($request->submitbutton == 'save-new'){
            return redirect("/admin/clients/create");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
    
        return view('clientarea.show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $representatives = Representative::all();

        return view('admin.clients.edit', compact('client', 'representatives'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {

        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);
        
        $client->name = request('name');
        $client->representative_id = request('representative');
        $client->email = request('email');
        $client->password = request('password');
        $client->save();

        //Redirect
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.clients.edit', $client);
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.clients.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.clients.create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {

        $client->delete();

        return redirect()->route('admin.clients.index');
    }

    public function search(Request $request, Client $client){

        $keyword = request('query');
        $results = Client::where('name', 'LIKE', '%'.$keyword.'%')->get();

        return view('admin.clients.results', compact('results'));
    }
}
