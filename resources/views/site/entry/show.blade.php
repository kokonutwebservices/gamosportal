@extends('site.layouts.master')
@section('metatags')
    <title>{{ Meta::entryTitle($entry) }}</title>
    <meta name="description" content="{{ $entry->metadescription }}">
    <meta name="keywords" content="{{ $entry->metakeywords }}">
    <meta name="geo.region" content="GR">
    <meta name="geo.position" content="{{ $entry->map }}">
    <meta name="ICBM" content="{{ $entry->map }}">
    <meta property="og:title" content="{{ $entry->metatitle }}">
    <meta property="og:type" content="website">
    <meta property="og:url"
        content="https://gamosportal.gr/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}/{{ $entry->slug }}">
    <meta property="og:description" content="{{ $entry->metadescription }}">
    <meta property="og:image"
        content="https://gamosportal.gr/storage/entries/{{ $category->id }}/{{ $entry->image }}">

    <script src='https://www.google.com/recaptcha/api.js'></script>
    {{ Meta::canonical($entry) }}
@endsection
@section('head-scripts')
    {{-- <!-- Event snippet for Υποβολή φόρμας δυνητικού πελάτη conversion page
    In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
    <script>
        function gtag_report_conversion(url) {
            var callback = function () {
                if (typeof(url) != 'undefined') {
                window.location = url;
                }
            };
            gtag('event', 'conversion', {
                'send_to': 'AW-957693429/LbnHCK-supMCEPX71MgD',
                'event_callback': callback
            });
            return false;
            }
    </script>     --}}
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="/css/photoswipe.css">
    <link rel="stylesheet" href="/css/default-skin/default-skin.css">
    <script src="/js/photoswipe.min.js"></script>
    <script src="/js/photoswipe-ui-default.min.js"></script>
@endsection

@section('content')
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    {{--  Breadcrubs  --}}
    <div class="container entry">
        <div class="row">
            <ol class="breadcrumb mb-3" itemscope itemtype="https://schema.org/BreadcrumbList">
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="/">
                        <span itemprop="name">Αρχική</span>
                    </a>
                    <meta itemprop="position" content="1" />
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="/{{ $district->slug }}">
                        <span itemprop="name">{{ $district->name }}</span>
                    </a>
                    <meta itemprop="position" content="2" />
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="/{{ $district->slug }}/{{ $category->cat_slug }}">
                        <span itemprop="name">{{ $category->cat_name }}</span>
                    </a>
                    <meta itemprop="position" content="3" />
                </li>
                <li class="breadcrumb-item active" itemprop="itemListElement" itemscope
                    itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{ $entry->name }}</span>
                    <meta itemprop="position" content="4" />
                </li>
            </ol>

        </div>
        <div class="entry-top ">
            <img loading="eager" class="profile-pic d-block d-lg-none mx-auto"
                src="/storage/entries/{{ $category->id }}/{{ $entry->image }}"
                alt="{{ $entry->category->cat_name }} {{ $entry->name }}"
                title="{{ $entry->category->cat_name }} {{ $entry->name }}">
            <h1 class="text-center"> {{ $entry->name }} </h1>
        </div>
        {{-- <div class="entry-top d-none d-lg-block float-left">
            <img loading="eager" class="profile-pic d-block d-lg-none mx-auto"
                src="/storage/entries/{{ $category->id }}/{{ $entry->image }}"
                alt="{{ $entry->category->cat_name }} {{ $entry->name }}"
                title="{{ $entry->category->cat_name }} {{ $entry->name }}">
        </div>
        <h1 class="text-center d-none d-lg-block my-3"> {{ $entry->name }} </h1> --}}
        <div class="row">
            <div class="col-lg-3 entry-contact">

                @include('site.entry.mobile-contact')
                @include('site.entry.entry-contact')
            </div>

            <div class="col-lg-6">
                <nav class="nav d-flex justify-content-between entrymenu">
                    <li class="nav-item">
                        <a href="/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}/{{ $entry->slug }}#profile"
                            title="Προφίλ"
                            class="nav-link {{ \Route::current()->getName() == 'profile' ? 'active' : '' }}">
                            <i class="fas fa-id-badge fa-2x d-block m-auto"></i>
                            Προφίλ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}/{{ $entry->slug }}/photos#albums"
                            title="Φωτογραφίες"
                            class="nav-link {{ \Route::current()->getName() == 'albums' || \Route::current()->getName() == 'photos' ? 'active' : '' }}">
                            <i class="fas fa-images fa-2x d-block m-auto"></i>
                            Φωτογραφίες
                        </a>
                    </li>
                    @if (count($entry->videos) > 0)
                        <li class="nav-item">
                            <a href="/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}/{{ $entry->slug }}/video#videos"
                                title="video"
                                class="nav-link {{ \Route::current()->getName() == 'video' ? 'active' : '' }}">
                                <i class="fab fa-youtube fa-2x d-block m-auto"></i>
                                Video
                            </a>
                        </li>
                    @endif
                    @if ($entry->facebook)
                        <li class="nav-item">
                            <a href="/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}/{{ $entry->slug }}/facebook#facebook"
                                class="nav-link {{ \Route::current()->getName() == 'facebook' ? 'active' : '' }}">
                                <i class="fab fa-facebook fa-2x d-block m-auto"></i>
                                Timeline
                            </a>
                        </li>
                    @endif

                </nav>
                @yield('entrycontent')
            </div>

            <div class="col-lg-3">
                <div class="fb-page d-none d-lg-block" data-href="{{ $entry->facebook }}" data-small-header="false"
                    data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                </div>
                <div class="">
                    <hr>
                    @include('site.entry.offer')
                    <hr>
                </div>
                <div id="contact-form">
                    @include('site.entry.contact')
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row mt-5">
            @include('site.entry.tags')
        </div>
        <div class="clearfix"></div>
        <div class="row mt-5">
            @include('site.entry.more')
        </div>
        <div class="d-lg-none mobile-contact-icon">
            <a href="#contact-form"><i class="far fa-envelope fa-2x"></i></a>
        </div>
    </div>
@endsection

@section('scripts')
    {{-- <script src="/js/photoswipe.min.js"></script>
    <script src="/js/photoswipe-ui-default.min.js"></script> --}}
    {{-- <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e9587fd1b16f887" async></script> --}}
@endsection
