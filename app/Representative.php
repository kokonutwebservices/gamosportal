<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representative extends Model
{
    protected $guarded = ['_token'];

    public function clients(){

        return $this->hasMany(Client::class);

    }
}
