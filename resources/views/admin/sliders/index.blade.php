@extends('admin.layouts.master')

@section('content')

    <h1>Διαχείριση Sliders</h1>
    @if (session('message'))
        <div class="alert alert-success" id="successMessage">
            {{ session('message') }}
        </div>
    @endif

    <table class="table">
        <thead class="thead-inverse">
            <tr>
            <th>@sortablelink('id')</th>
            <th>@sortablelink('entry', 'Καταχώρηση')</th>
            <th>@sortablelink('url', 'url') </th>
            <th>@sortablelink('expires', 'Ημ. Λήξης')</th>
            <th>Edit</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sliders as $slider)
                <tr>
                    <td>{{$slider->id}}</td>
                    <td>
                    @if($slider->entry_id != 0)
                        {{$slider->entry->name}}</td>
                    @endif                    
                    <td>{{$slider->url}}</td>
                    <td>{{$slider->expires}}</td>
                    <td><a href="sliders/{{$slider->id}}/edit">Edit</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <a class="btn btn-primary" href="/admin/sliders/create">Δημιουργία Slider</a>
    
@endsection