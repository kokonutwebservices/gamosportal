<?php

namespace App\Http\Controllers;

use App\Church;
use App\District;
use App\Metropole;
use App\Municipality;
use Storage;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;

class AdminChurchController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $churches = Church::sortable()->paginate(50);

        return view('admin.churches.index', compact('churches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = District::all();
        $metropoles = Metropole::all();

        return view('admin.churches.create', compact('districts', 'metropoles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Church::validateChurch($request);
        
        //Creation
        Church::create([
            'district_id' => $request->district_id,
            'metropole_id' => $request->metropole_id,
            'name' => $request->name,
            'slug' => $request->slug,
            'person' => $request->person,
            'address' => $request->address,
            'area' => $request->area,
            'phone' => $request->phone,
            'mobile' => $request->mobile,
            'website' => $request->website,
            'email' => $request->email,
            'map' => $request->map,
            'metatitle' => $request->metatitle,
            'metadescription' => $request->metadescription
        ]);

        $church = Church::all()->last();
        
        //Redirect
        return $church->redirectAfterSave($request);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Church  $church
     * @return \Illuminate\Http\Response
     */
    public function show(Church $church)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Church  $church
     * @return \Illuminate\Http\Response
     */
    public function edit(Church $church)
    {
        
        $districts = District::all();
        $metropoles = Metropole::all();
        return view('admin.churches.edit',compact('church', 'districts', 'metropoles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Church  $church
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Church $church)
    {

        Church::validateChurch($request);

        
        $church->district_id = $request->district_id;
        $church->metropole_id = $request->metropole_id;
        $church->name = $request->name;
        $church->slug = $request->slug;
        $church->person = $request->person;
        $church->address = $request->address;
        $church->area = $request->area;
        $church->phone = $request->phone;
        $church->mobile = $request->mobile;
        $church->website = $request->website;
        $church->email = $request->email;
        $church->map = $request->map;
        $church->metatitle = $request->metatitle;
        $church->metadescription = $request->metadescription;
        $church->save();

        //Redirect
        return $church->redirectAfterSave($request);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Church  $church
     * @return \Illuminate\Http\Response
     */
    public function destroy(Church $church)
    {
        $church->delete();

        return redirect()->route('admin.churches.index')->with('message', 'Η εκκλησία διαγράφηκε');
    }

    public function search(Request $request, Church $church){

        $keyword = request('query');
        $results = Church::where('name', 'LIKE', '%'.$keyword.'%')->get();

        return view('admin.churches.results', compact('results'));
    }
}
