@foreach($pages as $page)
    @if (count($page->category->entries->where('active', 1)) > 0)
        <a class="dropdown-item" href="/{{$page->slug}}">{{$page->category->cat_name}}</a>
    @endif    
@endforeach