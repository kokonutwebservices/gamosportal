@extends('admin.layouts.master')

@section('content')

<h1>Δημαρχεία</h1>

{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif

<div class="col-md-12">
    <div class="form-group col-md-4 pull-right">
    {!!Form::open(['method' => 'POST', 'action' => 'MunicipalityController@search'])!!}
        <div class="input-group">
        <input type="text" class="form-control" name="query" placeholder="Αναζήτηση" aria-label="Αναζήτηση">
        <span class="input-group-btn">
            <button class="btn btn-secondary" type="submit">Go!</button>
        </span>
        </div>
    {!!Form::close()!!}
    </div>
</div>

<table class="table">
<thead class="thead-inverse">
    <tr>
    <th>@sortablelink('id')</th>
    <th>@sortablelink('name', 'Όνομα Δημαρχείου')</th>
    <th>@sortablelink('district_id', 'Περιφέρεια') </th>
    </tr>
</thead>

    @foreach($municipalities as $municipality)
    <tr>
        <td>{{$municipality->id}}</td>
        <td><a href="municipalities/{{$municipality->id}}/edit">{{$municipality->name}}</a></td>
        <td>{{$municipality->district->name}}</td>
    </tr>

    @endforeach
</table>
<div>
    <a href="municipalities/create" class="btn btn-primary">Προσθήκη Νέου Δημαρχείου</a>
</div>
    
@endsection