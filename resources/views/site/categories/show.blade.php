@extends('site.layouts.master')

@section('metatags')
    <title>{{ Meta::title($district, $category) }}</title>
    <meta name="description" content="{{ Meta::description($district, $category) }}">
    <meta name="keywords" content="{{ Meta::keywords($district, $category) }}">
    <link rel="canonical" href="{{ $category->page ? config('app.url').'/'.$category->page->slug :'' }}" />

@endsection


@section('content')
    <div class="container">

        <div class="row mt-4">
            <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="/">
                        <span itemprop="name">Αρχική</span>
                    </a>
                    <meta itemprop="position" content="1" />
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="/{{ $district->slug }}">
                        <span itemprop="name">{{ $district->name }}</span>
                    </a>
                    <meta itemprop="position" content="2" />
                </li>
                <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{ $category->cat_name }}</span>
                    <meta itemprop="position" content="3" />
                </li>
            </ol>
            
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1>{{ $category->cat_name }}</h1>
                {!! $category->cat_description !!}
            </div>
            <div class="list d-flex align-content-between flex-wrap col-md-12">
                @foreach ($entries as $entry)
                    @if (Carbon\Carbon::parse($entry->end_at)->addMonth() > Carbon\Carbon::now() and $entry->active)
                        <div class="list-item col-md-4 col-lg-3 ">
                            <a href="{{ $category->cat_slug }}/{{ $entry->slug }}">
                                <img class="mx-auto d-block"
                                    src="/storage/entries/{{ $entry->cat_id }}/small/{{ $entry->image }}"
                                    alt="{{ $entry->name }}" title="{{ $entry->name }}">
                            </a>
                            <a href="{{ $category->cat_slug }}/{{ $entry->slug }}">
                                <h5 class="mb-2">{{ $entry->name }}</h5>
                            </a>
                            @if (count($entry->tags))
                                <span><i class="fas fa-tags mt-3"></i></span>
                            @endif
                            @foreach ($entry->tags as $tag)
                                <a class="tag" href="/tags/{{ $tag->page->slug }}">{{ $tag->name }}</a>
                            @endforeach
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection
