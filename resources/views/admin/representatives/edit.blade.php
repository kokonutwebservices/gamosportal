@extends('admin.layouts.master')

@section('content')
    <h1>Πωλητής: {{$representative->name}} </h1>

    <h2>Στοιχεία</h2>
    <form class="form-row" action="/admin/representatives/{{$representative->id}}" method="POST">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="PUT">
        <div class="form-group col-md-4">
          <label for="mobile">Τηλέφωνο</label>
          <input type="phone" class="form-control" name="mobile" id="" value="{{$representative->mobile}}" pattern="[0-9]{10}" required>
        </div>
        <div class="form-group col-md-4">
          <label for="email">Email</label>
          <input type="email" class="form-control" name="email" id="" value="{{$representative->email}}" pattern=".+@gamosportal.gr" required>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Ενημέρωση στοιχείων</button>
        </div>
    </form>
    
{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif

<h2>Πελατoλόγιο Πωλητή</h2>

<table class="table">
    <thead>
        <tr>
            <th>Πελάτης</th>
            <th>Καταχώρηση</th>
            <th>Κατηγορία</th>
            <th>Ημερομηνία Λήξης</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($representative->clients as $client)

            @foreach ($client->entries as $entry)
                <tr>
                    <td scope="row">{{$client->name}}</td>
                    <td>{{$entry->name}}</td>
                    <td>{{$entry->category->cat_name}}</td>
                    <td>{{$entry->end_at}}</td>
                </tr>                
            @endforeach
            
        @endforeach

    </tbody>
</table>

@endsection