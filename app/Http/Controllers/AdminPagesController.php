<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Category;
use Illuminate\Support\Facades\Storage;

class AdminPagesController extends Controller
{
    public function index(){
        $pages = Page::all()->sortBy('order');
        return view('admin.pages.index', compact('pages'));
    }

    public function create(){

        $categories = Category::where('cat_parent_id', '>', 0)->get();

        return view('admin.pages.create', compact('categories'));
    }

    public function store(Request $request){

        $page = new Page();
        
        $page->validatePage($request);

        $page->title = $request->title;
        $page->slug = $request->slug;
        $page->metatitle = $request->metatitle;
        $page->metadescription = $request->metadescription;
        $page->metakeywords = $request->metakeywords;
        $page->body = $request->body;
        $page->title2 = $request->title2;
        $page->category_id = $request->category_id;
        $page->save();

        $page = Page::all()->last();

        
        return $page->redirectAfterSave($request);

    }

    public function edit($id){

        $categories = Category::where('cat_parent_id', '>', 0)->get();
        $page = Page::find($id);
       

        return view('admin.pages.edit', compact(['page', 'categories']));
    }

    public function update(Request $request, Page $page)
    {    
        
      
        $page->validatePage($request);
                
        $page->title = $request->title;
        $page->slug = $request->slug;
        $page->metatitle = $request->metatitle;
        $page->metadescription = $request->metadescription;
        $page->metakeywords = $request->metakeywords;
        $page->body = $request->body;
        $page->title2 = $request->title2;
        $page->extratext = $request->extratext;
        $page->category_id = $request->category_id;
        $page->save();

        return $page->redirectAfterSave($request);
    }

    public function destroy (Page $page) {
        
        $page->delete();

        return redirect('admin/pages');
    }

    public function order (){
        $orders = explode(',',$_POST['order']);

        foreach ($orders as $neworder=>$value){
            
            $pageid = $value;	
            $order = $neworder+1;
            $page = Page::find($pageid);
            $page->order = $order;            
            $page->save();
        }
    }
}
