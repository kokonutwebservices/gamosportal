@extends('admin.layouts.master')

@section('content')
<h1>Σελίδες Πληροφοριών</h1>

{{ csrf_field() }}

<ul id="infoPageSortable">
    @foreach($infoPages as $infoPage)

        <li id="{{$infoPage->id}}"><a href="infopages/{{$infoPage->id}}/edit">{{$infoPage->title}}</a></li>

    @endforeach
</ul>

<div>
    <a href="infopages/create" class="btn btn-primary">Δημιουργία Νέας Σελίδας</a>
</div>
@endsection