@extends('admin.layouts.master')

@section('content')

    <h1>Εκκλησίες Gamos Portal</h1>
    {{--  Πλαίσιο σφαλμάτων  --}}

    <div class="col-md-12">
        <div class="form-group col-md-4 pull-right">
        {!!Form::open(['method' => 'POST', 'action' => 'ChurchController@search'])!!}
            <div class="input-group">
            <input type="text" class="form-control" name="query" placeholder="Αναζήτηση" aria-label="Αναζήτηση">
            <span class="input-group-btn">
                <button class="btn btn-secondary" type="submit">Go!</button>
            </span>
            </div>
        {!!Form::close()!!}
        </div>
    </div>
    <table class="table">
    <thead class="thead-inverse">
        <tr>
            <th>@sortablelink('id')</th>
            <th>@sortablelink('name', 'Όνομα Εκκλησίας')</th>
            <th>@sortablelink('metropole_id', 'Μητρόπολη')</th>
            <th>@sortablelink('district_id', 'Περιφέρεια') </th>
        </tr>
    </thead>
        @foreach($results as $church)
            <tr>
                <td>{{$church->id}}</td>
                <td><a href="{{$church->id}}/edit">{{$church->name}}</a></td>
                <td>{{$church->metropole->name}}</td>
                <td>{{$church->district->name}}</td>
            </tr>
        @endforeach
    </table>


@endsection