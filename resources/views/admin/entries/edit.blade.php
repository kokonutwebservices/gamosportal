@extends('admin.layouts.master')

@section('content')
@include('admin.layouts.errors')
{!! Form::open(['action'=>['EntriesController@update', $entry->id], 'method'=>'POST', 'enctype' => 'multipart/form-data']) !!}

<div class="form-row">

    <div class="form-group col-md-6">
        <label for="client_id">Πελάτης</label>
        <select class="form-control" id="client_id" name="client_id">
            @foreach ($clients as $client)
                @if($client->id == $entry->client_id)
                <option value="{{$client->id}}" selected>{{$client->name}}</option>
                @else
                <option value="{{$client->id}}">{{$client->name}}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-12">
        {{Form::label('name', 'Όνομα Επιχείρησης')}}
        {{Form::text('name', $entry->name, ['class'=>'form-control', 'placeholder' => 'Το όνομα όπως θα φαίνεται στην καταχώρηση', 'required'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('slug', 'slug')}}
        {{Form::text('slug', $entry->slug, ['class'=>'form-control', 'placeholder' => 'friendly url', 'required'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('logo', 'Logo')}}
        {{Form::file('logo')}}
        <img src="/storage/logos/{{$entry->cat_id}}/{{$entry->logo}}">
    </div>

    <div class="form-group col-md-12">
        {{Form::label('person', 'Υπεύθυνος')}}
        {{Form::text('person', $entry->person, ['class'=>'form-control', 'placeholder' => 'Όνομα Υπευθύνου'] )}}
    </div>

    <div class="form-group col-md-12">
        {{Form::label('address', 'Διεύθυνση')}}
        {{Form::text('address', $entry->address, ['class'=>'form-control', 'placeholder' => 'Διεύθυνση'] )}}
    </div>

    <div class="form-group col-md-4">
      <label for="tags">Tags</label>
      <select multiple class="form-control" name="tags[]" id="tags">
        
        @foreach ($tags as $tag)

            <option value="{{$tag->id}}" {{$entry->tags->contains($tag->id) ? 'selected' : ''}} >{{$tag->name}}</option>
            
        @endforeach
      </select>
    </div>

    <div class="form-group col-md-4">
        {{Form::label('phone', 'Τηλέφωνο')}}
        {{Form::text('phone', $entry->phone, ['class'=>'form-control', 'placeholder' => 'Τηλέφωνο'] )}}
    </div>

    <div class="form-group col-md-4">
        {{Form::label('mobile', 'Κινητό')}}
        {{Form::text('mobile', $entry->mobile, ['class'=>'form-control', 'placeholder' => 'Κινητό'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('website', 'Website')}}
        {{Form::text('website', $entry->website, ['class'=>'form-control', 'placeholder' => 'Website'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('email', 'Email')}}
        {{Form::text('email', $entry->email, ['class'=>'form-control', 'placeholder' => 'Email'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('facebook', 'Facebook')}}
        {{Form::text('facebook', $entry->facebook, ['class'=>'form-control', 'placeholder' => 'Facebook'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('google', 'Google+')}}
        {{Form::text('google', $entry->google, ['class'=>'form-control', 'placeholder' => 'Google+'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('twitter', 'Twitter')}}
        {{Form::text('twitter', $entry->twitter, ['class'=>'form-control', 'placeholder' => 'twitter'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('instagram', 'Instagram')}}
        {{Form::text('instagram', $entry->instagram, ['class'=>'form-control', 'placeholder' => 'Instagram'] )}}
    </div> 

    <div class="form-group col-md-6">
        {{Form::label('pinterest', 'Pinterest')}}
        {{Form::text('pinterest', $entry->pinterest, ['class'=>'form-control', 'placeholder' => 'Pinterest'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('youtube', 'YouTube')}}
        {{Form::text('youtube', $entry->youtube, ['class'=>'form-control', 'placeholder' => 'YouTube'] )}}
    </div>
    
    <div class="form-group col-md-6">
        {{Form::label('vimeo', 'Vimeo')}}
        {{Form::text('vimeo', $entry->vimeo, ['class'=>'form-control', 'placeholder' => 'Vimeo'] )}}
    </div>

    <div class="form-group col-md-6">
        {{Form::label('chat_id', 'Chat ID')}}
        {{Form::text('chat_id', $entry->chat_id, ['class'=>'form-control', 'placeholder' => 'Chat ID'] )}}
    </div> 

    <div class="form-group col-md-6">
        {{Form::label('map', 'Google Maps')}}
        {{Form::text('map', $entry->map, ['class'=>'form-control', 'placeholder' => 'Google Maps'] )}}
    </div>
    
    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-6">
        <label for="category_id">Κατηγορία</label>
        <select class="form-control" id="cat_id" name="cat_id">
            @foreach ($categories as $category)
                @if($category->cat_parent_id >0)
                    @if ($category->id == $entry->cat_id)
                    <option value="{{$category->id}}" selected>{{$category->cat_name}}</option>
                    @else
                    <option value="{{$category->id}}">{{$category->cat_name}}</option>
                    @endif
                @endif
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-6">
        <label for="district_id">Περιφέρεια</label>
        <select class="form-control" id="dist_id" name="dist_id">
            @foreach ($districts as $district)
                @if($district->id == $entry->dist_id)
                <option value="{{$district->id}}" selected>{{$district->name}}</option>
                @else
                <option value="{{$district->id}}">{{$district->name}}</option>
                @endif            
            @endforeach
        </select>
    </div>

    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group ">
        {{Form::label('image', 'Εικόνα Προφίλ')}}
        {{Form::file('image')}}
        <img src="/storage/entries/{{$entry->cat_id}}/{{$entry->image}}">
    </div>

    <div class="form-group col-md-12">
        <label for="profile">Κείμενο Προφίλ</label>        
        <textarea class="form-control tinyMCE" id="profile" name="profile" required >{{$entry->profile}}</textarea>
    </div> 

    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-12">
        {{Form::label('metatitle', 'Τίτλος')}}
        {{Form::text('metatitle', $entry->metatitle, ['class'=>'form-control', 'placeholder' => 'Τίτλος', 'required'] )}}
    </div> 

    <div class="form-group col-md-12">
        {{Form::label('metadescription', 'Meta Description')}}
        {{Form::text('metadescription', $entry->metadescription, ['class'=>'form-control', 'placeholder' => 'Meta Description', 'required'] )}}
    </div>

    <div class="form-group col-md-12">
        {{Form::label('metakeywords', 'Meta Keywords')}}
        {{Form::text('metakeywords', $entry->metakeywords, ['class'=>'form-control', 'placeholder' => 'Meta Keywords', 'required'] )}}
    </div>
    
    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-12">
        {{Form::label('offer', 'Προσφορά στο Gamos Deals')}}
        {{Form::text('offer', $entry->offer, ['class'=>'form-control', 'placeholder' => 'url προσφοράς'] )}}
    </div>

    <div class="form-group col-md-6">
        Συμμετοχή Bridal Expo:
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="newlife" class="form-check-input" type="radio" id="inlineCheckbox1" value="1" <?php if($entry->newlife == 1){echo "checked";}else{echo"";}?>> Ναι
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="newlife" class="form-check-input" type="radio" id="inlineCheckbox2" value="0" <?php if($entry->newlife == 0){echo "checked";}else{echo"";}?>> Όχι
            </label>
        </div>
    </div>

    <div class="form-group col-md-6">
        Ενεργό:
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="active" class="form-check-input" type="radio" id="inlineCheckbox1" value="1" <?php if($entry->active == 1){echo "checked";}else{echo"";}?>> Ναι
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input name="active" class="form-check-input" type="radio" id="inlineCheckbox2" value="0" <?php if($entry->active == 0){echo "checked";}else{echo"";}?>> Όχι
            </label>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
    </div>
    <h4>Ημερομηνίες</h4>
    <div class="col-md-12">

    <p>Δημιουργία Καταχώρησης: {{ Carbon\Carbon::parse($entry->created_at)->toDateTimeString() }}</p>

    </div>
    <div id="dates">
        <div class="form-group col-md-4 text-center">
            {{Form::label('start_at', 'Έναρξη Καταχώρησης')}}
            {{Form::date('start_at', $entry->start_at, ['class'=>'form-control date', 'id'=>'start_at'] )}}
        </div>

        <div class="form-group col-md-4 text-center">
            {{Form::label('end_at', 'Λήξη Καταχώρησης')}}
            {{Form::date('end_at', $entry->end_at, ['class'=>'form-control', 'id'=>'end_at'] )}}
        </div>    

        <div class="form-group col-md-4">
            <p class="text-center"><strong>Αυτόματη Απενεργοποίηση</strong></p>
            <p class="text-center">{{Carbon\Carbon::parse($entry->start_at)->addYear()->addMonth()->toDateTimeString()}}</p>
        </div>

        <div class="col-md-12 text-center">
            <span class="btn btn-success" id="renew"><i class="fas fa-sync-alt" aria-hidden="true"></i> Ανανέωση</span>

        </div>

    </div>

    <div class="col-md-12">
        <hr>
    </div>

    <div class="col-md-12">
        <a href="/admin/entries/albums/{{$entry->id}}" class="btn btn-default mr-md-3 col-md-2">Φωτογραφίες</a>
        <a href="/admin/entries/video/{{$entry->id}}" class="btn btn-default mr-md-3 col-md-2">Video</a>
        <a href="/admin/entries/links/{{$entry->id}}" class="btn btn-default mr-md-3 col-md-2">Links</a>
        <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}" target="_blank" class="btn btn-info mr-md-3 col-md-3">Προβολή καταχώρησης</a>
    </div>

    <div class="col-md-12">
        <hr>
    </div>

    <div class="form-group col-md-12">    
        <div class="col-md-2">
            {{Form::hidden('_method', 'PUT')}}
            {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
        </div>

        <div class="col-md-2">
            {{Form::hidden('_method', 'PUT')}}
            {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
        </div>

        <div class="col-md-2">
            {{Form::hidden('_method', 'PUT')}}
            {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
        </div>

        <div class="col-md-2">  
            <a href="/admin/entries" class="btn btn-warning pull-right">Cancel</a>
        </div>

        <div class="col-md-2">
        {!! Form::close() !!}

            {!! Form::open(['action' => ['EntriesController@destroy', $entry->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'pull-right', 'id' =>'deleteentry' ]) !!}

            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}

        {!! Form::close() !!}  
        </div>
      
       

    </div>
</div>
{!! Form::close() !!}

@endsection