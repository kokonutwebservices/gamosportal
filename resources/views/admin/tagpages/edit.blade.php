@extends('admin.layouts.master')

@section('content')

<h1>Δημιουργία Tag Page</h1>

<form action="/admin/tagpages/{{$tagpage->id}}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PATCH">

    <div class="form-group">
        <div class="form-group">
          <label for="tag_id">Tag</label>
          <select class="form-control" name="tag_id" id="tag_id">
            
            <option disabled>Επιλέξτε Tag</option>

            @foreach ($tags as $tag)

                <option value="{{$tag->id}}" {{$tagpage->tag->name == $tag->name ? 'selected' : ''}}>{{$tag->name}}</option>
                
            @endforeach

          </select>
        </div>
    </div>

    <div class="form-group">
        <label for="title">Τίτλος</label>
        <input type="text" class="form-control" name="title" id="title" aria-describedby="helpId" value="{{$tagpage->title}}">
    </div>

    <div class="form-group">
        <label for="slug">Slug</label>
        <input type="text" class="form-control" name="slug" id="slug" aria-describedby="helpId" value="{{$tagpage->slug}}">
    </div>
    
    <div class="form-group">
        <label for="metatitle">Meta Title</label>
        <input type="text" class="form-control" name="metatitle" id="metatitle" aria-describedby="helpId" value="{{$tagpage->metatitle}}">
    </div>
    <div class="form-group">
        <label for="metadescription">Meta Description</label>
        <input type="text" class="form-control" name="metadescription" id="metadescription" aria-describedby="helpId" value="{{$tagpage->metadescription}}">
    </div>

    <div class="form-group">
        <label for="metakeywords">Meta Keywords</label>
        <input type="text" class="form-control" name="metakeywords" id="metakeywords" aria-describedby="helpId" value="{{$tagpage->metakeywords}}">
    </div>

    <div class="form-group">
        <label for="body">Κείμενο</label>
        <textarea class="tinyMCE" name="body" id="" cols="30" rows="10"> {{$tagpage->body}} </textarea>
    </div>

    <div class="form-group">
      <label for="title2">Τίτλος 2</label>
      <input type="text"
        class="form-control" name="title2" id="title2" aria-describedby="helpId" value="{{$tagpage->title2}}">
    </div>

    <div class="row justify-content-between">
        <button type="submit" class="btn btn-primary" name="submitbutton" value="save-close">Save & Close</button>
        <button type="submit" class="btn btn-success" name="submitbutton" value="save-stay">Save & Stay</button>
        <button type="submit" class="btn btn-secondary" name="submitbutton" value="save-new">Save & New</button>
        
</form>

        <a href="/admin/tagpages" class="btn btn-warning">Cancel</a>
        <form id="delete" action="/admin/tagpages/{{$tagpage->id}}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">DELETE</button>
        </form>
    </div>

<div class="mt-3">
    @include('admin.layouts.errors')
</div>
@endsection