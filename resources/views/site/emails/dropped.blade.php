<h1>Η καταχώρηση {{$name}} έχει λήξει</h1>
<p>από τις {{\Carbon\Carbon::parse($end_at)}} και απενεργοποιήθηκε στις {{\Carbon\Carbon::parse($end_at)->addMonth()}}</p>

<ul>
    <li>Ημερομηνία Λήξης: {{$end_at}}</li>
    @if ($person)
        <li>Υπεύθυνος{{$person}}</li>
    @endif    
    <li>Επαγ. Κατηγ. : {{$category}}</li>
    <li>Περιφέρεια: {{$district}}</li>
    @if ($phone)
        <li>Τηλέφωνο: {{$phone}}</li>
    @endif
    @if ($mobile)
        <li>Κινητό: {{$mobile}}</li>
    @endif
    @if ($email)
        <li>Email: {{$email}}</li>
    @endif    
</ul>
