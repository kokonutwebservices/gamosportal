<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as InterImage;

class Image extends Model
{
    protected $guarded = ['_token'];
    
    public function album(){
        return $this->belongsTo(Album::class);
    }

    public function imageSize() {

        $imagePath = 'storage/uploads/'.$this->album->id.'/'.$this->image;

        $imageWidth = InterImage::make($imagePath)->width();
        $imageHeight = InterImage::make($imagePath)->height();
        echo $imageWidth.'x'.$imageHeight;
    }
    
}
