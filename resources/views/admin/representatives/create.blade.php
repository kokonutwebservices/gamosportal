@extends('admin.layouts.master')

@section('content')

<h1>Προσθήκη Πωλητή</h1>

<form action="/admin/representatives" method="post">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="name">Όνομα</label>
      <input type="text" name="name" id="" class="form-control" placeholder="Ονοματεπώνυμο Πωλητή">
    </div>
    <div class="form-group">
      <label for="mobile">Τηλέφωνο</label>
      <input type="text" class="form-control" name="mobile" id="" placeholder="μορφή: 6972825545" pattern="[0-9]{10}" required>
    </div>
    <div class="form-group">
      <label for="email">Email</label>
      <input type="text" class="form-control" name="email" id="" placeholder="email ....@gamosportal.gr" pattern=".+@gamosportal.gr" required>
    </div>

    <button type="submit" class="btn btn-primary">Δημιουργία</button>
</form>
    
@endsection