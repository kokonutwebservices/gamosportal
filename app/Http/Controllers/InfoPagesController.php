<?php

namespace App\Http\Controllers;

use App\InfoPage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class InfoPagesController extends Controller
{
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infoPages = InfoPage::all()->sortBy('order');
        return view('admin.info.index', compact('infoPages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.info.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'title' => 'required|max:80',
            'slug' => 'required|max:65',
            'metatitle' => 'required|max:80',
            'metadescription' => 'required|max:150',
            'metakeywords'=> 'required',
            'body' => 'required',           
        ]);

        if ($request->file('image')){
            $file = $request->file('image'); //Get the file
            $ext = $file->getClientOriginalExtension(); //Get the extension
            $fileNameBase = $request->input('slug');  //Get the slug to use as filename base        
            $filename = chop($fileNameBase,'.php').'.'.$ext; //create a new filename
            request()->file('image')->storeAs('infopages', $filename); //Store file            
        } else {
            $filename = "";            
        }        

        InfoPage::create([
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'metatitle' => $request->input('metatitle'),
            'metadescription' => $request->input('metadescription'),
            'metakeywords' => $request->input('metakeywords'),
            'body' => $request->input('body'),
            'image' => $filename,
            'image_alt' =>$request->input('image_alt')
            
        ]);

    
        $id = InfoPage::all()->last()->id;

        
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.infopages.edit', $id);
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.infopages.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.pages.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function show(InfoPage $infoPage, $infoSlug)
    {
        $page = InfoPage::where('slug', $infoSlug)->first();

        return view('site.infopages.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function edit(InfoPage $infopage)
    {
        return view('admin.info.edit', compact('infopage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InfoPage $infopage)
    {
         $this->validate(request(),[
             'title' => 'required|max:80',
             'slug' => 'required|max:65',
             'metatitle' => 'required|max:80',
             'metadescription' => 'required|max:150',
             'metakeywords'=> 'required',
             'body' => 'required',
         ]);
         
 
         //Image
         if($request->hasFile('image')){
            if ($request->file('image')){
                $file = $request->file('image'); //Get the file
                $ext = $file->getClientOriginalExtension(); //Get the extension
                $fileNameBase = $request->input('slug');  //Get the slug to use as filename base        
                $filename = chop($fileNameBase,'.php').'.'.$ext; //create a new filename
                request()->file('image')->storeAs('infopages', $filename); //Store file
            }
         } elseif($request->input('check') == 1 ) {
             $filename = $infopage->image;
             Storage::delete('infopages/'.$filename); 
             $filename = "";         
         } else{
             $filename = $infopage->image;
         }         
         
         $infopage->title = $request->input('title');
         $infopage->slug = $request->input('slug');
         $infopage->metatitle = $request->input('metatitle');
         $infopage->metadescription = $request->input('metadescription');
         $infopage->metakeywords = $request->input('metakeywords');
         $infopage->body = $request->input('body');         
         $infopage->image = $filename;         
         $infopage->image_alt = $request->input('image_alt');
         $infopage->save();
 

        if($request->submitbutton == 'save'){
            return redirect()->route('admin.infopages.edit', $infopage->id);
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.infopages.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.infopages.create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(InfoPage $infoPage)
    {
        Storage::delete("/storage/infopages/{{$infoPage->image}}");
        $infoPage->delete();

        return redirect()->route('admin.infopages.edit');
    }

    public function order (){
        $orders = explode(',',$_POST['order']);

        foreach ($orders as $neworder=>$value){
            
            $pageid = $value;	
            $order = $neworder+1;
            $page = InfoPage::find($pageid);
            $page->order = $order;            
            $page->save();
        }
    }
}
