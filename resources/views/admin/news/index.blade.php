@extends('admin.layouts.master')

@section('content')

<h1>News</h1>
{{csrf_field()}}
{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif
<hr>
<ul id="newsSortable">
@foreach($news as $new)

    <li id="{{$new->id}}" class="media">
        <i class="fas fa-arrows-alt mr-3" aria-hidden="true"></i>
        <img class="mr-3" src="/storage/news/{{$new->image}}" alt="" style="width:200px;">
        <div class="media-body">
            <a href="/admin/news/{{$new->id}}/edit">{{$new->title}}</a>
        </div>
    </li>
    
@endforeach
</ul>

<hr>
<a href="news/create" class="btn btn-primary">Δημιουργία Δημοσίευσης</a>
    
@endsection