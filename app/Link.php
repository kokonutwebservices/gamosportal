<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'links';
    public function districts(){
        return $this->belongsToMany(District::class);
    }

    public function categories(){
        return $this->belongsToMany(Category::class);
    }

    public function entry(){
        return $this->belongsTo(Entry::class);
    }
}
