<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Banner extends Model
{
    use Sortable;
    protected $guarded = ['_token'];

    public $sortable = ['id', 'type', 'entry', 'url', 'expires'];

    public function pages(){
        return $this->belongsToMany(Page::class);
    }

    public function home(){
        return $this->belongsToMany(Home::class)->withPivot('banner_home');
    }

    public function infoPages(){
        return $this->belongsToMany(InfoPage::class);
    }

    public function districts(){
        return $this->belongsToMany(District::class);
    }

    public function categories(){
        return $this->belongsToMany(Category::class);
    }

    public function churches(){
        return $this->belongsToMany(Church::class);
    }

    public function municipalities(){
        return $this->belongsToMany(Municipality::class);
    }

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function entry(){
        return $this->belongsTo(Entry::class);
    }
}
