@extends('admin.layouts.master')

@section('content')

<h1 class="my-5">Tags</h1>

<div class="d-flex flex-wrap justify-space-between my-5">
    @foreach ($tags as $tag)
       <div class="m-2 border border-info rounded p-2">{{$tag->name}}

        <form action="/admin/tags/{{$tag->id}}" method="POST" class="float-right px-2">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="text-danger btn-sm"><i class="far fa-trash-alt"></i></button>
        </form>

    </div>
    @endforeach
</div>

<form action="/admin/tags" method="POST">
    {{ csrf_field() }}
    <div class="input-group">
        <span class="input-group-btn">
          <button class="btn btn-primary" type="submit">+</button>
        </span>
        <input type="text" class="form-control" name="name" placeholder="">
      </div>
    
</form>
<div class="mt-5">
    @include('admin.layouts.errors')
</div> 
@endsection