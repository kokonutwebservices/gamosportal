@extends('site.specialentry.show')

@section('specialentrycontent')
    <img class="thumbnail profile-pic d-block mx-auto" src="/storage/entries/special/{{$specialentry->image}}" alt="{{$specialentry->name}}" title="{{$specialentry->name}}">
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <div class="addthis_inline_share_toolbox_vgy1"></div>
    <div class="mt-3">
    {!! $specialentry->profile !!}
    
    <a href="/specialentries/{{$specialentry->slug}}/photos" class="btn btn-default float-right my-3" >Φωτογραφίες <i class="fas fa-arrow-alt-circle-right"></i></a>

     @if($specialentry->newlife)
        <img src="/storage/new_life.jpg" alt="Συμμετέχουμε στην έκθεση New Life" title="Συμμετέχουμε στην έκθεση New Life">
    @endif 
    </div>
@endsection
