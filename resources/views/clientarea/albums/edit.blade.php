@extends('clientarea.layouts.master')

@section('leftnav')
    @include('clientarea.layouts.leftnav')
@endsection

@section('content')


<h2>Επεξεργασία Album "{{$album->name}}"</h2>
<!-- Button trigger modal -->
<button type="button" class="text-dark float-right" data-toggle="modal" data-target="#exampleModal">
    <i class="far fa-question-circle"></i>
</button>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Οδηγίες</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Κάθε album μπορεί να έχει μέχρι 24 φωτογραφίες.</p>
          <p>Οι φωτογραφίες πρέπει να είναι αποκλειστικά αρχεία .jpg.</p>
          <p>Το μέγεθος της εικόνας καλό είναι να μην ξεπερνάει τα 1100px. Μεγαλύτερο μέγεθος σημαίνει μεγαλύτερο μέγεθος αρχείου, με μεγαλύτερο χρόνο φόρτωσης και χωρίς κάποιο όφελος.</p>
          <p>Κάθε φωτογραφία μπορεί να έχει μία σύντομη περιγραφή, με σκοπό να βοηθήσει τον επισκέπτη. Αποφύγετε την επανάληψη της ίδιας περιγραφής σε όλες τις εικόνες.</p>
          <p>Για να αλλάξετε τη σειρά των φωτογραφιών μπορείτε να τις "τσιμπήσετε" με το ποντίκι και να τις μετακινήσετε στη θέση που θέλετε.</p>
          <p>Μπορειτε να μετακινήσετε μια φωτογραφία σε άλλο album, επιλέγοντας το εικονίδιο <i class="fa fa-suitcase" aria-hidden="true"></i></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
{{ csrf_field() }} 
<h5 id="albumdesc">Τίτλος Album  <i class="far fa-edit"></i></i></h5>

<div class="col-md-12 album-description">
    {!!Form::open(['action' => ['ClientAreaController@albumsUpdateTitle', $entry->id, $album->id], 'method'=> 'POST'  ])!!}
       {{Form::text('name', $album->name, ['class'=>'form-control'])}}
        {{Form::hidden('_method', 'PUT')}}
        {{Form::button('Αποθήκευση', ['type' => 'submit', 'class' => 'btn btn-primary'])}}
    {!!Form::close()!!}
</div>

<h5 id="albumdesc">Κείμενο Περιγραφής  <i class="far fa-edit"></i></i></h5>

<div class="col-md-12 album-description">
    {!!Form::open(['action' => ['ClientAreaController@albumsUpdateDescription', $entry->id, $album->id], 'method'=> 'POST'  ])!!}
        <textarea class="form-group tinyMCE" name="description"> {{$album->description}} </textarea>
        {{Form::hidden('_method', 'PUT')}}
        {{Form::button('Αποθήκευση', ['type' => 'submit', 'class' => 'btn btn-primary'])}}
    {!!Form::close()!!}
</div>

@if (Storage::exists('uploads/'.$album->id) && count(Storage::files('uploads/'.$album->id)) > 0)
<h5 id="albumdesc">Εξώφυλλο Album  <i class="far fa-edit"></i></i></h5>
<div class="col-md-12 album-description">
<div class="media">
    @if($album->cover)
        <img class="d-flex mr-3" src="/storage/uploads/{{$album->id}}/{{$album->cover}}" style="width: 100px">
    @endif
    
</div>

{!!Form::open(['method' => 'POST', 'action' => ['ClientAreaController@albumsUpdateCover', $entry->id, $album->id] ])!!}
    @foreach($images as $key=>$image)
        {{Form::label('cover', $key+1)}}
        {{Form::radio('cover', $image->image)}}        
    @endforeach
    {{Form::button('Επιλογή', ['class' => 'btn btn-primary', 'type' => 'submit'])}}
{!!Form::close()!!}
</div>
@endif
<div class="col-md-12">
    <hr>
</div>
<?php
    //Έλεγχος ώστε ο αριθμός των αρχείων να μην ξεπερνάει τα 24
    $imageNo = count(Storage::files('uploads/'.$album->id));
    $avail = 24 - $imageNo;
?>
@if($imageNo >= 24 OR $avail <= 0)
    <p>Το album έχει 24 φωτογραφίες. Δεν μπορείτε να προσθέσετε περισσότερες</p>
@else    
    <p>Το album "{{$album->name}}" έχει {{$imageNo}} φωτογραφίες. Μπορείτε να προσθέσετε ακόμη {{$avail}} </p>
    <p>Προτεινόμενες διαστάσεις: max: 1.000px η μεγάλη πλευρά.</p>

<div class="col-md-12">
    <hr>
</div>
    
{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif
@endif

{{--  Λίστα εικόνων  --}}
<div class="col-md-12 d-flex flex-wrap" id="clientsImagesSortable">

@foreach($images as $key=>$image)

<div class="img-thumbnail col-md-3 adminAlbumImage" id="{{$album->id}}-{{$image->id}}">
<?php $number = $key+1; ?>
{{$number}}
    <img src="/storage/uploads/{{$album->id}}/{{$image->image}}" style="width:200px;" class=" mx-auto mb-3 d-block">
    
    {{--  Κουμπί περιγραφής  --}}
    <button class="btn btn-info desc-button"><i class="far fa-comment"></i></i></button>

    {{--  Κουμπί μετακίνησης σε άλλο άλμπουμ  --}}
    <button class="btn btn-info change-album"><i class="fa fa-suitcase" aria-hidden="true"></i></button>

    {{--  Κουμπί Διαγραφής  --}}
    <a class="btn btn-info" href="/clientarea/{{$album->id}}/{{$image->id}}/delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
    
    {{--  Λίστα albums  --}}
    <div class="albumslist">
        <div class="">
            <h5 class="">Μετακίνηση σε άλλο album</h5>            
        </div>
        <div class="">
            {!!Form::open(['action' => ['ClientAreaController@changeAlbum', $entry->id, $album->id, $image->id], 'method' => 'POST'])!!}
                <select class="custom-select mb-3" name="newAlbum">
                    <?php $albums = App\Album::where('entry_id', $entry->id)->get(); ?>

                    @foreach($albums as $albumItem)
                        @if($albumItem->id == $album->id)
                            <option value="{{$albumItem->id}}" selected> {{$albumItem->name}} </option>
                        @else
                            <option value="{{$albumItem->id}}"> {{$albumItem->name}} </option>
                        @endif
                    @endforeach
                </select>        
            </div>
            <div class="">
                <button type="submit" class="btn btn-primary">Αποθήκευση</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Κλείσιμο</button>
            </div>
        
        {!!Form::close()!!}
    </div>
    
    {{--  Πλαίσιο περιγραφής  --}}
    <div class="image-description">
        <div class="">
            <h5 class="modal-title">Περιγραφή εικόνας</h5>
        </div>
        <div class="">
            {!!Form::open(['action' => ['ClientAreaController@imageStoreDescription', $entry->id, $album->id, $image->id], 'method' => 'POST'])!!}
            <textarea name="description" class="form-control mb-3"> {{$image->description}} </textarea>
            
            </div>
            <div class="">
                <button type="submit" class="btn btn-primary">Αποθήκευση</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Κλείσιμο</button>
            </div>
        {!!Form::close()!!}
    </div>
</div>
    
@endforeach
</div>

<div class="col-md-12">
    <hr>
</div>

{{--  Φόρμα ανεβάσματος εικόνων  --}}
@if($avail > 0)
    {!!Form::open(['method'=>'POST', 'action'=>['ClientAreaController@imagesStore', $entry->id, $album->id], 'class'=>'form', 'enctype'=>'multipart/form-data'])!!}
    <div class="form-controller">
        {{Form::file('image[]', ['multiple', 'class' => 'form-control-file'])}}
    </div>
    <div class="col-md-12">
    <hr>
    </div>
    <div class="form-controller">
        {{Form::hidden('available', $avail)}}
        {{Form::button('Προσθήκη', ['type'=>'submit', 'class' => 'btn btn-primary'])}}
        <a href="/clientarea/{{$entry->id}}/albums" class="btn btn-secondary">Επιστροφή στη λίστα των Album</a>
    </div>
{!!Form::close()!!}
@endif
<div id="uploading" style="display:none;" class="alert alert-info">Παρακαλούμε περιμένετε. Γίνεται βελτιστοποίηση των εικόνων.</div>

    
@endsection