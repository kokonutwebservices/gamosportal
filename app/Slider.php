<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Slider extends Model
{
    use Sortable;
    protected $guarded = ['_token'];

    public $sortable = ['id', 'entry', 'url', 'expires'];

    public function pages(){
        return $this->belongsToMany(Page::class);
    }

    public function entry(){
        return $this->belongsTo(Entry::class);
    }
}
