<nav class="navbar navbar-expand-md topnav d-none d-md-block">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse px-0" id="navbarNavAltMarkup">
    
    <ul class="nav navbar-nav float-none justify-content-between">
      <li><a href="/"><i class="fa fa-home" aria-hidden="true"></i> Αρχική</a></li>
      <li class="dropdown">
        <a href="#" class="px-md-1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-map-marker-alt" aria-hidden="true"></i> Περιοχές <span class="caret"></span></a>
        <div class="dropdown-menu">
          @include('site.nav.districts')
        </div>
      </li>
      <li class="dropdown">
        <a href="#" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="far fa-user-circle" aria-hidden="true"></i> Επαγγελματίες <span class="caret"></span></a>
        <div class="dropdown-menu">
          @include('site.nav.pages')
        </div>
      </li>
      <li><a href="/blog" target="_blank" title="Ιδέες, Συμβουλές, Νέα, Πραγματικοί Γάμοι"><i class="fas fa-rss-square" aria-hidden="true"></i> Gamos Portal Blog</a></li>
			<li><a href="https://www.gamos-deals.gr"  target="_blank" title="Προσφορές και ευκαιρίες" rel="nofollow"><i class="fas fa-euro-sign" aria-hidden="true"></i> Προσφορές Γάμου</a></li>
			<li><a href="https://www.vaptisiportal.gr"  target="_blank" title="Οδηγός Αγοράς Βάπτισης" rel="nofollow"><i class="far fa-hand-point-right"></i> Vaptisi Portal</a></li>
      
    </ul>
  </div>
</nav>


<nav class="navbar d-md-none" >
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle offcanvas-toggle" data-toggle="offcanvas" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <div class="navbar-offcanvas navbar-offcanvas-touch" id="myNavbar">
        <h4 class="mt-3 py-3 text-light">Gamos Portal Main Menu</h4>
        <ul class="nav navbar-nav mx-0">
          <li class="nav-item"><a href="/"><i class="fa fa-home" aria-hidden="true"></i>Αρχική</a></li>
          <li class="nav-item dropdown">
          
            <a href="#" class="px-md-1 nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-map-marker-alt" aria-hidden="true"></i>Περιοχές <span class="caret"></span></a>
            <div class="dropdown-menu">
              @include('site.nav.districts')
            </div>
         
          </li>
          <li class="dropdown">
            <a href="#" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="far fa-user-circle" aria-hidden="true"></i>Επαγγελματίες <span class="caret"></span></a>
            <div class="dropdown-menu">
              @include('site.nav.pages')
            </div>
          </li>
          <li><a href="https://www.gamos-portal.gr" target="_blank" title="Ιδέες, Συμβουλές, Νέα, Πραγματικοί Γάμοι" rel="nofollow"><i class="fas fa-rss-square" aria-hidden="true"></i>Gamos Portal Blog</a></li>
			    <li><a href="https://www.gamos-deals.gr"  target="_blank" title="Προσφορές και ευκαιρίες" rel="nofollow"><i class="fas fa-euro-sign" aria-hidden="true"></i>Προσφορές Γάμου</a></li>
          <li><a href="https://www.vaptisiportal.gr"  target="_blank" title="Οδηγός Αγοράς Βάπτισης" rel="nofollow"><i class="far fa-hand-point-right"></i> Vaptisi Portal</a></li>
      
        </ul>
      </div>
    </div>
  </div>
</nav>