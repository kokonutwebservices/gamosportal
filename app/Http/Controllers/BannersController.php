<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Client;
use App\Page;
use App\InfoPage;
use App\District;
use App\Category;
use App\Home;
use App\Entry;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;
use Storage;

class BannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::all();
        return view('admin.banners.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages = Page::all();
        $infoPages = InfoPage::all();
        $categories = Category::where('cat_parent_id', '!=', '0')->get();
        $districts = District::all();
        $entries = Entry::all();
        return view('admin.banners.create', compact('pages', 'infoPages', 'categories', 'districts', 'entries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate(request(),[
            'entry_id' => 'required',
            'type' => 'required',
            'banner' => 'required|image|mimes:jpeg,jpg,png,gif|max:1000',
            'url' => 'required',
            'expires' => 'required'
        ]);
        
        //Banner Image
        $imageFileName = request('banner')->getClientOriginalName();
        request()->file('banner')->storeAs('banners', $imageFileName); //Store file
        
        $banner = new Banner;
        $banner->entry_id = request('entry_id');
        $banner->type = request('type');
        $banner->banner = $imageFileName;
        $banner->url = request('url');
        $banner->expires = request('expires');
        $banner->save();
        $banner->pages()->sync($request->page);
        $banner->infoPages()->sync($request->infoPage);
        $banner->categories()->sync($request->category);
        $banner->districts()->sync($request->district);
        $banner->home()->sync($request->homepage);

        $id = Banner::all()->last()->id;

        //Redirect
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.banners.edit', $id)->with('message', 'Το banner αποθηκεύτηκε'); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.banners.index')->with('message', 'Το banner αποθηκεύτηκε');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.banners.create')->with('message', 'Το banner αποθηκεύτηκε');
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        $pages = Page::all();
        $infoPages = InfoPage::all();
        $homebanners = Home::all();
        $categories = Category::where('cat_parent_id', '!=', '0')->get();
        $districts = District::all();
        $entries = Entry::all();
        return view('admin.banners.edit', compact('banner', 'pages', 'infoPages', 'categories', 'districts', 'homebanners', 'entries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        $this->validate(request(),[
            'entry_id' => 'required',
            'type' => 'required',
            'banner' => 'image|mimes:jpeg,jpg,png,gif|max:1000',
            'url' => 'required',
            'expires' => 'required'
        ]);
        
        $banner->pages()->sync($request->page);
        $banner->infoPages()->sync($request->infoPage);
        $banner->categories()->sync($request->category);
        $banner->districts()->sync($request->district);
        $banner->home()->sync($request->homepage);
        
       
        //Update
        //Banner Image
        if($request->hasFile('banner')){
            //Αν υπάρχει εικόνα...
            Storage::delete("banners/$banner->banner");
            $imageFileName = request('banner')->getClientOriginalName(); 
            request()->file('banner')->storeAs('banners', $imageFileName); 
           
        } else {
            $imageFileName = $banner->banner;
        }

        $banner->entry_id = request('entry_id');
        $banner->type = request('type');
        $banner->banner = $imageFileName;
        $banner->url = request('url');
        $banner->expires = request('expires');
        $banner->save();

        //Redirect
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.banners.edit', $banner)->with('message', 'Το banner αποθηκεύτηκε'); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.banners.index')->with('message', 'Το banner αποθηκεύτηκε');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.banners.create')->with('message', 'Το banner αποθηκεύτηκε');
        }
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
               
        $image = $banner->banner;
        Storage::delete("banners/$image");

        $banner->pages()->detach();
        $banner->infoPages()->detach();
        $banner->categories()->detach();
        $banner->districts()->detach();
        $banner->home()->detach();

        $banner->delete();

        return redirect()->route('admin.banners.index')->with('message', 'Το banner διαγράφηκε');
    }

    public function cliententry(){
        //function που δέχεται το ajax call για τις καταχωρήσεις που αντιστοιχούν σε έναν πελάτη

        if($_POST['entry'] != 0){
            $id = $_POST['entry'];
            $entry = Entry::find($id);
            $district = $entry->district->slug;
            $category = $entry->category->cat_slug;
            echo "/$district/$category/$entry->slug";
        }    
    }
}
