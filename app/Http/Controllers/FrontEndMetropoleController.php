<?php

namespace App\Http\Controllers;

use App\District;
use App\Metropole;
use App\Church;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;


class FrontEndMetropoleController extends Controller
{
    public function index($districtSlug) {

        $district = District::where('slug', $districtSlug)->first();

        $metropoles = Metropole::where('district_id', $district->id)->get();

        // μεταβλητή που χρησιμοποιείται στο show(). Εδώ μπαίνει false για να μη χτυπάει σφάλμα.
        $lastSegment = FALSE;

        return view ('site.churches.metropoles', compact('district', 'metropoles', 'lastSegment'));

    }

    public function select (Request $request, $districtSlug) {

        $district = District::where('slug', $districtSlug)->first();

        $metropole = Metropole::where('id', $request->metropole)->first();

        $churches = Church::where('metropole_id', $metropole->id)->get();

        $metropoles = Metropole::where('district_id', $district->id)->get();


        // return view ('site.churches.metropole', compact('district', 'metropole', 'churches', 'metropoles'));

        return redirect ("$district->slug/ekklisies/$metropole->id")->with(['district' => $district, 'churches' => $churches, 'metropole' => $metropole, 'metropoles' => $metropoles]);

    }

    public function show ($districtSlug, Metropole $metropole) {

        $district = District::where('slug', $districtSlug)->first();
        $metropoles = Metropole::where('district_id', $district->id)->get();
        
        // Το τελευταίο κομμάτι του url, για να βγει η επιλεγμένη Μητρόπολη selected
        $lastSegment = last(request()->segments(3));
        

        return view ('site.churches.metropole', compact('district', 'metropole', 'metropoles', 'lastSegment'));
    }

   
}
