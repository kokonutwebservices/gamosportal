@extends('admin.layouts.master')

@section('content')

<h1>Δημιουργία Δημοσίευσης</h1>
@include('admin.layouts.errors')
<hr>
{!!Form::open(['method' => 'post', 'action' => 'NewsController@store', 'enctype' => 'multipart/form-data'])!!}

<div class="form-group">
{{Form::label('title', 'Τίτλος')}}
{{Form::text('title', '', ['class' => 'form-control', 'required'])}}
</div>

<div class="form-group">
{{Form::label('image', 'Εικόνα')}}
{{Form::file('image', ['id'=>'image', 'required'])}}
</div>

<div class="form-group">
{{Form::label('description', 'Κείμενο')}}
<textarea class="tinyMCE" name="description"></textarea>
</div>

<div class="form-group">
{{Form::label('url', 'Link')}}
{{Form::text('url', '', ['class' => 'form-control', 'required'])}}
</div>

<div class="form-group">
    <div class="col-md-2">                
        {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
    </div>  

    <div class="col-md-2">
        {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
    </div>

    <div class="col-md-2">
        {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
    </div>

    <div class="col-md-2">  
        <a href="/admin/news" class="btn btn-warning pull-right">Cancel</a>
    </div>    
</div>


{!!Form::close()!!}
    
@endsection