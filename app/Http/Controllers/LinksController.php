<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entry;
use App\Category;
use App\District;
use App\Link;

class LinksController extends Controller
{

    public function index($id){
        $entry = Entry::find($id);
        $categories = Category::where('cat_parent_id', '<>', '0')->get();
        $districts = District::all();
        $link = Link::where('entry_id', $entry->id)->first();

        return view('admin.links.index', compact('entry', 'categories', 'districts', 'link'));
    }

    public function update(Request $request, $id){
    
        $link = Link::where('entry_id', $id)->first();

        if($request->category OR $request->district){
            if(!$link){
                $link = new Link;
                $link->entry_id = $id;
                $link->save();
                $link->categories()->sync($request->category);
                $link->districts()->sync($request->district);

                return back()->with('message', 'Όλα καλά');
            } else {
                $link->categories()->sync($request->category);
                $link->districts()->sync($request->district);
                return back()->with('message', 'Όλα καλά');
            }
        } else {
            return back()->with('message', 'Θα πρέπει να επιλέξετε κάτι');
        }
       
    
    }
}
