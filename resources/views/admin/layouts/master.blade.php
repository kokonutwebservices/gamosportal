<!DOCTYPE html>
<html>
    <head>
        <title> Gamos Portal Admin </title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
		{{--  <meta name="description" content="{{$page->metadescription}}" >
		<meta name="keywords" content="{{$page->metakeywords}}" >  --}}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=cisfuks53sirpyxf6z5uhhy0vk3y58eiruscfv3jdi8mji7x"></script>
        <script>
            tinymce.init({
            selector: '.tinyMCE',
            height: 300,
            theme: 'modern',		  
            entity_encoding:'raw',            
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]		  
            });
		</script>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body class="admin">
    <header>
        <div class="container">
            @include('admin.layouts.nav')
        </div>        
    </header>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                @if(! Request::is('*/login'))
                    @include('admin.layouts.leftnav')
                @endif
                </div>
                <div class="col-md-9">
                @yield('content')
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            @include('admin.layouts.footer')
        </div>
    </footer>
    
    <script src="{{asset('/js/app.js')}}" ></script>
    </body>
</html>