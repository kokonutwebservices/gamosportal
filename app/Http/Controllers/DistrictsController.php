<?php

namespace App\Http\Controllers;

use App\District;
use Illuminate\Http\Request;

class DistrictsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $districts = District::all();
        return view('admin.districts.index', compact('districts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.districts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $district = new District();

        $attributes = $district->validation($request);

        $district->create($attributes);

        $district = District::all()->last();

        
        //Redirect
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.districts.edit', $district); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.districts.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect(route('admin.districts.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function show(District $district)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function edit(District $district)
    {
    
        return view('admin.districts.edit', compact('district'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, District $district)
    {
        $attributes = $district->validation($request);

        $district->update($attributes);

        // $districts->name = $request->input('name');
        // $districts->slug = $request->input('slug');
        // $districts->metatitle = $request->input('metatitle');
        // $districts->metadescription = $request->input('metadescription');
        // $districts->metakeywords = $request->input('metakeywords');
        // $districts->save();

        //Redirect
        if($request->submitbutton == 'save'){
            return redirect()->route('admin.districts.edit', $district); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect()->route('admin.districts.index');
        } elseif ($request->submitbutton == 'save-new'){
            return redirect()->route('admin.districts.create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function destroy(District $district)
    {
        $district->delete();

        return redirect()->route('admin.districts.index');
    }
}
