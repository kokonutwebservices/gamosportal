<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $guarded = [];

    private $rules = [
            'name' => 'required|min:5',
            'slug' => 'required|min:5',
            'metatitle' => 'required|min:5',
            'metadescription' => 'required|min:5',
            'metakeywords' => 'required|min:5',
        ];

    public function validation($request)
    {
        return $request->validate($this->rules);
    }

    public function banners(){
        return $this->belongsToMany(Banner::class);
    }

    public function entries(){
        return $this->hasMany(Entry::class, 'dist_id');
    }

    public function churches(){
        return $this->hasMany(Church::class);
    }

    public function links(){
        return $this->belongsToMany(Link::class);
    }

    public function municipalities(){
        return $this->hasMany(Municipality::class);
    }

    public function metropoles() {
        return $this->hasMany(Metropole::class);
    }
    
}
