@extends('clientarea.layouts.master')

@section('leftnav')
    @include('clientarea.layouts.leftnav')
@endsection

@section('content')
@if(Auth::user()->id == $entry->client_id)

    <h1>Video Καταχώρησης {{$entry->name}}</h1>
    <div class="col-md-12"><hr></div>
    {{--  Πλαίσιο σφαλμάτων  --}}
    @include('admin.layouts.errors') 
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    @if (session('errorMessage'))
        <div class="alert alert-danger">
            {{ session('errorMessage') }}
        </div>
    @endif

    {{--  Εμφάνιση υπαρχόντων video  --}}
    <div id="clientsVideoList">
    @foreach($videos as $video)
        @if($video->YouTubeId)
        <div class="col-md-3 video-item" id="{{$entry->id}}-{{$video->id}}">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{$video->YouTubeId}}" allowfullscreen></iframe>
            </div>
            <i class="fas fa-arrows-alt text-primary" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Μετακίνηση"></i> 
            <a href="/clientarea/{{$entry->id}}/video/{{$video->id}}/delete" class="text-danger" data-toggle="tooltip" data-placement="top" title="Διαγραφή"><i class="fa fa-trash" aria-hidden="true"></i></a>
        </div>
        @elseif($video->VimeoId)
        <div class="col-md-3 video-item" id="{{$entry->id}}-{{$video->id}}">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://player.vimeo.com/video/{{$video->VimeoId}}?byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
            <i class="fas fa-arrows-alt text-primary" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Μετακίνηση"></i> 
            <a href="/clientarea/{{$entry->id}}/video/{{$video->id}}/delete" class="text-danger" data-toggle="tooltip" data-placement="top" title="Διαγραφή"><i class="fa fa-trash" aria-hidden="true"></i></a>
        </div>
        @elseif($video->FacebookId)
        <div class="col-md-3 video-item" id="{{$entry->id}}-{{$video->id}}">
                <!-- Load Facebook SDK for JavaScript -->
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>

                <!-- Your embedded video player code -->
                <div class="fb-video" data-href="https://www.facebook.com/facebook/videos/{{$video->FacebookId}}/" data-width="500" data-show-text="false">
                    <div class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/facebook/videos/{{$video->FacebookId}}/"></a>
                    </div>
                </div>
             <i class="fas fa-arrows-alt text-primary" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Μετακίνηση"></i> 
            <a href="/clientarea/{{$entry->id}}/video/{{$video->id}}/delete" class="text-danger" data-toggle="tooltip" data-placement="top" title="Διαγραφή"><i class="fa fa-trash" aria-hidden="true"></i></a>
        </div>
        @endif
    @endforeach
    </div>
    <div class="col-md-12"><hr></div>
 
    <div class="col-md-12">
    {!!Form::open(['method'=>'POST', 'action'=>['ClientAreaController@videoStore', $entry->id], 'class' => 'form-inline'])!!}

    <div class="col-md-12">
        <div class="form-group col-md-3">
            {{Form::radio('vservice', 'YouTube')}} <span>YouTube</span>
        </div>
        <div class="form-group col-md-3">
            {{Form::radio('vservice', 'Vimeo')}} <span>Vimeo</span>
        </div>
        <div class="form-group col-md-3">
            {{Form::radio('vservice', 'Facebook')}} <span>Facebook</span>
        </div>
    </div>
   
    <div class="form-group">      
      {{Form::text('video', '', ['class' => 'form-control', 'placeholder' => 'Προσθέστε ολόκληρο το link του video', 'size' => '100'])}}
    </div>

    <div class="form-group mx-sm-3">
      {{Form::button('Προσθήκη', ['type' => 'submit', 'class' => 'btn btn-primary'])}}
    </div> 
      
    {!!Form::close()!!}
    </div>
    <div class="col-md-12"><hr>
    
    </div>
@else
<span class="text text-danger">Δεν μπορείτε να δείτε αυτό το περιεχόμενο</span>
@endif
@endsection