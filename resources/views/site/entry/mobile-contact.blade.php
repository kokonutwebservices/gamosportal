<div class="d-lg-none" itemscope itemtype="http://schema.org/Person">
    
    <a id="" class="btn btn-primary float-right my-3" href="#contact-form" role="button">Ζητήστε προσφορά</a>
    <div class="clearfix"></div>
    <ul class="list-group list-group-flush">
        @if ($entry->address)
        <li class="list-group-item">
            <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span><i class="fas fa-map-marker-alt"></i></span> <span itemprop="streetAddress">{{$entry->address}} </span>
            </div>
        </li>            
        @endif
        @if ($entry->phone)
            <li class="list-group-item">
                <span><i class="fas fa-phone"></i></span> <a class="call" href="tel:+30{{$entry->phone}}"><span itemprop="telephone">{{$entry->phone}}</span></a>
            </li> 
        @endif
        @if($entry->mobile)
        <li class="list-group-item">
            <span><i class="fas fa-mobile-alt"></i></span> <a class="call" href="tel:+30{{$entry->mobile}}"><span itemprop="telephone">{{$entry->mobile}}</span></a>
        </li>       
        @endif
        @if ($entry->person)
        <li class="list-group-item">
            <span><i class="fas fa-user"></i></span> {{$entry->person}}
        </li>
        @endif
        {{-- @if($entry->email)
        <li class="list-group-item">
            <span><i class="fas fa-envelope"></i></span> 
            <a class="email" href="mailto:{{$entry->email}}?subject=Είδα την καταχώρησή σας στο Gamos Portal" itemprop="email">             
                {{$entry->email}}
            </a>
        </li>       
        @endif --}}
        @if($entry->website)
        <li class="list-group-item">
            <span><i class="fas fa-globe"></i></span>
            <a href="http://{{$entry->website}}?utm_source=gamosportal&utm_medium=referral" target="_blank" rel="nofollow" itemprop="url">
                {{$entry->website}}
            </a>
        </li>       
        @endif
        <li class="list-group-item">
            @if($entry->facebook)
                <a href="http://{{$entry->facebook}}" rel="nofollow" target="_blank"><i class="fab fa-facebook mr-3" aria-hidden="true"></i></a>    
            @endif
            @if($entry->twitter)
                <a href="http://{{$entry->twitter}}" rel="nofollow" target="_blank"><i class="fab fa-twitter mr-3" aria-hidden="true"></i></a>    
            @endif
            @if($entry->instagram)
                <a href="http://{{$entry->instagram}}" rel="nofollow" target="_blank"><i class="fab fa-instagram mr-3" aria-hidden="true"></i></a>    
            @endif
            @if($entry->pinterest)
                <a href="http://{{$entry->pinterest}}" rel="nofollow" target="_blank"><i class="fab fa-pinterest-p mr-3" aria-hidden="true"></i></a>    
            @endif
            @if($entry->youtube)
                <a href="http://{{$entry->youtube}}" rel="nofollow" target="_blank"><i class="fab fa-youtube mr-3" aria-hidden="true"></i></a>    
            @endif 
            @if($entry->vimeo)
            <a href="http://{{$entry->vimeo}}" rel="nofollow" target="_blank"><i class="fab fa-vimeo-v mr-3" aria-hidden="true"></i></a>    
            @endif            
        </li>
    </ul>
</div>
