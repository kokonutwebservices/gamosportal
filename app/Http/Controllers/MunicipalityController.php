<?php

namespace App\Http\Controllers;

use App\Municipality;
use App\District;
use Illuminate\Http\Request;
use Storage;
use Kyslik\ColumnSortable\Sortable;

class MunicipalityController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $municipalities = Municipality::sortable()->paginate(50);

        return view('admin.municipalities.index', compact('municipalities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = District::all();
        return view('admin.municipalities.create', compact('districts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'district_id' => 'required',
            'name' => 'required',
            'slug' => 'required',
        ]);
        
        //Images check                      
        //Profile Image
        if($request->hasFile('image')){
        $image = $request->file('image'); //Get the image file
        $ext = $image->getClientOriginalExtension(); //Get the extension
        $imageFileNameBase = $request->input('slug'); //Get the slug to use as filename base
        $imageFileName = $imageFileNameBase.time().'.'.$ext; //create a new filename
        request()->file('image')->storeAs('municipalities', $imageFileName); //Store file

        $filepath = public_path('storage/municipalities/'.$imageFileName);        
            
            try {
                \Tinify\setKey("xVBaCSpeVqNE0VFe5_KNBpXweIB-YwZL");
                $source = \Tinify\fromFile($filepath);
                $source->toFile($filepath);
            } catch(\Tinify\AccountException $e) {
                // Verify your API key and account limit.
                return redirect('municipalities/create')->with('error', $e->getMessage());
            } catch(\Tinify\ClientException $e) {
                // Check your source image and request options.
                return redirect('municipalities/create')->with('error', $e->getMessage());
            } catch(\Tinify\ServerException $e) {
                // Temporary issue with the Tinify API.
                return redirect('municipalities/create')->with('error', $e->getMessage());
            } catch(\Tinify\ConnectionException $e) {
                // A network connection error occurred.
                return redirect('municipalities/create')->with('error', $e->getMessage());
            } catch(Exception $e) {
                // Something else went wrong, unrelated to the Tinify API.
                return redirect('municipalities/create')->with('error', $e->getMessage());
            }
        } else {
            $imageFileName = "";
        }
        //Creation
        Municipality::create([
            'district_id' => $request->input('district_id'),
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'person' => $request->input('person'),
            'address' => $request->input('address'),
            'area' => $request->input('area'),
            'phone' => $request->input('phone'),
            'mobile' => $request->input('mobile'),
            'website' => $request->input('website'),
            'email' => $request->input('email'),
            'map' => $request->input('map'),
            'image' => $imageFileName,
            'profile' => $request->input('profile'),
            'metatitle' => $request->input('metatitle'),
            'metadescription' => $request->input('metadescription'),
            'metakeywords' => $request->input('metakeywords'),
        ]);

        $id = Municipality::all()->last()->id;
        
        //Redirect
        if($request->submitbutton == 'save'){
            return redirect("/admin/municipalities/$id/edit"); 
        } elseif ($request->submitbutton == 'save-close') {
            return redirect("/admin/municipalities");
        } elseif ($request->submitbutton == 'save-new'){
            return redirect("/admin/municipalities/create");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Municipality  $municipality
     * @return \Illuminate\Http\Response
     */
    public function show(Municipality $municipality)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Municipality  $municipality
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $municipality = Municipality::find($id);
        $districts = District::all();
        return view('admin.municipalities.edit',compact('municipality', 'districts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Municipality  $municipality
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Municipality $municipality)
    {
        $districts = District::all();

        $this->validate(request(), [
            'district_id' => 'required',
            'name' => 'required',
            'slug' => 'required',
        ]);

        //Update
        //Church Image
        if($request->hasFile('image')){
            //Αν υπάρχει εικόνα, το όνομα αρχείου παραμένει ίδιο
            $filename = $municipality->image;
            request()->file('image')->storeAs('municipalities', $filename); 
        } else {
            $filename = $municipality->image;
        }
        
        $municipality->district_id = $request->input('district_id');
        $municipality->name = $request->input('name');
        $municipality->slug = $request->input('slug');
        $municipality->person = $request->input('person');
        $municipality->address = $request->input('address');
        $municipality->area = $request->input('area');
        $municipality->phone = $request->input('phone');
        $municipality->mobile = $request->input('mobile');
        $municipality->website = $request->input('website');
        $municipality->email = $request->input('email');
        $municipality->map = $request->input('map');
        $municipality->image = $filename;
        $municipality->profile = $request->input('profile');
        $municipality->metatitle = $request->input('metatitle');
        $municipality->metadescription = $request->input('metadescription');
        $municipality->metakeywords = $request->input('metakeywords');
        $municipality->save();


        //Redirect
        if($request->submitbutton == 'save'){
            return redirect("/admin/municipalities/$municipality->id/edit");
        } elseif ($request->submitbutton == 'save-close') {
            return redirect("/admin/municipalities");
        } elseif ($request->submitbutton == 'save-new'){
            return redirect("/admin/municipalities/create");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Municipality  $municipality
     * @return \Illuminate\Http\Response
     */
    public function destroy(Municipality $municipality)
    {
        
        
        $image = $municipality->image;
        Storage::delete("municipalities/$image");

        $municipality->delete();

        return redirect('admin/municipalities')->with('message', 'Το Δημαρχείο διαγράφηκε');
    }

    public function search(Request $request, Municipality $municipality){
        
                $keyword = request('query');
                $results = Municipality::where('name', 'LIKE', '%'.$keyword.'%')->get();
        
                return view('admin.municipalities.results', compact('results'));
            }
}
