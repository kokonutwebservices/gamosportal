@component('mail::message')
# Έχετε μήνυμα!

Το παρακάτω μήνυμα στάλθηκε σε εσάς από τη φόρμα επικοινωνίας που υπάρχει στην καταχώρησή σας **{{$entry->name}}**, στην κατηγορία **{{$category->cat_name}}**, στο [GamosPortal.gr](https://gamosportal.gr). 

## Στοιχεία μηνύματος

* Όνομα: {{$request->name}}
* Τηλέφωνο: {{$request->phone}}
* email: {{$request->email}}
* Πιθανή ημερομηνία: {{$request->date}}
* Μήνυμα: {{$request->message}}

    
</div>

@component('mail::button', ['url' => "mailto:$request->email"])
Στείλτε απάντηση
@endcomponent

Σας ευχαριστούμε που επιλέγετε το [GamosPortal.gr](https://gamosportal.gr) για την προβολή σας.

Με εκτίμηση,<br>
Η ομάδα του [Gamos Portal](https://gamosportal.gr) 
@endcomponent
