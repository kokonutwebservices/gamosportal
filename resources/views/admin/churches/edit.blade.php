@extends('admin.layouts.master')

@section('content')

<h1>Επεξεργασία Εκκλησίας: {{$church->name}}</h1>

<form action="/admin/churches/{{$church->id}}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group col-md-6 pl-0">
        <label for="metropole_id">Μητρόπολη</label>
        <select class="form-control" name="metropole_id" id="metropole_id">
          <option disabled {{$church->metropole_id == NULL ? "selected" : ''}}>Επιλέξτε Μητρόπολη</option>
          @foreach ($metropoles as $metropole)
              <option {{$metropole->id === $church->metropole_id ? 'selected' : ''}} value="{{$metropole->id}}" >{{$metropole->name}}</option>            
          @endforeach
        </select>
  
        @error('metropole_id')
          <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>

      <div class="form-group col-md-6 pr-0">
        <label for="district_id">Περιφέρεια</label>
        <select class="form-control" name="district_id" id="district_id">
          <option disabled>Επιλέξτε Περιφέρεια</option>
          @foreach ($districts as $district)
              <option {{$church->district_id == $district->id ? 'selected' : ''}} value="{{$district->id}}">{{$district->name}}</option>
          @endforeach
        </select>
        @error('district_id')
          <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>
  
      <div class="form-group">
        <label for="name">Όνομα Εκκλησίας</label>
        <input type="text" class="form-control" name="name" id="name" value="{{$church->name}}" placeholder="Το όνομα όπως θα φαίνεται στην καταχώρηση">
        @error('name')
          <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>
  
      <div class="form-group">
        <label for="slug">Slug</label>
        <input type="text" class="form-control" name="slug" id="slug" value="{{$church->slug}}" placeholder="friendly url">
          @error('slug')
              <small class="form-text text-danger">{{$message}}</small>
          @enderror
      </div>
  
      <div class="form-group">
        <label for="person">Υπεύθυνος</label>
        <input type="text" class="form-control" name="person" id="person" value="{{$church->person}}" placeholder="Όνομα Υπευθύνου">
      </div>
  
      <div class="form-group col-md-9 pl-0">
        <label for="address">Διεύθυνση</label>
        <input type="text"
          class="form-control" name="address" id="address" value="{{$church->address}}" placeholder="Διεύθυνση">
      </div>
  
      <div class="form-group col-md-3 pr-0">
        <label for="area">Περιοχή</label>
        <input type="text"
          class="form-control" name="area" id="area" value="{{$church->area}}" placeholder="Περιοχή">
      </div>
  
      <div class="form-group col-md-6 pl-0">
        <label for="phone">Τηλέφωνο</label>
        <input type="text"
          class="form-control" name="phone" id="phone" value="{{$church->phone}}" placeholder="Τηλέφωνο">
      </div>
  
      <div class="form-group col-md-6 pr-0">
        <label for="mobile">Κινητό</label>
        <input type="text"
          class="form-control" name="mobile" id="mobile" value="{{$church->mobile}}" placeholder="Κινητό">
      </div>
  
      <div class="form-group">
        <label for="website">Website</label>
        <input type="text" class="form-control" name="website" id="website" value="{{$church->website}}" placeholder="website">
      </div>
  
      <div class="form-group col-md-6 pl-0">
        <label for="email">Email</label>
        <input type="text" class="form-control" name="email" id="email" value="{{$church->email}}" placeholder="Email">
      </div>
  
      <div class="form-group col-md-6 pr-0">
        <label for="map">Google Maps</label>
        <input type="text" class="form-control" name="map" id="map" value="{{$church->map}}" placeholder="Google Maps">
      </div>
  
      <div class="form-group">
        <label for="metatitle">Meta Title</label>
        <input type="text" class="form-control" name="metatitle" id="metatitle" value="{{$church->metatitle}}" placeholder="Meta Title">
      </div>
  
      <div class="form-group">
        <label for="metadescription">Meta Description</label>
        <input type="text" class="form-control" name="metadescription" id="metadescription" value="{{$church->metadescription}}" placeholder="Meta Description">
      </div>
  
  
      <div class="d-flex justify-content-between my-5">
  
          @include('admin.layouts.savebuttons')
          
          <a href="/admin/churches" class="btn btn-warning">Cancel</a>
  
</form>

        <form action="/admin/churches/{{$church->id}}" method="POST">
            @csrf
            @method('DELETE')

            <button type="submit" id="delete" class="btn btn-danger">Delete</button>

        </form>
        </div>
    
@endsection