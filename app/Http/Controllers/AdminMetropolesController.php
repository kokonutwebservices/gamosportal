<?php

namespace App\Http\Controllers;

use App\District;
use App\Metropole;
use Illuminate\Http\Request;

class AdminMetropolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metropoles = Metropole::all();

        return view ('admin.metropoles.index', compact('metropoles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = District::all();

        return view ('admin.metropoles.create', compact('districts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Metropole::validateMetropole($request);

        $metropole = New Metropole();
        $metropole->district_id = $request->district_id;
        $metropole->name = $request->name;
        $metropole->areas = $request->areas;
        $metropole->save();

        $metropole = Metropole::all()->last();

        return $metropole->redirectAfterSave($request);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Metropole  $metropole
     * @return \Illuminate\Http\Response
     */
    public function edit(Metropole $metropole)
    {
        $districts = District::all();
        return view('admin.metropoles.edit', compact('metropole', 'districts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Metropole  $metropole
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Metropole $metropole)
    {
        Metropole::validateMetropole($request);

        $metropole->name = $request->name;
        $metropole->areas = $request->areas;
        $metropole->district_id = $request->district_id;
        $metropole->save();

        return $metropole->redirectAfterSave($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Metropole  $metropole
     * @return \Illuminate\Http\Response
     */
    public function destroy(Metropole $metropole)
    {
        $metropole->delete();

        return redirect()->route('admin.metropoles.index');
    }
}
