@extends('site.entry.show')

@section('entrycontent')
<div id="albums" class="d-flex flex-wrap">
    @foreach($albums as $album)
        <div class="album col-md-4">
            <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}/photos/{{$album->id}}#album">
                @if ($album->cover)
                <img class="thumbnail" src="/storage/uploads/{{$album->id}}/{{$album->cover}}" alt="{{ $album->name }}">                
                @else
                <img class="thumbnail" src="/storage/amaksi.jpg" alt="{{ $entry->name }}">
                @endif
                <p class="text-center"> {{$album->name}} <small>({{count($album->images)}})</small> </p>              
            </a>
        </div>
    @endforeach
</div>
<div class="col-lg-12 clearfix">
    <div class="float-left">
        <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}" class="btn btn-default float-right my-3" ><i class="fas fa-arrow-alt-circle-left"></i> Προφίλ </a>
    </div>
    <div class="float-right">
        @if (count($entry->videos) > 0)
            <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}/video#videos" class="btn btn-default float-right my-3" >Video <i class="fas fa-arrow-alt-circle-right"></i></a>
        @elseif($entry->facebook)
            <a href="/{{$entry->district->slug}}/{{$entry->category->cat_slug}}/{{$entry->slug}}/facebook" class="btn btn-default float-right my-3" >Timeline <i class="fas fa-arrow-alt-circle-right"></i></a>
        @endif
    </div>
</div>
@endsection

