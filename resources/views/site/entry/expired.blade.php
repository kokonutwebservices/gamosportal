@extends('site.layouts.master')
@section('metatags')
    <title>{{ Meta::entryTitle($entry) }}</title>
    <meta name="description" content="{{ $entry->metadescription }}">
    <meta name="keywords" content="{{ $entry->metakeywords }}">
    <meta name="geo.region" content="GR">
    <meta name="geo.position" content="{{ $entry->map }}">
    <meta name="ICBM" content="{{ $entry->map }}">
    <meta property="og:title" content="{{ $entry->metatitle }}">
    <meta property="og:type" content="website">
    <meta property="og:url"
        content="https://gamosportal.gr/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}/{{ $entry->slug }}">
    <meta property="og:description" content="{{ $entry->metadescription }}">
    <meta property="og:image"
        content="https://gamosportal.gr/storage/entries/{{ $category->id }}/{{ $entry->image }}">

    <script src='https://www.google.com/recaptcha/api.js'></script>
    {{ Meta::canonical($entry) }}
@endsection



@section('content')
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.11&appId=164926320922283';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    {{--  Breadcrubs  --}}
    <div class="container entry">
        <div class="row">
            <ol class="breadcrumb mb-3" itemscope itemtype="https://schema.org/BreadcrumbList">
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="/">
                        <span itemprop="name">Αρχική</span>
                    </a>
                    <meta itemprop="position" content="1" />
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="/{{ $district->slug }}">
                        <span itemprop="name">{{ $district->name }}</span>
                    </a>
                    <meta itemprop="position" content="2" />
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="/{{ $district->slug }}/{{ $category->cat_slug }}">
                        <span itemprop="name">{{ $category->cat_name }}</span>
                    </a>
                    <meta itemprop="position" content="3" />
                </li>
                <li class="breadcrumb-item active" itemprop="itemListElement" itemscope
                    itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{ $entry->name }}</span>
                    <meta itemprop="position" content="4" />
                </li>
            </ol>

        </div>

        <h1 class="text-center d-none d-lg-block my-3"> Η καταχώρηση {{ $entry->name }} δεν είναι πλέον ενεργή </h1>
        <p class="text-center">Ίσως σας ενδιαφέρουν οι παρακάτω καταχωρήσεις επαγγελματιών της ίδιας κατηγορίας, στην ίδια περιφέρεια.</p>
        
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        <div class="row mt-5">
            
            <div class="col more">
                <h3>Δείτε παρόμοιες επιχειρήσεις</h3>
                <div class="d-flex flex-wrap">
                    @foreach($entry->relatedEntries() as $item)
                        <div class="col-md-4 col-lg-2 my-3">
                            <a href="/{{$item->district->slug}}/{{$item->category->cat_slug}}/{{$item->slug}}">
                                <img loading="lazy" class="thumbnail" src="/storage/entries/{{$item->cat_id}}/{{$item->image}}" alt="{{$item->category->cat_name}} - {{$item->district->name}} " title="{{$item->category->cat_name}} - {{$item->district->name}} ">
                                <div class="text-center">
                                    {{$item->name}}
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>


        </div>
        <div class="d-lg-none mobile-contact-icon">
            <a href="#contact-form"><i class="far fa-envelope fa-2x"></i></a>
        </div>
    </div>
@endsection

@section('scripts')
    {{-- <script src="/js/photoswipe.min.js"></script>
    <script src="/js/photoswipe-ui-default.min.js"></script> --}}
    <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e9587fd1b16f887" async></script>
@endsection
