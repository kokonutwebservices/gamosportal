@extends('clientarea.layouts.master')

@section('leftnav')
    @include('clientarea.layouts.leftnav')
@endsection

@section('content')
@if(Auth::user()->id == $client->id)
<h1>Πελάτης: {{$client->name}}</h1>
<h2>Καταχωρήσεις</h2>
<ul class="media-list">

    @foreach($client->entries as $entry)
    <li class="media">
        <div class="media-left">
            <a href="clientarea/{{$entry->id}}/info">
                <img class="media-object" src="/storage/entries/{{$entry->cat_id}}/{{$entry->image}}" alt="" style="width: 200px;">
            </a>
        </div>
        <div class="media-body">
        <a href="clientarea/{{$entry->id}}/info">
            <h4 class="media-heading">{{$entry->name}}</h4>
            <p>{{$entry->category->cat_name}}</p>
        </a>       
    @endforeach
    
</ul>
@endif
@endsection