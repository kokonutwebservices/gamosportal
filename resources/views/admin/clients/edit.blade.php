@extends('admin.layouts.master')

@section('content')

<h1>Επεξεργασία Πελάτη: {{$client->name}}</h1>
<div class="col-md-12">
    {{Form::open(['action' => ['ClientsController@update', $client->id], 'method' => 'POST'])}}

    <div class="form-group">
        {{Form::label('name', 'Εμπορική Επωνυμία')}}
        {{Form::text('name', $client->name, ['class'=>'form-control', 'placeholder' => 'Το εμπορικό όνομα, π.χ. Gamos Portal'])}}
    </div>

    <div class="form-group">
        {{Form::label('representative', 'Πωλητής')}}
        <select name="representative" id="" class="form-control">
            <option selected>Επιλέξτε πωλητή</option>
            @foreach ($representatives as $representative)
                <option value="{{$representative->id}}" {{ $representative->name === $client->representative['name'] ? 'selected' : '' }}>{{$representative->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        {{Form::label('email', 'Email')}}
        {{Form::text('email', $client->email, ['class'=>'form-control', 'placeholder' => 'email'])}}
    </div>

    <div class="form-group">
        {{Form::label('password', 'Password')}}
        {{Form::text('password', $client->password, ['class'=>'form-control', 'placeholder' => 'password', 'rel' => 'gp', 'data-size' => '8', 'data-character-set' => 'a-z,0-9'])}}
        <button type="button" class="btn btn-default btn-lg getNewPass"><span class="fas fa-sync-alt"></span></button>
    </div>

    <div class="form-group col-md-12">    
        <div class="col-md-2">
            {{Form::hidden('_method', 'PUT')}}
            {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
        </div>

        <div class="col-md-2">
            {{Form::hidden('_method', 'PUT')}}
            {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
        </div>

        <div class="col-md-2">
            {{Form::hidden('_method', 'PUT')}}
            {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
        </div>

        <div class="col-md-2">  
            <a href="/admin/clients" class="btn btn-warning pull-right">Cancel</a>
        </div>

        <div class="col-md-2">
        {!! Form::close() !!}

            {!! Form::open(['action' => ['ClientsController@destroy', $client->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'pull-right' ]) !!}

            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}

        {!! Form::close() !!}  
        </div>
    </div>

{{Form::close()}}
</div>

<div class="col-md-12 mt-5">
    <ul>
        @foreach ($client->entries as $entry)

            <li><a href="/admin/entries/{{$entry->id}}/edit">{{$entry->name}}</a></li>
            
        @endforeach
    </ul>
</div>
    
@endsection