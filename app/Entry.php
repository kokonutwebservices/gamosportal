<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Entry extends Model
{
    use Sortable;
    protected $guarded = ['_token'];
    protected $table = 'entries';

    public $sortable = ['id', 'name', 'cat_id', 'dist_id', 'client_id', 'end_at'];


    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function category(){
        return $this->belongsTo(Category::class, 'cat_id'); //Σημαντικό! Εδώ γίνεται σύνδεση με το cat_id, καθώς by default ψάχνει το category_id
    }

    public function district(){
        return $this->belongsTo(District::class, 'dist_id'); //Ομοίως για dist_id
    }

    public function albums(){
        return $this->hasMany(Album::class);
    }

    public function videos(){
        return $this->hasMany(Video::class);
    }

    public function banners(){
        return $this->hasMany(Banner::class);
    }

    public function links(){
        return $this->hasMany(Link::class);
    }

    public function tags () {
        return $this->belongsToMany(Tag::class);
    }

    public function seperatePhones($number){

        $delimiters = array (',', '|', '-');
        $replaceDelimiters = str_replace($delimiters, $delimiters[0], $number);
        $phones = explode($delimiters[0], $replaceDelimiters);

        return $phones;
    }

    public function seperateAddresses($address) {

        $delimiters = array (',', '|');
        $replaceDelimiters = str_replace($delimiters, $delimiters[0], $address);
        $addresses = explode($delimiters[0], $replaceDelimiters);
        

        return $addresses;
        
    }

    public function mapsLink ($address) {

        $mapLink = 'https://www.google.com/maps/place/'.$address;

        return $mapLink;
    }

    public function relatedEntries()
    {
        return self::where('cat_id', $this->category->id)
            ->where('dist_id', $this->district->id)
            ->where('id', '<>' ,$this->id)
            ->get();
    }
}
