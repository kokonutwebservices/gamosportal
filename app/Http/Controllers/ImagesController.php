<?php

namespace App\Http\Controllers;

use App\Image;
use App\Album;
use App\Entry;
use Carbon\Carbon;
use Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ImagesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Entry $entryid, Album $albumid)
    {
        $album = $albumid;
        $entry = $entryid;
        
        if (count(request('image')) > request('available')){
            return redirect()->back()->withErrors(['Δεν μπορείτε να προσθέσετε περισσότερες από '.request('available').' φωτογραφίες']);
        } else {
        
        $this->validate(request(),[
            'image.*' => 'image|max:2000|mimes:jpeg',
            
        ]);
        
        //Για το μαζικό upload το πρόβλημα με το filename είναι ότι με το timestamp τα ονόματα συνέπιπταν και δεν αποθηκεύονταν όλα τα αρχεία.
        //Παρατήρησα ότι στο tmp το αρχείο έπαιρνε ένα μοναδικό όνομα, το οποίο απομονώνω και το προσθέτω στο slug της καταχώρησης.
        
        foreach($request->image as $file){
        
            $file_path = $file->getPathName();
            $file_sub = substr($file_path, -9, -4);

            $ext = $file->getClientOriginalExtension(); //Get the extension
        
            $filename = $entry->slug. "-" . $file_sub.".".$ext;

            $upload = $file->storeAs('uploads/'.$album->id, $filename); //Store file

            


            $image = new Image;
            $image->image = $filename;
            $image->album_id = $album->id;
            $image->save();
        }
        return redirect("/admin/entries/albums/$entry->id/$album->id/edit");}
        
    }

    public function storeDescription(Request $request, $entryid, $albumid, $imageid){

        $entry = Entry::find($entryid);
        $album = Album::find($albumid);
        $image = Image::find($imageid);

        // dd($image->id);

        $image->description = request('description');
        $image->save();

        return redirect()->back();
    }

    public function changeAlbum(Request $request, Entry $entryid, Album $albumid, Image $imageid){
        
        $newAlbumId = request('newAlbum');
        $oldAlbum = $albumid;
        $image = $imageid;        

        $newAlbum = Album::find($newAlbumId);

        if(count($newAlbum->images) >= 24){
            return redirect()->back()->with('message', 'Αυτό το album έχει ήδη 24 εικόνες');
        } else {
            $filename = $image->image;
            
            $oldFilePath = "uploads/$oldAlbum->id/$filename";
            // dd($oldFilePath);
            $newFilePath = "uploads/$newAlbumId/$filename";

            Storage::move($oldFilePath, $newFilePath);

            $image->album_id = $newAlbumId;
            $image->save();

            return redirect()->back()->with('message', 'Η εικόνα μεταφέρθηκε με επιτυχία');

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entry $entryid, Album $albumid, Image $imageid)
    {
        $entry = $entryid;
        $album = $albumid;
        $image = $imageid;
        
        Storage::delete("uploads/$album->id/$image->image");

        $image->delete();

        return redirect()->back()->with('message', 'Η εικόνα διαγράφηκε με επιτυχία');

    }

    public function refresh(){
        
    }

    //Function που παίρνει την νέα σειρά από το Ajax call του sortable, αναλύει το id κάθε item, και γράφει στην db το νέο order 
    public function order(){
        $orders = explode(',',$_POST['order']);

        foreach ($orders as $neworder=>$value){
            $parts = explode('-',$value);
            $albumid = $parts[0];
			$id = $parts[1];			
            $order = $neworder+1;
            $image = Image::find($id);
            $image->order = $order;
            $image->save();
        }
    }
}
