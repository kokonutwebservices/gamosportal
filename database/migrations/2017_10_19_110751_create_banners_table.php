<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->text('type');
            $table->integer('entry_id')->nullable();
            $table->text('banner');
            $table->text('url');
            $table->date('expires');
            $table->timestamps();
        });

        Schema::create('banner_page', function (Blueprint $table) {
            $table->integer('banner_id');
            $table->integer('page_id');
            $table->primary(['banner_id', 'page_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
        Schema::dropIfExists('banner_page');
    }
}
