@extends('admin.layouts.master')

@section('content')

<h1>Testimonials</h1>
{{csrf_field()}}

<a class="btn btn-primary" href="/admin/testimonials/create"> Προσθήκη </a>

<ul id="testimonials" class="list-group mt-5" >

    @foreach ($testimonials as $testimonial)

        <li class="list-group-item py-3" id="{{$testimonial->id}}" class="list-group"> 
            <div class="row">
                <div class="col-md-4"><a href="/admin/testimonials/{{$testimonial->id}}/edit">{{$testimonial->name}}</a></div>
                <div class="col-md-4">{{$testimonial->company}}</div>
                <div class="col-md-2 text-right">{{\Carbon\Carbon::parse($testimonial->created_at)->format('d/m/Y')}}</div>
                <div class="col-md-2 text-right">
                    <form action="/admin/testimonials/{{$testimonial->id}}" method="POST">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>            
        </li>

    @endforeach

</ul>
    
@endsection