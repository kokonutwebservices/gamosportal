<div class="visited">
@if(Visited::alsoVisited() AND count(Visited::alsoVisited()) > 0)
    <h5>Έχετε επισκεφθεί ακόμη</h5>
    <ul class="list-unstyled">
        @foreach(Visited::alsoVisited() as $visited)
        @if($visited->name != $entry->name)
            <li>
                <a href="/{{$visited->district->slug}}/{{$visited->category->cat_slug}}/{{$visited->slug}}">
                    {{$visited->name}} (<small>{{$visited->category->cat_name}}</small>)
                </a>
            </li>
        @endif
        @endforeach
    </ul>
@endif
</div>
