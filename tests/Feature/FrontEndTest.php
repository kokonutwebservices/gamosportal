<?php

namespace Tests\Feature;

use App\Page;
use App\Entry;
use App\TagPage;
use App\Category;
use App\District;
use App\InfoPage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FrontEndTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test 
     * Test if district page shows
    */
    public function test_show_district()
    {
        $district = District::inRandomOrder()->first();

        $response = $this->get("/$district->slug");

        $response->assertStatus(200);
    }

    /** 
     * @test
     * @testdox  Test if category shows
     */
    public function test_show_category()
    {
        $district = District::inRandomOrder()->first();
        $category = Category::where('cat_parent_id', '<>', 0)->inRandomOrder()->first();

        $response = $this->get("/$district->slug/$category->cat_slug");

        $response->assertStatus(200);
    }

    /** 
     * @test
     * @testdox  Show page
     */
    public function test_page_show()
    {
        $page = Page::inRandomOrder()->first();

        $response = $this->get("/$page->slug");
        
        $response->assertStatus(200);
    }
    
    
    /** 
     * @test
     * @testdox  Show infopage
     */
    public function test_infopage_show()
    {
        $page = InfoPage::inRandomOrder()->first();

        $response = $this->get("/$page->slug");

        $response->assertStatus(200);
    }
    

    /** 
     * @test
     * @testdox  Shows Tag Page
     */
    public function test_show_tag_page()
    {
        $tagPage = TagPage::inRandomOrder()->first();

        $response = $this->get("/tags/$tagPage->slug");

        $response->assertStatus(200);

    }

    /** 
     * @test
     * @testdox  Show Entry
     */
    // public function test_show_entry()
    // {

    //     $entry = Entry::where('slug', 'vergina-nyfika')->first();

    //     $district = $entry->district->slug;
    //     $category = $entry->category->cat_slug;
    //     $slug = $entry->slug;

    //     $response = $this->get("/$district/$category/$slug");

    //     // $response->assertStatus(200);

    //     $response->assertSeeText($entry->name);
    // }
    
}
