<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cat_name');
            $table->text('cat_description');
            $table->integer('cat_parent_id');
            $table->integer('cat_order');
            $table->string('cat_img');
            $table->text('meta_title_1');
            $table->text('meta_keywords_1');
            $table->text('meta_description_1');
            $table->text('meta_title_2');
            $table->text('meta_keywords_2');
            $table->text('meta_description_2');
            $table->text('meta_title_3');
            $table->text('meta_keywords_3');
            $table->text('meta_description_3');
            $table->text('meta_title_4');
            $table->text('meta_keywords_4');
            $table->text('meta_description_4');
            $table->text('meta_title_5');
            $table->text('meta_keywords_5');
            $table->text('meta_description_5');
            $table->text('meta_title_6');
            $table->text('meta_keywords_6');
            $table->text('meta_description_6');
            $table->text('meta_title_7');
            $table->text('meta_keywords_7');
            $table->text('meta_description_7');
            $table->text('meta_title_8');
            $table->text('meta_keywords_8');
            $table->text('meta_description_8');
            $table->text('meta_title_9');
            $table->text('meta_keywords_9');
            $table->text('meta_description_9');
            $table->text('meta_title_10');
            $table->text('meta_keywords_10');
            $table->text('meta_description_10');
            $table->text('meta_title_11');
            $table->text('meta_keywords_11');
            $table->text('meta_description_11');
            $table->text('meta_title_12');
            $table->text('meta_keywords_12');
            $table->text('meta_description_12');
            $table->text('meta_title_13');
            $table->text('meta_keywords_13');
            $table->text('meta_description_13');
            $table->text('meta_title_14');
            $table->text('meta_keywords_14');
            $table->text('meta_description_14');
            $table->string('cat_slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
