@extends('site.layouts.master')

@section('metatags')
    <title>{{$district->name}} - Δημαρχεία για Πολιτικό Γάμο - Αναζήτηση: {{$keyword}}</title>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <ol class="breadcrumb"> 
        <li class="breadcrumb-item"><a href="/">Αρχική</a></li>
        <li class="breadcrumb-item"><a href="/{{$district->slug}}">{{$district->name}}</a></li>
        <li class="breadcrumb-item"><a href="/{{$district->slug}}/dimarxeia">Δημαρχεία</a></li>
        </ol>
    </div>

    <div class="row">
        <div class="col-md-12">
        <h1>{{$district->name}} - Δημαρχεία για Πολιτικό Γάμο</h1>
        <div class="col-md-4 float-right">
            <form action="/{{$district->slug}}/dimarxeia" method="POST">
            {{ csrf_field() }}
            <div class="form-group d-flex">
            <input type="text"
                class="form-control" name="query" id="" aria-describedby="helpId" placeholder="Αναζήτηση στα Δημαρχεία">
                <button type="submit" class="float-right"><i class="fas fa-search"></i></button>
            </form>
            </div>
        </div>
        </div>
        <div class="table-responsive">   
        <table class="table table-striped table-hover">
            <thead class="thead-inverse">
                    <th>Δημαρχείο</th>
                    <th>Διεύθυνση</th>
                    <th>Τηλέφωνο</th>
                    <th>email</th>
                    <th>website</th>
                </thead>
        @foreach($results as $municipality)
            <tr>
                <td>
                    <h5><a href="/{{$municipality->district->slug}}/dimarxeia/{{$municipality->slug}}">{{$municipality->name}}</a></h5>
                </td>
                <td>
                    {{$municipality->address}}
                </td>
                <td>
                    <a href="tel:{{$municipality->phone}}">{{$municipality->phone}}</a>
                </td>
                <td>
                    <a href="mailto:{{$municipality->email}}">{{$municipality->email}}</a>
                </td>
                <td>
                    <a href="http://{{$municipality->website}}" rel="nofollow" target="_blank">{{$municipality->website}}
                </td>
            </tr>                    
        @endforeach
        </table>
    </div> 
    </div> 
</div>  
@endsection