<?php

namespace App\Http\Controllers;

use App\SpecialImage;
use Illuminate\Http\Request;
use Storage;
use App\SpecialEntry;

class SpecialImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SpecialEntry $specialentry)
    {

        foreach ($request->image as $file) {
            $file_path = $file->getPathName();
            $file_sub = substr($file_path, -9, -4);
            $ext = $file->getClientOriginalExtension(); //Get the extension
            $filename = $specialentry->slug. "-" . $file_sub.".".$ext;
            $upload = $file->storeAs('uploads/special/'.$specialentry->id, $filename); //Store file

            $filepath = public_path('storage/uploads/special/'.$specialentry->id.'/'.$filename);
        
            
            try {
                \Tinify\setKey("xVBaCSpeVqNE0VFe5_KNBpXweIB-YwZL");
                $source = \Tinify\fromFile($filepath);
                $source->toFile($filepath);
            } catch(\Tinify\AccountException $e) {
                // Verify your API key and account limit.
                return redirect()->back()->with('error', $e->getMessage());
            } catch(\Tinify\ClientException $e) {
                // Check your source image and request options.
                return redirect()->back()->with('error', $e->getMessage());
            } catch(\Tinify\ServerException $e) {
                // Temporary issue with the Tinify API.
                return redirect()->back()->with('error', $e->getMessage());
            } catch(\Tinify\ConnectionException $e) {
                // A network connection error occurred.
                return redirect()->back()->with('error', $e->getMessage());
            } catch(Exception $e) {
                // Something else went wrong, unrelated to the Tinify API.
                return redirect()->back()->with('error', $e->getMessage());
            }
            
            $specialImage = new SpecialImage;
            $specialImage->image = $filename;
            $specialImage->specialentry_id = $specialentry->id;
            $specialImage->save();

        }
        return redirect("/admin/specialentries/$specialentry->id/images");
    }

    public function storeDescription (Request $request, SpecialEntry $specialentry, SpecialImage $specialimage){

        $specialimage->description = request('description');
        $specialimage->save();

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpecialImage  $specialImage
     * @return \Illuminate\Http\Response
     */
    public function show(SpecialImage $specialImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpecialImage  $specialImage
     * @return \Illuminate\Http\Response
     */
    public function edit(SpecialImage $specialImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpecialImage  $specialImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpecialImage $specialImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpecialImage  $specialImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpecialEntry $specialentry, SpecialImage $specialimage)
    {
        Storage::delete("uploads/special/$specialentry->id/$specialimage->image");

        $specialimage->delete();

        return back()->with('message', 'Η εικόνα διαγράφηκε με επιτυχία');
    }

    public function order(){
        $orders = explode(',',$_POST['order']);

        foreach ($orders as $neworder=>$value){
            
            $id = $value;
            $order = $neworder+1;
            $image = SpecialImage::find($id);
            $image->order = $order;
            $image->save();
        }
    }
}
