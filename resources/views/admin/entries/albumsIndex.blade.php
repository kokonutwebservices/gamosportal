@extends('admin.layouts.master')

@section('content')
    <h2>Albums Καταχώρησης: {{$entry->name}} του πελάτη {{$entry->client->name}}</h2>
    {{ csrf_field() }}

{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif

    <ul id="albumSortable" class="list-group">
    
        @foreach($albums as $album)
            <?php $images = App\Image::where('album_id', $album->id)->get(); ?>
            <li id="{{$album->entry_id}}-{{$album->id}}" class="list-group-item">
            <div class="media">
                @if($album->cover)
                    <a href="{{$album->entry_id}}/{{$album->id}}/edit"><img src="/storage/uploads/{{$album->id}}/{{$album->cover}}" class="d-flex mr-3" style="width: 50px;"></a>
                @else
                    <a href="{{$album->entry_id}}/{{$album->id}}/edit"><img src="/storage/default.png" class="d-flex mr-3" style="width: 50px;"></a>
                @endif            
                <div class="media-body">
                <a href="{{$album->entry_id}}/{{$album->id}}/edit">{{$album->name}} - {{\Carbon\Carbon::parse($album->created_at)->diffForHumans()}}</a>
                <div> (Περιεχ. {{count($images)}} Εικόνες)</div>
                @if($album->description)
                <div>Κείμενο Περιγραφής: <i class="fa fa-check text-success" aria-hidden="true"></i></div>
                @endif
                <div><a class="btn btn-danger pull-right" href="/admin/entries/albums/{{$album->id}}/delete"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                </div>
            </div>
            </li>            

        @endforeach
    </ul>

    <a href="{{$entry->id}}/create" class="btn btn-primary">Δημιουργία album</a>
    <a href="/admin/entries/{{$entry->id}}/edit" class="btn btn-secondary">Επιστροφή</a>
@endsection