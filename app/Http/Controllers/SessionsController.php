<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{
    

    public function index(){
        return view('admin.index');
    }


    public function create(){
        return view('admin.sessions.create');
    }

    public function store(){

        if (! auth()->attempt(request(['email', 'password']))){
            return back()->withErrors([
                'message' => 'Κάποιο από τα πεδία είναι λάθος. Προσπαθήστε ξανά!'
            ]);
        };

        return redirect()->route('admin');

    }

    public function destroy(){
        auth()->guard('web')->logout();
        return redirect('/');
    }
}
