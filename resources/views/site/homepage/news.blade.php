<div class="news">
    <h2>ΝΕΑ ΚΑΙ ΕΙΔΗΣΕΙΣ ΓΙΑ ΤΟ ΓΑΜΟ <br> <small>Επιλεγμένα θέματα από το Blog του Gamos Portal</small></h2>
    @foreach($news as $new)
        <div class="news-item">
            <div class="row">
                <div class="col-md-3">
                <a href="{{$new->url}}" target="_blank"><img loading="lazy" class="mr-3 px-0" src="/storage/news/{{$new->image}}" alt="{{$new->title}}" title="{{$new->title}}"></a>
                        </div>            
                <div class="col-md-9">
                    <a href="{{$new->url}}" target="_blank"><h3 class="mt-md-0">{{$new->title}}</h3></a>
                    <div class="d-none d-md-block">{!!$new->description!!}</div>
                </div>
            </div>            
        </div>   
        <hr>
    @endforeach
</div>