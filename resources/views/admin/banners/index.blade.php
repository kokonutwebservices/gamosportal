@extends('admin.layouts.master')

@section('content')

<h1>Διαχείριση Banners</h1>
@include('admin.layouts.errors')
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif

<table class="table">
    <thead class="thead-inverse">
        <tr>
        <th>@sortablelink('id')</th>
        <th>@sortablelink('type', 'Τύπος')</th>
        <th>@sortablelink('entry', 'Καταχώρηση')</th>
        <th>@sortablelink('url', 'url') </th>
        <th>@sortablelink('expires', 'Ημ. Λήξης')</th>
        <th>Edit</th>
        </tr>
    </thead>
    <tbody>
        @foreach($banners as $banner)
            <tr>
                <td>{{$banner->id}}</td>
                <td>{{$banner->type}}</td>
                <td>
                @if($banner->entry != NULL && $banner->entry_id != 0)
                    {{$banner->entry->name}}</td>
                @endif                    
                <td>{{$banner->url}}</td>
                <td>{{$banner->expires}}</td>
                <td><a href="banners/{{$banner->id}}/edit">Edit</a></td>
            </tr>
        @endforeach
    </tbody>
</table>

<a class="btn btn-primary" href="banners/create">Δημιουργία Banner</a>
    
@endsection