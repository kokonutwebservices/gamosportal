<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ClientResetPasswordNotification;

class Client extends Authenticatable
{
    use Notifiable;

    protected $guard = 'client';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    public $sortable = ['id', 'name', 'email'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ClientResetPasswordNotification($token));
    }

    // public function setPasswordAttribute($password)
    // {   
    //     $this->attributes['password'] = \Hash::make($password); 
    // }

    public function entries(){
        return $this->hasMany(Entry::class);
    }

    public function specialentries(){
        return $this->hasMany(SpecialEntry::class);
    }

    public function representative(){
        return $this->belongsTo(Representative::class);
    }
}
