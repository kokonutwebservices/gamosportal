@extends('admin.layouts.master')

@section('content')

    <h1>Πελάτες Gamos Portal</h1>
    {{--  Πλαίσιο σφαλμάτων  --}}

    <div class="col-md-12">
        <div class="form-group col-md-4 pull-right">
        {!!Form::open(['method' => 'POST', 'action' => 'ClientsController@search'])!!}
            <div class="input-group">
            <input type="text" class="form-control" name="query" placeholder="Αναζήτηση" aria-label="Αναζήτηση">
            <span class="input-group-btn">
                <button class="btn btn-secondary" type="submit">Go!</button>
            </span>
            </div>
        {!!Form::close()!!}
        </div>
    </div>
    <table class="table">
    <thead class="thead-inverse">
        <tr>
            <th>@sortablelink('id')</th>
            <th>@sortablelink('name', 'Όνομα Καταχώρησης')</th>
            <th>@sortablelink('email', 'Email')</th>        
        </tr>
    </thead>
        @foreach($results as $client)
            <tr>
                <td>{{$client->id}}</td>
                <td><a href="/admin/clients/{{$client->id}}/edit">{{$client->name}}</a></td>
                <td>{{$client->email}}</td>                
            </tr>
        @endforeach
    </table>

<a href="/admin/clients" class="btn btn-secondary pull-right">Reset</a>
    {{--  {{ $results->links() }}  --}}

{{--  <div class="col-md-12">
    <a href="entries/create" class="btn btn-primary">Δημιουργία Καταχώρησης</a>
</div>  --}}


@endsection