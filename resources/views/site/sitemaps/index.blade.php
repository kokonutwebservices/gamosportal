<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://gamosportal.gr/</loc>
        <lastmod>{{ $entries->pluck('updated_at')->max()->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>daily</changefreq>
        <priority>1</priority>
    </url>


    @foreach ($pages as $page)
        <url>
            <loc>https://gamosportal.gr/{{ $page->slug }}</loc>
            @if ($page->updated_at)
                <lastmod>{{ $page->updated_at->tz('UTC')->toAtomString() }}</lastmod>
            @endif
            <changefreq>weekly</changefreq>
            <priority>1</priority>
        </url>
    @endforeach

    @foreach ($districts as $district)
        <url>
            <loc>https://gamosportal.gr/{{ $district->slug }}</loc>
            @if ($district->updated_at)
                <lastmod>{{ $district->updated_at->tz('UTC')->toAtomString() }}</lastmod>
            @endif
            <changefreq>yearly</changefreq>
            <priority>0.9</priority>
        </url>

        {{-- @foreach ($categories->where('cat_parent_id', '>', 0) as $category)
            @if ($category->entries->where('dist_id', $district->id)->count())
                <url>
                    <loc>https://gamosportal.gr/{{$district->slug}}/{{ $category->cat_slug }}</loc>
                    @if ($category->updated_at)
                        <lastmod>{{ $category->updated_at->tz('UTC')->toAtomString() }}</lastmod>
                    @endif                
                    <changefreq>weekly</changefreq>
                    <priority>.9</priority>
                </url>
            @endif
        @endforeach --}}


        @foreach ($district->municipalities as $municipality)
            <url>
                <loc>https://gamosportal.gr/{{ $district->slug }}/dimarxeia/{{ $municipality->slug }}</loc>
                @if ($municipality->updated_at)
                    <lastmod>{{ $municipality->updated_at->tz('UTC')->toAtomString() }}</lastmod>
                @endif
                <changefreq>monthly</changefreq>
                <priority>.7</priority>
            </url>
        @endforeach
    @endforeach

    @foreach ($tagpages as $page)
        <url>
            <loc>https://gamosportal.gr/tags/{{ $page->slug }}</loc>
            @if ($page->updated_at)
                <lastmod>{{ $page->updated_at->tz('UTC')->toAtomString() }}</lastmod>
            @endif
            <changefreq>monthly</changefreq>
            <priority>.9</priority>
        </url>
    @endforeach


    {{-- @foreach ($entries->where('cat_id', $page->category->id) as $entry)
        <url>
            <loc>
                https://gamosportal.gr/{{ $entry->district->slug }}/{{ $entry->category->cat_slug }}/{{ $entry->slug }}
            </loc>
            @if ($entry->updated_at)
                <lastmod>{{ $entry->updated_at->tz('UTC')->toAtomString() }}</lastmod>
            @endif
            <changefreq>monthly</changefreq>
            <priority>.8</priority>
        </url>
    @endforeach --}}

    @foreach ($infopages as $infopage)
        <url>
            <loc>https://gamosportal.gr/{{ $infopage->slug }}</loc>
            @if ($infopage->updated_at)
                <lastmod>{{ $infopage->updated_at->tz('UTC')->toAtomString() }}</lastmod>
            @endif
            <changefreq>monthly</changefreq>
            <priority>.6</priority>
        </url>
    @endforeach

    @foreach ($metropoles as $metropole)
        <url>
            <loc>https://gamosportal.gr/{{ $metropole->district->slug }}/ekklisies/{{ $metropole->id }}</loc>
            @if ($metropole->updated_at)
                <lastmod>{{ $infopage->updated_at->tz('UTC')->toAtomString() }}</lastmod>
            @endif
            <changefreq>monthly</changefreq>
            <priority>.6</priority>
        </url>
        @foreach ($metropole->churches as $church)
            <url>
                <loc>
                    https://gamosportal.gr/{{ $metropole->district->slug }}/ekklisies/{{ $metropole->id }}/{{ $church->slug }}
                </loc>
                @if ($church->updated_at)
                    <lastmod>{{ $infopage->updated_at->tz('UTC')->toAtomString() }}</lastmod>
                @endif
                <changefreq>monthly</changefreq>
                <priority>.7</priority>
            </url>
        @endforeach
    @endforeach

</urlset>
