@extends('site.layouts.master')

@section('metatags')
<title>Είπαν για εμάς - Σχόλια που γράφτηκαν για το Gamos Portal</title>
<meta name="description" content="Σχόλια επαγγελματιών και επισκεπτών για το Gamos Portal.">
<meta name="keywords" content="testimonials, είπαν για εμάς">
    
@endsection

@section('content')

<section id="main">
    <div class="container testimonials">
        <div class="row mb-5">
            <div class="col-md-12">
                <h1>Είπαν για εμάς</h1>
                <p>Από το 2011 στόχος μας είναι να χτίζουμε μακροχρόνιες συνεργασίες με τους επαγγελματίες της αγοράς του γάμου. Τα σχόλια, οι παρατηρήσεις, αλλά και τα καλά τους λόγια, είναι η κινητήρια δύναμη σε κάθε μας βήμα. Η προτίμησή τους είναι η καλύτερη ανταμοιβή μας.</p>
                <p>Αν είστε επαγγελματίας και επιδιώκετε το καλύτερο για την προβολή της επιχείρησήσας, επικοινωνήστε μαζί μας, <a href="/add-your-business.php">εδώ</a></p>
            </div>
        </div>
        <div class="row justify-content-center">
            
            @foreach ($testimonials as $testimonial)
                
                <div class="card p-3 col-md-4 my-2 ">
                    <img src="storage/testimonials/{{$testimonial->image}}" alt="" class="d-block m-auto">

                    <h3 class="text-center"><a href="{{$testimonial->link}}">{{$testimonial->company}}</a></h3>

                    {!! $testimonial->testimonial !!}
                
                    <div class="mt-4">
                        <cite><strong>{{$testimonial->name}}</strong> - {{$testimonial->position}}</cite>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
</section>

    
@endsection