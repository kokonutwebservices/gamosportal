@extends('admin.layouts.master')

@section('content')
<h1>Δημιουργία Σελίδας Πληροφοριών</h1>

{!! Form::open(['action'=>'InfoPagesController@store', 'method'=>'POST', 'enctype' => 'multipart/form-data']) !!}

<div class="form-group">
    {{Form::label('title', 'Τίτλος')}}
    {{Form::text('title', '', ['class'=>'form-control', 'placeholder' => 'Τίτλος', 'required'] )}}
</div>

<div class="form-group">
    {{Form::label('slug', 'slug')}}
    {{Form::text('slug', '', ['class'=>'form-control', 'placeholder' => 'url', 'required' ] )}}
</div>

<div class="form-group">
    {{Form::label('metatitle', 'MetaTitle')}}
    {{Form::text('metatitle', '', ['class'=>'form-control', 'placeholder' => 'Τίτλος browser μέχρι 60 χαρακτήρες', 'required' ] )}}
</div>

<div class="form-group">
    {{Form::label('metadescription', 'MetaDescription')}}
    {{Form::text('metadescription', '', ['class'=>'form-control', 'placeholder' => 'Περιγραφή σελίδας (μέχρι 150 χαρακτήρες)', 'required'] )}}
</div>

<div class="form-group">
    {{Form::label('metakeywords', 'MetaKeywords')}}
    {{Form::text('metakeywords', '', ['class'=>'form-control', 'placeholder' => 'Keyowords σελίδας, χωρισμένα με ,', 'required'] )}}
</div>

<div class="form-group">
    {{Form::label('body', 'Κείμενο')}}
    {{Form::textarea('body', '', ['class'=>'form-control tinyMCE', 'required'] )}}
</div>

<div class="form-group">
    {{Form::label('image', 'Εικόνα')}}
    {{Form::file('image' )}}
</div>

<div class="form-group">
    {{Form::label('image_alt', 'Alt Tag | Image Title')}}
    {{Form::text('image_alt', '', ['class'=>'form-control'] )}}
</div>

<div class="form-group">    
        <div class="col-md-2">  
        {{Form::button('Save & Close', ['class'=>'btn btn-primary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-close'] )}}
    </div>  

   <div class="col-md-2">
        {{Form::button('Save & Stay', ['class'=>'btn btn-success pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save'] )}}
    </div>

    <div class="col-md-2">
        {{Form::button('Save & New', ['class'=>'btn btn-secondary pull-left', 'type'=> 'submit', 'name' => 'submitbutton', 'value' => 'save-new'] )}}
    </div>

    <div class="col-md-2">  
        <a href="/admin/infopages" class="btn btn-warning pull-right">Cancel</a>
    </div>  
</div>


{!! Form::close() !!}


@endsection